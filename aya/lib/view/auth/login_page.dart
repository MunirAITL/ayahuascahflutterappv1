import 'package:aitl/config/server/APIAuthCfg.dart';
import 'package:aitl/config/server/APITypeCFg.dart';
import 'package:aitl/config/server/Server.dart';
import 'package:aitl/config/theme/MyTheme.dart';
import 'package:aitl/controller/form_validator/UserProfileVal.dart';
import 'package:aitl/data/app_data/UserData.dart';
import 'package:aitl/data/db/DBMgr.dart';
import 'package:aitl/data/model/auth/ForgotAPIModel.dart';
import 'package:aitl/data/model/auth/LoginAPIModel.dart';
import 'package:aitl/data/network/ModelMgr.dart';
import 'package:aitl/data/network/NetworkMgr.dart';
import 'package:aitl/view/auth/reg1_page.dart';
import 'package:aitl/view_model/api/api_view_model.dart';
import 'package:aitl/view_model/helper/auth/LoginHelper.dart';
import 'package:aitl/view_model/observer/APIStateProvider.dart';
import 'package:aitl/view/auth/otp/sms_page1.dart';
import 'package:aitl/view/auth/otp/sms_page2.dart';
import 'package:aitl/view/dashboard/dashboard_page.dart';
import 'package:aitl/view/widgets/InputBox.dart';
import 'package:aitl/view/widgets/MMBtn.dart';
import 'package:aitl/view/widgets/Txt.dart';
import 'package:aitl/view/widgets/dialog/ForgotDialog.dart';
import 'package:aitl/view_model/helper/auth/AuthHelper.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:aitl/mixin.dart';
import 'auth_base.dart';

class LoginPage extends StatefulWidget {
  @override
  State createState() => _LoginPageState();
}

class _LoginPageState extends BaseAuth<LoginPage> with APIStateListener {
  final _email = TextEditingController();
  final _pwd = TextEditingController();

  APIStateProvider _apiStateProvider;
  @override
  onAPIStateChanged(apiState, model) async {
    try {
      if (apiState.type == APIType.login && apiState.cls == this.runtimeType) {
        if (model != null && mounted) {
          checkLoginRes(model as LoginAPIModel);
        }
      } else if (apiState.type == APIType.forgot &&
          apiState.cls == this.runtimeType) {
        if (model != null && mounted) {
          if (model.success) {
            try {
              final msg = model.messages.forgotpassword[0].toString();
              showAlert(msg: msg, isToast: true, which: 1);
            } catch (e) {
              log(e.toString());
            }
          } else {
            try {
              final err = model.errorMessages.forgotpassword[0].toString();
              showAlert(msg: err, isToast: true);
            } catch (e) {
              log(e.toString());
            }
          }
        }
      } else if (apiState.type == APIType.loginWithG &&
          apiState.cls == this.runtimeType) {
        if (model != null && mounted) {
          checkLoginRes(model as LoginAPIModel);
        }
      } else if (apiState.type == APIType.loginWithFB &&
          apiState.cls == this.runtimeType) {
        if (model != null && mounted) {
          checkLoginRes(model as LoginAPIModel);
        }
      }
    } catch (e) {
      log(e.toString());
    }
  }

  @override
  void initState() {
    super.initState();
    appInit();
  }

  //@mustCallSuper
  @override
  void dispose() {
    _email.dispose();
    _pwd.dispose();
    try {
      _apiStateProvider.unsubscribe(this);
      _apiStateProvider = null;
    } catch (e) {
      log(e.toString());
    }
    try {
      NetworkMgr().dispose();
    } catch (e) {}
    super.dispose();
  }

  appInit() {
    try {
      if (Server.isTest) {
        _email.text =
            "anisur5001@yopmail.com"; //"occbs.cust0081@yopmail.com"; //"tah12@yopmail.com";
        _pwd.text = "123456";
        /*
nipunrozario@rocketmail.com
1234
        */
      }
    } catch (e) {}

    try {
      _apiStateProvider = new APIStateProvider();
      _apiStateProvider.subscribe(this);
    } catch (e) {}
  }

  validate() {
    if (UserProfileVal().isEmpty(_email, 'Email/Phone', MyTheme.redColor)) {
      return false;
    } else if (!UserProfileVal().isPwdOK(_pwd, MyTheme.redColor)) {
      return false;
    }
    return true;
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: MyTheme.bgColor,
        body: GestureDetector(
          behavior: HitTestBehavior.opaque,
          onPanDown: (detail) {
            FocusScope.of(context).requestFocus(new FocusNode());
          },
          onTap: () {
            FocusScope.of(context).requestFocus(new FocusNode());
          },
          child: drawLayout(),
        ),
      ),
    );
  }

  drawLayout() {
    return Container(
      child: SingleChildScrollView(
        child: Container(
          child: Column(
            children: [
              drawTopLogo(),
              Container(
                child: Padding(
                  padding: const EdgeInsets.only(left: 20, right: 20),
                  child: Column(
                    children: [
                      InputBox(
                        ctrl: _email,
                        lableTxt: "Email or Phone Number",
                        kbType: TextInputType.emailAddress,
                        len: 50,
                      ),
                      InputBox(
                        ctrl: _pwd,
                        lableTxt: "Password",
                        kbType: TextInputType.text,
                        len: 20,
                        isPwd: true,
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          GestureDetector(
                              onTap: () {
                                showForgotDialog(
                                    context: context,
                                    email: _email,
                                    callback: (String emailStr) {
                                      //
                                      if (emailStr != null) {
                                        if (emailStr.length > 0) {
                                          APIViewModel().req<ForgotAPIModel>(
                                            context: context,
                                            apiState: APIState(APIType.forgot,
                                                this.runtimeType, null),
                                            url: APIAuthCfg.FORGOT_URL,
                                            param: {
                                              'email': _email.text.trim()
                                            },
                                            reqType: ReqType.Post,
                                          );
                                        } else {
                                          showAlert(
                                              msg: "Missing email",
                                              isToast: true);
                                        }
                                      }
                                    });
                              },
                              child: Txt(
                                  txt: "Forgot Password ?",
                                  txtColor: MyTheme.blueColor,
                                  txtSize: MyTheme.txtSize,
                                  txtAlign: TextAlign.center,
                                  isBold: false)),
                          SizedBox(width: 10),
                          MMBtn(
                            txt: "Login",
                            //txtColor: Colors.white,
                            //bgColor: accentColor,
                            width: getWP(context, 30),
                            height: getHP(context, MyTheme.btnHpa),
                            radius: 20,
                            callback: () {
                              if (validate()) {
                                APIViewModel().req<LoginAPIModel>(
                                  context: context,
                                  apiState: APIState(
                                      APIType.login, this.runtimeType, null),
                                  url: APIAuthCfg.LOGIN_URL,
                                  param: LoginHelper().getParam(
                                      email: _email.text.trim(),
                                      pwd: _pwd.text.trim()),
                                  reqType: ReqType.Post,
                                  isCookie: true,
                                );
                              }
                            },
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
              ),
              SizedBox(height: 20),
              Txt(
                txt: "Or continue with",
                txtColor: MyTheme.gray4Color,
                txtSize: MyTheme.txtSize + .3,
                txtAlign: TextAlign.center,
                isBold: false,
              ),
              Padding(
                padding: const EdgeInsets.only(left: 20, right: 20, top: 20),
                child: MMBtn(
                  txt: "Log in with Mobile",
                  width: getW(context),
                  height: getHP(context, MyTheme.btnHpa),
                  radius: 10,
                  callback: () async {
                    Get.to(
                      () => Sms1Page(),
                    ).then((value) {
                      //callback(route);
                    });
                  },
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(top: 20, left: 20, right: 20),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    AuthHelper().drawGoogleFBLoginButtons(
                      context: context,
                      height: getHP(context, MyTheme.btnHpa),
                      width: getW(context),
                      icon: "ic_fb",
                      txt: "Continue with Facebook",
                      callback: () {
                        loginWithFB(this.runtimeType);
                      },
                    ),
                    SizedBox(height: 20),
                    AuthHelper().drawGoogleFBLoginButtons(
                      context: context,
                      height: getHP(context, MyTheme.btnHpa),
                      width: getW(context),
                      icon: "ic_google",
                      txt: "Continue with Google",
                      callback: () {
                        loginWithGoogle(this.runtimeType);
                      },
                    ),
                  ],
                ),
              ),
              SizedBox(height: 50),
            ],
          ),
        ),
      ),
    );
  }
}
