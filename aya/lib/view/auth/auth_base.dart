import 'package:aitl/config/server/APILoginWithCfg.dart';
import 'package:aitl/config/server/Server.dart';
import 'package:aitl/controller/classes/Common.dart';
import 'package:aitl/data/app_data/UserData.dart';
import 'package:aitl/data/app_data/UserData.dart';
import 'package:aitl/data/db/DBMgr.dart';
import 'package:aitl/data/model/auth/LoginAPIModel.dart';
import 'package:aitl/data/model/auth/loginWith/LoginWithModel.dart';
import 'package:aitl/data/network/ModelMgr.dart';
import 'package:aitl/data/network/NetworkMgr.dart';
import 'package:aitl/mixin.dart';
import 'package:aitl/view/auth/otp/sms_page2.dart';
import 'package:aitl/view/dashboard/dashboard_page.dart';
import 'package:aitl/view_model/api/api_view_model.dart';
import 'package:aitl/view_model/helper/utils/UIHelper.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:flutter_facebook_login/flutter_facebook_login.dart';

abstract class BaseAuth<T extends StatefulWidget> extends State<T>
    with Mixin, UIHelper {
  drawTopLogo() {
    return Center(
      child: Container(
        width: getWP(context, 70),
        height: getHP(context, 40),
        child: Image.asset(
          'assets/images/logo/logo.png',
        ),
      ),
    );
  }

  loginWithGoogle(cls) async {
    try {
      final FirebaseAuth _auth = FirebaseAuth.instance;
      final GoogleSignIn googleSignIn = GoogleSignIn();
      try {
        if (await googleSignIn.isSignedIn()) await googleSignIn.signOut();
      } catch (e) {
        log("sign out error " + e.toString());
      }

      final GoogleSignInAccount googleSignInAccount =
          await googleSignIn.signIn();
      final GoogleSignInAuthentication googleSignInAuthentication =
          await googleSignInAccount.authentication;

      // Create a new credential
      final credential = GoogleAuthProvider.credential(
        accessToken: googleSignInAuthentication.accessToken,
        idToken: googleSignInAuthentication.idToken,
      );

      final authResult = await _auth.signInWithCredential(credential);
      final user = authResult.user;

      assert(!user.isAnonymous);
      assert(await user.getIdToken() != null);

      try {
        final nameMap = Common.splitName(user.displayName);
        final json = {
          'Email': user.email.toString(),
          'FirstName': nameMap['fname'],
          'LastName': nameMap['lname'],
          'Persist': true,
          'ReferenceId': user.uid.toString(),
          'ReferenceType': "google"
        };
        var model = LoginWithModel.fromJson(json);
        APIViewModel().req<LoginAPIModel>(
          context: context,
          apiState: APIState(APIType.loginWithG, cls, null),
          url: APILoginWithCfg.GOOGLE_LOGIN_URL,
          param: model.toJson(),
          reqType: ReqType.Post,
        );
      } catch (e) {
        log("google login uid = " + e.toString());
      }
    } catch (e) {
      log("google login uid 2 = " + e.toString());
    }
  }

  loginWithFB(cls) async {
    try {
      final facebookLogin = FacebookLogin();
      // Let's force the users to login using the login dialog based on WebViews. Yay!
      facebookLogin.loginBehavior = FacebookLoginBehavior.webViewOnly;
      final result = await facebookLogin.logIn(['email', 'public_profile']);

      switch (result.status) {
        case FacebookLoginStatus.loggedIn:
          //_sendTokenToServer(result.accessToken.token);
          //_showLoggedInUI();
          final OAuthCredential credential =
              FacebookAuthProvider.credential(result.accessToken.token);

          final FirebaseAuth _auth = FirebaseAuth.instance;
          final userCredential = await _auth.signInWithCredential(credential);
          final user = userCredential.user;

          try {
            final nameMap = Common.splitName(user.displayName);
            final json = {
              'Email': user.email.toString(),
              'FirstName': nameMap['fname'],
              'LastName': nameMap['lname'],
              'Persist': true,
              'ReferenceId': user.uid.toString(),
              'ReferenceType': "Facebook"
            };
            var model = LoginWithModel.fromJson(json);
            APIViewModel().req<LoginAPIModel>(
              context: context,
              apiState: APIState(APIType.loginWithFB, cls, null),
              url: APILoginWithCfg.FB_LOGIN_URL,
              param: model.toJson(),
              reqType: ReqType.Post,
            );
          } catch (e) {
            log("google login uid = " + e.toString());
          }
          break;
        case FacebookLoginStatus.cancelledByUser:
          //_showCancelledMessage();
          break;
        case FacebookLoginStatus.error:
          //_showErrorOnUI(result.errorMessage);
          showAlert(msg: result.errorMessage, isToast: true);
          break;
      }
    } catch (e) {
      log("google login uid 2 = " + e.toString());
    }
  }

  checkLoginRes(LoginAPIModel model) async {
    if (model.success) {
      try {
        await DBMgr.shared.setUserProfile(user: model.responseData.user);
        await userData.setUserModel();
      } catch (e) {
        log(e.toString());
      }
      if (Server.isTest) {
        Get.off(() => DashboardPage());
      } else {
        if (model.responseData.user.isMobileNumberVerified) {
          Get.off(() => DashboardPage());
        } else {
          Get.to(() => Sms2Page(
                mobile: '',
              )).then((value) {
            //callback(route);
          });
        }
      }
    } else {
      try {
        final err = model.errorMessages.login[0];
        showAlert(msg: err, isToast: true);
      } catch (e) {
        log(e.toString());
      }
    }
  }
}
