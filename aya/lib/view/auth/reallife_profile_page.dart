import 'package:aitl/config/server/APIAuthCfg.dart';
import 'package:aitl/config/server/ResCfg.dart';
import 'package:aitl/config/theme/MyTheme.dart';
import 'package:aitl/controller/form_validator/UserProfileVal.dart';
import 'package:aitl/data/app_data/UserData.dart';
import 'package:aitl/data/db/DBMgr.dart';
import 'package:aitl/data/model/auth/profile/RegProfileAPIModel.dart';
import 'package:aitl/data/model/dashboard/more/res/ResAPIModel.dart';
import 'package:aitl/data/network/ModelMgr.dart';
import 'package:aitl/data/network/NetworkMgr.dart';
import 'package:aitl/view/auth/brief_profile_page.dart';
import 'package:aitl/view/auth/otp/sms_page2.dart';
import 'package:aitl/view/dashboard/dashboard_page.dart';
import 'package:aitl/view/dashboard/main/main_page.dart';
import 'package:aitl/view/widgets/BottomBtn.dart';
import 'package:aitl/view/widgets/DatePickerView.dart';
import 'package:aitl/view/widgets/InputBox.dart';
import 'package:aitl/view/widgets/InputBoxHT.dart';
import 'package:aitl/view/widgets/MMBtn.dart';
import 'package:aitl/view/widgets/Txt.dart';
import 'package:aitl/view/widgets/google/GPlacesView.dart';
import 'package:aitl/view_model/api/api_view_model.dart';
import 'package:aitl/view_model/observer/APIStateProvider.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:google_maps_webservice/geolocation.dart';
import 'package:intl/intl.dart';
import 'auth_base.dart';

class ReallifeProfilePage extends StatefulWidget {
  @override
  State createState() => _ReallifeProfilePageState();
}

enum genderEnum { male, female }

class _ReallifeProfilePageState extends BaseAuth<ReallifeProfilePage>
    with APIStateListener {
  final name = TextEditingController();
  final city = TextEditingController();

  //  reg2
  String dob = "";
  genderEnum _gender = genderEnum.male;

  APIStateProvider _apiStateProvider;
  @override
  onAPIStateChanged(apiState, model) async {
    try {
      if (apiState.type == APIType.reg2 && apiState.cls == this.runtimeType) {
        if (model != null && mounted) {
          if (model.success) {
            try {
              await DBMgr.shared.setUserProfile(
                user: model.responseData.user,
              );
              await userData.setUserModel();
            } catch (e) {
              log(e.toString());
            }
            if (model.responseData.user.isMobileNumberVerified) {
              Get.off(() => DashboardPage());
            } else {
              Get.to(
                () => Sms2Page(
                  mobile: userData.userModel.mobileNumber,
                ),
              ).then((value) {
                //callback(route);
              });
            }
          } else {
            try {
              final err = model.messages.postUser[0].toString();
              showAlert(msg: err, isToast: true);
            } catch (e) {
              log(e.toString());
            }
          }
        }
      }
    } catch (e) {
      log(e.toString());
    }
  }

  @override
  void initState() {
    super.initState();
    appInit();
  }

  //@mustCallSuper
  @override
  void dispose() {
    name.dispose();
    city.dispose();
    //_compName.dispose();
    try {
      _apiStateProvider.unsubscribe(this);
      _apiStateProvider = null;
    } catch (e) {
      log(e.toString());
    }
    try {
      NetworkMgr().dispose();
    } catch (e) {}
    super.dispose();
  }

  appInit() {
    try {
      _apiStateProvider = new APIStateProvider();
      _apiStateProvider.subscribe(this);
    } catch (e) {}
  }

  validate() {
    if (!UserProfileVal().isFullName(name.text, MyTheme.redColor)) {
      return false;
    } else if (!UserProfileVal().isDOBOK(dob, MyTheme.redColor)) {
      return false;
    } else if (city.text.trim() == "") {
      showAlert(msg: "Please enter your city", isToast: true);
      return false;
    }
    return true;
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: MyTheme.bgColor,
        bottomNavigationBar: drawBottomBtn(
            context: context,
            text: "Let's go",
            bgColor: MyTheme.greenColor,
            txtColor: Colors.black,
            callback: () async {
              /*if (validate()) {
                APIViewModel().req<RegProfileAPIModel>(
                    context: context,
                    apiState: APIState(APIType.reg2, this.runtimeType, null),
                    url: APIAuthCfg.REG_PROFILE_PUT_URL,
                    reqType: ReqType.Put,
                    param: {
                      "Address": (_area.toString().trim() + ' ' + regAddr)
                          .toString()
                          .trim(),
                      "BriefBio": '',
                      "CommunityId": userData.communityId,
                      "DateofBirth": dob,
                      "Email": userData.userModel.email,
                      "FirstName": userData.userModel.firstName,
                      "Cohort":
                          (_gender == genderEnum.male) ? "Male" : "Female",
                      "Headline": '',
                      "Id": userData.userModel.id,
                      "LastName": userData.userModel.lastName,
                      "Latitude": loc.lat,
                      "Longitude": loc.lng,
                      "MobileNumber": userData.userModel.mobileNumber,
                    });
              }*/

              Get.to(() => DashboardPage());
            }),
        body: drawLayout(),
      ),
    );
  }

  drawLayout() {
    return Container(
      child: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.only(left: 20, right: 20),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              drawTopbar(true, "Create Your\nReal-life Profile"),
              SizedBox(height: 40),
              Txt(
                  txt:
                      "Your real profile is the identity you use in real life, with your family given names, location etc.",
                  txtColor: Colors.black54,
                  txtSize: MyTheme.txtSize,
                  txtAlign: TextAlign.start,
                  isBold: false),
              SizedBox(height: 20),
              drawInputBox("Name:", name),
              SizedBox(height: 20),
              drawBirthBox(),
              drawInputBox("City:", city),
              SizedBox(height: 20),
              drawGender(),
              SizedBox(height: 30),
            ],
          ),
        ),
      ),
    );
  }

  drawInputBox(String title, TextEditingController input) {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.end,
      mainAxisAlignment: MainAxisAlignment.start,
      children: [
        Container(
          width: getWP(context, 20),
          child: Column(
            children: [
              Txt(
                  txt: title,
                  txtColor: Colors.black,
                  txtSize: MyTheme.txtSize,
                  txtAlign: TextAlign.start,
                  isBold: false),
              SizedBox(height: 25),
            ],
          ),
        ),
        Expanded(
          child: InputBox(
            ctrl: input,
            lableTxt: "",
            kbType: TextInputType.text,
            len: 100,
            ecap: eCap.Word,
          ),
        ),
      ],
    );
  }

  drawBirthBox() {
    final DateTime dateNow = DateTime.now();
    return Row(
      crossAxisAlignment: CrossAxisAlignment.end,
      mainAxisAlignment: MainAxisAlignment.start,
      children: [
        Container(
          width: getWP(context, 20),
          child: Txt(
              txt: "Birthday:",
              txtColor: Colors.black,
              txtSize: MyTheme.txtSize,
              txtAlign: TextAlign.start,
              isBold: false),
        ),
        Expanded(
            child: GestureDetector(
          onTap: () {
            showDatePicker(
              context: context,
              initialDate:
                  DateTime(dateNow.year - 18, dateNow.month, dateNow.day),
              firstDate:
                  DateTime(dateNow.year - 100, dateNow.month, dateNow.day),
              lastDate: DateTime(dateNow.year - 18, dateNow.month, dateNow.day),
              builder: (context, child) {
                return Theme(
                  data: ThemeData.light().copyWith(
                    colorScheme: ColorScheme.light(primary: MyTheme.redColor),
                    buttonTheme:
                        ButtonThemeData(textTheme: ButtonTextTheme.primary),
                  ), // This will change to light theme.
                  child: child,
                );
              },
            ).then((value) {
              if (value != null) {
                setState(() {
                  try {
                    dob = DateFormat('dd-MMM-yyyy').format(value).toString();
                  } catch (e) {
                    log(e.toString());
                  }
                });
              }
            });
          },
          child: Container(
            //height: getHP(context, MyTheme.btnHpa),
            decoration: BoxDecoration(
              //borderRadius: BorderRadius.circular(10),
              border: Border(
                bottom: BorderSide(color: Colors.black, width: 1),
              ),
            ),
            child: Transform.translate(
              offset: Offset(0, -10),
              child: Txt(
                txt: dob,
                txtColor: Colors.black,
                txtSize: MyTheme.txtSize,
                txtAlign: TextAlign.start,
                isBold: false,
              ),
            ),
          ),
        )),
      ],
    );
  }

  drawGender() {
    return Container(
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          Container(
            width: getWP(context, 20),
            child: Txt(
              txt: "Gender:",
              txtColor: Colors.black,
              txtSize: MyTheme.txtSize,
              txtAlign: TextAlign.start,
              isBold: false,
            ),
          ),
          Expanded(
            child: Transform.translate(
              offset: Offset(-10, -10),
              child: Theme(
                data: Theme.of(context).copyWith(
                  unselectedWidgetColor: Colors.black,
                  disabledColor: Colors.black,
                  selectedRowColor: Colors.black,
                  indicatorColor: Colors.black,
                  toggleableActiveColor: Colors.black,
                ),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    InkWell(
                      onTap: () {
                        if (mounted) {
                          setState(() {
                            _gender = genderEnum.male;
                          });
                        }
                      },
                      child: Radio(
                        value: genderEnum.male,
                        groupValue: _gender,
                        onChanged: (genderEnum value) {
                          if (mounted) {
                            setState(() {
                              _gender = value;
                            });
                          }
                        },
                      ),
                    ),
                    Txt(
                      txt: "Male",
                      txtColor: Colors.black,
                      txtSize: MyTheme.txtSize - .2,
                      txtAlign: TextAlign.center,
                      isBold: false,
                    ),
                    InkWell(
                      onTap: () {
                        if (mounted) {
                          setState(() {
                            _gender = genderEnum.female;
                          });
                        }
                      },
                      child: Radio(
                        value: genderEnum.female,
                        groupValue: _gender,
                        onChanged: (genderEnum value) {
                          if (mounted) {
                            setState(() {
                              _gender = value;
                            });
                          }
                        },
                      ),
                    ),
                    Txt(
                      txt: "Female",
                      txtColor: Colors.black,
                      txtSize: MyTheme.txtSize - .2,
                      txtAlign: TextAlign.center,
                      isBold: false,
                    ),
                  ],
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
