import 'package:aitl/config/cfg/AppDefine.dart';
import 'package:aitl/config/server/APIAuthCfg.dart';
import 'package:aitl/config/server/APITypeCFg.dart';
import 'package:aitl/controller/form_validator/UserProfileVal.dart';
import 'package:aitl/data/app_data/UserData.dart';
import 'package:aitl/data/model/auth/otp/sms1/MobileUserOtpPostAPIModel.dart';
import 'package:aitl/data/model/country_picker/ola_like_country_picker.dart';
import 'package:aitl/data/network/ModelMgr.dart';
import 'package:aitl/data/network/NetworkMgr.dart';
import 'package:aitl/view/widgets/CountryMobilePicker.dart';
import 'package:aitl/view/widgets/MMBtn.dart';
import 'package:aitl/view_model/api/api_view_model.dart';
import 'package:aitl/view_model/observer/APIStateProvider.dart';
import 'package:aitl/view/auth/otp/sms_page3.dart';
import 'package:aitl/view/widgets/Btn.dart';
import 'package:aitl/view/widgets/Txt.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:aitl/config/theme/MyTheme.dart';
import 'package:aitl/mixin.dart';
import 'package:get/get.dart';

class Sms2Page extends StatefulWidget {
  final String mobile;
  Sms2Page({Key key, this.mobile = ''}) : super(key: key);
  @override
  State createState() => _Sms2PageState();
}

class _Sms2PageState extends State<Sms2Page> with APIStateListener, Mixin {
  Color btnBgColor;
  Color btnTxtColor;

  CountryPicker c;
  Country country = Country.fromJson(countryCodes[94]);
  var countryFlag = "assets/images/flags/" + AppDefine.COUNTRY_FLAG + ".png";
  var countryDialCode = AppDefine.COUNTRY_DIALCODE;
  var countryName = AppDefine.COUNTRY_NAME;
  final mobile = TextEditingController();

  APIStateProvider _apiStateProvider;
  @override
  onAPIStateChanged(apiState, model) async {
    try {
      if (apiState.type == APIType.otp_post &&
          apiState.cls == this.runtimeType) {
        if (model != null && mounted) {
          if (model.success) {
            Get.to(
              () => Sms3Screen(
                mobileUserOTPModel: model.responseData.userOTP,
              ),
            ).then((value) {
              //callback(route);
            });
          } else {
            try {
              final err = model.messages.postUserotp[0].toString();
              showAlert(msg: err, isToast: true);
            } catch (e) {
              log(e.toString());
            }
          }
        }
      }
    } catch (e) {
      log(e.toString());
    }
  }

  @override
  void initState() {
    super.initState();
    appInit();
  }

  @override
  void dispose() {
    try {
      _apiStateProvider.unsubscribe(this);
      _apiStateProvider = null;
    } catch (e) {
      log(e.toString());
    }
    c = null;
    country = null;
    countryFlag = null;
    countryDialCode = null;
    countryName = null;
    mobile.dispose();
    super.dispose();
  }

  appInit() async {
    try {
      _apiStateProvider = new APIStateProvider();
      _apiStateProvider.subscribe(this);
    } catch (e) {}

    try {
      if (widget.mobile.length == 0) {
        btnBgColor = Colors.grey[300];
        btnTxtColor = Colors.black;
      } else {
        btnBgColor = MyTheme.redColor;
        btnTxtColor = Colors.white;
      }

      try {
        c = CountryPicker(onCountrySelected: (Country country) {
          log(country);
          setState(() {
            this.country = country;
            countryName = country.name;
            countryFlag = country.flagUri;
            countryDialCode = country.dialCode;
          });
        });
      } catch (e) {}

      mobile.text = widget.mobile;
      mobile.addListener(() {
        if (mounted) {
          if (mobile.text.trim().length < UserProfileVal.PHONE_LIMIT) {
            btnBgColor = Colors.grey[300];
            btnTxtColor = Colors.black;
          } else {
            btnBgColor = MyTheme.redColor;
            btnTxtColor = Colors.white;
          }
          setState(() {});
        }
      });
    } catch (e) {}
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: MyTheme.bgColor,
        appBar: AppBar(
          //automaticallyImplyLeading: false,
          elevation: MyTheme.appbarElevation,
          iconTheme: IconThemeData(color: Colors.black),
          backgroundColor: MyTheme.bgColor,
        ),
        body: GestureDetector(
          onTap: () {
            FocusScope.of(context).requestFocus(new FocusNode());
          },
          child: drawLayout(),
        ),
      ),
    );
  }

  drawLayout() {
    return Form(
      child: Column(
        //crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          SizedBox(height: 50),
          Padding(
            padding: const EdgeInsets.only(left: 20, right: 20),
            child: Txt(
              txt: "Enter your mobile number",
              txtColor: MyTheme.gray4Color,
              txtSize: MyTheme.txtSize + .3,
              txtAlign: TextAlign.center,
              //txtLineSpace: 1.0,
              isBold: false,
            ),
          ),
          SizedBox(height: 10),
          Padding(
            padding: const EdgeInsets.only(left: 20, right: 20),
            child: drawCountryMobilePicker(
                context: context,
                mobile: mobile,
                hint: "XXXXXXXXX",
                labelColor: MyTheme.gray4Color,
                isTxtCenter: true,
                isFocus: true,
                countryDialCode: countryDialCode,
                countryFlag: countryFlag,
                callback: () {
                  c.launch(context);
                }),
          ),
          MMBtn(
            txt: "Next",
            txtColor: btnTxtColor,
            bgColor: btnBgColor,
            width: getWP(context, 90),
            height: getHP(context, MyTheme.btnHpa),
            radius: 10,
            callback: () async {
              if (mobile.text.trim().length >= UserProfileVal.PHONE_LIMIT) {
                final param = {
                  "MobileNumber": "+" +
                      countryDialCode.replaceAll("+", "") +
                      (mobile.text.trim().replaceAll(countryDialCode, "")),
                  "OTPCode": "",
                  "Status": 101,
                  "UserId":
                      (userData.userModel != null) ? userData.userModel.id : 0,
                };
                log(param);
                APIViewModel().req<MobileUserOtpPostAPIModel>(
                  context: context,
                  apiState: APIState(APIType.otp_post, this.runtimeType, null),
                  url: APIAuthCfg.LOGIN_MOBILE_OTP_POST_URL,
                  reqType: ReqType.Post,
                  param: param,
                );
              } else {
                showAlert(
                    msg: 'Please enter your valid phone number', isToast: true);
              }
            },
          ),
        ],
      ),
    );
  }
}
