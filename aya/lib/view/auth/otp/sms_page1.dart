import 'package:aitl/config/theme/MyTheme.dart';
import 'package:aitl/mixin.dart';
import 'package:aitl/view/auth/otp/sms_page2.dart';
import 'package:aitl/view/widgets/IcoTxtIcoMM.dart';
import 'package:aitl/view/widgets/TCView.dart';
import 'package:aitl/view/widgets/Txt.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class Sms1Page extends StatefulWidget {
  @override
  State createState() => _Sms1PageState();
}

class _Sms1PageState extends State<Sms1Page> with Mixin {
  @override
  void initState() {
    super.initState();
    appInit();
  }

  //@mustCallSuper
  @override
  void dispose() {
    super.dispose();
  }

  appInit() {
    /*try {
      SystemChrome.setSystemUIOverlayStyle(
        SystemUiOverlayStyle(
            statusBarColor: MyTheme.redColor,
            statusBarIconBrightness: Brightness.dark),
      );
    } catch (e) {}*/
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: MyTheme.grayColor,
        appBar: AppBar(
          //automaticallyImplyLeading: false,
          elevation: MyTheme.appbarElevation,
          iconTheme: IconThemeData(color: Colors.black),
          backgroundColor: MyTheme.bgColor,
        ),
        body: GestureDetector(
          onTap: () {
            FocusScope.of(context).requestFocus(new FocusNode());
          },
          child: drawLayout(),
        ),
        bottomNavigationBar: BottomAppBar(
          color: MyTheme.grayColor,
          child: Padding(
            padding:
                const EdgeInsets.only(left: 20, right: 20, top: 0, bottom: 20),
            child: TCView(
              enum_tc: eTC.SMS_TC,
              txt1Color: Colors.white,
              txt2Color: Colors.white,
            ),
          ),
        ),
      ),
    );
  }

  drawLayout() {
    return Container(
      child: SingleChildScrollView(
        child: Column(
          //shrinkWrap: true,
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            SizedBox(height: getHP(context, 10)),
            Padding(
              padding: const EdgeInsets.only(left: 20, right: 20),
              child: Txt(
                txt: "Please give your mobile number to start using the app",
                txtColor: Colors.white,
                txtSize: MyTheme.txtSize + .5,
                txtAlign: TextAlign.center,
                //txtLineSpace: 1.2,
                isBold: false,
              ),
            ),
            SizedBox(height: getHP(context, 5)),
            Padding(
              padding: const EdgeInsets.all(20),
              child: Container(
                //height: getHP(context, 45),
                // width: getWP(context, 80),
                decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: new BorderRadius.only(
                    topLeft: const Radius.circular(20.0),
                    topRight: const Radius.circular(20.0),
                    bottomLeft: const Radius.circular(20.0),
                    bottomRight: const Radius.circular(20.0),
                  ),
                ),
                child: Padding(
                  padding: const EdgeInsets.only(top: 50, bottom: 50),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Txt(
                        txt: "Please give your mobile number",
                        txtColor: MyTheme.gray5Color,
                        txtSize: MyTheme.txtSize,
                        txtAlign: TextAlign.center,
                        isBold: false,
                      ),
                      SizedBox(height: 10),
                      GestureDetector(
                        onTap: () async {
                          //Get.dialog(SMS1Dialog()).then((value) {});
                          Get.to(() => Sms2Page());
                        },
                        child: Padding(
                          padding: const EdgeInsets.only(left: 10, right: 10),
                          child: IcoTxtIcoMM(
                            leftIcon: Icons.mobile_screen_share,
                            txt: "Log in with Mobile",
                            rightIcon: Icons.arrow_right,
                            iconColor: MyTheme.redColor,
                            leftIconSize: 40,
                            rightIconSize: 50,
                          ),
                        ),
                      ),
                      /*Expanded(
                    child: Container(
                      width: getWP(context, 80),
                      //height: getHP(context, 20),
                      child: Image.asset(
                        'assets/images/login/login_mobile_bg.png',
                        fit: BoxFit.fill,
                      ),
                    ),
                  ),*/
                    ],
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
