import 'package:aitl/config/server/ResCfg.dart';
import 'package:aitl/config/theme/MyTheme.dart';
import 'package:aitl/data/model/dashboard/more/res/ResAPIModel.dart';
import 'package:aitl/data/network/ModelMgr.dart';
import 'package:aitl/data/network/NetworkMgr.dart';
import 'package:aitl/view/auth/auth_base.dart';
import 'package:aitl/view/auth/reallife_profile_page.dart';
import 'package:aitl/view/auth/spiritual_profile_page.dart';
import 'package:aitl/view/widgets/BottomBtn.dart';
import 'package:aitl/view/widgets/InputBox.dart';
import 'package:aitl/view/widgets/InputBoxHT.dart';
import 'package:aitl/view/widgets/Txt.dart';
import 'package:aitl/view/widgets/Txt.dart';
import 'package:aitl/view_model/api/api_view_model.dart';
import 'package:aitl/view_model/observer/APIStateProvider.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:carousel_slider/carousel_slider.dart';
import '../../config/theme/MyTheme.dart';
import '../widgets/Txt.dart';

enum eProfileType {
  Reallife,
  Spiritual,
}

class ProfileTypePage extends StatefulWidget {
  const ProfileTypePage({Key key}) : super(key: key);

  @override
  State createState() => _ProfileTypePageState();
}

class _ProfileTypePageState extends BaseAuth<ProfileTypePage>
    with APIStateListener {
  eProfileType eProfType = eProfileType.Reallife;

  APIStateProvider _apiStateProvider;
  @override
  onAPIStateChanged(apiState, model) async {
    try {
      if (apiState.type == APIType.res && apiState.cls == this.runtimeType) {
        if (model != null && mounted) {
          if (model.success) {
            showAlert(msg: "Submitted successfully", isToast: true, which: 1);
          }
        }
      }
    } catch (e) {}
  }

  @override
  void initState() {
    super.initState();
    appInit();
  }

  @override
  void dispose() {
    //myTaskController.dispose();
    try {
      _apiStateProvider.unsubscribe(this);
      _apiStateProvider = null;
    } catch (e) {
      log(e.toString());
    }

    super.dispose();
  }

  appInit() {
    try {
      _apiStateProvider = new APIStateProvider();
      _apiStateProvider.subscribe(this);
    } catch (e) {}
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: MyTheme.bgColor,
        /*appBar: AppBar(
          iconTheme: IconThemeData(color: Colors.black //change your color here
              ),
          elevation: MyTheme.appbarElevation,
          backgroundColor: MyTheme.bgColor,
          title: Txt(
              txt: "Brief Profile",
              txtColor: Colors.black,
              txtSize: MyTheme.appbarTitleFontSize,
              txtAlign: TextAlign.start,
              isBold: false),
          centerTitle: true,
          leading: IconButton(
              icon: Icon(Icons.arrow_back),
              onPressed: () async {
                Get.back();
              }),
        ),*/
        bottomNavigationBar: drawBottomBtn(
            context: context,
            text: "Let's go",
            bgColor: MyTheme.greenColor,
            txtColor: Colors.black,
            callback: () async {
              /*APIViewModel().req<ResAPIModel>(
                context: context,
                apiState: APIState(APIType.res, this.runtimeType, null),
                url: ResCfg.RES_POST_URL,
                reqType: ReqType.Post,
                param: {},
              );*/

              if (eProfType == eProfileType.Reallife) {
                Get.to(() => ReallifeProfilePage());
              } else if (eProfType == eProfileType.Spiritual) {
                Get.to(() => SpiritualProfilePage());
              }
            }),
        body: GestureDetector(
          onTap: () {
            FocusScope.of(context).requestFocus(new FocusNode());
          },
          child: drawLayout(),
        ),
      ),
    );
  }

  drawLayout() {
    return Container(
      child: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            drawTopbar(true, "You can create two\nprofiles in this platform"),
            drawChoiceBox(),
          ],
        ),
      ),
    );
  }

  drawChoiceBox() {
    return Padding(
      padding: const EdgeInsets.only(top: 50, left: 20, right: 20),
      child: Column(
        children: [
          drawCardBox(eProfileType.Reallife, "1. Create Real-life Profile",
              "Your real profile is the identity you use in real life, with your family given names, location etc."),
          SizedBox(height: 20),
          drawCardBox(eProfileType.Spiritual, "2. Create Spiritual Profile",
              "Your spiritual profile is the identity when you have discovered who you 'truly' are through any of your spiritual experiences."),
        ],
      ),
    );
  }

  drawCardBox(eProfileType e, String title, String subTitle) {
    double elevation = 0;
    String ico = "on";
    if (eProfType == e) {
      ico = "on";
      elevation = 10;
    } else {
      ico = "off";
      elevation = 0;
    }
    return GestureDetector(
      onTap: () {
        eProfType = e;
        setState(() {});
      },
      child: Card(
        elevation: elevation,
        child: Padding(
          padding: const EdgeInsets.all(20),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Image.asset(
                "assets/images/icons/ico_check_" + ico + ".png",
              ),
              SizedBox(width: 15),
              Expanded(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Txt(
                        txt: title,
                        txtColor: Colors.black,
                        txtSize: MyTheme.txtSize,
                        txtAlign: TextAlign.start,
                        txtLineSpace: 1.3,
                        isBold: true),
                    SizedBox(height: 10),
                    Txt(
                        txt: subTitle,
                        txtColor: Colors.black54,
                        txtSize: MyTheme.txtSize,
                        txtAlign: TextAlign.start,
                        txtLineSpace: 1.3,
                        isBold: false),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
