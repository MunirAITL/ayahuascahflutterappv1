import 'package:aitl/config/cfg/AppDefine.dart';
import 'package:aitl/config/server/APIAuthCfg.dart';
import 'package:aitl/config/theme/MyTheme.dart';
import 'package:aitl/controller/classes/Common.dart';
import 'package:aitl/controller/form_validator/UserProfileVal.dart';
import 'package:aitl/data/app_data/UserData.dart';
import 'package:aitl/data/db/DBMgr.dart';
import 'package:aitl/data/model/auth/LoginAPIModel.dart';
import 'package:aitl/data/model/auth/RegAPIModel.dart';
import 'package:aitl/data/model/country_picker/ola_like_country_picker.dart';
import 'package:aitl/data/network/ModelMgr.dart';
import 'package:aitl/data/network/NetworkMgr.dart';
import 'package:aitl/mixin.dart';
import 'package:aitl/view/auth/auth_base.dart';
import 'package:aitl/view/auth/brief_profile_page.dart';
import 'package:aitl/view/auth/login_page.dart';
import 'package:aitl/view/auth/otp/sms_page2.dart';
import 'package:aitl/view/auth/reg2_page.dart';
import 'package:aitl/view/dashboard/dashboard_page.dart';
import 'package:aitl/view/widgets/InputBox.dart';
import 'package:aitl/view/widgets/InputBoxHT.dart';
import 'package:aitl/view/widgets/MMBtn.dart';
import 'package:aitl/view/widgets/Txt.dart';
import 'package:aitl/view_model/api/api_view_model.dart';
import 'package:aitl/view_model/helper/auth/AuthHelper.dart';
import 'package:aitl/view_model/helper/auth/LoginHelper.dart';
import 'package:aitl/view_model/observer/APIStateProvider.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class RegPage1 extends StatefulWidget {
  const RegPage1({Key key}) : super(key: key);
  @override
  State createState() => _RegPage1State();
}

class _RegPage1State extends BaseAuth<RegPage1> with APIStateListener {
  //  reg1
  final name = TextEditingController();
  final email = TextEditingController();
  final pwd = TextEditingController();

  CountryPicker c;
  Country country = Country.fromJson(countryCodes[94]);
  var countryFlag = "assets/images/flags/" + AppDefine.COUNTRY_FLAG + ".png";
  var countryDialCode = AppDefine.COUNTRY_DIALCODE;
  var countryName = AppDefine.COUNTRY_NAME;
  final mobile = TextEditingController();

  bool isObscure = true;

  APIStateProvider _apiStateProvider;
  @override
  onAPIStateChanged(apiState, model) async {
    try {
      if (apiState.type == APIType.login && apiState.cls == this.runtimeType) {
        if (model != null && mounted) {
          if (model.success) {
            //log(model.responseData.user.address);
            //DBMgr.shared.getUserProfile();
            try {
              await DBMgr.shared.setUserProfile(
                user: model.responseData.user,
                otpID: model.responseData.otpId.toString(),
                otpMobileNumber: model.responseData.otpMobileNumber,
              );
              await userData.setUserModel();
            } catch (e) {
              log(e.toString());
            }
            Get.off(
              () => RegPage2(),
            ).then((value) {
              //callback(route);
            });
          } else {
            try {
              if (mounted) {
                //final err =
                //model2.errorMessages.login[0].toString();
                showAlert(
                    msg: "You have entered invalid credentials", isToast: true);
              }
            } catch (e) {
              log(e.toString());
            }
          }
        }
      } else if (apiState.type == APIType.reg1 &&
          apiState.cls == this.runtimeType) {
        if (model != null && mounted) {
          try {
            if (model.success) {
              //  recall login to get cookie
              APIViewModel().req<LoginAPIModel>(
                context: context,
                apiState: APIState(APIType.login, this.runtimeType, null),
                url: APIAuthCfg.LOGIN_URL,
                param: LoginHelper()
                    .getParam(email: email.text.trim(), pwd: pwd.text.trim()),
                reqType: ReqType.Post,
                isCookie: true,
              );
            } else {
              try {
                final err = model.errorMessages.register[0].toString();
                showAlert(msg: err, isToast: true);
              } catch (e) {
                log(e.toString());
              }
            }
          } catch (e) {
            log(e.toString());
          }
        }
      } else if (apiState.type == APIType.loginWithG &&
          apiState.cls == this.runtimeType) {
        if (model != null && mounted) {
          checkLoginRes(model as LoginAPIModel);
        }
      } else if (apiState.type == APIType.loginWithFB &&
          apiState.cls == this.runtimeType) {
        if (model != null && mounted) {
          checkLoginRes(model as LoginAPIModel);
        }
      }
    } catch (e) {
      log(e.toString());
    }
  }

  validate() {
    if (!UserProfileVal().isFullName(name.text, MyTheme.redColor)) {
      return false;
    } else if (!UserProfileVal()
        .isEmailOK(email, "Invalid email address", MyTheme.redColor)) {
      return false;
    } else if (!UserProfileVal().isPhoneOK(mobile, MyTheme.redColor)) {
      return false;
    } else if (!UserProfileVal().isPwdOK(pwd, MyTheme.redColor)) {
      return false;
    }
    return true;
  }

  @override
  void initState() {
    super.initState();
    appInit();
  }

  //@mustCallSuper
  @override
  void dispose() {
    email.dispose();
    pwd.dispose();
    name.dispose();
    mobile.dispose();
    c = null;
    country = null;
    countryFlag = null;
    countryDialCode = null;
    countryName = null;
    try {
      _apiStateProvider.unsubscribe(this);
      _apiStateProvider = null;
    } catch (e) {
      log(e.toString());
    }
    try {
      NetworkMgr().dispose();
    } catch (e) {}
    super.dispose();
  }

  appInit() {
    try {
      _apiStateProvider = new APIStateProvider();
      _apiStateProvider.subscribe(this);
    } catch (e) {}
    try {
      c = CountryPicker(onCountrySelected: (Country country) {
        log(country);
        setState(() {
          this.country = country;
          countryName = country.name;
          countryFlag = country.flagUri;
          countryDialCode = country.dialCode;
        });
      });
    } catch (e) {}
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
          backgroundColor: MyTheme.bgColor,
          body: GestureDetector(
            behavior: HitTestBehavior.opaque,
            onPanDown: (detail) {
              FocusScope.of(context).requestFocus(new FocusNode());
            },
            onTap: () {
              FocusScope.of(context).requestFocus(new FocusNode());
            },
            child: drawLayout(),
          )),
    );
  }

  drawLayout() {
    return Container(
      child: SingleChildScrollView(
        child: Column(
          children: [
            drawTopLogo(),
            drawInputBox(),
          ],
        ),
      ),
    );
  }

  drawInputBox() {
    return Padding(
      padding: const EdgeInsets.only(left: 20, right: 20),
      child: Container(
        child: Column(
          children: [
            InputBox(
              ctrl: name,
              lableTxt: "Full Name",
              kbType: TextInputType.name,
              len: 20,
              ecap: eCap.Word,
            ),
            InputBox(
              ctrl: email,
              lableTxt: "Email",
              kbType: TextInputType.emailAddress,
              len: 50,
            ),
            GestureDetector(
              onTap: () {
                c.launch(context);
              },
              child: Container(
                child: Row(
                  children: [
                    Flexible(
                      child: Container(
                        //width: getWP(context, 30),
                        color: Colors.grey.withOpacity(.45),
                        child: Row(
                          children: [
                            SizedBox(width: 5),
                            Image.asset(
                              countryFlag,
                              width: 50,
                              height: 50,
                            ),
                            //SizedBox(width: 3),
                            Expanded(
                              child: Text(
                                "+" + countryDialCode.replaceAll("+", ""),
                                textAlign: TextAlign.center,
                                style: TextStyle(
                                  color: Colors.black,
                                  fontSize: 20,
                                ),
                              ),
                            ),
                            Icon(
                              Icons.arrow_drop_down,
                              color: Colors.black,
                            )
                          ],
                        ),
                      ),
                    ),
                    SizedBox(width: 10),
                    Expanded(
                      flex: 2,
                      child: InputBox(
                        ctrl: mobile,
                        lableTxt: "Mobile number",
                        kbType: TextInputType.phone,
                        len: 15,
                      ),
                    ),
                  ],
                ),
              ),
            ),
            InputBox(
              ctrl: pwd,
              lableTxt: "Password",
              kbType: TextInputType.text,
              len: 20,
              isPwd: isObscure,
              suffixIcon: IconButton(
                  icon: Icon((isObscure)
                      ? Icons.visibility_outlined
                      : Icons.visibility_off_rounded),
                  color: Colors.grey,
                  onPressed: () {
                    setState(() {
                      FocusScope.of(context).requestFocus(new FocusNode());
                      isObscure = !isObscure;
                    });
                  }),
            ),
            SizedBox(height: 10),
            MMBtn(
              txt: "SIGN UP",
              bgColor: MyTheme.greenColor,
              txtColor: Colors.black,
              width: getW(context),
              height: getHP(context, MyTheme.btnHpa),
              radius: 10,
              callback: () async {
                if (validate()) {
                  final nameMap = Common.splitName(name.text.trim());
                  String fname = nameMap['fname'];
                  String lname = nameMap['lname'];
                  APIViewModel().req<RegAPIModel>(
                    context: context,
                    apiState: APIState(APIType.reg1, this.runtimeType, null),
                    url: APIAuthCfg.REG_URL,
                    reqType: ReqType.Post,
                    param: {
                      "Address": '',
                      "Agreement": true,
                      "BriefBio": "",
                      "Cohort": '',
                      "CommunityId": "",
                      "ConfirmPassword": pwd.text.trim(),
                      "DateofBirth": '',
                      "DeviceType": Common.getDeviceType(),
                      "Email": email.text.trim(),
                      "FirstName": fname.trim(),
                      "Headline": "",
                      "LastName": lname.trim(),
                      "Latitude": "0",
                      "LinkedUrl": "",
                      "Longitude": "0",
                      "MobileNumber":
                          countryDialCode + ' ' + mobile.text.trim(),
                      "Password": pwd.text.trim(),
                      "ReferenceId": "",
                      "ReferenceType": "",
                      "ReferrerId": 0,
                      "Remarks": "",
                      "ReturnUrl": "",
                      "StanfordWorkplaceURL": "",
                      "TwitterUrl": "",
                      "Version": "28",
                    },
                  );
                }
              },
            ),
            SizedBox(height: 20),
            Txt(
              txt: "Or continue with",
              txtColor: MyTheme.gray4Color,
              txtSize: MyTheme.txtSize + .3,
              txtAlign: TextAlign.center,
              isBold: false,
            ),
            SizedBox(height: 20),
            Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                AuthHelper().drawGoogleFBLoginButtons(
                  context: context,
                  height: getHP(context, MyTheme.btnHpa),
                  width: getW(context),
                  icon: "ic_fb",
                  txt: "Continue with Facebook",
                  callback: () {
                    loginWithFB(this.runtimeType);
                  },
                ),
                SizedBox(height: 20),
                AuthHelper().drawGoogleFBLoginButtons(
                  context: context,
                  height: getHP(context, MyTheme.btnHpa),
                  width: getW(context),
                  icon: "ic_google",
                  txt: "Continue with Google",
                  callback: () {
                    loginWithGoogle(this.runtimeType);
                  },
                ),
              ],
            ),
            SizedBox(height: 50),
          ],
        ),
      ),
    );
  }
}
