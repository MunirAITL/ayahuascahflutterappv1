import 'package:aitl/config/server/ResCfg.dart';
import 'package:aitl/config/theme/MyTheme.dart';
import 'package:aitl/data/model/dashboard/more/res/ResAPIModel.dart';
import 'package:aitl/data/network/ModelMgr.dart';
import 'package:aitl/data/network/NetworkMgr.dart';
import 'package:aitl/view/auth/auth_base.dart';
import 'package:aitl/view/auth/profile_type_page.dart';
import 'package:aitl/view/auth/spiritual_profile_page.dart';
import 'package:aitl/view/widgets/BottomBtn.dart';
import 'package:aitl/view/widgets/Txt.dart';
import 'package:aitl/view/widgets/Txt.dart';
import 'package:aitl/view_model/api/api_view_model.dart';
import 'package:aitl/view_model/observer/APIStateProvider.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class BriefProfilePage extends StatefulWidget {
  const BriefProfilePage({Key key}) : super(key: key);

  @override
  State createState() => _BriefProfilePageState();
}

class _BriefProfilePageState extends BaseAuth<BriefProfilePage>
    with APIStateListener {
  final cmt = TextEditingController();
  final listInterests = [
    "Ayahuasca",
    "Yoga",
    "Spirituality",
  ];
  final listInterestsChild = [
    "MDMA",
    "Psilocybin",
    "Truffles",
    "Workout",
    "Vedanta",
    "Exercise",
  ];

  APIStateProvider _apiStateProvider;
  @override
  onAPIStateChanged(apiState, model) async {
    try {
      if (apiState.type == APIType.res && apiState.cls == this.runtimeType) {
        if (model != null && mounted) {
          if (model.success) {
            cmt.clear();
            showAlert(msg: "Submitted successfully", isToast: true, which: 1);
          }
        }
      }
    } catch (e) {}
  }

  @override
  void initState() {
    super.initState();
    appInit();
  }

  @override
  void dispose() {
    //myTaskController.dispose();
    try {
      _apiStateProvider.unsubscribe(this);
      _apiStateProvider = null;
    } catch (e) {
      log(e.toString());
    }
    cmt.dispose();
    super.dispose();
  }

  appInit() {
    try {
      _apiStateProvider = new APIStateProvider();
      _apiStateProvider.subscribe(this);
    } catch (e) {}
  }

  validate() {
    if (cmt.text.trim().isEmpty) {
      showAlert(msg: "Description cannot be blank", isToast: true);
      return false;
    }
    return true;
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: MyTheme.bgColor,
        /*appBar: AppBar(
          iconTheme: IconThemeData(color: Colors.black //change your color here
              ),
          elevation: MyTheme.appbarElevation,
          backgroundColor: MyTheme.bgColor,
          title: Txt(
              txt: "Brief Profile",
              txtColor: Colors.black,
              txtSize: MyTheme.appbarTitleFontSize,
              txtAlign: TextAlign.start,
              isBold: false),
          centerTitle: true,
          leading: IconButton(
              icon: Icon(Icons.arrow_back),
              onPressed: () async {
                Get.back();
              }),
        ),*/
        bottomNavigationBar: drawBottomBtn(
            context: context,
            text: "Let's go",
            bgColor: MyTheme.greenColor,
            txtColor: Colors.black,
            callback: () async {
              /*if (validate()) {
                APIViewModel().req<ResAPIModel>(
                  context: context,
                  apiState: APIState(APIType.res, this.runtimeType, null),
                  url: ResCfg.RES_POST_URL,
                  reqType: ReqType.Post,
                  param: {},
                );
              }*/
              Get.to(() => ProfileTypePage());
            }),
        body: GestureDetector(
          onTap: () {
            FocusScope.of(context).requestFocus(new FocusNode());
          },
          child: drawLayout(),
        ),
      ),
    );
  }

  drawLayout() {
    return Container(
      child: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            SizedBox(height: 50),
            drawCmtInput(),
            drawInterestsBox(),
            drawInterestsChildBox(),
          ],
        ),
      ),
    );
  }

  drawCmtInput() {
    return Padding(
      padding: const EdgeInsets.all(20),
      child: Container(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Txt(
                txt: "Brief Profile",
                txtColor: Colors.black,
                txtSize: MyTheme.txtSize,
                txtAlign: TextAlign.start,
                isBold: true),
            SizedBox(height: 10),
            Card(
              elevation: 10,
              child: Padding(
                padding: const EdgeInsets.only(
                    left: 20, right: 20, top: 20, bottom: 20),
                child: Container(
                  child: TextFormField(
                    controller: cmt,
                    minLines: 5,
                    maxLines: 10,
                    autocorrect: false,
                    keyboardType: TextInputType.multiline,
                    style: TextStyle(
                      color: Colors.black,
                      fontSize: getTxtSize(
                          context: context, txtSize: MyTheme.txtSize),
                    ),
                    decoration: new InputDecoration(
                      border: InputBorder.none,
                      focusedBorder: InputBorder.none,
                      enabledBorder: InputBorder.none,
                      errorBorder: InputBorder.none,
                      disabledBorder: InputBorder.none,
                      hintText: "",
                      hintStyle: new TextStyle(
                        color: Colors.grey,
                        fontSize: getTxtSize(
                            context: context, txtSize: MyTheme.txtSize),
                      ),
                      contentPadding: const EdgeInsets.symmetric(vertical: 0),
                    ),
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  drawInterestsBox() {
    return Padding(
      padding: const EdgeInsets.only(left: 20, right: 20),
      child: Container(
          child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Txt(
              txt: "Interests",
              txtColor: Colors.black,
              txtSize: MyTheme.txtSize,
              txtAlign: TextAlign.start,
              isBold: true),
          Padding(
            padding: const EdgeInsets.only(top: 10, bottom: 20),
            child: Container(
              width: getW(context),
              height: 40,
              child: ListView.builder(
                primary: false,
                shrinkWrap: true,
                itemCount: listInterests.length,
                scrollDirection: Axis.horizontal,
                itemBuilder: (context, index) {
                  return Container(
                    margin: EdgeInsets.all(2),
                    decoration: BoxDecoration(
                        color: MyTheme.blueColor,
                        borderRadius: BorderRadius.circular(15),
                        border: Border.all(color: Colors.grey, width: .5)),
                    child: Padding(
                      padding: const EdgeInsets.only(left: 15, right: 15),
                      child: Center(
                        child: Txt(
                            txt: listInterests[index],
                            txtColor: Colors.white,
                            txtSize: MyTheme.txtSize,
                            txtAlign: TextAlign.center,
                            isBold: false),
                      ),
                    ),
                  );
                },
              ),
            ),
          ),
        ],
      )),
    );
  }

  drawInterestsChildBox() {
    return Padding(
      padding: const EdgeInsets.only(left: 20, right: 20),
      child: Container(
          child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Padding(
            padding: const EdgeInsets.only(bottom: 20),
            child: Container(
              width: getW(context),
              height: 40,
              child: ListView.builder(
                primary: false,
                shrinkWrap: true,
                itemCount: listInterestsChild.length,
                scrollDirection: Axis.horizontal,
                itemBuilder: (context, index) {
                  return Container(
                    margin: EdgeInsets.all(2),
                    decoration: BoxDecoration(
                        color: MyTheme.gray2Color,
                        borderRadius: BorderRadius.circular(15),
                        border: Border.all(color: Colors.black, width: .5)),
                    child: Padding(
                      padding: const EdgeInsets.only(left: 15, right: 15),
                      child: Center(
                        child: Txt(
                            txt: listInterestsChild[index],
                            txtColor: Colors.black,
                            txtSize: MyTheme.txtSize,
                            txtAlign: TextAlign.center,
                            isBold: false),
                      ),
                    ),
                  );
                },
              ),
            ),
          ),
        ],
      )),
    );
  }
}
