import 'dart:ui';
import 'package:aitl/config/theme/MyTheme.dart';
import 'package:aitl/view/auth/login_page.dart';
import 'package:aitl/view/auth/reg1_page.dart';
import 'package:aitl/view/widgets/Txt.dart';
import 'package:flutter/material.dart';
import 'package:aitl/mixin.dart';
import 'package:get/get.dart';

class AuthScreen extends StatefulWidget {
  bool isSwitch = true;
  String fname = '';
  String lname = '';
  String email = '';
  String mobile = '';
  String pwd = '';
  String compName = '';

  @override
  State createState() => _AuthScreenState();
}

class _AuthScreenState extends State<AuthScreen> with Mixin {
  @override
  void initState() {
    super.initState();
  }

  //@mustCallSuper
  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 2,
      initialIndex: 1,
      child: SafeArea(
        child: Scaffold(
          backgroundColor: MyTheme.bgColor,
          appBar: AppBar(
            backgroundColor: MyTheme.bgColor,
            elevation: MyTheme.appbarElevation,
            automaticallyImplyLeading: false,
            bottom: PreferredSize(
              preferredSize: Size.fromHeight(getHP(context, 2)),
              child: Container(
                color: MyTheme.greenColor,
                height: getHP(context, MyTheme.btnHpa),
                child: TabBar(
                  //labelColor: Colors.deepOrange,
                  unselectedLabelColor: Colors.grey,
                  indicatorColor: Colors.black,
                  indicatorSize: TabBarIndicatorSize.tab,
                  indicatorWeight: 3,
                  tabs: [
                    Container(
                        //color: MyTheme.blueColor,
                        //height: getHP(context, MyTheme.btnHpa),
                        child: Txt(
                      txt: "CREATE AN ACCOUNT",
                      txtColor: Colors.white,
                      txtSize: MyTheme.txtSize - .2,
                      txtAlign: TextAlign.center,
                      isBold: false,
                    )),
                    Container(
                        child: Txt(
                      txt: "LOG IN",
                      txtColor: Colors.white,
                      txtSize: MyTheme.txtSize - .2,
                      txtAlign: TextAlign.center,
                      isBold: false,
                    )),
                  ],
                ),
              ),
            ),
          ),
          body: GestureDetector(
            behavior: HitTestBehavior.opaque,
            onPanDown: (detail) {
              FocusScope.of(context).requestFocus(new FocusNode());
            },
            onTap: () {
              FocusScope.of(context).requestFocus(new FocusNode());
            },
            child: TabBarView(
              children: [
                RegPage1(),
                LoginPage(),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
