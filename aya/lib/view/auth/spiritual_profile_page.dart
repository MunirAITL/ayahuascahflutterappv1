import 'package:aitl/config/server/ResCfg.dart';
import 'package:aitl/config/theme/MyTheme.dart';
import 'package:aitl/data/model/dashboard/more/res/ResAPIModel.dart';
import 'package:aitl/data/network/ModelMgr.dart';
import 'package:aitl/data/network/NetworkMgr.dart';
import 'package:aitl/view/auth/auth_base.dart';
import 'package:aitl/view/auth/profile_type_page.dart';
import 'package:aitl/view/widgets/BottomBtn.dart';
import 'package:aitl/view/widgets/InputBox.dart';
import 'package:aitl/view/widgets/InputBoxHT.dart';
import 'package:aitl/view/widgets/Txt.dart';
import 'package:aitl/view/widgets/Txt.dart';
import 'package:aitl/view_model/api/api_view_model.dart';
import 'package:aitl/view_model/observer/APIStateProvider.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:carousel_slider/carousel_slider.dart';
import '../../config/theme/MyTheme.dart';
import '../widgets/Txt.dart';

class SpiritualProfilePage extends StatefulWidget {
  const SpiritualProfilePage({Key key}) : super(key: key);

  @override
  State createState() => _SpiritualProfilePageState();
}

class _SpiritualProfilePageState extends BaseAuth<SpiritualProfilePage>
    with APIStateListener, SingleTickerProviderStateMixin {
  TabController tabController;
  final name = TextEditingController();

  int totalTabs = 8;

  APIStateProvider _apiStateProvider;
  @override
  onAPIStateChanged(apiState, model) async {
    try {
      if (apiState.type == APIType.res && apiState.cls == this.runtimeType) {
        if (model != null && mounted) {
          if (model.success) {
            name.clear();
            showAlert(msg: "Submitted successfully", isToast: true, which: 1);
          }
        }
      }
    } catch (e) {}
  }

  @override
  void initState() {
    super.initState();
    appInit();
  }

  @override
  void dispose() {
    //myTaskController.dispose();
    try {
      _apiStateProvider.unsubscribe(this);
      _apiStateProvider = null;
    } catch (e) {
      log(e.toString());
    }
    try {
      tabController.dispose();
      tabController = null;
    } catch (e) {}
    name.dispose();
    super.dispose();
  }

  appInit() {
    try {
      _apiStateProvider = new APIStateProvider();
      _apiStateProvider.subscribe(this);
    } catch (e) {}
    try {
      tabController = new TabController(vsync: this, length: totalTabs);
      tabController.addListener(() {
        setState(() {});
      });
      tabController.index = 1;
    } catch (e) {}
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: DefaultTabController(
        length: totalTabs,
        child: Scaffold(
          backgroundColor: MyTheme.bgColor,
          /*appBar: AppBar(
            iconTheme: IconThemeData(color: Colors.black //change your color here
                ),
            elevation: MyTheme.appbarElevation,
            backgroundColor: MyTheme.bgColor,
            title: Txt(
                txt: "Brief Profile",
                txtColor: Colors.black,
                txtSize: MyTheme.appbarTitleFontSize,
                txtAlign: TextAlign.start,
                isBold: false),
            centerTitle: true,
            leading: IconButton(
                icon: Icon(Icons.arrow_back),
                onPressed: () async {
                  Get.back();
                }),
          ),*/
          bottomNavigationBar: drawBottomBtn(
              context: context,
              text: "Let's go",
              bgColor: MyTheme.greenColor,
              txtColor: Colors.black,
              callback: () async {
                /*APIViewModel().req<ResAPIModel>(
                  context: context,
                  apiState: APIState(APIType.res, this.runtimeType, null),
                  url: ResCfg.RES_POST_URL,
                  reqType: ReqType.Post,
                  param: {},
                );*/
                Get.to(() => ProfileTypePage());
              }),
          body: GestureDetector(
            onTap: () {
              FocusScope.of(context).requestFocus(new FocusNode());
            },
            child: drawLayout(),
          ),
        ),
      ),
    );
  }

  drawLayout() {
    return Container(
      child: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            drawTopbar(true, "Create Your\nSpiritual Profile"),
            drawTitleBox(),
            drawInputBox(),
            drawPicSlider(),
          ],
        ),
      ),
    );
  }

  drawTitleBox() {
    return Padding(
      padding: const EdgeInsets.only(top: 20, left: 20, right: 20),
      child: Txt(
          txt:
              "Your spiritual profile is the identity when you have discovered who you 'truly' are through any of your spiritual experiences.",
          txtColor: Colors.black54,
          txtSize: MyTheme.txtSize,
          txtAlign: TextAlign.start,
          txtLineSpace: 1.3,
          isBold: false),
    );
  }

  drawInputBox() {
    final w = getWP(context, 20);
    return Padding(
      padding: EdgeInsets.only(top: 40, left: w, right: w),
      child: Container(
          child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Txt(
              txt: "Your Spiritual Name",
              txtColor: Colors.black,
              txtSize: MyTheme.txtSize + .6,
              txtAlign: TextAlign.center,
              isBold: false),
          InputBox(
            ctrl: name,
            lableTxt: "",
            labelColor: Colors.grey,
            kbType: TextInputType.name,
            len: 20,
            align: TextAlign.center,
            ecap: eCap.Word,
            borderLineWidth: 3,
          ),
        ],
      )),
    );
  }

  drawPicSlider() {
    return Padding(
      padding: const EdgeInsets.only(top: 40, left: 20, right: 20),
      child: Container(
        child: TabBar(
          controller: tabController,
          isScrollable: true,
          //labelColor: Colors.deepOrange,
          unselectedLabelColor: MyTheme.grayColor,
          indicatorColor: MyTheme.blueColor,
          indicatorSize: TabBarIndicatorSize.tab,
          indicatorWeight: 5,
          tabs: [
            drawBoy(0),
            drawBoy(1),
            drawBoy(2),
            drawBoy(3),
            drawBoy(4),
            drawBoy(5),
            drawBoy(6),
            drawBoy(7),
          ],
        ),
      ),
    );
  }

  drawBoy(int tabIndex) {
    final w = (getW(context) / 3) - 40;
    return Column(
      children: [
        Container(
          width: w,
          height: w * 2,
          child: Image.asset(
            "assets/images/pic_slider/boy" + (tabIndex + 1).toString() + ".png",
            fit: BoxFit.fill,
          ),
        ),
        (tabController.index == tabIndex)
            ? SizedBox()
            : Container(
                width: w,
                height: 5,
                color: Colors.grey,
              )
      ],
    );
  }
}
