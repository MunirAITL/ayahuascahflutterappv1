import 'package:aitl/config/server/APIAuthCfg.dart';
import 'package:aitl/config/server/APIMiscCfg.dart';
import 'package:aitl/config/theme/MyTheme.dart';
import 'package:aitl/controller/classes/Common.dart';
import 'package:aitl/data/app_data/UserData.dart';
import 'package:aitl/data/model/auth/RegAPIModel.dart';
import 'package:aitl/data/model/dashboard/more/help/WelcomeSmsAPIModel.dart';
import 'package:aitl/data/model/misc/CommonAPIModel.dart';
import 'package:aitl/data/network/ModelMgr.dart';
import 'package:aitl/data/network/NetworkMgr.dart';
import 'package:aitl/view_model/api/api_view_model.dart';
import 'package:aitl/view_model/helper/utils/APIHelper.dart';
import 'package:aitl/view_model/observer/APIStateProvider.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import 'app_tut2_base.dart';

class AppTut2Page extends StatefulWidget {
  const AppTut2Page({Key key}) : super(key: key);

  @override
  State createState() => _AppTut2PageState();
}

class _AppTut2PageState extends BaseTut2Statefull<AppTut2Page>
    with APIStateListener {
  //  **************  app states start

  @override
  void onDetached() {
    try {
      log("app state = onDetached");
    } catch (e) {}
  }

  @override
  void onInactive() {
    try {
      log("app state = onInactive");
    } catch (e) {}
  }

  @override
  void onPaused() {
    try {
      log("app state = onPaused");
    } catch (e) {}
  }

  @override
  void onResumed() {
    try {
      log("app state = onResumed");
    } catch (e) {}
  }

  //  **************  app states end

  APIStateProvider _apiStateProvider;
  @override
  onAPIStateChanged(apiState, model) async {
    try {
      if (apiState.type == APIType.welcome_sms &&
          apiState.cls == this.runtimeType) {
        if (model != null && mounted) {
          if (model.success) {}
        }
      } else if (apiState.type == APIType.reg1 &&
          apiState.cls == this.runtimeType) {
        if (model != null && mounted) {
          try {
            if (model.success) {
              //  recall login to get cookie

            } else {
              //final err = (model as RegAPIModel).errorMessages.register[0];
              //showAlert(msg: err, isToast: true);
            }
          } catch (e) {
            log(e.toString());
          }
        }
      } else if (apiState.type == APIType.reg_fb_mobile &&
          apiState.cls == this.runtimeType) {
        if (model != null && mounted) {
          try {
            if (model.success) {
              //  recall login to get cookie

            } else {
              //final err = (model as RegAPIModel).errorMessages.register[0];
              //showAlert(msg: err, isToast: true);
            }
          } catch (e) {
            log(e.toString());
          }
        }
      }
    } catch (e) {}
  }

  onGetStarted() async {
    try {
      final user = userData.userModel;
      final param = {
        "Address": user.address,
        "Agreement": true,
        "BriefBio": user.briefBio,
        "Cohort": '',
        "CommunityId": user.communityID,
        "ConfirmPassword": '',
        "DateofBirth": user.dateofBirth,
        "DeviceType": Common.getDeviceType(),
        "Email": user.email,
        "FirstName": user.firstName,
        "Headline": user.headline,
        "LastName": user.lastName,
        "Latitude": "0",
        "LinkedUrl": "",
        "Longitude": "0",
        "MobileNumber": user.mobileNumber,
        "Password": '',
        "ReferenceId": "",
        "ReferenceType": "",
        "ReferrerId": 0,
        "Remarks": "",
        "ReturnUrl": "",
        "StanfordWorkplaceURL": "",
        "TwitterUrl": "",
        "Version": APIHelper.map['deviceVersion'].toString(),
      };
      log(param.toString());
      await APIViewModel().req<RegAPIModel>(
        context: context,
        apiState: APIState(APIType.reg1, this.runtimeType, null),
        url: APIAuthCfg.REG_URL,
        reqType: ReqType.Post,
        param: param,
      );

      await APIViewModel().req<CommonAPIModel>(
        context: context,
        apiState: APIState(APIType.reg_fb_mobile, this.runtimeType, null),
        url: APIAuthCfg.LIGIN_REG_FB_MOBILE,
        reqType: ReqType.Post,
        param: {
          "DeviceType": APIHelper.map['deviceName'],
          "MobileNumber": userData.userModel.mobileNumber,
          "Version": APIHelper.map['deviceVersion'].toString()
        },
      );

      Get.back(result: true);
    } catch (e) {}
  }

  @override
  void initState() {
    super.initState();
    appInit();
  }

  @override
  void dispose() {
    try {
      _apiStateProvider.unsubscribe(this);
      _apiStateProvider = null;
    } catch (e) {
      log(e.toString());
    }
    controller.dispose();
    super.dispose();
  }

  appInit() async {
    try {
      controller.addListener(() {
        setState(() {
          currentPage = controller.page;
        });
      });
    } catch (e) {}
    try {
      _apiStateProvider = new APIStateProvider();
      _apiStateProvider.subscribe(this);
    } catch (e) {}
    try {
      await APIViewModel().req<WelcomeSmsAPIModel>(
        context: context,
        apiState: APIState(APIType.welcome_sms, this.runtimeType, null),
        url: APIMiscCfg.WELCOME_URL,
        param: {"UserId": userData.userModel.id},
        reqType: ReqType.Get,
      );
    } catch (e) {}
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: MyTheme.bgColor,
        bottomNavigationBar: drawBottomBar(),
        body: drawLayout(),
      ),
    );
  }

  @override
  drawLayout() {
    return PageView.builder(
        controller: controller,
        itemCount: pageCount,
        itemBuilder: (BuildContext context, int index) {
          switch (index) {
            case 0:
              return drawPage1();
              break;
            case 1:
              return drawPage2();
              break;
            case 2:
              return drawPage3();
              break;
            default:
              return Container();
          }
        });
  }
}
