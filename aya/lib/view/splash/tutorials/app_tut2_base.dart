import 'package:aitl/config/theme/MyTheme.dart';
import 'package:aitl/view/auth/auth_screen.dart';
import 'package:aitl/view/auth/brief_profile_page.dart';
import 'package:aitl/view/auth/login_page.dart';
import 'package:aitl/view/auth/reg1_page.dart';
import 'package:aitl/view/dashboard/dashboard_base.dart';
import 'package:aitl/view/widgets/MMBtn.dart';
import 'package:aitl/view/widgets/Txt.dart';
import 'package:dots_indicator/dots_indicator.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

abstract class BaseTut2Statefull<T extends StatefulWidget>
    extends BaseDashboard<T> {
  double currentPage = 0;
  PageController controller = PageController(
    initialPage: 0,
  );
  int pageCount = 3;

  onGetStarted();

  drawPage1() {
    return SingleChildScrollView(
      child: Padding(
        padding: const EdgeInsets.all(20),
        child: Container(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              SizedBox(height: 20),
              Padding(
                padding: const EdgeInsets.only(left: 20, right: 20),
                child: Txt(
                    txt: "Welcome",
                    txtColor: Colors.black,
                    txtSize: MyTheme.txtSize + 1.5,
                    txtAlign: TextAlign.center,
                    isBold: true),
              ),
              SizedBox(height: 30),
              Container(
                width: getWP(context, 60),
                height: getHP(context, 30),
                child: Image.asset(
                  "assets/images/tutorial/demo1.png",
                  fit: BoxFit.fill,
                ),
              ),
              SizedBox(height: 20),
              Txt(
                  txt: "LEARN MORE",
                  txtColor: Colors.black,
                  txtSize: MyTheme.txtSize + 1.3,
                  txtAlign: TextAlign.center,
                  isBold: false),
              Txt(
                  txt: "WITH COMMUNITY",
                  txtColor: Colors.black,
                  txtSize: MyTheme.txtSize + 1.3,
                  txtAlign: TextAlign.center,
                  isBold: false),
              Padding(
                padding: const EdgeInsets.only(top: 10, left: 20, right: 20),
                child: Txt(
                    txt:
                        "Learn more about the Holy Medicine of the Holy and Divine Mother Ayahuasca and transform your life and help heal yourself as well as heal the people around you!",
                    txtColor: MyTheme.blackLColor,
                    txtSize: MyTheme.txtSize + .3,
                    txtAlign: TextAlign.center,
                    txtLineSpace: 1.3,
                    isBold: false),
              ),
              SizedBox(height: 20),
            ],
          ),
        ),
      ),
    );
  }

  drawPage2() {
    return SingleChildScrollView(
      child: Padding(
        padding: const EdgeInsets.all(20),
        child: Container(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              SizedBox(height: 20),
              Container(
                width: getWP(context, 70),
                height: getHP(context, 40),
                child: Image.asset(
                  "assets/images/tutorial/demo2.png",
                  fit: BoxFit.fill,
                ),
              ),
              SizedBox(height: 20),
              Txt(
                  txt: "JOIN COMMUNITY,",
                  txtColor: Colors.black,
                  txtSize: MyTheme.txtSize + 1.3,
                  txtAlign: TextAlign.center,
                  isBold: false),
              Txt(
                  txt: "MAKE FRIENDS",
                  txtColor: Colors.black,
                  txtSize: MyTheme.txtSize + 1.3,
                  txtAlign: TextAlign.center,
                  isBold: false),
              Padding(
                padding: const EdgeInsets.only(top: 10, left: 20, right: 20),
                child: Txt(
                    txt:
                        "Join the Ayahuasca Social Community and make friends in the same journey as yourself! Meetup, learn from one another, and do much more...!",
                    txtColor: MyTheme.blackLColor,
                    txtSize: MyTheme.txtSize + .3,
                    txtAlign: TextAlign.center,
                    txtLineSpace: 1.3,
                    isBold: false),
              ),
              SizedBox(height: 20),
            ],
          ),
        ),
      ),
    );
  }

  drawPage3() {
    return SingleChildScrollView(
      child: Padding(
        padding: const EdgeInsets.all(20),
        child: Container(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              SizedBox(height: 20),
              Container(
                width: getWP(context, 70),
                height: getHP(context, 40),
                child: Image.asset(
                  "assets/images/tutorial/demo3.png",
                  fit: BoxFit.fill,
                ),
              ),
              SizedBox(height: 20),
              Txt(
                  txt: "JOIN RETREATS,",
                  txtColor: Colors.black,
                  txtSize: MyTheme.txtSize + 1.3,
                  txtAlign: TextAlign.center,
                  isBold: false),
              Txt(
                  txt: "EVENTS, MEETUPS",
                  txtColor: Colors.black,
                  txtSize: MyTheme.txtSize + 1.3,
                  txtAlign: TextAlign.center,
                  isBold: false),
              Padding(
                padding: const EdgeInsets.only(top: 10, left: 20, right: 20),
                child: Txt(
                    txt:
                        "Ayahuasca Social Community has many events, meetups and retreats. Join upcoming retreats, meetups, and events near your area or in your preferred cities.",
                    txtColor: MyTheme.blackLColor,
                    txtSize: MyTheme.txtSize + .3,
                    txtAlign: TextAlign.center,
                    txtLineSpace: 1.3,
                    isBold: false),
              ),
              SizedBox(height: 20),
            ],
          ),
        ),
      ),
    );
  }

  drawBottomBar() {
    return BottomAppBar(
      color: MyTheme.bgColor,
      child: Padding(
        padding: const EdgeInsets.only(left: 20, right: 20, top: 5, bottom: 5),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            (currentPage == 2)
                ? Padding(
                    padding: const EdgeInsets.only(bottom: 20),
                    child: MMBtn(
                        txt: "Get started!",
                        txtColor: Colors.white,
                        bgColor: MyTheme.greenColor,
                        height: getHP(context, MyTheme.btnHpa),
                        width: getWP(context, 50),
                        radius: 20,
                        callback: () {
                          Get.to(() => AuthScreen());
                        }),
                  )
                : SizedBox(),
            Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                (currentPage != 2)
                    ? SizedBox(width: getWP(context, 10))
                    : SizedBox(),
                Expanded(
                  child: DotsIndicator(
                    dotsCount: pageCount,
                    position: currentPage,
                    decorator: DotsDecorator(
                      color: MyTheme.grayDColor, // Inactive color
                      activeColor: MyTheme.greenColor,
                      size: const Size.square(15),
                      activeSize: const Size.square(15),
                    ),
                  ),
                ),
                (currentPage < 2)
                    ? MMBtn(
                        txt: "Skip",
                        txtColor: Colors.black,
                        bgColor: MyTheme.greenColor,
                        height: getHP(context, MyTheme.btnHpa),
                        width: getWP(context, 20),
                        radius: 20,
                        callback: () {
                          Get.to(() => AuthScreen());
                        })
                    : SizedBox(),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
