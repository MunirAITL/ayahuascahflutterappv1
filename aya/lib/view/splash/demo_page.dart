import 'package:aitl/config/app/events/DeviceEventTypesCfg.dart';
import 'package:aitl/config/server/Server.dart';
import 'package:aitl/config/theme/MyTheme.dart';
import 'package:aitl/data/app_data/CommonData.dart';
import 'package:aitl/data/app_data/PrefMgr.dart';
import 'package:aitl/data/app_data/UserData.dart';
import 'package:aitl/data/db/DBMgr.dart';
import 'package:aitl/data/network/CookieMgr.dart';
import 'package:aitl/mixin.dart';
import 'package:aitl/services/PushNotificationService.dart';
import 'package:aitl/view/auth/auth_screen.dart';
import 'package:aitl/view/auth/login_page.dart';
import 'package:aitl/view/dashboard/dashboard_page.dart';
import 'package:aitl/view/splash/tutorials/app_tut2_page.dart';
import 'package:aitl/view/widgets/utils/case_alert_page.dart';
import 'package:cookie_jar/cookie_jar.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_ringtone_player/flutter_ringtone_player.dart';
import 'package:get/get.dart';

class DemoPage extends StatefulWidget {
  @override
  State createState() => _DemoPageState();
}

class _DemoPageState extends State<DemoPage> with Mixin {
  static final FirebaseMessaging _firebaseMessaging = FirebaseMessaging();

  bool hasInstalled = false;

  @override
  void initState() {
    super.initState();
    appInit();
  }

  //@mustCallSuper
  @override
  void dispose() {
    super.dispose();
  }

  appInit() async {
    try {
      SystemChrome.setEnabledSystemUIOverlays(SystemUiOverlay.values);
      SystemChrome.setSystemUIOverlayStyle(
          SystemUiOverlayStyle(statusBarColor: MyTheme.bgColor));

      hasInstalled = await PrefMgr.shared
          .getPrefBool(DeviceEventTypesCfg.USERDEVICE_EVENTTYPE_INSTALL);

      //  apns
      //  https://console.firebase.google.com/project/_/notification
      //  https://medium.com/comerge/implementing-push-notifications-in-flutter-apps-aef98451e8f1
      final pushNotificationService =
          PushNotificationService(_firebaseMessaging);
      await pushNotificationService.initialise(
          callback: (enumFCM which, Map<String, dynamic> message) {
        switch (which) {
          case enumFCM.onMessage:
            fcmClickNoti(message);
            break;
          case enumFCM.onLaunch:
            break;
          case enumFCM.onResume:
            break;
          default:
        }
      });

      try {
        //  cookie
        CookieJar cj = await CookieMgr().getCookiee();
        final listCookies = cj.loadForRequest(Uri.parse(Server.BASE_URL));
        if (listCookies.length > 0 &&
            await DBMgr.shared.getTotalRow("User") > 0) {
          await userData.setUserModel();
          Get.to(() => DashboardPage()).then((value) {
            setState(() {
              go2Next();
            });
          });
        } else {
          Future.delayed(const Duration(seconds: 2), () {
            setState(() {
              go2Next();
            });
          });
        }
      } catch (e) {
        log(e.toString());
      }
    } catch (e) {}
  }

  fcmClickNoti(Map<String, dynamic> message) async {
    try {
      if (mounted && comData.isNotiTestPage != null) {
        if (comData.isNotiTestPage) {
          FlutterRingtonePlayer.play(
            android: AndroidSounds.notification,
            ios: IosSounds.glass,
            looping: false, // Android only - API >= 28
            volume: 0.1, // Android only - API >= 28
            asAlarm: false, // Android only - all APIs
          );

          Get.to(() => CaseAlertPage(
                message: message,
              )).then((value) async {
            await userData.setUserModel();
            Get.off(() => DashboardPage()).then((value) {
              setState(() {
                go2Next();
              });
            });
          });
        } else {
          final msg = message['data']['Message'] +
              '\n\n' +
              message['data']['Description'];
          showAlert(msg: msg);
        }
      }
    } catch (e) {
      log(e.toString());
    }
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
          resizeToAvoidBottomInset: false,
          backgroundColor: MyTheme.bgColor,
          body: GestureDetector(
            behavior: HitTestBehavior.opaque,
            onPanDown: (detail) {
              FocusScope.of(context).requestFocus(new FocusNode());
            },
            onTap: () {
              FocusScope.of(context).requestFocus(new FocusNode());
            },
            child: startSplash(),
          )),
    );
  }

  startSplash() {
    return Center(
      child: Image.asset(
        'assets/images/logo/logo.png',
      ),
    );
  }

  go2Next() {
    if (hasInstalled == null) {
      //await PrefMgr.shared.setPrefBool(
      //DeviceEventTypesCfg.USERDEVICE_EVENTTYPE_INSTALL, true);
      Get.to(() => AppTut2Page());
    } else {
      Get.to(() => AuthScreen());
    }
  }
}
