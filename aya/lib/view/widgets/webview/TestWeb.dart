import 'package:aitl/view/widgets/Txt.dart';
import 'package:flutter/material.dart';
import 'package:flutter_webview_plugin/flutter_webview_plugin.dart';
import 'package:aitl/config/theme/MyTheme.dart';
import 'package:aitl/mixin.dart';
import '../AppbarBotProgbar.dart';

class TestWeb extends StatefulWidget {
  final String url;
  final String title;

  const TestWeb({
    Key key,
    @required this.url,
    @required this.title,
  }) : super(key: key);

  @override
  State createState() => _TestWebState();
}

class _TestWebState extends State<TestWeb> with Mixin {
  final flutterWebviewPlugin = new FlutterWebviewPlugin();
  double progress = 0;
  @override
  void initState() {
    super.initState();
    appInit();
  }

  @override
  void dispose() {
    flutterWebviewPlugin.close();
    super.dispose();
  }

  appInit() {
    try {
      // Enable hybrid composition.
      flutterWebviewPlugin.onUrlChanged.listen((String url) {
        log("onUrlChanged::" + url);
      });
    } catch (e) {}
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: WebviewScaffold(
        url: widget.url,
        withLocalStorage: true,
        //withZoom: true,
        hidden: true,
        //resizeToAvoidBottomInset: true,
        //appCacheEnabled: true,
        debuggingEnabled: true,
        clearCookies: false,
        mediaPlaybackRequiresUserGesture: true,
        ignoreSSLErrors: true,
        javascriptChannels: jsChannels,
        withJavascript: true,
        initialChild: Container(
          color: Colors.white,
          child: Center(
            child: Txt(
                txt: "Loading...",
                txtColor: Colors.black,
                txtSize: MyTheme.txtSize,
                txtAlign: TextAlign.center,
                isBold: false),
          ),
        ),
        appBar: AppBar(
          elevation: MyTheme.appbarElevation,
          backgroundColor: MyTheme.themeData.accentColor,
          title: Txt(
              txt: widget.title,
              txtColor: Colors.white,
              txtSize: MyTheme.appbarTitleFontSize,
              txtAlign: TextAlign.start,
              isBold: false),
          centerTitle: false,
          leading: IconButton(
            icon: Icon(Icons.arrow_back, color: Colors.white),
            onPressed: () async {
              Navigator.pop(context);
            },
          ),
        ),
      ),
    );
  }

  Widget _buildProgressBar() {
    if (progress != 1.0) {
      //return CircularProgressIndicator();
      // You can use LinearProgressIndicator also
      return AppbarBotProgBar(
        backgroundColor: MyTheme.appbarProgColor,
      );
    }
    return Container();
  }

  // ignore: prefer_collection_literals
  final Set<JavascriptChannel> jsChannels = [
    JavascriptChannel(
        name: 'Print',
        onMessageReceived: (JavascriptMessage message) {
          print(message.message);
        }),
  ].toSet();
}
