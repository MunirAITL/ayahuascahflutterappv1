import 'dart:io';
import 'package:aitl/data/app_data/PrefMgr.dart';
import 'package:aitl/view/widgets/Txt.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:aitl/config/theme/MyTheme.dart';
import 'package:aitl/mixin.dart';
import 'package:get/get.dart';

class CamPicker with Mixin {
  List<String> _items = ['From Gallery', 'From Camera', 'Skip'];

  Future<File> _openCamera({isRear = true, String key}) async {
    final pickedFile = await ImagePicker().getImage(
      source: ImageSource.camera,
      preferredCameraDevice: (isRear) ? CameraDevice.rear : CameraDevice.front,
    );
    if (pickedFile != null) {
      File path = File(pickedFile.path);
      await PrefMgr.shared.setPrefStr(key, pickedFile.path);
      return path;
    }
  }

  Future<File> _openGallery({String key}) async {
    final pickedFile = await ImagePicker().getImage(
      source: ImageSource.gallery,
    );
    if (pickedFile != null) {
      File path = File(pickedFile.path);
      await PrefMgr.shared.setPrefStr(key, pickedFile.path);
      return path;
    }
  }

  void showCamDialog(
      {BuildContext context, Function callback, bool isRear = true}) {
    showDialog<int>(
      context: context,
      builder: (context) =>
          AlertDialog(content: Text("Choose image source"), actions: [
        MaterialButton(
          child: Text("Gallery"),
          onPressed: () => Navigator.pop(context, 0),
        ),
        MaterialButton(
          child: Text("Camera"),
          onPressed: () => Navigator.pop(context, 1),
        ),
      ]),
    ).then((int choice) async {
      if (choice != null) {
        switch (choice) {
          case 0:
            callback(await _openGallery()); //  gallery
            break;
          case 1:
            callback(await _openCamera(isRear: isRear)); //  cam
            break;
          default:
            break;
        }
      }
    });
  }
}
