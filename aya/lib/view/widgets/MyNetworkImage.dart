import 'package:aitl/config/server/Server.dart';
import 'package:aitl/config/server/ServerUrls.dart';
import 'package:flutter/material.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:mime/mime.dart';

class MyNetworkImage {
  static isValidUrl(String url) {
    if (url != null) {
      try {
        String mimeStr = lookupMimeType(url);
        var fileType = mimeStr.split('/');
        //print('file type ${fileType}');
        if (url.startsWith('http') &&
            fileType != null &&
            !url.contains("localhost") &&
            !url.contains("|")) {
          return true;
        } else
          return false;
      } catch (e) {
        return false;
      }
    }
    return false;
  }

  static checkUrl(String url) {
    if (isValidUrl(url)) {
      return url;
    } else
      return ServerUrls.MISSING_IMG;
  }

  static loadProfileImage(url) {
    return isValidUrl(url)
        ? NetworkImage(url)
        : AssetImage('assets/images/icons/user_icon.png');
  }

  loadFadeImage({String url}) {
    try {
      if (isValidUrl(url)) {
        return FadeInImage(
          image: NetworkImage(url),
          placeholder: AssetImage("assets/images/icons/loading_icon.png"),
          imageErrorBuilder: (context, error, stackTrace) {
            return Image.asset('assets/images/icons/img_nf_icon.png',
                fit: BoxFit.fitWidth);
          },
          fit: BoxFit.fitWidth,
        );
      } else {
        return Image.network(ServerUrls.MISSING_IMG);
      }
    } catch (e) {
      return Image.network(ServerUrls.MISSING_IMG);
    }
  }

  loadCacheImage({String url, isCircle = false}) {
    try {
      if (isValidUrl(url)) {
        return CachedNetworkImage(
          imageUrl: (url != null) ? url : ServerUrls.MISSING_IMG,
          imageBuilder: (context, imageProvider) => Container(
            decoration: BoxDecoration(
              shape: (isCircle) ? BoxShape.circle : BoxShape.rectangle,
              image: DecorationImage(image: imageProvider, fit: BoxFit.fill),
            ),
          ),
          placeholder: (context, url) => CircularProgressIndicator(),
          errorWidget: (context, url, error) => Image.asset(
              'assets/images/icons/img_nf_icon.png',
              fit: BoxFit.fitWidth),
        );
      } else {
        return Image.network(ServerUrls.MISSING_IMG);
      }
    } catch (e) {
      return Image.network(ServerUrls.MISSING_IMG);
    }
  }
}
