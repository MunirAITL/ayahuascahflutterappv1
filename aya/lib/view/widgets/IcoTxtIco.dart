import 'package:aitl/view/widgets/Txt.dart';
import 'package:flutter/material.dart';
import 'package:aitl/config/theme/MyTheme.dart';
import 'package:aitl/mixin.dart';

class IcoTxtIco extends StatelessWidget with Mixin {
  final IconData leftIcon;
  final IconData rightIcon;
  final txt;
  TextAlign txtAlign;
  double leftIconSize;
  double rightIconSize;
  double height;
  double topbotHeight;
  Color iconColor;
  Color txtColor;
  double borderWidth;

  IcoTxtIco({
    Key key,
    @required this.txt,
    @required this.leftIcon,
    @required this.rightIcon,
    this.iconColor = Colors.black,
    this.txtColor = Colors.black87,
    this.txtAlign = TextAlign.center,
    this.leftIconSize = 25,
    this.rightIconSize = 30,
    this.height = 10,
    this.topbotHeight = 0,
    this.borderWidth = .5,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      //height: getHP(context, MyTheme.btnHpa),
      decoration: BoxDecoration(
        //borderRadius: BorderRadius.circular(10),
        border: Border(
          bottom: BorderSide(
            color: Colors.black,
            width: borderWidth,
          ),
        ),
      ),
      alignment: Alignment.center,
      child: Container(
        color: (topbotHeight > 0) ? Colors.white : null,
        child: Padding(
          padding: EdgeInsets.only(
              left: (leftIcon != null) ? 10 : 0,
              right: 0,
              top: topbotHeight,
              bottom: topbotHeight),
          child: Row(
            //dense: true,
            //contentPadding: EdgeInsets.only(left: 0.0, right: 0.0),
            children: [
              (leftIcon != null)
                  ? Padding(
                      padding: const EdgeInsets.only(right: 5),
                      child: Icon(
                        leftIcon,
                        color: iconColor,
                        size: leftIconSize,
                      ),
                    )
                  : SizedBox(),
              Expanded(
                child: Txt(
                  txt: txt,
                  txtColor: txtColor,
                  txtSize: MyTheme.txtSize,
                  txtAlign: TextAlign.start,
                  isBold: false,
                ),
              ),
              (rightIcon != null)
                  ? Icon(
                      rightIcon,
                      color: iconColor,
                      size: rightIconSize,
                    )
                  : SizedBox(),
            ],
          ),
        ),
      ),
    );
  }
}
