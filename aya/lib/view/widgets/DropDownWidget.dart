import 'package:aitl/config/theme/MyTheme.dart';
import 'package:aitl/view/widgets/Txt.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class DropDownWidget extends StatelessWidget {
  String dropdownValue;
  final List<String> list;
  Function(String) callback;
  bool isExpanded;
  Color txtColor;
  DropDownWidget({
    Key key,
    @required this.list,
    this.dropdownValue,
    this.txtColor,
    @required this.isExpanded,
    @required this.callback,
  }) {
    if (txtColor == null) txtColor = MyTheme.gray5Color;
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 10, vertical: 5),
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(10),
        border: Border.all(color: Colors.black, width: .5),
      ),
      // dropdown below..
      child: DropdownButton<String>(
          dropdownColor: Colors.white,
          value: dropdownValue,
          isExpanded: true,
          icon: Icon(
            Icons.arrow_drop_down,
            color: Colors.grey,
          ),
          iconSize: 42,
          underline: SizedBox(),
          onChanged: (String newValue) {
            callback(newValue);
          },
          items: list.map<DropdownMenuItem<String>>((String value) {
            return DropdownMenuItem<String>(
              value: value,
              child: Txt(
                  txt: value,
                  txtColor: txtColor,
                  txtSize: MyTheme.txtSize,
                  txtAlign: TextAlign.start,
                  isBold: false),
            );
          }).toList()),
    );
  }
}
