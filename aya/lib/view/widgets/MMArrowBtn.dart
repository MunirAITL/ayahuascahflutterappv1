import 'package:aitl/view/widgets/Txt.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:aitl/config/theme/MyTheme.dart';
import 'package:aitl/mixin.dart';
import 'dart:math' as math;

class MMArrowBtn extends StatelessWidget with Mixin {
  final String txt;
  Color bgColor;
  Color txtColor;
  IconData icon;
  final double width;
  final double height;
  final double radius;
  final Function callback;

  MMArrowBtn({
    Key key,
    @required this.txt,
    @required this.width,
    @required this.height,
    this.radius = 20,
    this.icon = Icons.arrow_drop_down,
    @required this.callback,
    this.bgColor,
    this.txtColor,
  }) {
    if (bgColor == null) bgColor = MyTheme.greenColor;
    if (txtColor == null) txtColor = Colors.black;
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        callback();
      },
      child: Container(
        width: width,
        height: (height != null) ? height : getHP(context, 6),
        alignment: Alignment.center,
        //color: MyTheme.brownColor,
        decoration: new BoxDecoration(
          color: bgColor,
          borderRadius: new BorderRadius.circular(radius),
        ),
        child: Row(
          //minLeadingWidth: 0,
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            SizedBox(width: (icon == Icons.arrow_drop_down) ? 50 : 30),
            Expanded(
              child: Txt(
                txt: txt,
                txtColor: txtColor,
                txtSize: MyTheme.txtSize + .3,
                txtAlign: TextAlign.center,
                isBold: false,
              ),
            ),
            RotatedBox(
              quarterTurns: (icon == Icons.arrow_drop_down) ? 3 : 0,
              child: Icon(
                icon,
                color: Colors.white,
                size: (icon == Icons.arrow_drop_down) ? 50 : 30,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
