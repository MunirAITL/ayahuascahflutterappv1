import 'dart:ui';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:aitl/mixin.dart';

class AlrtDialog extends StatefulWidget {
  final int which;
  final String msg;
  final Color bgColor;
  Color txtColor;
  AlrtDialog({
    Key key,
    @required this.which,
    @required this.msg,
    @required this.bgColor,
    this.txtColor = Colors.white,
  }) : super(key: key);
  @override
  State createState() => _AlertDlgState();
}

class _AlertDlgState extends State<AlrtDialog> with Mixin {
  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Dialog(
      backgroundColor: Colors.transparent,
      child: Padding(
        padding: const EdgeInsets.all(20),
        child: Card(
          color: widget.bgColor,
          elevation: 10,
          child: ListView(
            shrinkWrap: true,
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.all(20),
                child: Text(
                  widget.msg,
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    color: widget.txtColor,
                    fontSize: 17,
                    fontWeight: FontWeight.bold,
                    height: 1.5,
                  ),
                ),
              ),
              TextButton(
                onPressed: () {
                  Get.back();
                },
                child: Text(
                  "Ok",
                  style: TextStyle(
                    color: widget.txtColor,
                    fontSize: 17,
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
