import 'package:aitl/config/cfg/AppDefine.dart';
import 'package:aitl/config/server/Server.dart';
import 'package:aitl/config/server/ServerUrls.dart';
import 'package:aitl/view/widgets/webview/WebScreen.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:aitl/config/theme/MyTheme.dart';
import 'package:aitl/mixin.dart';

enum eTC {
  REG_TC,
  SMS_TC,
}

class TCView extends StatelessWidget with Mixin {
  final eTC enum_tc;
  Color txt1Color;
  Color txt2Color;

  TCView({
    this.enum_tc,
    this.txt1Color = Colors.black,
    this.txt2Color,
  });

  @override
  Widget build(BuildContext context) {
    switch (enum_tc) {
      case eTC.REG_TC:
        return regTC(context);
        break;
      case eTC.SMS_TC:
        return smsTC(context);
      default:
        return Container();
    }
  }

  regTC(context) {
    return Row(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Expanded(
            child: RichText(
              textAlign: TextAlign.center,
              text: TextSpan(
                  text:
                      "By clicking on 'Create an account' you confirm that you accept the ",
                  style: TextStyle(
                      height: MyTheme.txtLineSpace,
                      color: txt1Color,
                      fontSize: getTxtSize(
                          context: context, txtSize: MyTheme.txtSize)),
                  children: <TextSpan>[
                    TextSpan(
                        text: AppDefine.APP_NAME + ' terms and Conditions',
                        style: TextStyle(
                            height: MyTheme.txtLineSpace,
                            color: (txt2Color == null)
                                ? MyTheme.brandColor
                                : txt2Color,
                            fontSize: getTxtSize(
                                context: context, txtSize: MyTheme.txtSize),
                            fontWeight: FontWeight.bold),
                        recognizer: TapGestureRecognizer()
                          ..onTap = () {
                            // navigate to desired screen
                            Get.to(
                              () => WebScreen(
                                title: "Terms & Conditions",
                                url: ServerUrls.TC_URL,
                              ),
                            ).then((value) {
                              //callback(route);
                            });
                          })
                  ]),
            ),
          ),
        ]);
  }

  smsTC(context) {
    return Row(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Expanded(
            child: RichText(
              textAlign: TextAlign.center,
              text: TextSpan(
                  text:
                      "By clicking on 'Log in with Mobile' you confirm that you accept the ",
                  style: TextStyle(
                      height: MyTheme.txtLineSpace,
                      color: txt1Color,
                      fontSize: getTxtSize(
                          context: context, txtSize: MyTheme.txtSize)),
                  children: <TextSpan>[
                    TextSpan(
                        text: AppDefine.APP_NAME + ' Terms and Conditions',
                        style: TextStyle(
                            height: MyTheme.txtLineSpace,
                            color: MyTheme.cornflower_blue_dark,
                            fontSize: getTxtSize(
                                context: context, txtSize: MyTheme.txtSize),
                            fontWeight: FontWeight.bold),
                        recognizer: TapGestureRecognizer()
                          ..onTap = () {
                            // navigate to desired screen
                            Get.to(
                              () => WebScreen(
                                title: "Terms & Conditions",
                                url: ServerUrls.TC_URL,
                              ),
                            ).then((value) {
                              //callback(route);
                            });
                          })
                  ]),
            ),
          ),
        ]);
  }
}
