import 'package:aitl/view/widgets/MMBtn.dart';
import 'package:aitl/view/widgets/Txt.dart';
import 'package:flutter/material.dart';
import 'package:aitl/config/theme/MyTheme.dart';
import 'package:aitl/mixin.dart';
import 'package:get/get.dart';

class CaseAlertPage extends StatelessWidget with Mixin {
  final Map<String, dynamic> message;

  const CaseAlertPage({Key key, @required this.message}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: MyTheme.bgColor,
        appBar: AppBar(
          backgroundColor: MyTheme.bgColor,
          elevation: MyTheme.appbarElevation,
          title: Txt(
              txt: 'Case alert',
              txtColor: Colors.black,
              txtSize: MyTheme.appbarTitleFontSize,
              txtAlign: TextAlign.center,
              isBold: false),
          centerTitle: false,
        ),
        body: drawLayout(context),
      ),
    );
  }

  drawLayout(context) {
    return Center(
      child: Container(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            SizedBox(height: getHP(context, 3)),
            Padding(
              padding: const EdgeInsets.all(20),
              child: Txt(
                  txt: message['data']['Message'].toString() ?? '',
                  txtColor: Colors.black,
                  txtSize: MyTheme.txtSize + .8,
                  txtAlign: TextAlign.center,
                  isBold: false),
            ),
            Padding(
              padding: const EdgeInsets.only(left: 20, right: 20),
              child: Txt(
                  txt: message['data']['Description'].toString() ?? '',
                  txtColor: Colors.black,
                  txtSize: MyTheme.txtSize,
                  txtAlign: TextAlign.center,
                  isBold: false),
            ),
            Padding(
              padding: const EdgeInsets.all(20),
              child: MMBtn(
                txt: "Close",
                width: getW(context),
                height: getHP(context, MyTheme.btnHpa),
                radius: 10,
                callback: () {
                  Get.back();
                },
              ),
            ),
          ],
        ),
      ),
    );
  }
}
