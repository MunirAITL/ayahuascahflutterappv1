import 'package:aitl/view/widgets/Txt.dart';
import 'package:flutter/material.dart';
import 'package:aitl/config/theme/MyTheme.dart';
import 'package:responsive_flutter/responsive_flutter.dart';

showDeactivateProfileDialog(
    {BuildContext context,
    TextEditingController email,
    Function callback}) async {
  await showDialog(
      context: context,
      builder: (context) {
        return AlertDialog(
            backgroundColor: Colors.white,
            title: Row(
              children: [
                Expanded(
                  child: Txt(
                      txt: "My Profile",
                      txtColor: Colors.black,
                      txtSize: MyTheme.txtSize,
                      txtAlign: TextAlign.start,
                      isBold: false),
                ),
                IconButton(
                    icon: Icon(Icons.close, color: Colors.black, size: 30),
                    onPressed: () {
                      Navigator.pop(context);
                      //callback(null);
                    })
              ],
            ),
            content: SingleChildScrollView(
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: [
                  TextField(
                    controller: email,
                    keyboardType: TextInputType.emailAddress,
                    minLines: 3,
                    maxLines: 5,
                    maxLength: 255,
                    style: TextStyle(
                      color: Colors.black,
                      fontSize: ResponsiveFlutter.of(context)
                          .fontSize(MyTheme.txtSize),
                    ),
                    decoration: InputDecoration(
                      labelText: 'Cancel Reason?',
                      labelStyle: TextStyle(
                        color: Colors.grey,
                        fontSize: ResponsiveFlutter.of(context)
                            .fontSize(MyTheme.txtSize),
                      ),
                      hintMaxLines: 1,
                      enabledBorder: const OutlineInputBorder(
                        borderSide:
                            const BorderSide(color: Colors.black, width: 0.0),
                      ),
                      border: new OutlineInputBorder(
                          borderSide: new BorderSide(color: Colors.blue)),
                    ),
                  ),
                  MaterialButton(
                    onPressed: () {
                      Navigator.pop(context);
                      callback(email.text.trim());
                    },
                    child: Txt(
                        txt: "Deactivate",
                        txtColor: Colors.white,
                        txtSize: MyTheme.txtSize,
                        txtAlign: TextAlign.start,
                        isBold: false),
                    color: Colors.grey,
                  ),
                ],
              ),
            ),
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(10.0)));
      });
}
