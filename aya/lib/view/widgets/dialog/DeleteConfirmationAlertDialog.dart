import 'package:aitl/config/theme/MyTheme.dart';
import 'package:aitl/view/widgets/Txt.dart';
import 'package:flutter/material.dart';

// ignore: non_constant_identifier_names
Future<Widget> DeleteConfirmationAlertDialog(
    {@required var alertBody,
    @required var alertTitle,
    @required var deleteTxt,
    @required var cancelTxt,
    @required Function deleteClick,
    @required Function cancelClick,
    @required var context}) {
  return showDialog(
    barrierDismissible: false,
    context: context,
    builder: (ctx) => Dialog(
      insetAnimationDuration: const Duration(microseconds: 1000),
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(15.0),
      ),
      backgroundColor: Colors.white,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisSize: MainAxisSize.min,
        children: [
          Padding(
            padding: const EdgeInsets.all(16.0),
            child: Column(
              children: [
                Center(
                  child: Txt(
                    txt: alertTitle,
                    txtColor: MyTheme.redColor,
                    txtSize: MyTheme.txtSize + 1,
                    isBold: true,
                    txtAlign: TextAlign.center,
                  ),
                ),
                SizedBox(height: 20.0),
                Center(
                  child: Txt(
                    txtAlign: TextAlign.center,
                    txt: alertBody,
                    txtColor: Colors.black,
                    txtSize: MyTheme.txtSize,
                    isBold: false,
                  ),
                ),
              ],
            ),
          ),
          SizedBox(height: 15.0),
          Row(
            children: [
              Expanded(
                child: InkWell(
                  onTap: deleteClick,
                  /*() {
                  // Navigator.of(context, rootNavigator: true).pop();

                },*/
                  child: Container(
                    height: 60,
                    decoration: BoxDecoration(
                      color: MyTheme.redColor,
                      borderRadius: BorderRadius.only(
                        bottomLeft: Radius.circular(15.0),
                      ),
                    ),
                    child: Center(
                      child: Txt(
                        txtAlign: TextAlign.center,
                        txt: deleteTxt,
                        txtColor: Colors.white,
                        txtSize: MyTheme.txtSize,
                        isBold: false,
                      ),
                    ),
                  ),
                ),
              ),
              Expanded(
                child: InkWell(
                  onTap: cancelClick,
                  child: Container(
                    height: 60,
                    decoration: BoxDecoration(
                      color: Colors.blue,
                      borderRadius: BorderRadius.only(
                        bottomRight: Radius.circular(15.0),
                      ),
                    ),
                    child: Center(
                      child: Txt(
                        txtAlign: TextAlign.center,
                        txt: cancelTxt,
                        txtColor: Colors.white,
                        txtSize: MyTheme.txtSize,
                        isBold: false,
                      ),
                    ),
                  ),
                ),
              ),
            ],
          ),
        ],
      ),
    ),
  );
}
