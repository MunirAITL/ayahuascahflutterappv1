import 'dart:ui';
import 'package:aitl/config/theme/MyTheme.dart';
import 'package:aitl/view_model/helper/utils/HelpTutHelper.dart';
import 'package:aitl/view_model/observer/StateProvider.dart';
import 'package:aitl/view_model/rx/BotNavController.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:aitl/mixin.dart';
import '../Txt.dart';

class HelpTutDialog extends StatefulWidget {
  @override
  State createState() => _HelpTutDialogState();
}

class _HelpTutDialogState extends State<HelpTutDialog> with Mixin {
  int index = 0;
  final botNavController = Get.put(BotNavController());

  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    HelpTutHelper().listTips = null;
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Dialog(
      backgroundColor: HexColor.fromHex("#61000000").withOpacity(.38),
      child: GestureDetector(
        onTap: () {
          botNavController.isShowHelpDialogExtraHand.value = false;
          if (index == HelpTutHelper().listTips.length - 1) Get.back();
        },
        child: Container(
          child: ListView(
            shrinkWrap: true,
            children: <Widget>[
              index != HelpTutHelper().listTips.length - 1
                  ? Align(
                      alignment: Alignment.topRight,
                      child: IconButton(
                          iconSize: 30,
                          icon: Icon(
                            Icons.close,
                            color: Colors.white,
                          ),
                          onPressed: () {
                            botNavController.isShowHelpDialogExtraHand.value =
                                false;
                            Get.back();
                          }),
                    )
                  : SizedBox(height: 20),
              Padding(
                padding: const EdgeInsets.only(left: 20, right: 20, bottom: 10),
                child: Txt(
                  txt: HelpTutHelper().listTips[index],
                  txtColor: Colors.white,
                  txtSize: MyTheme.txtSize,
                  txtAlign: TextAlign.center,
                  isBold: false,
                  //txtLineSpace: 1.5,
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(10),
                child: Container(
                  decoration: BoxDecoration(
                    color: index != HelpTutHelper().listTips.length - 1
                        ? MyTheme.redColor
                        : MyTheme.brandColor,
                    borderRadius: BorderRadius.circular(20),
                  ),
                  height: getHP(context, MyTheme.btnHpa - 1),
                  width: double.infinity,
                  child: Row(
                    children: [
                      (index == 0 ||
                              index == HelpTutHelper().listTips.length - 1)
                          ? SizedBox(width: 50)
                          : Transform.translate(
                              offset: Offset(0, -10),
                              child: IconButton(
                                iconSize: 50,
                                icon: Icon(
                                  Icons.arrow_left,
                                  color: Colors.white,
                                ),
                                onPressed: () {
                                  setState(() {
                                    index--;
                                    _switchTabsHand(index);
                                  });
                                },
                              ),
                            ),
                      Expanded(
                        child: Txt(
                          txt: index != HelpTutHelper().listTips.length - 1
                              ? "Continue"
                              : "Finish", //HelpTutHelper().listTab[index].toString(),
                          txtColor: Colors.white,
                          txtSize: MyTheme.txtSize,
                          txtAlign: TextAlign.center,
                          isBold: false,
                        ),
                      ),
                      (index == HelpTutHelper().listTips.length - 1)
                          ? SizedBox(width: 50)
                          : Transform.translate(
                              offset: Offset(0, -10),
                              child: IconButton(
                                iconSize: 50,
                                icon: Icon(
                                  Icons.arrow_right,
                                  color: Colors.white,
                                ),
                                onPressed: () {
                                  setState(() {
                                    index++;
                                    _switchTabsHand(index);
                                  });
                                },
                              ),
                            ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  _switchTabsHand(int index2) {
    if (index2 == HelpTutHelper().listTips.length - 1) {
      botNavController.isShowHelpDialogExtraHand.value = true;
      StateProvider().notify(ObserverState.STATE_BOTNAV, null);
    } else {
      botNavController.isShowHelpDialogExtraHand.value = false;
      StateProvider().notify(ObserverState.STATE_RELOAD_TAB, index2);
    }
  }
}
