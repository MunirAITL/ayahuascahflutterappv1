import 'package:flutter/material.dart';
import 'package:aitl/config/theme/MyTheme.dart';
import 'package:responsive_flutter/responsive_flutter.dart';

class Txt extends StatelessWidget {
  final txt;
  Color txtColor;
  double txtSize;
  double txtLineSpace;
  TextAlign txtAlign;
  final TextStyle style;
  final isBold;
  final isOverflow;
  final int maxLines;

  Txt({
    Key key,
    @required this.txt,
    @required this.txtColor,
    @required this.txtSize,
    @required this.txtAlign,
    @required this.isBold,
    this.txtLineSpace,
    this.isOverflow = false,
    this.maxLines,
    this.style,
  }) {
    if (this.txtLineSpace == null) {
      this.txtLineSpace = MyTheme.txtLineSpace;
    }
  }

  @override
  Widget build(BuildContext context) {
    return //FittedBox(
        //fit: BoxFit.fitWidth,
        //child:
        Text(
      txt,
      textAlign: txtAlign,
      maxLines: maxLines,
      overflow: (isOverflow) ? TextOverflow.ellipsis : TextOverflow.visible,
      style: (style != null)
          ? style
          : TextStyle(
              height: txtLineSpace,
              fontSize: ResponsiveFlutter.of(context).fontSize(txtSize),
              color: txtColor,
              fontWeight: (isBold) ? FontWeight.bold : FontWeight.normal,
            ),
      //),
    );
  }
}

extension CustomStyles on TextTheme {
  TextStyle get error {
    return TextStyle(
      fontSize: 20.0,
      color: Colors.black,
      fontWeight: FontWeight.bold,
    );
  }
}
