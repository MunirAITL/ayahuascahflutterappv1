import 'package:aitl/config/theme/MyTheme.dart';
import 'package:aitl/view/widgets/Btn.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:aitl/mixin.dart';

class HorizontalBtns extends StatelessWidget with Mixin {
  final BuildContext context;
  final List<String> listTopBtn;
  final Color bgColor;
  final Color bgColorHighlight;
  final Color txtColor;
  final Color txtColorHighlight;
  Color borderColor;
  final double txtSize;
  double paddingSize;
  double radius;
  int index;
  double fixedWidth;
  double fixedHeight;
  final Function(int) callback;
  HorizontalBtns({
    Key key,
    @required this.listTopBtn,
    @required this.context,
    @required this.bgColor,
    @required this.bgColorHighlight,
    @required this.txtColor,
    @required this.txtColorHighlight,
    @required this.txtSize,
    this.borderColor = Colors.transparent,
    this.paddingSize = 10,
    this.radius = 20,
    this.index = 0,
    this.fixedWidth,
    this.fixedHeight = 4,
    @required this.callback,
  }) {
    indexTopBtn = ValueNotifier<int>(this.index);
  }

  ValueNotifier<int> indexTopBtn;
  @override
  Widget build(BuildContext context) {
    return drawHorizontalButtons();
  }

  drawHorizontalButtons() {
    final listBtn = [];
    for (int i = 0; i < listTopBtn.length; i++) listBtn.add(drawTopBtn(i));
    return Padding(
      padding: EdgeInsets.only(
          left: 5, right: 5, top: paddingSize, bottom: paddingSize),
      child: Container(
        //width: (fixedWidth!=null)?fixedWidth:null,
        alignment: Alignment.center,
        height: getHP(context, fixedHeight),
        color: Colors.transparent,
        child: ListView.separated(
          physics: BouncingScrollPhysics(),
          shrinkWrap: true,
          primary: false,
          scrollDirection: Axis.horizontal,
          itemCount: listBtn.length,
          separatorBuilder: (BuildContext context, int index) =>
              Container(width: 10),
          itemBuilder: (BuildContext context, int index) => listBtn[index],
        ),
      ),
    );
  }

  drawTopBtn(int index) {
    //final w = (getWP(context, 100) / 3) - 20;
    return ValueListenableBuilder(
      valueListenable: indexTopBtn,
      builder: (context, value, child) => Container(
        //width: w,
        width: (fixedWidth != null) ? fixedWidth : null,
        child: Btn(
            txt: listTopBtn[index],
            txtSize: 1.7,
            txtColor: (value == index) ? txtColorHighlight : txtColor,
            bgColor: (value == index) ? bgColorHighlight : bgColor,
            borderColor: (value == index) ? Colors.transparent : borderColor,
            radius: radius,
            isTxtBold: false,
            callback: () {
              indexTopBtn.value = index;
              callback(index);
            }),
      ),
    );
  }
}
