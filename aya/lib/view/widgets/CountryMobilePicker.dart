import 'package:aitl/data/model/country_picker/ola_like_country_picker.dart';
import 'package:aitl/view/widgets/InputBox.dart';
import 'package:flutter/material.dart';

drawCountryMobilePicker({
  BuildContext context,
  TextEditingController mobile,
  String hint,
  bool isTxtCenter = false,
  bool isFocus = false,
  Color labelColor = Colors.black,
  String countryDialCode,
  String countryFlag,
  Function callback,
}) {
  return GestureDetector(
    onTap: () {
      FocusScope.of(context).requestFocus(new FocusNode());
      callback();
    },
    child: Container(
      child: Row(
        children: [
          Flexible(
            child: Container(
              //width: getWP(context, 30),
              color: Colors.grey.withOpacity(.45),
              child: Row(
                children: [
                  SizedBox(width: 5),
                  Image.asset(
                    countryFlag,
                    width: 50,
                    height: 50,
                  ),
                  //SizedBox(width: 3),
                  Expanded(
                    child: Text(
                      "+" + countryDialCode.replaceAll("+", ""),
                      textAlign: TextAlign.center,
                      style: TextStyle(
                        color: Colors.black,
                        fontSize: 20,
                      ),
                    ),
                  ),
                  Icon(
                    Icons.arrow_drop_down,
                    color: Colors.black,
                  )
                ],
              ),
            ),
          ),
          SizedBox(width: 10),
          Expanded(
            flex: 2,
            child: InputBox(
              ctrl: mobile,
              lableTxt: (hint == null) ? "Mobile number" : hint,
              labelColor: labelColor,
              isShowHint: (hint == null) ? false : true,
              autofocus: isFocus,
              kbType: TextInputType.phone,
              align: (isTxtCenter) ? TextAlign.center : TextAlign.left,
              len: 15,
            ),
          ),
        ],
      ),
    ),
  );
}
