import 'package:aitl/config/theme/MyTheme.dart';
import 'package:aitl/view/widgets/Txt.dart';
import 'package:flutter/material.dart';
import 'package:aitl/Mixin.dart';

class BtnOutlineAndBackground extends StatelessWidget with Mixin {
  final String txt;
  final Color txtColor;
  final Color borderColor;
  final Color backgroundColor;
  final Function callback;

  const BtnOutlineAndBackground({
    Key key,
    @required this.txt,
    @required this.txtColor,
    @required this.borderColor,
    @required this.backgroundColor,
    @required this.callback,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: 45,
      height: 45,
      child: Container(
        child: MaterialButton(
          elevation: 0.0,
          child: new Txt(
              txt: txt,
              txtColor: txtColor,
              txtSize: MyTheme.txtSize - .1,
              txtAlign: TextAlign.start,
              isBold: false),

          onPressed: () {
            callback();
          },
          color: backgroundColor,
          shape: new OutlineInputBorder(
            borderRadius: new BorderRadius.circular(10.0),
            borderSide: BorderSide(
                style: BorderStyle.solid, width: 1.0, color: borderColor),
          ),

          //color: Colors.black,
        ),
      ),
    );
  }
}
