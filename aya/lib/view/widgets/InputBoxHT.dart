import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:aitl/config/theme/MyTheme.dart';
import 'package:aitl/mixin.dart';
import 'package:intl/intl.dart';
import 'package:rxdart/rxdart.dart';

enum eCap {
  All,
  Word,
  Sentence,
  None,
}

class InputBoxHT extends StatefulWidget {
  final TextEditingController ctrl;
  final lableTxt, kbType, len, isPwd, minLines, maxLines;
  Widget prefixIco;
  bool autofocus;
  eCap ecap;
  TextAlign txtAlign;
  double txtSize;
  final Function onChange;
  InputBoxHT({
    Key key,
    @required this.ctrl,
    @required this.lableTxt,
    @required this.kbType,
    @required this.len,
    this.minLines,
    this.maxLines,
    this.isPwd = false,
    this.autofocus = false,
    this.ecap = eCap.None,
    this.prefixIco,
    this.txtAlign = TextAlign.start,
    this.txtSize,
    this.onChange,
  }) {
    if (txtSize == null) {
      txtSize = MyTheme.txtSize;
    }
  }

  @override
  _InputBoxHTState createState() => _InputBoxHTState();
}

class _InputBoxHTState extends State<InputBoxHT> with Mixin {
  //final subject = new PublishSubject<String>();

  @override
  void initState() {
    super.initState();

    /*subject.stream
        .debounce((_) => TimerStream(true, Duration.zero))
        .listen(_loadNewData);*/
  }

  void _loadNewData(String newData) {
    //do update here
    switch (widget.ecap) {
      case eCap.Sentence:
        /*getChar(widget.ctrl.text, newData);
        widget.ctrl.selection = TextSelection.fromPosition(
          TextPosition(offset: widget.ctrl.text.length),
        );*/
        break;
      default:
    }
  }

  //@mustCallSuper
  @override
  void dispose() {
    //subject.close();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      //color: Colors.black,
      child: TextFormField(
        controller: widget.ctrl,
        autofocus: widget.autofocus,
        keyboardType: widget.kbType,
        minLines: widget.minLines,
        maxLines: widget.maxLines,
        /*onChanged: (widget.ecap != eCap.None)
            ? (string) => (subject.add(string))
            : widget.onChange,*/
        onChanged: widget.onChange,
        //textCapitalization: TextCapitalization.sentences,
        //textCapitalization: TextCapitalization.sentences,
        /*inputFormatters: (widget.kbType == TextInputType.phone)
            ? <TextInputFormatter>[FilteringTextInputFormatter.digitsOnly]
            : (widget.kbType == TextInputType.emailAddress)
                ? [
                    FilteringTextInputFormatter.allow(RegExp(
                        "^[a-zA-Z0-9_.+-]*(@([a-zA-Z0-9-.]*(\\.[a-zA-Z0-9-]*)?)?)?")),
                  ]
                : null,*/
        inputFormatters: (widget.kbType == TextInputType.phone)
            ? <TextInputFormatter>[FilteringTextInputFormatter.digitsOnly]
            : (widget.kbType == TextInputType.emailAddress)
                ? [
                    FilteringTextInputFormatter.allow(RegExp(
                        "^[a-zA-Z0-9_.+-]*(@([a-zA-Z0-9-.]*(\\.[a-zA-Z0-9-]*)?)?)?")),
                  ]
                : (widget.ecap != eCap.None)
                    ? [
                        (widget.ecap == eCap.All)
                            ? AllUpperCaseTextFormatter()
                            : (widget.ecap == eCap.Sentence)
                                ? FirstUpperCaseTextFormatter()
                                : WordsUpperCaseTextFormatter()
                      ]
                    : null,
        obscureText: widget.isPwd,
        maxLength: widget.len,
        autocorrect: false,
        enableSuggestions: false,
        textAlign: widget.txtAlign,
        style: TextStyle(
          color: Colors.black,
          fontSize: getTxtSize(context: context, txtSize: widget.txtSize),
          height: MyTheme.txtLineSpace,
        ),
        decoration: new InputDecoration(
          //contentPadding: const EdgeInsets.symmetric(vertical: 20.0),
          prefixIcon: (widget.prefixIco != null)
              ? Padding(
                  padding: const EdgeInsets.all(10),
                  child: widget.prefixIco,
                )
              : null,
          counterText: "",
          filled: true,
          fillColor: Colors.white,
          hintText: widget.lableTxt,
          hintStyle: new TextStyle(
            color: MyTheme.lgreyColor,
            fontSize: getTxtSize(context: context, txtSize: widget.txtSize),
            height: MyTheme.txtLineSpace,
          ),
          labelStyle: new TextStyle(
            color: Colors.black,
            fontSize: getTxtSize(context: context, txtSize: widget.txtSize),
            height: MyTheme.txtLineSpace,
          ),
          contentPadding:
              EdgeInsets.only(left: 10, right: 10, top: 10, bottom: 10),
          enabledBorder: OutlineInputBorder(
            borderSide: BorderSide(color: MyTheme.lgreyColor),
            borderRadius: const BorderRadius.all(
              const Radius.circular(10.0),
            ),
          ),
          focusedBorder: OutlineInputBorder(
            borderSide: BorderSide(color: Colors.black, width: 1),
            borderRadius: const BorderRadius.all(
              const Radius.circular(10.0),
            ),
          ),
          border: OutlineInputBorder(
            borderSide: BorderSide(color: MyTheme.lgreyColor, width: 1),
            borderRadius: const BorderRadius.all(
              const Radius.circular(10.0),
            ),
          ),
        ),
      ),
    );
  }
}

class AllUpperCaseTextFormatter extends TextInputFormatter {
  @override
  TextEditingValue formatEditUpdate(
      TextEditingValue oldValue, TextEditingValue newValue) {
    return newValue.copyWith(text: newValue.text.allInCaps);
  }
}

class FirstUpperCaseTextFormatter extends TextInputFormatter {
  /*@override
  TextEditingValue formatEditUpdate(
      TextEditingValue oldValue, TextEditingValue newValue) {
    var newText = toBeginningOfSentenceCase(newValue.text.inCaps);
    return newValue.copyWith(text: newText);
  }*/
  @override
  TextEditingValue formatEditUpdate(
      TextEditingValue oldValue, TextEditingValue newValue) {
    return newValue.copyWith(text: newValue.text.capitalizeFirstofEachDot);
  }
}

class WordsUpperCaseTextFormatter extends TextInputFormatter {
  @override
  TextEditingValue formatEditUpdate(
      TextEditingValue oldValue, TextEditingValue newValue) {
    return newValue.copyWith(text: newValue.text.capitalizeFirstofEach2);
  }
}

extension CapExtension on String {
  String get capitalizeFirstofEach2 =>
      this.split(" ").map((str) => str.inCaps).join(" ");
}

extension SentenceExtension on String {
  String get capitalizeFirstofEachDot =>
      this.split(". ").map((str) => str.inCaps).join(". ");
}
