import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:aitl/config/theme/MyTheme.dart';
import 'package:aitl/mixin.dart';

import 'InputBoxHT.dart';

class InputBox extends StatelessWidget with Mixin {
  final ctrl, lableTxt, kbType, len, isPwd, labelColor, align, isShowHint;
  final prefixIcon, suffixIcon;
  bool autofocus;
  bool isFirstLetterCap;
  eCap ecap;
  double borderLineWidth;
  InputBox({
    Key key,
    @required this.ctrl,
    @required this.lableTxt,
    @required this.kbType,
    @required this.len,
    this.isPwd = false,
    this.autofocus = false,
    this.labelColor = Colors.black,
    this.align = TextAlign.left,
    this.isShowHint = false,
    this.isFirstLetterCap = false,
    this.prefixIcon,
    this.suffixIcon,
    this.ecap = eCap.None,
    this.borderLineWidth = 1,
  }) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return TextField(
      controller: ctrl,
      autofocus: autofocus,
      keyboardType: kbType,
      //textCapitalization: TextCapitalization.sentences,
      inputFormatters: (kbType == TextInputType.phone)
          ? <TextInputFormatter>[FilteringTextInputFormatter.digitsOnly]
          : (kbType == TextInputType.emailAddress)
              ? [
                  FilteringTextInputFormatter.allow(RegExp(
                      "^[a-zA-Z0-9_.+-]*(@([a-zA-Z0-9-.]*(\\.[a-zA-Z0-9-]*)?)?)?")),
                ]
              : (ecap != eCap.None)
                  ? [
                      (ecap == eCap.All)
                          ? AllUpperCaseTextFormatter()
                          : (ecap == eCap.Sentence)
                              ? FirstUpperCaseTextFormatter()
                              : WordsUpperCaseTextFormatter()
                    ]
                  : null,
      obscureText: isPwd,
      maxLength: len,
      autocorrect: false,
      enableSuggestions: false,
      textAlign: align,
      style: TextStyle(
        color: Colors.black,
        fontSize: getTxtSize(context: context, txtSize: MyTheme.txtSize),
        height: MyTheme.txtLineSpace,
      ),
      decoration: new InputDecoration(
        hintText: (isShowHint) ? lableTxt : '',
        labelText: (!isShowHint) ? lableTxt : '',
        prefixIcon: (prefixIcon != null) ? prefixIcon : null,
        suffixIcon: (suffixIcon != null) ? suffixIcon : null,
        hintStyle: new TextStyle(
          color: labelColor,
          fontSize: getTxtSize(context: context, txtSize: MyTheme.txtSize),
          height: MyTheme.txtLineSpace,
        ),
        labelStyle: new TextStyle(
          color: labelColor,
          fontSize: getTxtSize(context: context, txtSize: MyTheme.txtSize),
          height: MyTheme.txtLineSpace,
        ),
        //contentPadding: EdgeInsets.only(left: 20, right: 20),
        enabledBorder: UnderlineInputBorder(
          borderSide: BorderSide(
            color: labelColor,
            width: borderLineWidth,
          ),
        ),
        focusedBorder: UnderlineInputBorder(
          borderSide: BorderSide(
            color: MyTheme.blueColor,
            width: borderLineWidth,
          ),
        ),
        border: UnderlineInputBorder(
          borderSide: BorderSide(
            color: Colors.black,
            width: borderLineWidth,
          ),
        ),
      ),
    );
  }
}

class _UpperCaseTextFormatter extends TextInputFormatter {
  @override
  TextEditingValue formatEditUpdate(
      TextEditingValue oldValue, TextEditingValue newValue) {
    return TextEditingValue(
      text: newValue.text.toUpperCase(),
      selection: newValue.selection,
    );
  }
}
