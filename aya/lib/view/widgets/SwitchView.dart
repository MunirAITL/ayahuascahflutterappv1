import 'package:aitl/config/theme/MyTheme.dart';
import 'package:aitl/view/widgets/Txt.dart';
import 'package:flutter/material.dart';
import 'package:aitl/mixin.dart';

class SwitchView extends StatefulWidget {
  final onTxt, offTxt;
  final bool value;
  Color txtColorOn;
  Color txtColorOff;
  Color bgColorOn;
  Color bgColorOff;
  final double width;
  final double height;
  final ValueChanged<bool> onChanged;

  SwitchView({
    Key key,
    @required this.onTxt,
    @required this.offTxt,
    @required this.value,
    @required this.onChanged,
    this.txtColorOn,
    this.txtColorOff,
    this.bgColorOn,
    this.bgColorOff,
    this.width,
    this.height,
  }) {
    if (this.bgColorOn == null) bgColorOn = MyTheme.brandColor;
    if (this.bgColorOff == null) bgColorOff = Colors.white;
    if (this.txtColorOff == null) txtColorOff = MyTheme.brandColor;
    if (this.bgColorOn == null) bgColorOn = Colors.white;
  }

  @override
  State createState() => _SwitchViewState();
}

class _SwitchViewState extends State<SwitchView> with Mixin {
  @override
  Widget build(BuildContext context) {
    return Container(
      //width: (widget.width != null) ? getWP(context, widget.width) : null,
      height: getHP(context, MyTheme.switchBtnHpa),
      alignment: Alignment.center,
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Flexible(
            child: Container(
              decoration: new BoxDecoration(
                  color: (widget.value) ? widget.bgColorOn : widget.bgColorOff,
                  border: Border.all(
                      color: (!widget.value)
                          ? widget.bgColorOn
                          : widget.bgColorOff,
                      width: .5),
                  borderRadius: new BorderRadius.only(
                    topLeft: const Radius.circular(20.0),
                    bottomLeft: const Radius.circular(20.0),
                  )),
              child: MaterialButton(
                child: new Txt(
                    txt: widget.onTxt,
                    txtColor:
                        (widget.value) ? widget.txtColorOn : widget.txtColorOff,
                    txtSize: MyTheme.txtSize - 0.2,
                    txtAlign: TextAlign.center,
                    isBold: false),
                onPressed: () async {
                  widget.onChanged(true);
                },
              ),
            ),
          ),
          Flexible(
            child: Container(
              decoration: new BoxDecoration(
                  color: (!widget.value) ? widget.bgColorOn : widget.bgColorOff,
                  border: Border.all(
                      color:
                          (widget.value) ? widget.bgColorOn : widget.bgColorOff,
                      width: .5),
                  borderRadius: new BorderRadius.only(
                    topRight: const Radius.circular(20.0),
                    bottomRight: const Radius.circular(20.0),
                  )),
              child: MaterialButton(
                child: new Txt(
                    txt: widget.offTxt,
                    txtColor: (!widget.value)
                        ? widget.txtColorOn
                        : widget.txtColorOff,
                    txtSize: MyTheme.txtSize - 0.2,
                    txtAlign: TextAlign.center,
                    isBold: false),
                onPressed: () async {
                  widget.onChanged(false);
                },
              ),
            ),
          ),
        ],
      ),
    );
  }
}
