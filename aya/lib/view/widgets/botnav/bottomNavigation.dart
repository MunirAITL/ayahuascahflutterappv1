import 'package:aitl/config/theme/MyTheme.dart';
import 'package:aitl/view/dashboard/dashboard_page.dart';
import 'package:aitl/view/widgets/Txt.dart';
import 'package:aitl/view_model/observer/StateProvider.dart';
import 'package:aitl/view_model/rx/BotNavController.dart';
import 'package:flutter/material.dart';
import 'package:aitl/Mixin.dart';
import 'package:get/get.dart';
import 'tabItem.dart';
import 'package:badges/badges.dart';

class BottomNavigation extends StatefulWidget {
  final BuildContext context;
  final ValueChanged<int> onSelectTab;
  final botNavController;
  final List<TabItem> tabs;
  final bool isHelpTut;
  final int totalMsg;
  final int totalNoti;
  BottomNavigation({
    @required this.context,
    @required this.onSelectTab,
    @required this.botNavController,
    @required this.tabs,
    @required this.isHelpTut,
    @required this.totalMsg,
    @required this.totalNoti,
  });

  @override
  State createState() => _BottomNavigationState();
}

class _BottomNavigationState extends State<BottomNavigation>
    with Mixin, StateListener {
  int curTab = 0;

  StateProvider _stateProvider;
  @override
  onStateChanged(ObserverState state, dynamic data) {
    try {
      if (state == ObserverState.STATE_BOTNAV) {
        setState(() {});
      }
    } catch (e) {}
  }

  @override
  void initState() {
    super.initState();
    appInit();
  }

  //@mustCallSuper
  @override
  void dispose() {
    try {
      _stateProvider.unsubscribe(this);
      _stateProvider = null;
    } catch (e) {
      log(e.toString());
    }
    super.dispose();
  }

  appInit() async {
    try {
      _stateProvider = new StateProvider();
      _stateProvider.subscribe(this);
    } catch (e) {}
  }

  @override
  Widget build(BuildContext context) {
    curTab = widget.botNavController.index.value;

    return BottomAppBar(
      color: MyTheme.bgColor,
      shape: CircularNotchedRectangle(),
      //notchMargin: 4,
      //clipBehavior: Clip.antiAlias,
      child: Container(
        decoration: BoxDecoration(
            color: Colors.white,
            border: Border(top: BorderSide(color: Colors.black, width: .5))),
        child: BottomNavigationBar(
          //selectedLabelStyle: TextStyle(fontSize: 14),
          selectedItemColor: MyTheme.redColor,
          //unselectedLabelStyle: TextStyle(fontSize: 14),
          elevation: MyTheme.botbarElevation,
          unselectedItemColor: Colors.black,
          backgroundColor: Colors.white,
          type: BottomNavigationBarType.fixed,
          items: widget.tabs
              .map(
                (e) => _buildItem(
                  index: e.getIndex(),
                  icon: e.icon,
                  tabName: e.tabName,
                ),
              )
              .toList(),
          onTap: (index) => widget.onSelectTab(
            index,
          ),
        ),
      ),
    );
  }

  BottomNavigationBarItem _buildItem(
      {int index, AssetImage icon, String tabName}) {
    final topBadgePos = getHP(widget.context, 4);
    final endBadgePos = getHP(widget.context, 2);
    final int totalBadge = (index == 2 && widget.totalMsg > 0)
        ? widget.totalMsg
        : (index == 3 && widget.totalNoti > 0)
            ? widget.totalNoti
            : 0;
    if (widget.botNavController.isShowHelpDialogExtraHand.value) curTab = 2;
    return BottomNavigationBarItem(
      icon: (widget.isHelpTut && curTab == index)
          ? Stack(clipBehavior: Clip.none, children: <Widget>[
              (index == 2 || index == 3)
                  ? Badge(
                      showBadge: (totalBadge > 0) ? true : false,
                      position: BadgePosition.topEnd(
                          top: -topBadgePos, end: -endBadgePos),
                      badgeContent: Text(
                        totalBadge.toString(),
                        style: TextStyle(
                          color: Colors.white,
                        ),
                      ),
                      child: ImageIcon(
                        icon,
                        color: _tabColor(index: index),
                      ),
                    )
                  : ImageIcon(
                      icon,
                      color: _tabColor(index: index),
                    ),
              new Positioned(
                top: (widget.botNavController.isShowHelpDialogExtraHand.value)
                    ? -getHP(widget.context, 29)
                    : -getHP(widget.context, 9),
                right: -10,
                child: Image.asset(
                  "assets/images/icons/hand_" +
                      ((widget.botNavController.isShowHelpDialogExtraHand.value)
                          ? 'up'
                          : 'down') +
                      ".png",
                  width: 60,
                  height: 70,
                ),
              )
            ])
          : Badge(
              showBadge: (totalBadge > 0) ? true : false,
              position:
                  BadgePosition.topEnd(top: -topBadgePos, end: -endBadgePos),
              badgeContent: Text(
                totalBadge.toString(),
                style: TextStyle(
                  color: Colors.white,
                ),
              ),
              child: ImageIcon(
                icon,
                color: _tabColor(index: index),
              ),
            ),
      // ignore: deprecated_member_use
      title: Txt(
          txt: tabName,
          txtColor: _tabColor(index: index),
          txtSize: MyTheme.txtSize - .9,
          txtAlign: TextAlign.center,
          isBold: (curTab == index) ? true : false),
    );
  }

  Color _tabColor({int index}) {
    return curTab == index ? MyTheme.redColor : Colors.black;
  }
}
