import 'package:aitl/config/server/ResCfg.dart';
import 'package:aitl/config/theme/MyTheme.dart';
import 'package:aitl/data/model/dashboard/more/res/ResAPIModel.dart';
import 'package:aitl/data/network/ModelMgr.dart';
import 'package:aitl/data/network/NetworkMgr.dart';
import 'package:aitl/view/auth/auth_base.dart';
import 'package:aitl/view/auth/profile_type_page.dart';
import 'package:aitl/view/widgets/BottomBtn.dart';
import 'package:aitl/view/widgets/InputBox.dart';
import 'package:aitl/view/widgets/InputBoxHT.dart';
import 'package:aitl/view/widgets/Txt.dart';
import 'package:aitl/view/widgets/Txt.dart';
import 'package:aitl/view_model/api/api_view_model.dart';
import 'package:aitl/view_model/observer/APIStateProvider.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import '../dashboard_base.dart';

class MainPage extends StatefulWidget {
  const MainPage({Key key}) : super(key: key);
  @override
  State createState() => _MainPageState();
}

class _MainPageState extends BaseDashboard<MainPage> {
  final listItems = [
    {
      'title': "Knowledge Center",
      'desc':
          "Learn about the Holy Medicine of the Holy and Divine Mother Ayahuasca",
      'icon': Image.asset(
        "assets/images/db/db_knowledge_center2.png",
        fit: BoxFit.fill,
      ),
    },
    {
      'title': "Search Retreats",
      'desc': "Find Retreats and Sacred Ceremonies available",
      'icon': Image.asset(
        "assets/images/db/db_search_retreats2.png",
        fit: BoxFit.fill,
      ),
    },
    {
      'title': "Find New Friends",
      'desc': "Find like-minded members",
      'icon': Image.asset(
        "assets/images/db/db_find_new_f2.png",
        fit: BoxFit.fill,
      ),
    },
    {
      'title': "Discussion Board",
      'desc': "Join Discussion, share ideas, and questions and more.",
      'icon': Image.asset(
        "assets/images/db/db_dis_board2.png",
        fit: BoxFit.fill,
      ),
    },
    {
      'title': "Visit Shop",
      'desc': "Buy consciously made simple living products",
      'icon': Image.asset(
        "assets/images/db/db_visit_shop2.png",
        fit: BoxFit.fill,
      ),
    },
  ];

  //  **************  app states start

  @override
  void onDetached() {
    try {
      log("app state = onDetached");
    } catch (e) {}
  }

  @override
  void onInactive() {
    try {
      log("app state = onInactive");
    } catch (e) {}
  }

  @override
  void onPaused() {
    try {
      log("app state = onPaused");
    } catch (e) {}
  }

  @override
  void onResumed() {
    try {
      log("app state = onResumed");
    } catch (e) {}
  }

  //  **************  app states end

  @override
  void initState() {
    super.initState();
    appInit();
  }

  @override
  void dispose() {
    super.dispose();
  }

  appInit() {
    try {} catch (e) {}
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: MyTheme.bgColor,
        appBar: AppBar(
          iconTheme: IconThemeData(color: Colors.black //change your color here
              ),
          elevation: MyTheme.appbarElevation,
          backgroundColor: MyTheme.bgColor,
          title: Txt(
              txt: "Dashboard",
              txtColor: Colors.black,
              txtSize: MyTheme.appbarTitleFontSize,
              txtAlign: TextAlign.start,
              isBold: false),
          centerTitle: true,
          actions: <Widget>[
            IconButton(
                icon: Image.asset("assets/images/icons/ico_help.png"),
                onPressed: () {}),
          ],
        ),
        body: GestureDetector(
          onTap: () {
            FocusScope.of(context).requestFocus(new FocusNode());
          },
          child: drawLayout(),
        ),
      ),
    );
  }

  drawLayout() {
    return Container(
      child: GridView.count(
          crossAxisCount: 2,
          crossAxisSpacing: 15,
          mainAxisSpacing: 15,
          padding: EdgeInsets.all(20),
          children: List.generate(listItems.length, (index) {
            final map = listItems[index];
            return Card(
              elevation: 10,
              shadowColor: Colors.black38,
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(15.0),
              ),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Container(
                    width: double.infinity,
                    decoration: new BoxDecoration(
                        color: MyTheme.pinkColor,
                        borderRadius: new BorderRadius.only(
                          topLeft: const Radius.circular(15),
                          topRight: const Radius.circular(15),
                        )),
                    child: map['icon'],
                  ),
                  Padding(
                    padding:
                        const EdgeInsets.only(left: 10, right: 10, top: 10),
                    child: Txt(
                        txt: map['title'],
                        txtColor: Colors.black,
                        txtSize: MyTheme.txtSize,
                        txtAlign: TextAlign.start,
                        isBold: false),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 10, right: 10),
                    child: Txt(
                        txt: map['desc'],
                        txtColor: Colors.black38,
                        txtSize: MyTheme.txtSize - .4,
                        txtAlign: TextAlign.start,
                        maxLines: 3,
                        isBold: false),
                  ),
                ],
              ),
            );
          })),
    );
  }
}
