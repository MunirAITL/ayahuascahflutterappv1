import 'package:aitl/config/app/status/NotiStatusCfg.dart';
import 'package:aitl/config/cfg/AppConfig.dart';
import 'package:aitl/config/server/APIMyTasksCfg.dart';
import 'package:aitl/config/server/ServerUrls.dart';
import 'package:aitl/config/theme/MyTheme.dart';
import 'package:aitl/controller/classes/DateFun.dart';
import 'package:aitl/data/model/dashboard/more/noti/NotiModel.dart';
import 'package:aitl/data/model/dashboard/more/profile/UserRatingSummaryDataModel.dart';
import 'package:aitl/data/network/NetworkMgr.dart';
import 'package:aitl/view/dashboard/dashboard_base.dart';
import 'package:aitl/view/dashboard/more/noti/noti_page.dart';
import 'package:aitl/view/widgets/MyNetworkImage.dart';
import 'package:aitl/view/widgets/ToggleSwitch.dart';
import 'package:aitl/view/widgets/Txt.dart';
import 'package:aitl/view/widgets/webview/WebScreen.dart';
import 'package:aitl/view_model/api/api_view_model.dart';
import 'package:aitl/view_model/helper/utils/UIHelper.dart';
import 'package:aitl/view_model/rx/ProfileController.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

abstract class BaseDashboardMoreStatefull<T extends StatefulWidget>
    extends BaseDashboard<T> {
  final profileController = Get.put(ProfileController());
  int posterSwitchValue = 0;
  bool isTasker = true;

  drawUserSwitchView() {
    return Center(
      child: Padding(
        padding: const EdgeInsets.only(top: 20),
        child: ToggleSwitch(
          isBorderColor: true,
          minWidth: getWP(context, 35),
          minHeight: getHP(context, MyTheme.switchBtnHpa),
          initialLabelIndex: posterSwitchValue,
          cornerRadius: 50.0,
          fontSize: 20,
          activeBgColor: MyTheme.heroTheamColors,
          activeFgColor: Colors.white,
          inactiveBgColor: Colors.white,
          inactiveFgColor: MyTheme.heroTheamColors,
          labels: ['As a Tasker', 'As a Poster'],
          //icons: [FontAwesomeIcons.mars, FontAwesomeIcons.venus],
          onToggle: (index) {
            posterSwitchValue = index;
            isTasker = (posterSwitchValue == 0) ? true : false;
            setState(() {});
          },
        ),
      ),
    );
  }
}
