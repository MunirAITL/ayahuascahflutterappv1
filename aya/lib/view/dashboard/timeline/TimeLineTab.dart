import 'package:aitl/config/cfg/AppConfig.dart';
import 'package:aitl/config/cfg/NewCaseCfg.dart';
import 'package:aitl/config/server/APITimelineCfg.dart';
import 'package:aitl/config/server/ServerUrls.dart';
import 'package:aitl/config/theme/MyTheme.dart';
import 'package:aitl/controller/classes/Common.dart';
import 'package:aitl/controller/classes/DateFun.dart';
import 'package:aitl/data/model/dashboard/timeline/TimelineUserModel.dart';
import 'package:aitl/data/network/NetworkMgr.dart';
import 'package:aitl/view/dashboard/timeline/MessageTypeList.dart';
import 'package:aitl/view/widgets/AppbarBotProgbar.dart';
import 'package:aitl/view/widgets/MyNetworkImage.dart';
import 'package:aitl/view/widgets/Txt.dart';
import 'package:aitl/Mixin.dart';
import 'package:aitl/view_model/helper/timeline/TimelineAPIMgr.dart';
import 'package:aitl/view_model/helper/utils/UIHelper.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';

class TimeLineTab extends StatefulWidget {
  @override
  State createState() => _TimeLineTabState();
}

class _TimeLineTabState extends State<TimeLineTab> with Mixin {
  List<TimelineUserModel> listTimelineUserModel = [];

  //  page stuff start here
  bool isPageDone = false;
  bool isLoading = false;
  int pageStart = 0;
  int pageCount = AppConfig.page_limit;

  int caseStatus = NewCaseCfg.ALL;

  onPageLoad() async {
    try {
      setState(() {
        isLoading = true;
      });
      TimelineAPIMgr().wsOnPageLoad(
        context: context,
        pageStart: pageStart,
        pageCount: pageCount,
        status: caseStatus,
        callback: (model) {
          if (model != null && mounted) {
            try {
              if (model.success) {
                try {
                  final List<dynamic> timelineUserModel =
                      model.responseData.users;
                  if (timelineUserModel != null && mounted) {
                    //  checking to see whether page is finished to stop on reload data through API after end of scrolling for scalibility
                    if (timelineUserModel.length != pageCount) {
                      isPageDone = true;
                    }
                    try {
                      for (TimelineUserModel user in timelineUserModel) {
                        listTimelineUserModel.add(user);
                      }
                    } catch (e) {
                      log(e.toString());
                    }
                    log("time line adviser list =" +
                        listTimelineUserModel.length.toString());
                    log("time line adviser list =" +
                        listTimelineUserModel.toString());
                    if (mounted) {
                      setState(() {
                        isLoading = false;
                      });
                    }
                  } else {
                    if (mounted) {
                      setState(() {
                        isLoading = false;
                      });
                    }
                  }
                } catch (e) {
                  log(e.toString());
                }
              } else {
                try {
                  //final err = model.errorMessages.login[0].toString();
                  if (mounted) {
                    showAlert(msg: "Timelines not found", isToast: true);
                  }
                } catch (e) {
                  log(e.toString());
                  if (mounted) {
                    setState(() {
                      isLoading = false;
                    });
                  }
                }
              }
            } catch (e) {
              log(e.toString());
              if (mounted) {
                setState(() {
                  isLoading = false;
                });
              }
            }
          } else {
            log("Time line tab screen not in");
            if (mounted) {
              setState(() {
                isLoading = false;
              });
            }
          }
        },
      );
    } catch (e) {
      log(e.toString());
      if (mounted) {
        setState(() {
          isLoading = false;
        });
      }
    }
  }

  Future<void> _getRefreshData() async {
    pageStart = 0;
    isPageDone = false;
    isLoading = true;
    listTimelineUserModel.clear();
    onPageLoad();
  }

  @override
  void initState() {
    super.initState();
    try {
      appInit();
    } catch (e) {}
  }

  @override
  void dispose() {
    listTimelineUserModel = null;

    try {
      NetworkMgr().dispose();
    } catch (e) {}
    super.dispose();
  }

  appInit() async {
    try {
      _getRefreshData();
    } catch (e) {}
  }

  drawAppbarNavBar() {
    return PreferredSize(
      preferredSize: Size.fromHeight(AppConfig.private_msg_height),
      child: Container(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            GestureDetector(
              onTap: () {
                //Get.to(() => ChatPage(isSupport: true));
              },
              child: Container(
                color: Colors.grey,
                child: Padding(
                  padding: const EdgeInsets.only(left: 5, top: 10, bottom: 5),
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Container(
                        width: 50,
                        height: 50,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.all(Radius.circular(50)),
                          color: Colors.white,
                        ),
                        child: Center(
                          child: Container(
                            width: 30,
                            height: 30,
                            child: SvgPicture.asset(
                                'assets/images/logo/logo2.svg',
                                fit: BoxFit.cover),
                          ),
                        ),
                      ),
                      SizedBox(width: 20),
                      Center(
                        child: Txt(
                            txt: "Message Support",
                            txtColor: Colors.white,
                            txtSize: MyTheme.txtSize,
                            txtAlign: TextAlign.center,
                            isBold: false),
                      ),
                    ],
                  ),
                ),
              ),
            ),
            (isLoading)
                ? AppbarBotProgBar(
                    backgroundColor: MyTheme.appbarProgColor,
                  )
                : Container(
                    height: 4,
                    color: Colors.grey,
                  )
          ],
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: MyTheme.themeData.accentColor,
        appBar: AppBar(
          centerTitle: false,
          elevation: MyTheme.appbarElevation,
          title: Txt(
              txt: "Private Message",
              txtColor: MyTheme.greenColor,
              txtSize: MyTheme.appbarTitleFontSize,
              txtAlign: TextAlign.start,
              isBold: true),
          actions: <Widget>[
            IconButton(
              iconSize: 30,
              icon: Image.asset("assets/images/icons/ico_add.png"),
              onPressed: () {},
            )
          ],
          iconTheme: IconThemeData(color: MyTheme.redColor),
          backgroundColor: MyTheme.themeData.accentColor,
          bottom: PreferredSize(
            preferredSize: new Size(getW(context), getHP(context, 9)),
            child: Column(
              children: [
                drawAppbarNavBar(),
                (isLoading)
                    ? AppbarBotProgBar(
                        backgroundColor: MyTheme.appbarProgColor,
                      )
                    : SizedBox(),
              ],
            ),
          ),
        ),
        body: GestureDetector(
          behavior: HitTestBehavior.opaque,
          onPanDown: (detail) {
            FocusScope.of(context).requestFocus(new FocusNode());
          },
          onTap: () {
            FocusScope.of(context).requestFocus(new FocusNode());
          },
          child: drawLayout(),
        ),
      ),
    );
  }

  drawLayout() {
    return Container(
      child: (listTimelineUserModel.length > 0)
          ? NotificationListener(
              onNotification: (scrollNotification) {
                if (scrollNotification is ScrollStartNotification) {
                  //print('Widget has started scrolling');
                } else if (scrollNotification is ScrollEndNotification) {
                  if (!isPageDone) {
                    pageStart++;
                    onPageLoad();
                  }
                }
                return true;
              },
              child: Theme(
                data: MyTheme.refreshIndicatorTheme,
                child: RefreshIndicator(
                  onRefresh: _getRefreshData,
                  child: Padding(
                    padding: const EdgeInsets.only(
                        left: 5, right: 5, top: 10, bottom: 10),
                    child: ListView.builder(
                      addAutomaticKeepAlives: true,
                      cacheExtent: AppConfig.page_limit.toDouble(),
                      scrollDirection: Axis.vertical,
                      shrinkWrap: true,
                      itemCount: listTimelineUserModel.length,
                      itemBuilder: (context, index) {
                        return drawRecentCaseItem(index);
                      },
                    ),
                  ),
                ),
              ),
            )
          : (!isLoading)
              ? Center(
                  child: Padding(
                    padding: const EdgeInsets.only(left: 50, right: 50),
                    child: ListView(
                      //mainAxisAlignment: MainAxisAlignment.start,
                      shrinkWrap: true,
                      children: [
                        Container(
                          width: getWP(context, 100),
                          height: getHP(context, 48),
                          child: Image.asset(
                            'assets/images/screens/db_cus/my_cases/case_nf.png',
                            fit: BoxFit.fill,
                          ),
                        ),
                        //SizedBox(height: 40),
                        Padding(
                          padding: const EdgeInsets.only(left: 10, right: 10),
                          child: Container(
                            width: getWP(context, 70),
                            child: Txt(
                              txt: "You have no message yet",
                              txtColor: MyTheme.mycasesNFBtnColor,
                              txtSize: MyTheme.txtSize + .2,
                              txtAlign: TextAlign.center,
                              isBold: false,
                              //txtLineSpace: 1.5,
                            ),
                          ),
                        ),
                        /*  Padding(
                          padding: const EdgeInsets.only(left: 10, right: 10),
                          child: Container(
                            width: getWP(context, 70),
                            child: Txt(
                              txt: "Looks like you haven't got any Adviser yet",
                              txtColor: MyTheme.mycasesNFBtnColor,
                              txtSize: MyTheme.txtSize + .2,
                              txtAlign: TextAlign.center,
                              isBold: false,
                              //txtLineSpace: 1.5,
                            ),
                          ),
                        ),
                        SizedBox(height: 20),
                        Padding(
                          padding: const EdgeInsets.only(left: 20, right: 20),
                          child: MMBtn(
                            txt: "Refresh",
                            bgColor: MyTheme.redColor,
                            width: getWP(context, 50),
                            height: getHP(context, 8),
                            callback: () {
                              onPageLoad();
                            },
                          ),
                        ),*/
                      ],
                    ),
                  ),
                )
              : SizedBox(),
    );
  }

  drawRecentCaseItem(index) {
    try {
      TimelineUserModel timelineUserModel = listTimelineUserModel[index];
      final unreadMessageCount = (timelineUserModel.unreadMessageCount == 0);
      final doubletik =
          (unreadMessageCount) ? "check2_icon.png" : "check1_icon.png";
      String lastLoginDate = timelineUserModel.lastChatDateTime ?? '';
      bool isHideFieldsIfLessByYear = false;
      try {
        DateTime checkedTime = DateTime.parse(lastLoginDate);
        DateTime currentTime = DateTime.now();
        final diff = currentTime.difference(checkedTime);
        var year = ((diff.inDays) / 365).round();
        if (year > APITimelineCfg.hideFieldsIfLessByYear) {
          isHideFieldsIfLessByYear = true;
        }
      } catch (e) {}
      try {
        lastLoginDate = DateFun.getTimeByDate(lastLoginDate);
      } catch (e) {}
      return GestureDetector(
        onTap: () async {
          try {
            Get.to(
              () => MessageTypeList(
                title: timelineUserModel.name,
                online: timelineUserModel.isOnline,
                adviserOrIntroducerId: timelineUserModel.id,
                pictureUrl: timelineUserModel.profileImageUrl,
              ),
            ).then((value) {
              //callback(route);
            });
            // navTo(
            //   context: context,
            //   page: () => TaskBiddingScreen(
            //       title: timelineUserModel.name,
            //       // taskId: 74119 /*--locationsModel.id*/),
            //       taskId: timelineUserModel.id),
            // ).then((value) {
            //   //callback(route);
            // });
          } catch (e) {
            log(e.toString());
          }
        },
        child: Card(
          elevation: 0,
          //color: Colors.black,
          child: Padding(
            padding: const EdgeInsets.only(left: 0.0, right: 10),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    Container(
                      decoration: BoxDecoration(shape: BoxShape.circle),
                      child: CircleAvatar(
                        radius: 30,
                        backgroundColor: Colors.transparent,
                        backgroundImage: new CachedNetworkImageProvider(
                          MyNetworkImage.checkUrl(
                              (timelineUserModel.profileImageUrl != null)
                                  ? timelineUserModel.profileImageUrl
                                  : ServerUrls.MISSING_IMG),
                        ),
                      ),
                    ),
                    SizedBox(width: 20),
                    Expanded(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          Txt(
                              txt: timelineUserModel.name,
                              txtColor: Colors.black,
                              txtSize: MyTheme.txtSize,
                              txtAlign: TextAlign.start,
                              isBold: true),
                          SizedBox(height: 10),
                          Txt(
                              txt: timelineUserModel.lastMessage,
                              txtColor: Colors.grey,
                              txtSize: MyTheme.txtSize,
                              txtAlign: TextAlign.start,
                              isBold: true),
                        ],
                      ),
                    ),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.end,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Row(
                          children: [
                            Image.asset(
                              "assets/images/icons/" + doubletik,
                              width: 20,
                              height: 20,
                              color: (unreadMessageCount)
                                  ? Colors.grey
                                  : Colors.grey.shade500,
                            ),
                            SizedBox(width: 5),
                            Txt(
                                txt: lastLoginDate,
                                txtColor: Colors.grey,
                                txtSize: MyTheme.txtSize - .6,
                                txtAlign: TextAlign.end,
                                isBold: false),
                          ],
                        ),
                        SizedBox(height: 20),
                        timelineUserModel.unreadMessageCount == 0
                            ? Container(
                                width: 30,
                                height: 30,
                                child: Center(
                                  child: Txt(
                                      txt:
                                          (timelineUserModel.unreadMessageCount)
                                              .toString(),
                                      txtColor: Colors.white,
                                      txtSize: MyTheme.txtSize - .4,
                                      txtAlign: TextAlign.start,
                                      isBold: true),
                                ),
                                decoration: BoxDecoration(
                                    shape: BoxShape.circle,
                                    color: MyTheme.redColor),
                              )
                            : SizedBox(
                                width: 30,
                                height: 30,
                              ),
                      ],
                    ),
                  ],
                ),
                SizedBox(height: 10),
                Padding(
                    padding: EdgeInsets.only(left: getWP(context, 15)),
                    child: UIHelper().drawLine()),
                SizedBox(height: 10),
              ],
            ),
          ),
        ),
      );
    } catch (e) {
      log(e.toString());
    }
  }
}
