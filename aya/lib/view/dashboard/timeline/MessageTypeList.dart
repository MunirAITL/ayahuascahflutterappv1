import 'package:aitl/Mixin.dart';
import 'package:aitl/config/cfg/AppConfig.dart';
import 'package:aitl/config/theme/MyTheme.dart';
import 'package:aitl/data/model/dashboard/timeline/MessageTypeAdviserModel.dart';
import 'package:aitl/data/network/NetworkMgr.dart';
import 'package:aitl/view/dashboard/timeline/chat/TimeLinePostScreenNew.dart';
import 'package:aitl/view/widgets/AppbarBotProgbar.dart';
import 'package:aitl/view/widgets/Txt.dart';
import 'package:aitl/view_model/helper/timeline/MessageTypeAPIMgr.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

// ignore: must_be_immutable
class MessageTypeList extends StatefulWidget {
  String title;
  int adviserOrIntroducerId;
  bool online;
  String pictureUrl;

  MessageTypeList(
      {this.title, this.online, this.adviserOrIntroducerId, this.pictureUrl});

  @override
  State createState() => _TimeLineTabState();
}

class _TimeLineTabState extends State<MessageTypeList> with Mixin {
  bool isLoading = false;
  MessageTypeAdviserModel messageTypeAdviserModel;

  List<MessageTypeAdviserModel> messageTypeAdviserModelList = [];

  onPageLoad() async {
    try {
      setState(() {
        isLoading = true;
      });
      MessageTypeAPIMgr().wsOnMessageTypeLoad(
        context: context,
        adviserOrIntroducerId: widget.adviserOrIntroducerId,
        callback: (model) {
          if (model != null && mounted) {
            try {
              if (model.success) {
                messageTypeAdviserModel = new MessageTypeAdviserModel(
                    id: 0,
                    title: "General Chat",
                    description: "",
                    addressOfPropertyToBeMortgaged: "General Address",
                    howMuchDoYouWishToBorrow: "0");
                messageTypeAdviserModelList.add(messageTypeAdviserModel);
                try {
                  final List<dynamic> modelList =
                      model.responseData.messageTypeAdviserModelList;
                  print("messageTypeAdvieAdvisermodelList.length}");
                  if (modelList != null && mounted) {
                    try {
                      for (MessageTypeAdviserModel messageType in modelList) {
                        messageTypeAdviserModelList.add(messageType);
                      }
                    } catch (e) {
                      log(e.toString());
                    }
                    log("Message Type Response = " +
                        messageTypeAdviserModelList.toString());
                    if (mounted) {
                      setState(() {
                        isLoading = false;
                      });
                    }
                  } else {
                    if (mounted) {
                      setState(() {
                        isLoading = false;
                      });
                    }
                  }
                } catch (e) {
                  log(e.toString());
                }
              } else {
                try {
                  //final err = model.errorMessages.login[0].toString();
                  if (mounted) {
                    showAlert(msg: "Timelines not found", isToast: true);
                  }
                } catch (e) {
                  log(e.toString());
                  if (mounted) {
                    setState(() {
                      isLoading = false;
                    });
                  }
                }
              }
            } catch (e) {
              log(e.toString());
              if (mounted) {
                setState(() {
                  isLoading = false;
                });
              }
            }
          } else {
            log("message type list not in");
            if (mounted) {
              setState(() {
                isLoading = false;
              });
            }
          }
        },
      );
    } catch (e) {
      log(e.toString());
      if (mounted) {
        setState(() {
          isLoading = false;
        });
      }
    }
  }

  Future<void> _getRefreshData() async {
    isLoading = true;
    messageTypeAdviserModelList.clear();
    onPageLoad();
  }

  @override
  void initState() {
    super.initState();
    try {
      appInit();
    } catch (e) {}
  }

  @override
  void dispose() {
    messageTypeAdviserModelList = null;

    try {
      NetworkMgr().dispose();
    } catch (e) {}
    super.dispose();
  }

  appInit() async {
    try {
      _getRefreshData();
    } catch (e) {}
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: MyTheme.themeData.accentColor,
        appBar: AppBar(
          centerTitle: true,
          elevation: MyTheme.appbarElevation,
          title: Txt(
              txt: "${widget.title}",
              txtColor: MyTheme.greenColor,
              txtSize: MyTheme.appbarTitleFontSize,
              txtAlign: TextAlign.center,
              isBold: true),
          actions: <Widget>[
            SizedBox(width: 30)
            /* IconButton(
              iconSize: 30,
              icon: Image.asset("assets/images/icons/help_circle_icon.png"),
              onPressed: () {
                // do something
                _stateProvider.notify(ObserverState.STATE_CHANGED_tabbar5);
              },
            )*/
          ],
          iconTheme: IconThemeData(color: MyTheme.greenColor),
          backgroundColor: MyTheme.bgColor,
          bottom: PreferredSize(
            preferredSize:
                new Size(getW(context), getHP(context, (isLoading) ? 1 : 0)),
            child: (isLoading)
                ? AppbarBotProgBar(
                    backgroundColor: MyTheme.appbarProgColor,
                  )
                : SizedBox(),
          ),
        ),
        body: GestureDetector(
          behavior: HitTestBehavior.opaque,
          onPanDown: (detail) {
            FocusScope.of(context).requestFocus(new FocusNode());
          },
          onTap: () {
            FocusScope.of(context).requestFocus(new FocusNode());
          },
          child: drawLayout(),
        ),
      ),
    );
  }

  drawLayout() {
    return Container(
        height: getH(context),
        child: NotificationListener(
          child: Theme(
            data: MyTheme.refreshIndicatorTheme,
            child: RefreshIndicator(
              onRefresh: _getRefreshData,
              child: Padding(
                padding: const EdgeInsets.all(20),
                child: ListView.builder(
                  addAutomaticKeepAlives: true,
                  cacheExtent: AppConfig.page_limit.toDouble(),
                  scrollDirection: Axis.vertical,
                  shrinkWrap: true,
                  itemCount: messageTypeAdviserModelList.length,
                  itemBuilder: (context, index) {
                    return Padding(
                      padding: const EdgeInsets.only(
                          left: 8.0, right: 8.0, top: 20, bottom: 20),
                      child: GestureDetector(
                        onTap: () {
                          Get.to(
                            () => TimeLinePostScreenNew(
                                title: messageTypeAdviserModelList[index].title,
                                taskId: messageTypeAdviserModelList[index].id,
                                adviserID: widget.adviserOrIntroducerId,
                                online: widget.online,
                                imageurl: widget.pictureUrl,
                                name: widget.title),
                          ).then((value) {
                            //callback(route);
                          });
                        },
                        child: Container(
                          padding: EdgeInsets.only(left: 20),
                          width: getWP(context, 100),
                          height: 125,
                          decoration: BoxDecoration(
                              color: MyTheme.greenColor,
                              borderRadius: new BorderRadius.circular(10)),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Expanded(
                                flex: 7,
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    messageTypeAdviserModelList[index].id != 0
                                        ? Txt(
                                            txt:
                                                "Case No ${messageTypeAdviserModelList[index].id}",
                                            txtColor: Colors.white,
                                            txtSize: MyTheme.txtSize,
                                            txtAlign: TextAlign.left,
                                            isBold: true,
                                          )
                                        : SizedBox(),
                                    Txt(
                                      txt:
                                          "${messageTypeAdviserModelList[index].title}",
                                      txtColor: Colors.white,
                                      txtSize:
                                          messageTypeAdviserModelList[index]
                                                      .id ==
                                                  0
                                              ? MyTheme.txtSize
                                              : MyTheme.txtSize - .1,
                                      txtAlign: TextAlign.center,
                                      isBold: messageTypeAdviserModelList[index]
                                                  .id ==
                                              0
                                          ? true
                                          : false,
                                    ),
                                    messageTypeAdviserModelList[index].id == 0
                                        ? SizedBox()
                                        : Txt(
                                            maxLines: 2,
                                            txt:
                                                "${messageTypeAdviserModelList[index].addressOfPropertyToBeMortgaged.trim()}",
                                            txtColor: Colors.white60,
                                            txtSize:
                                                messageTypeAdviserModelList[
                                                                index]
                                                            .id ==
                                                        0
                                                    ? MyTheme.txtSize
                                                    : MyTheme.txtSize - .3,
                                            txtAlign: TextAlign.left,
                                            isBold: messageTypeAdviserModelList[
                                                            index]
                                                        .id ==
                                                    0
                                                ? true
                                                : false,
                                          ),
                                  ],
                                ),
                              ),
                              Expanded(
                                flex: 1,
                                child: Padding(
                                  padding: const EdgeInsets.only(right: 8.0),
                                  child: Icon(
                                    Icons.message,
                                    color: Colors.white,
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                      /*child: Btn(
                          txt: "Case No ${messageTypeAdviserModelList[index].id} \n${messageTypeAdviserModelList[index].title}",
                          txtColor: Colors.white,
                          bgColor: MyTheme.redColor,
                          width: getW(context),
                          height: 75,
                          callback: () {

                            navTo(
                              context: context,
                              page: () => TimeLinePostScreenNew(
                                title: messageTypeAdviserModelList[index].title,
                                taskId: messageTypeAdviserModelList[index].id,
                                adviserID: widget.adviserOrIntroducerId,
                                  online: widget.online
                              ),
                            ).then((value) {
                              //callback(route);
                            });




                          }),*/
                    );
                  },
                ),
              ),
            ),
          ),
        ));
  }
}
