import 'dart:async';
import 'dart:developer';
import 'package:aitl/config/cfg/AppConfig.dart';
import 'package:aitl/config/cfg/PubnubCfg.dart';
import 'package:aitl/config/server/ServerUrls.dart';
import 'package:aitl/config/theme/MyTheme.dart';
import 'package:aitl/controller/classes/DateFun.dart';
import 'package:aitl/data/app_data/UserData.dart';
import 'package:aitl/data/model/dashboard/timeline/TimeLinePostModel.dart';
import 'package:aitl/data/network/NetworkMgr.dart';
import 'package:aitl/view/widgets/AppbarBotProgbar.dart';
import 'package:aitl/view/widgets/MyNetworkImage.dart';
import 'package:aitl/view/widgets/Txt.dart';
import 'package:aitl/view_model/helper/timeline/TimeLinePostHelper.dart';
import 'package:aitl/view_model/helper/timeline/TimelineChatAPIMgr.dart';
import 'package:aitl/view_model/helper/utils/UIHelper.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:intl/intl.dart';
import 'package:flutter/material.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:pubnub/core.dart';
import 'package:pubnub/pubnub.dart';
import '../../../../Mixin.dart';
import 'utils/ChatBubble.dart';
import 'package:jiffy/jiffy.dart';

class TimeLinePostScreenNew extends StatefulWidget {
  final String title;
  final int taskId;

  final int adviserID;
  final bool online;
  final String imageurl;
  final String name;

  TimeLinePostScreenNew({
    Key key,
    @required this.title,
    @required this.taskId,
    @required this.adviserID,
    @required this.online,
    @required this.imageurl,
    @required this.name,
  }) : super(key: key);

  @override
  State createState() => _TimeLinePostScreenState();
}

class _TimeLinePostScreenState extends State<TimeLinePostScreenNew> with Mixin {
  final _scaffoldKey = GlobalKey<ScaffoldState>();

  final TextEditingController textController = TextEditingController();
  ScrollController _scrollController = new ScrollController();

  List<TimeLinePostModel> listTimeLineModel = [];

  var pubnub;
  var myChannel;

  //Timer timerGetTimeLine;
  //Timer timerScrollToBottom;
  //static const int callTimelineSec = 35;
  //  page stuff start here
  bool isPageDone = false;
  bool isLoading = false;
  int pageStart = 1;
  int pageCount = AppConfig.page_limit;

  String msgTmp = '';

  postTimelineAPI() async {
    try {
      isLoading = true;
      final param = TimeLinePostHelper().getParam(
        message: msgTmp,
        postTypeName: "status",
        additionalAttributeValue: null,
        inlineTags: [],
        checkin: "",
        fromLat: 0,
        fromLng: 0,
        smallImageName: "",
        receiverId: widget.adviserID,
        ownerId: 0,
        taskId: widget.taskId,
        isPrivate: true,
      );
      log(param.toString());
      TimelineChatAPIMgr().wsPostTimelineAPI(
          context: context,
          param: param,
          callback: (model) async {
            if (model != null && mounted) {
              try {
                debugPrint(
                    "post a message = ${model.responseData.post.toJson()}");
                if (model.success) {
                  User user = new User(
                      id: userData.userModel.id, name: userData.userModel.name);
                  TimeLinePostModel linePostModel = model.responseData.post;
                  Data2 data2 = new Data2(
                      id: linePostModel.id,
                      ownerId: linePostModel.ownerId,
                      ownerEntityType: linePostModel.ownerEntityType,
                      ownerImageUrl: linePostModel.ownerImageUrl,
                      ownerProfileUrl: linePostModel.ownerProfileUrl,
                      ownerName: linePostModel.ownerName,
                      postTypeName: linePostModel.postTypeName,
                      isSponsored: linePostModel.isSponsored,
                      message: linePostModel.message,
                      additionalAttributeValue:
                          linePostModel.additionalAttributeValue,
                      dateCreatedUtc: linePostModel.dateCreatedUtc,
                      dateUpdatedUtc: linePostModel.dateUpdatedUtc,
                      dateCreated: linePostModel.dateCreated,
                      dateUpdated: linePostModel.dateUpdated,
                      totalLikes: linePostModel.totalLikes,
                      totalComments: linePostModel.totalComments,
                      canDelete: linePostModel.canDelete,
                      publishDateUtc: linePostModel.publishDateUtc,
                      publishDate: linePostModel.publishDate,
                      likeStatus: linePostModel.likeStatus,
                      isOwner: linePostModel.isOwner,
                      checkin: linePostModel.checkin,
                      fromLat: linePostModel.fromLat,
                      fromLng: linePostModel.fromLng,
                      userCommentPublicModelList:
                          linePostModel.userCommentPublicModelList,
                      receiverId: linePostModel.receiverId,
                      senderId: linePostModel.senderId,
                      taskId: linePostModel.taskId,
                      userCompanyId: userData.userModel.userCompanyID,
                      isOnline: true);

                  Data data = new Data(data2: data2, user: user);
                  postAMessage(data);

                  onPageLoad(1);
                } else {}
              } catch (e) {
                log(e.toString());
              }
            }
          });
    } catch (e) {
      log(e.toString());
      isLoading = false;
    }
  }

  onPageLoad(int startPage) async {
    try {
      setState(() {
        isLoading = true;
      });
      TimelineChatAPIMgr().wsOnPageLoad(
        context: context,
        startPage: startPage,
        pageCount: pageCount,
        taskId: widget.taskId,
        taskBiddingUserId: widget.adviserID,
        callback: (model) {
          if (model != null && mounted) {
            msgTmp = '';
            try {
              if (model.success) {
                try {
                  final List<dynamic> timeLines =
                      model.responseData.timelinePosts;
                  if (timeLines != null && mounted) {
                    //  checking to see whether page is finished to stop on reload data through API after end of scrolling for scalibility

                    try {
                      setState(() {
                        isLoading = false;
                      });
                      if (listTimeLineModel.length > 0) {
                        try {
                          TimeLinePostModel tlModel1 = listTimeLineModel[0];
                          TimeLinePostModel tlModel2 =
                              model.responseData.timelinePosts[0];
                          if (tlModel1.id == tlModel2.id) {
                            return;
                          }
                        } catch (e) {
                          log(e.toString());
                        }
                      }

                      //await _removeFromMeMsg();
                      if (startPage == 1) {
                        listTimeLineModel.clear();
                        //await _removeFromMeMsg();
                      }
                      for (TimeLinePostModel timeLine in timeLines) {
                        //if (!listTimeLineModel.contains(timeLine))
                        listTimeLineModel.add(timeLine);
                      }
                      setState(() {
                        if (timeLines.length != pageCount) {
                          isPageDone = true;
                        }
                      });
                    } catch (e) {
                      log(e.toString());
                    }
                    log(listTimeLineModel.toString());
                  }
                } catch (e) {
                  log(e.toString());
                }
              }
            } catch (e) {
              log(e.toString());
            }
          }
        },
      );
    } catch (e) {
      log(e.toString());
      setState(() {
        isLoading = false;
      });
    }
  }

  @override
  void initState() {
    try {
      appInit();
    } catch (e) {}
    super.initState();
  }

  /*_removeFromMeMsg() async {
    try {
      for (TimeLinePostModel tlModel in listTimeLineModel) {
        try {
          if (tlModel.isFromMe) {
            listTimeLineModel.remove(tlModel);
          }
        } catch (e) {}
      }
    } catch (e) {}
  }*/

  //  ******************* Pub Nub Start Here....

  /*initPubNub() async {
    userModel =  DashBoardScreenState.userModel;
    final pubnub = pn.PubNub(
        defaultKeyset: Keyset(
            subscribeKey: PubNubCfg.subscribeKey,
            publishKey: PubNubCfg.publishKey,
            uuid: UUID(await Common.getUDID(context))));

    final param = TimeLinePostHelper().getParam(
      message: "testinggggg pubnub",
      postTypeName: "status",
      additionalAttributeValue: null,
      inlineTags: [],
      checkin: "",
      fromLat: 0,
      fromLng: 0,
      smallImageName: "",
      receiverId: widget.receiverId,
      ownerId: 0,
      senderId: userModel.id,
      taskId: widget.taskId,
      isPrivate: true,
    );

    final myChannel = pubnub.channel(userModel.id.toString());
    myChannel.publish({
      'data': param,
      'user': userModel.id,
    });

    var subscription = pubnub.subscribe(channels: {userModel.id.toString()});

    subscription.messages.listen((envelope) {
      print('${envelope.uuid} sent a message: ${envelope.payload}');
    });

    var history = myChannel.history(chunkSize: 50);
    log(history.toString());

    appInit();
    /*myChannel.subscribe().messages.listen((envelope) {
      print(envelope.payload);
    });*/
  }*/

  //  ******************* Pub Nub End Here....
  @override
  void dispose() {
    listTimeLineModel = null;
    textController.dispose();
    _scrollController.dispose();
    _scrollController = null;
    try {
      NetworkMgr().dispose();
    } catch (e) {}
    super.dispose();
  }

  appInit() async {
    try {
      await onPageLoad(1);
      pageStart++;
      /* timerGetTimeLine =
          Timer.periodic(Duration(seconds: callTimelineSec), (Timer t) {
            //  call to ws for getting any new msg of the sender
            if (!isLoading && msgTmp.length == 0) {
              onPageLoad(1);
            }
          });*/
    } catch (e) {
      log(e.toString());
    }
    try {
      initPubNub();
    } catch (e) {
      debugPrint("Error pubnub = ${e.toString()}");
    }
  }

  Widget buildSingleMessage(index) {
    try {
      TimeLinePostModel timelineModel = listTimeLineModel[index];
      bool fromMe =
          (timelineModel.senderId == userData.userModel.id) ? true : false;

      //bool isFromMeSent = false;
      String lastMsgDate;
      DateTime date1;
      try {
        //if (index  <= listTimeLineModel.length) {
        TimeLinePostModel timelineModel2 = listTimeLineModel[(index + 1)];
        //var jiffy1 = Jiffy(timelineModel2.dateCreated);
        //var jiffy2 = Jiffy(timelineModel2.dateCreated)..date;

        date1 = Jiffy(timelineModel.dateCreated).dateTime;
        DateTime date2 = Jiffy(timelineModel2.dateCreated).dateTime;
        //final days = (jiffy2.diff(jiffy1.date));
        if (date1.day > date2.day) {
          // log(days.toString());
          var inputDate = DateTime.parse(date1.toString());
          var outputFormat = DateFormat('dd-MMMM-yyyy');
          lastMsgDate = outputFormat.format(inputDate);
          //isFromMeSent = true;
        }
        //}
      } catch (e) {
        log(e.toString());
      }

      Alignment alignment = fromMe ? Alignment.topRight : Alignment.topLeft;
      Alignment chatArrowAlignment =
          fromMe ? Alignment.topRight : Alignment.topLeft;

      Color chatBgColor = fromMe ? MyTheme.greenColor : MyTheme.redColor;
      EdgeInsets edgeInsets = fromMe
          ? EdgeInsets.fromLTRB(5, 5, 15, 5)
          : EdgeInsets.fromLTRB(15, 5, 5, 5);
      EdgeInsets margins = fromMe
          ? EdgeInsets.fromLTRB(80, 5, 10, 5)
          : EdgeInsets.fromLTRB(10, 5, 80, 5);

      return Padding(
        padding: const EdgeInsets.only(left: 10, right: 10),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            //(!isFromMeSent && fromMe && timelineModel.fromMeDate != null)

            (fromMe && lastMsgDate != null)
                ? Padding(
                    padding: const EdgeInsets.all(20),
                    child: Center(
                      child: Txt(
                          txt: lastMsgDate,
                          txtColor: Colors.grey,
                          txtSize: MyTheme.txtSize,
                          txtAlign: TextAlign.start,
                          isBold: false),
                    ),
                  )
                : SizedBox(),
            Row(
              children: [
                (!fromMe) ? getImage(timelineModel.ownerImageUrl) : SizedBox(),
                Expanded(
                  child: Container(
                    color: Colors.white,
                    margin: margins,
                    child: Align(
                      alignment: alignment,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          CustomPaint(
                            painter: ChatBubble(
                              color: chatBgColor,
                              alignment: chatArrowAlignment,
                            ),
                            child: Container(
                              margin: EdgeInsets.all(10),
                              child: Stack(
                                children: <Widget>[
                                  Padding(
                                    padding: edgeInsets,
                                    child: Txt(
                                        txt: timelineModel.message
                                            .toString()
                                            .trim(),
                                        txtColor: Colors.white,
                                        txtSize: MyTheme.txtSize - .3,
                                        txtAlign: TextAlign.start,
                                        isBold: false),
                                  ),
                                ],
                              ),
                            ),
                          ),
                          SizedBox(height: 5),
                        ],
                      ),
                    ),
                  ),
                ),
                (fromMe) ? getImage(timelineModel.ownerImageUrl) : SizedBox(),
              ],
            ),

            Align(
              alignment:
                  (fromMe) ? Alignment.bottomRight : Alignment.bottomLeft,
              child: Padding(
                padding: (!fromMe)
                    ? EdgeInsets.only(left: getWP(context, 13))
                    : EdgeInsets.only(right: getWP(context, 13)),
                child: Txt(
                    txt: DateFun.getTimeAgoTxt(timelineModel.dateCreated),
                    txtColor: Colors.grey,
                    txtSize: MyTheme.txtSize - .2,
                    txtAlign: TextAlign.start,
                    isBold: false),
              ),
            ),
            SizedBox(height: 20),
          ],
        ),
      );
    } catch (e) {
      return Container();
    }
  }

  getImage(url) {
    return Container(
      decoration: MyTheme.picEmboseCircleDeco,
      child: CircleAvatar(
        radius: 25.0,
        backgroundColor: Colors.transparent,
        backgroundImage:
            new CachedNetworkImageProvider(MyNetworkImage.checkUrl(url)),
      ),
    );
  }

  Widget buildMessageList() {
    return Flexible(
      child: NotificationListener(
        onNotification: (scrollNotification) {
          if (scrollNotification is ScrollStartNotification) {
            if (!isLoading && !isPageDone && msgTmp.length == 0) {
              onPageLoad(pageStart++);
            }
          }
          return true;
        },
        child: ListView.builder(
          controller: _scrollController,
          padding: new EdgeInsets.all(8.0),
          reverse: true,
          itemCount: listTimeLineModel.length,
          itemBuilder: (BuildContext context, int index) {
            return buildSingleMessage(index);
          },
        ),
      ),
    );
  }

  _bottomChatArea() {
    return Container(
      padding: EdgeInsets.all(10.0),
      child: Row(
        children: <Widget>[
          _chatTextArea(),
          /*GestureDetector(
            onLongPressStart: (details) async {
              bool hasPermission = await checkPermission();
              if (hasPermission) {
                log("start recording");
                // Check and request permission
                Directory tempDir = await getTemporaryDirectory();
                String tempPath = tempDir.path;
                // https://pub.dev/packages/record_mp3
                //start record
                RecordMp3.instance.start(tempPath, (type) {
                  // record fail callback
                  log(type);
                });
              } else {
                log("cannot start recording:: permission not allowed");
              }
            },
            onLongPressEnd: (details) {
              log("end recording");
              //complete record and export a record file
              RecordMp3.instance.stop();
            },
            child: Padding(
              padding: const EdgeInsets.only(left: 10),
              child: Container(
                child: Image.asset(
                  "assets/images/icons/mic_icon.png",
                  width: 60,
                  height: 60,
                ),
              ),
            ),
          ),*/
          IconButton(
            iconSize: 30,
            icon: Icon(
              Icons.send,
              color: MyTheme.dgreyColor,
            ),
            onPressed: () async {
              //Check if the textfield has text or not
              FocusScope.of(context).requestFocus(FocusNode());
              onSendClicked();
            },
          ),
        ],
      ),
    );
  }

  onSendClicked() async {
    try {
      if (textController.text.isNotEmpty) {
        //Add the message to the list
        String formattedDate = DateFormat('dd-MMM-yyyy').format(DateTime.now());
        if (mounted) {
          //Scrolldown the list to show the latest message
          msgTmp = textController.text.trim();
          final TimeLinePostModel timeLinePostModel = TimeLinePostModel();
          timeLinePostModel.senderId = userData.userModel.id;
          timeLinePostModel.ownerId = userData.userModel.id;
          timeLinePostModel.ownerImageUrl = userData.userModel.profileImageURL;
          timeLinePostModel.message = msgTmp;
          timeLinePostModel.dateCreatedUtc = formattedDate;
          timeLinePostModel.isFromMe = true;
          listTimeLineModel.insert(0, timeLinePostModel);
          textController.text = '';
          _scrollController.animateTo(
            0.0,
            curve: Curves.easeOut,
            duration: const Duration(milliseconds: 300),
          );
          //_chatListScrollToBottom();
          //setState(() {});
          //Future.delayed(Duration(seconds: 5), () async {
          pageStart = 1;
          await postTimelineAPI();
          //});
        }
      }
    } catch (e) {}
  }

  _chatTextArea() {
    return Expanded(
      child: Padding(
        padding: const EdgeInsets.only(left: 20, right: 20),
        child: Container(
          decoration: new BoxDecoration(
              color: MyTheme.greyColor,
              borderRadius: new BorderRadius.only(
                topLeft: const Radius.circular(40.0),
                topRight: const Radius.circular(40.0),
                bottomLeft: const Radius.circular(40.0),
                bottomRight: const Radius.circular(40.0),
              )),
          child: Row(
            children: [
              Padding(
                padding: const EdgeInsets.only(left: 10),
                child: IconButton(
                    iconSize: 30,
                    icon: Icon(
                      Icons.tag_faces,
                      color: Colors.black54,
                    ),
                    onPressed: () {}),
              ),
              Expanded(
                child: TextField(
                  controller: textController,
                  textInputAction: TextInputAction.send,
                  textAlign: TextAlign.center,
                  onSubmitted: (value) {
                    onSendClicked();
                  },
                  maxLength: 255,
                  autocorrect: false,
                  style: TextStyle(
                    color: Colors.black,
                    fontSize:
                        getTxtSize(context: context, txtSize: MyTheme.txtSize),
                  ),
                  decoration: InputDecoration(
                    border: InputBorder.none,
                    focusedBorder: InputBorder.none,
                    enabledBorder: InputBorder.none,
                    errorBorder: InputBorder.none,
                    disabledBorder: InputBorder.none,
                    hintStyle: TextStyle(color: Colors.black54),
                    counter: Offstage(),
                    contentPadding: EdgeInsets.fromLTRB(0, 20.0, 20.0, 10.0),
                    hintText: 'Type a message...',
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        key: _scaffoldKey,
        //resizeToAvoidBottomPadding: false,
        backgroundColor: MyTheme.themeData.accentColor,
        appBar: AppBar(
          elevation: MyTheme.appbarElevation,
          backgroundColor: MyTheme.themeData.accentColor,
          title: Txt(
              txt: "Message",
              txtColor: MyTheme.greenColor,
              txtSize: MyTheme.appbarTitleFontSize,
              txtAlign: TextAlign.center,
              isBold: true),
          /*IconButton(
                  iconSize: 30,
                  icon: Image.asset("assets/images/icons/help_circle_icon.png"),
                  onPressed: () {
                    // do something
                    Get.to(
                      () => WebScreen(
                        title: "Help",
                        url: Server.HELP_INFO_URL,
                      ),
                    ).then((value) {
                      //callback(route);
                    });
                  },
                )*/

          iconTheme: IconThemeData(color: MyTheme.greenColor),
          centerTitle: true,
          bottom: PreferredSize(
            preferredSize: new Size(getW(context), getHP(context, 10)),
            child: Column(
              children: [
                (isLoading)
                    ? AppbarBotProgBar(
                        backgroundColor: MyTheme.appbarProgColor,
                      )
                    : SizedBox(),
                Divider(color: Colors.black, height: 10),
                Container(
                  child: Padding(
                    padding: const EdgeInsets.only(left: 10),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Flexible(
                          flex: 6,
                          child: widget.taskId != 0
                              ? Row(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  // crossAxisAlignment: CrossAxisAlignment.center,
                                  children: [
                                    Container(
                                      child: Container(
                                        width: 60,
                                        height: 60,
                                        // child: ImageViewNetworkCircular(imageUrl: ""),
                                        decoration: MyTheme.picEmboseCircleDeco,
                                        child: CircleAvatar(
                                          radius: 10,
                                          backgroundColor: Colors.transparent,
                                          backgroundImage:
                                              new CachedNetworkImageProvider(
                                            MyNetworkImage.checkUrl(
                                                (widget.imageurl != null)
                                                    ? widget.imageurl
                                                    : ServerUrls.MISSING_IMG),
                                          ),
                                        ),
                                      ),
                                    ),
                                    SizedBox(
                                      width: 10,
                                    ),
                                    Container(
                                      child: Column(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: [
                                          Row(
                                            children: [
                                              Txt(
                                                  txt: "${widget.name}",
                                                  txtColor: MyTheme
                                                      .timelinePostCallerNameColor,
                                                  txtSize: MyTheme.txtSize,
                                                  txtAlign: TextAlign.start,
                                                  isBold: true),
                                              SizedBox(
                                                width: 5,
                                              ),
                                              Container(
                                                child: UIHelper().drawCircle(
                                                  context: context,
                                                  color: (widget.online)
                                                      ? Colors.green
                                                      : Colors.red,
                                                  size: 3,
                                                ),
                                              )
                                            ],
                                          ),
                                          SizedBox(
                                            width: 5,
                                          ),
                                          Txt(
                                              txt: "Case No: ${widget.taskId}",
                                              txtColor: Colors.grey[600],
                                              txtSize: MyTheme.txtSize - .5,
                                              txtAlign: TextAlign.start,
                                              isBold: true),
                                        ],
                                      ),
                                    ),
                                  ],
                                )
                              /*Row(
                                  children: [
                                    Txt(
                                        txt: "Case No: ${widget.taskId}",
                                        txtColor:
                                            MyTheme.timelinePostCallerNameColor,
                                        txtSize: MyTheme.txtSize + 1,
                                        txtAlign: TextAlign.start,
                                        isBold: true),
                                    SizedBox(
                                      width: 5,
                                    ),
                                    drawCircle(
                                      context: context,
                                      color: (widget.online)
                                          ? Colors.green
                                          : Colors.red,
                                      size: 3,
                                    ),
                                  ],
                                )*/
                              : Row(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  // crossAxisAlignment: CrossAxisAlignment.center,
                                  children: [
                                    Container(
                                      child: Container(
                                        width: 60,
                                        height: 60,
                                        // child: ImageViewNetworkCircular(imageUrl: ""),
                                        decoration: MyTheme.picEmboseCircleDeco,
                                        child: CircleAvatar(
                                          radius: 10,
                                          backgroundColor: Colors.transparent,
                                          backgroundImage:
                                              new CachedNetworkImageProvider(
                                            MyNetworkImage.checkUrl(
                                                (widget.imageurl != null)
                                                    ? widget.imageurl
                                                    : ServerUrls.MISSING_IMG),
                                          ),
                                        ),
                                      ),
                                    ),
                                    SizedBox(
                                      width: 10,
                                    ),
                                    Container(
                                      child: Column(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: [
                                          Row(
                                            children: [
                                              Txt(
                                                  txt: "${widget.name}",
                                                  txtColor: MyTheme
                                                      .timelinePostCallerNameColor,
                                                  txtSize: MyTheme.txtSize,
                                                  txtAlign: TextAlign.start,
                                                  isBold: true),
                                              SizedBox(
                                                width: 5,
                                              ),
                                              Container(
                                                child: UIHelper().drawCircle(
                                                  context: context,
                                                  color: (widget.online)
                                                      ? Colors.green
                                                      : Colors.red,
                                                  size: 3,
                                                ),
                                              )
                                            ],
                                          ),
                                          SizedBox(
                                            width: 5,
                                          ),
                                          Txt(
                                              txt: "${widget.title}",
                                              txtColor: Colors.grey[600],
                                              txtSize: MyTheme.txtSize - .5,
                                              txtAlign: TextAlign.start,
                                              isBold: true),
                                        ],
                                      ),
                                    ),
                                  ],
                                ),
                        ),
                        /* Flexible(
                          flex: 3,
                          child: Txt(
                              txt: widget.taskId==0?"${widget.title}":"Case No: ${widget.taskId}",
                              txtColor: MyTheme.timelinePostCallerNameColor,
                              txtSize: MyTheme.txtSize + 1,
                              txtAlign: TextAlign.start,
                              isBold: true),
                        ),*/
                        // SizedBox(width: 5),

                        Expanded(child: SizedBox()),
                        Expanded(
                          child: IconButton(
                            iconSize: 40,
                            icon: Image.asset(
                                'assets/images/icons/phone_icon.png'),
                            onPressed: () async {
                              openUrl(context,
                                  "tel://" + userData.userModel.mobileNumber);
                            },
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
        body: GestureDetector(
          onTap: () {
            FocusScope.of(context).requestFocus(new FocusNode());
          },
          child: (mounted) ? drawChatUI() : SizedBox(),
        ),
      ),
    );
  }

  drawChatUI() {
    return Container(
      width: double.infinity,
      height: double.infinity,
      child: Column(
        children: <Widget>[
          buildMessageList(),
          //Divider(height: 1.0),
          _bottomChatArea(),
        ],
      ),
    );
  }

  Future<bool> checkPermission() async {
    Map<PermissionGroup, PermissionStatus> map = await new PermissionHandler()
        .requestPermissions(
            [PermissionGroup.storage, PermissionGroup.microphone]);
    print(map[PermissionGroup.microphone]);
    return map[PermissionGroup.microphone] == PermissionStatus.granted;
  }

  initPubNub() async {
    log("init pub nub call =");

    pubnub = PubNub(
        defaultKeyset: Keyset(
            subscribeKey: PubNubCfg.subscribeKey,
            publishKey: PubNubCfg.publishKey,
            uuid: UUID(userData.userModel.id.toString())));

    Subscription subscription =
        pubnub.subscribe(channels: {userData.userModel.id.toString()});

    print('receive message id : ${userData.userModel.id}');

    subscription.messages.listen((envelope) {
      print('sent a message full : ${envelope.payload}');
      print('receive message id : ${userData.userModel.id}');
      onPageLoad(1);
    });
  }

  void postAMessage(Data data) {
    myChannel = pubnub.channel(widget.adviserID.toString());

    print('sent a message id : ${widget.adviserID.toString()}');
    myChannel.publish({
      'data': data.toJson(),
      'user': userData.userModel.id,
    });
  }
}

class Data {
  Data2 data2;
  User user;

  Data({this.data2, this.user});

  Data.fromJson(Map<String, dynamic> json) {
    data2 = json['data'] != null ? new Data2.fromJson(json['data']) : null;
    user = json['user'] != null ? new User.fromJson(json['user']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.data2 != null) {
      data['data'] = this.data2.toJson();
    }
    if (this.user != null) {
      data['user'] = this.user.toJson();
    }
    return data;
  }
}

class Data2 {
  int id;
  int ownerId;
  String ownerEntityType;
  String ownerImageUrl;
  String ownerProfileUrl;
  String ownerName;
  String postTypeName;
  bool isSponsored;
  String message;
  dynamic additionalAttributeValue;
  String dateCreatedUtc;
  String dateUpdatedUtc;
  String dateCreated;
  String dateUpdated;
  int totalLikes;
  int totalComments;
  bool canDelete;
  String publishDateUtc;
  String publishDate;
  int likeStatus;
  bool isOwner;
  String checkin;
  double fromLat;
  double fromLng;
  dynamic userCommentPublicModelList;
  int receiverId;
  int senderId;
  int taskId;
  int userCompanyId;
  bool isOnline;

  Data2(
      {this.id,
      this.ownerId,
      this.ownerEntityType,
      this.ownerImageUrl,
      this.ownerProfileUrl,
      this.ownerName,
      this.postTypeName,
      this.isSponsored,
      this.message,
      this.additionalAttributeValue,
      this.dateCreatedUtc,
      this.dateUpdatedUtc,
      this.dateCreated,
      this.dateUpdated,
      this.totalLikes,
      this.totalComments,
      this.canDelete,
      this.publishDateUtc,
      this.publishDate,
      this.likeStatus,
      this.isOwner,
      this.checkin,
      this.fromLat,
      this.fromLng,
      this.userCommentPublicModelList,
      this.receiverId,
      this.senderId,
      this.taskId,
      this.userCompanyId,
      this.isOnline});

  Data2.fromJson(Map<String, dynamic> json) {
    id = json['Id'];
    ownerId = json['OwnerId'];
    ownerEntityType = json['OwnerEntityType'];
    ownerImageUrl = json['OwnerImageUrl'];
    ownerProfileUrl = json['OwnerProfileUrl'];
    ownerName = json['OwnerName'];
    postTypeName = json['PostTypeName'];
    isSponsored = json['IsSponsored'];
    message = json['Message'];
    additionalAttributeValue = json['AdditionalAttributeValue'];
    dateCreatedUtc = json['DateCreatedUtc'];
    dateUpdatedUtc = json['DateUpdatedUtc'];
    dateCreated = json['DateCreated'];
    dateUpdated = json['DateUpdated'];
    totalLikes = json['TotalLikes'];
    totalComments = json['TotalComments'];
    canDelete = json['CanDelete'];
    publishDateUtc = json['PublishDateUtc'];
    publishDate = json['PublishDate'];
    likeStatus = json['LikeStatus'];
    isOwner = json['IsOwner'];
    checkin = json['Checkin'];
    fromLat = json['FromLat'];
    fromLng = json['FromLng'];
    userCommentPublicModelList = json['UserCommentPublicModelList'];
    receiverId = json['ReceiverId'];
    senderId = json['SenderId'];
    taskId = json['TaskId'];
    userCompanyId = json['UserCompanyId'];
    isOnline = json['IsOnline'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['Id'] = this.id;
    data['OwnerId'] = this.ownerId;
    data['OwnerEntityType'] = this.ownerEntityType;
    data['OwnerImageUrl'] = this.ownerImageUrl;
    data['OwnerProfileUrl'] = this.ownerProfileUrl;
    data['OwnerName'] = this.ownerName;
    data['PostTypeName'] = this.postTypeName;
    data['IsSponsored'] = this.isSponsored;
    data['Message'] = this.message;
    data['AdditionalAttributeValue'] = this.additionalAttributeValue;
    data['DateCreatedUtc'] = this.dateCreatedUtc;
    data['DateUpdatedUtc'] = this.dateUpdatedUtc;
    data['DateCreated'] = this.dateCreated;
    data['DateUpdated'] = this.dateUpdated;
    data['TotalLikes'] = this.totalLikes;
    data['TotalComments'] = this.totalComments;
    data['CanDelete'] = this.canDelete;
    data['PublishDateUtc'] = this.publishDateUtc;
    data['PublishDate'] = this.publishDate;
    data['LikeStatus'] = this.likeStatus;
    data['IsOwner'] = this.isOwner;
    data['Checkin'] = this.checkin;
    data['FromLat'] = this.fromLat;
    data['FromLng'] = this.fromLng;
    data['UserCommentPublicModelList'] = this.userCommentPublicModelList;
    data['ReceiverId'] = this.receiverId;
    data['SenderId'] = this.senderId;
    data['TaskId'] = this.taskId;
    data['UserCompanyId'] = this.userCompanyId;
    data['IsOnline'] = this.isOnline;
    return data;
  }
}

class User {
  int id;
  String name;

  User({this.id, this.name});

  User.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['name'] = this.name;
    return data;
  }
}
