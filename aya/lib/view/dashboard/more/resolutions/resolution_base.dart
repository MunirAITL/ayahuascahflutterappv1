import 'package:aitl/view/dashboard/dashboard_base.dart';
import 'package:aitl/view/widgets/dropdown/DropListModel.dart';
import 'package:flutter/material.dart';

abstract class BaseResolutionStatefull<T extends StatefulWidget>
    extends BaseDashboard<T> {
  //  dropdown
  DropListModel dd = DropListModel([
    OptionItem(id: 1, title: "Technical Problem with software"),
    OptionItem(id: 2, title: "I found a bug in the software"),
    OptionItem(id: 3, title: "I have a non-technical issue"),
    OptionItem(id: 4, title: "I have a Complain"),
    OptionItem(id: 5, title: "I have a question about My cases"),
  ]);

  OptionItem opt = OptionItem(id: null, title: "Select Support Ticket Type");
}
