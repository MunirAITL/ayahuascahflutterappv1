import 'dart:developer';
import 'dart:io';
import 'package:aitl/config/cfg/AppConfig.dart';
import 'package:aitl/config/server/ResCfg.dart';
import 'package:aitl/config/theme/MyTheme.dart';
import 'package:aitl/data/app_data/UserData.dart';
import 'package:aitl/data/model/dashboard/more/res/ResolutionAPIModel.dart';
import 'package:aitl/data/model/misc/media_upload/MediaUploadFilesModel.dart';
import 'package:aitl/data/network/ModelMgr.dart';
import 'package:aitl/data/network/NetworkMgr.dart';
import 'package:aitl/view/widgets/CamPicker.dart';
import 'package:aitl/view/widgets/MMBtn.dart';
import 'package:aitl/view/widgets/Txt.dart';
import 'package:aitl/view/widgets/dropdown/DropDownPicker.dart';
import 'package:aitl/view/widgets/dropdown/DropListModel.dart';
import 'package:aitl/view/widgets/webview/WebScreen.dart';
import 'package:aitl/view_model/api/api_view_model.dart';
import 'package:aitl/view_model/observer/APIStateProvider.dart';
import 'package:flutter/material.dart';
import 'package:file_picker/file_picker.dart';
import 'package:dotted_border/dotted_border.dart';
import 'package:get/get.dart';
import 'package:aitl/Mixin.dart';
import 'resolution_base.dart';

class ResolutionScreen extends StatefulWidget {
  @override
  State createState() => _ResolutionScreenState();
}

class _ResolutionScreenState extends BaseResolutionStatefull<ResolutionScreen>
    with APIStateListener {
  final cmt = TextEditingController();
  List<MediaUploadFilesModel> listMediaUploadFilesModel = [];

  //  **************  app states start

  @override
  void onDetached() {
    try {
      log("app state = onDetached");
    } catch (e) {}
  }

  @override
  void onInactive() {
    try {
      log("app state = onInactive");
    } catch (e) {}
  }

  @override
  void onPaused() {
    try {
      log("app state = onPaused");
    } catch (e) {}
  }

  @override
  void onResumed() {
    try {
      log("app state = onResumed");
    } catch (e) {}
  }

  //  **************  app states end

  APIStateProvider _apiStateProvider;
  @override
  onAPIStateChanged(apiState, model) async {
    try {
      if (apiState.type == APIType.media_upload_file &&
          apiState.cls == this.runtimeType) {
        if (model != null && mounted) {
          if (model.success) {
            listMediaUploadFilesModel.add(model.responseData.images[0]);
            setState(() {});
          } else {
            final err = model.errorMessages.upload_pictures[0].toString();
            showAlert(msg: err, isToast: true);
          }
        }
      } else if (apiState.type == APIType.resolution &&
          apiState.cls == this.runtimeType) {
        if (model != null && mounted) {
          if (model.success) {
            cmt.text = "";
            listMediaUploadFilesModel.clear();
            final msg = model.messages.resolution_post[0].toString();
            showAlert(msg: msg, isToast: true, which: 1);
            setState(() {});
          } else {
            final err = model.errorMessages.upload_pictures[0].toString();
            showAlert(msg: err, isToast: true);
          }
        }
      }
    } catch (e) {}
  }

  @override
  void initState() {
    super.initState();
    try {
      appInit();
    } catch (e) {}
  }

  @override
  void dispose() {
    cmt.dispose();
    try {
      _apiStateProvider.unsubscribe(this);
      _apiStateProvider = null;
    } catch (e) {
      log(e.toString());
    }

    listMediaUploadFilesModel = null;
    try {
      NetworkMgr().dispose();
    } catch (e) {}
    super.dispose();
  }

  appInit() async {
    try {
      _apiStateProvider = new APIStateProvider();
      _apiStateProvider.subscribe(this);
    } catch (e) {}
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: MyTheme.bgColor,
        resizeToAvoidBottomInset: true,
        appBar: AppBar(
          elevation: MyTheme.appbarElevation,
          backgroundColor: MyTheme.bgColor,
          title: Txt(
              txt: 'Contact us',
              txtColor: MyTheme.appbarTxtColor,
              txtSize: MyTheme.appbarTitleFontSize,
              txtAlign: TextAlign.center,
              isBold: false),
          centerTitle: false,
        ),
        body: GestureDetector(
          onTap: () {
            FocusScope.of(context).requestFocus(new FocusNode());
          },
          child: drawLayout(),
        ),
      ),
    );
  }

  drawLayout() {
    return ListView(
      shrinkWrap: true,
      children: [
        Padding(
          padding: const EdgeInsets.only(left: 20, right: 20, top: 20),
          child: DropDownPicker(
            cap: "Support Ticket Type",
            titleColor: MyTheme.gray4Color,
            txtColor: MyTheme.gray4Color,
            itemSelected: opt,
            dropListModel: dd,
            onOptionSelected: (optionItem) {
              opt = optionItem;
              setState(() {});
            },
          ),
        ),
        Padding(
          padding: const EdgeInsets.only(left: 20, right: 20, top: 20),
          child: Txt(
              txt: "How can we help you?",
              txtColor: MyTheme.gray4Color,
              txtSize: MyTheme.txtSize,
              txtAlign: TextAlign.start,
              isBold: false),
        ),
        Container(
          margin: const EdgeInsets.only(left: 20.0, right: 20, top: 10),
          decoration: BoxDecoration(
              border: Border.all(color: Colors.grey),
              borderRadius: BorderRadius.all(Radius.circular(10))),
          child: TextField(
            controller: cmt,
            minLines: 5,
            maxLines: 10,
            //expands: true,
            autocorrect: false,
            maxLength: 500,
            keyboardType: TextInputType.multiline,
            style: TextStyle(
              color: Colors.black,
              fontSize: getTxtSize(context: context, txtSize: MyTheme.txtSize),
            ),
            decoration: InputDecoration(
              hintText: 'Description',
              hintStyle: TextStyle(color: Colors.grey),
              //labelText: 'Your message',
              border: InputBorder.none,
              focusedBorder: InputBorder.none,
              enabledBorder: InputBorder.none,
              errorBorder: InputBorder.none,
              disabledBorder: InputBorder.none,
              contentPadding:
                  EdgeInsets.only(left: 15, bottom: 11, top: 11, right: 15),
            ),
          ),
        ),
        Padding(
          padding: const EdgeInsets.only(left: 20, right: 20, top: 20),
          child: Txt(
              txt: "Attachments - " +
                  listMediaUploadFilesModel.length.toString() +
                  ' files added',
              txtColor: MyTheme.gray4Color,
              txtSize: MyTheme.txtSize,
              txtAlign: TextAlign.start,
              isBold: false),
        ),
        for (MediaUploadFilesModel model in listMediaUploadFilesModel)
          Container(
            child: ListTile(
              leading: IconButton(
                  icon: Icon(
                    Icons.remove_circle,
                    color: Colors.red,
                  ),
                  onPressed: () {
                    listMediaUploadFilesModel.remove(model);
                    setState(() {});
                  }),
              title: Align(
                alignment: Alignment(-1.2, 0),
                child: Txt(
                    txt: model.url.split('/').last ?? '',
                    txtColor: MyTheme.gray4Color,
                    txtSize: MyTheme.txtSize - .5,
                    txtAlign: TextAlign.center,
                    isBold: false),
              ),
            ),
          ),
        Padding(
          padding: const EdgeInsets.all(20),
          child: DottedBorder(
            borderType: BorderType.RRect,
            radius: Radius.circular(12),
            padding: EdgeInsets.all(6),
            color: Colors.grey,
            strokeWidth: 3,
            child: GestureDetector(
              onTap: () async {
                CamPicker().showCamDialog(
                  context: context,
                  isRear: false,
                  callback: (File path) async {
                    if (path != null) {
                      if (listMediaUploadFilesModel.length >
                          AppConfig.totalUploadLimit - 1) {
                        showAlert(
                            msg: "Maximum file upload limit is " +
                                AppConfig.totalUploadLimit.toString() +
                                " files",
                            isToast: true);
                        return;
                      }
                      await APIViewModel().upload(
                        context: context,
                        apiState: APIState(
                            APIType.media_upload_file, this.runtimeType, null),
                        file: path,
                      );
                    }
                  },
                );
              },
              child: Container(
                height: 50,
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Icon(
                      Icons.attach_file,
                      color: MyTheme.gray4Color,
                      size: 30,
                    ),
                    Txt(
                        txt: "Add upto 5 files",
                        txtColor: MyTheme.gray4Color,
                        txtSize: MyTheme.txtSize,
                        txtAlign: TextAlign.center,
                        isBold: false),
                  ],
                ),
              ),
            ),
          ),
        ),
        Padding(
          padding: const EdgeInsets.only(left: 20, right: 20),
          child: MMBtn(
            txt: "Submit",
            width: getW(context),
            height: getHP(context, MyTheme.btnHpa),
            radius: 10,
            callback: () {
              if (opt.id == null) {
                showAlert(
                    msg: "Please choose ticket type from the list",
                    isToast: true);
                return;
              } else if (cmt.text.trim().length == 0) {
                showAlert(msg: "Please enter description", isToast: true);
                return;
              }
              List<String> listFileUrl = [];
              for (MediaUploadFilesModel model in listMediaUploadFilesModel) {
                listFileUrl.add(model.url);
              }

              APIViewModel().req<ResolutionAPIModel>(
                context: context,
                apiState: APIState(APIType.resolution, this.runtimeType, null),
                url: ResCfg.RES_POST_URL,
                reqType: ReqType.Post,
                param: {
                  "Description": 'Contact from App : ' + cmt.text.trim(),
                  "InitiatorId": userData.userModel.id,
                  "Remarks": "",
                  "ResolutionType": "Other",
                  "ServiceDate": DateTime.now().toString(),
                  "Status": 101,
                  "Title": opt.title,
                  "FileUrl": listFileUrl.join(','),
                  "UserId": userData.userModel.id,
                },
              );
            },
          ),
        ),
        SizedBox(height: 50),
      ],
    );
  }
}
