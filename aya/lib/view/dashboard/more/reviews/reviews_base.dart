import 'package:aitl/config/cfg/AppConfig.dart';
import 'package:aitl/config/theme/MyTheme.dart';
import 'package:aitl/controller/classes/DateFun.dart';
import 'package:aitl/data/model/dashboard/more/profile/UserRatingsModel.dart';
import 'package:aitl/view/dashboard/dashboard_base.dart';
import 'package:aitl/view/widgets/MyNetworkImage.dart';
import 'package:aitl/view/widgets/Txt.dart';
import 'package:aitl/view_model/helper/utils/UIHelper.dart';
import 'package:flutter/material.dart';

abstract class BaseReviewsStatefull<T extends StatefulWidget>
    extends BaseDashboard<T> {
  refreshData();

  drawUserRatingList(List<UserRatingsModel> listUserRating) {
    return Padding(
      padding: const EdgeInsets.only(bottom: 20),
      child: Container(
        child: Theme(
          data: MyTheme.refreshIndicatorTheme,
          child: RefreshIndicator(
            onRefresh: refreshData,
            child: ListView.builder(
              addAutomaticKeepAlives: true,
              cacheExtent: AppConfig.page_limit.toDouble(),
              scrollDirection: Axis.vertical,
              shrinkWrap: true,
              primary: true,
              itemCount: listUserRating.length,
              itemBuilder: (BuildContext context, int index) {
                return drawItem(listUserRating[index]);
              },
            ),
          ),
        ),
      ),
    );
  }

  drawItem(UserRatingsModel userRatingsModel) {
    return Padding(
      padding: const EdgeInsets.only(left: 10, right: 10, top: 20),
      child: ListTile(
        leading: CircleAvatar(
          radius: 30,
          backgroundColor: Colors.transparent,
          backgroundImage:
              MyNetworkImage.loadProfileImage(userRatingsModel.profileImageUrl),
        ),
        title: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Txt(
                txt: userRatingsModel.taskTitle,
                txtColor: MyTheme.gray4Color,
                txtSize: MyTheme.txtSize,
                txtAlign: TextAlign.center,
                isBold: false),
            SizedBox(height: 5),
            UIHelper().getStarRatingView(
              rate: userRatingsModel.rating.toInt(),
              reviews: null,
              starColor: MyTheme.brandColor,
              align: MainAxisAlignment.start,
            ),
            SizedBox(height: 5),
            drawLine(h: .1, colr: MyTheme.gray1Color),
          ],
        ),
      ),
    );
  }
}
