import 'package:aitl/config/server/APIAuthCfg.dart';
import 'package:aitl/config/server/APIProfileCfg.dart';
import 'package:aitl/config/theme/MyTheme.dart';
import 'package:aitl/controller/form_validator/UserProfileVal.dart';
import 'package:aitl/data/app_data/UserData.dart';
import 'package:aitl/data/db/DBMgr.dart';
import 'package:aitl/data/model/auth/profile/RegProfileAPIModel.dart';
import 'package:aitl/data/model/country_picker/ola_like_country_picker.dart';
import 'package:aitl/data/model/dashboard/more/profile/AboutAPIModel.dart';
import 'package:aitl/data/model/dashboard/more/profile/AboutModel.dart';
import 'package:aitl/data/model/dashboard/more/profile/UserPortFolioAPIModel.dart';
import 'package:aitl/data/model/dashboard/more/profile/UserPortfulioModel.dart';
import 'package:aitl/data/model/misc/media_upload/MediaUploadFilesAPIModel.dart';
import 'package:aitl/data/model/misc/media_upload/MediaUploadFilesModel.dart';
import 'package:aitl/data/network/ModelMgr.dart';
import 'package:aitl/data/network/NetworkMgr.dart';
import 'package:aitl/view/widgets/MMArrowBtn.dart';
import 'package:aitl/view/widgets/Txt.dart';
import 'package:aitl/view/widgets/dialog/ConfirmDialog.dart';
import 'package:aitl/view_model/api/api_view_model.dart';
import 'package:aitl/view_model/observer/APIStateProvider.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:google_maps_webservice/directions.dart';
import 'edit_profile_base.dart';

class EditProfilePage extends StatefulWidget {
  const EditProfilePage({Key key}) : super(key: key);
  @override
  State createState() => _EditProfilePageState();
}

class _EditProfilePageState extends BaseEditProfileStatefull<EditProfilePage>
    with APIStateListener {
  //  **************  app states start

  @override
  void onDetached() {
    try {
      log("app state = onDetached");
    } catch (e) {}
  }

  @override
  void onInactive() {
    try {
      log("app state = onInactive");
    } catch (e) {}
  }

  @override
  void onPaused() {
    try {
      log("app state = onPaused");
    } catch (e) {}
  }

  @override
  void onResumed() {
    try {
      log("app state = onResumed");
    } catch (e) {}
  }

  //  **************  app states end

  APIStateProvider _apiStateProvider;
  @override
  onAPIStateChanged(apiState, model) async {
    try {
      if (apiState.type == APIType.reg2 && apiState.cls == this.runtimeType) {
        if (model != null && mounted) {
          if (model.success) {
            try {
              await DBMgr.shared.setUserProfile(
                user: model.responseData.user,
              );
              await userData.setUserModel();
              showAlert(
                  msg: 'Profile updated successfully.',
                  isToast: true,
                  which: 1);
            } catch (e) {
              log(e.toString());
            }
          } else {
            try {
              if (mounted) {
                final err = model.messages.postUser[0].toString();
                showAlert(msg: err, isToast: true);
              }
            } catch (e) {
              log(e.toString());
            }
          }
        }
      }
      if (apiState.type == APIType.portfolio &&
          apiState.cls == this.runtimeType) {
        if (model != null && mounted) {
          if (model.success) {
            final userPortfulioModel =
                (model as UserPortFolioAPIModel).responseData.userPortfulio;
            listUrl = userPortfulioModel.portfulioItems.split('|');
            setState(() {});
          }
        }
      }
      if (apiState.type == APIType.about_skills &&
          apiState.cls == this.runtimeType) {
        if (model != null && mounted) {
          if (model.success) {
            aboutModel = (model as AboutAPIModel).responseData.about;
            setState(() {});
          }
        }
      }
      if (apiState.type == APIType.media_upload_file &&
          apiState.cls == this.runtimeType) {
        if (model != null && mounted) {
          if (model.success) {
            final MediaUploadFilesModel mediaModel =
                (model as MediaUploadFilesAPIModel).responseData.images[0];
            if (!listUrl.contains(mediaModel.url)) {
              listUrl.add(mediaModel.url);
            }
            isPortfolioUploaded = true;
            setState(() {});
          }
        }
      }
      if (apiState.type == APIType.portfolio_upload &&
          apiState.cls == this.runtimeType) {
        if (model != null && mounted) {
          if (model.success) {}
        }
      }
    } catch (e) {}
  }

  validate() {
    if (!UserProfileVal().isDOBOK(dob, MyTheme.redColor)) {
      return false;
    } else if (address == "" || address == "Search") {
      showAlert(msg: "Please pick your location", isToast: true);
      return false;
    }
    return true;
  }

  wsUpdateProfile() {
    try {
      if (validate()) {
        APIViewModel().req<RegProfileAPIModel>(
            context: context,
            apiState: APIState(APIType.reg2, this.runtimeType, null),
            url: APIAuthCfg.REG_PROFILE_PUT_URL,
            reqType: ReqType.Put,
            param: {
              "Address": address.replaceAll("Search", "").trim(),
              "BriefBio": bio.text.trim(),
              "CommunityId": userData.communityId,
              "DateofBirth": dob,
              "Email": email.text.trim(),
              "FirstName": fname.text.trim(),
              "Cohort": "",
              "Headline": headline.text.trim(),
              "Id": userData.userModel.id,
              "LastName": lname.text.trim(),
              "Latitude": cord.lat,
              "Longitude": cord.lng,
              "MobileNumber": mobile.text.trim(),
            });
      }
    } catch (e) {}
  }

  wsUploadPortfolio() async {
    try {
      if (listUrl.length > 0) {
        final portfulioItemsUrls = listUrl.join('|');
        await APIViewModel().req<UserPortFolioAPIModel>(
          context: context,
          apiState: APIState(APIType.portfolio_upload, this.runtimeType, null),
          url: APIProfileCFg.USER_PORTFOLIO_UPLOAD_URL,
          param: {
            "DocumentUrl": "",
            "Id": 0,
            "PortfulioItemsUrls": portfulioItemsUrls,
            "UserId": userData.userModel.id,
          },
          reqType: ReqType.Post,
        );
      }
    } catch (e) {}
  }

  wsGetAboutSkills() async {
    try {
      await APIViewModel().req<AboutAPIModel>(
        context: context,
        apiState: APIState(APIType.about_skills, this.runtimeType, null),
        url: APIProfileCFg.ABOUT_GET_URL,
        param: {"UserId": userData.userModel.id},
        reqType: ReqType.Get,
      );
    } catch (e) {}
  }

  @override
  void initState() {
    super.initState();
    appInit();
  }

  @override
  void dispose() {
    try {
      _apiStateProvider.unsubscribe(this);
      _apiStateProvider = null;
    } catch (e) {
      log(e.toString());
    }
    //
    fname.dispose();
    lname.dispose();
    headline.dispose();
    bio.dispose();
    email.dispose();
    mobile.dispose();
    //
    c = null;
    country = null;
    countryFlag = null;
    countryDialCode = null;
    countryName = null;
    dob = null;
    cord = null;
    address = null;
    aboutModel = null;
    listUrl = null;
    super.dispose();
  }

  appInit() async {
    try {
      _apiStateProvider = new APIStateProvider();
      _apiStateProvider.subscribe(this);
    } catch (e) {}
    try {
      c = CountryPicker(onCountrySelected: (Country country) {
        log(country);
        setState(() {
          this.country = country;
          countryName = country.name;
          countryFlag = country.flagUri;
          countryDialCode = country.dialCode;
        });
      });
    } catch (e) {}

    try {
      await APIViewModel().req<UserPortFolioAPIModel>(
        context: context,
        apiState: APIState(APIType.portfolio, this.runtimeType, null),
        url: APIProfileCFg.USER_PORTFOLIO_URL,
        param: {"UserId": userData.userModel.id},
        reqType: ReqType.Get,
      );
      wsGetAboutSkills();
    } catch (e) {}
    onUpdateInfo();
  }

  onUpdateInfo() {
    try {
      final user = userData.userModel;
      fname.text = user.firstName;
      lname.text = user.lastName;
      address = user.address;
      cord = Location(0, 0);
      headline.text = user.headline;
      bio.text = user.briefBio;
      email.text = user.email;
      mobile.text = user.mobileNumber;
      dob = user.dateofBirth;
    } catch (e) {}
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: MyTheme.bgColor,
        appBar: AppBar(
          elevation: MyTheme.appbarElevation,
          backgroundColor: MyTheme.bgColor,
          leading: IconButton(
              icon: Icon(Icons.arrow_back),
              onPressed: () {
                if (isPortfolioUploaded) {
                  Get.dialog(ConfirmDialog(
                      callback: () {
                        Get.back(result: true);
                      },
                      title: "Unsaved changes",
                      msg: "Do you want to discard the changes?"));
                } else {
                  Get.back();
                }
              }),
          title: Txt(
              txt: 'Edit profile',
              txtColor: MyTheme.appbarTxtColor,
              txtSize: MyTheme.appbarTitleFontSize,
              txtAlign: TextAlign.center,
              isBold: true),
          centerTitle: false,
          actions: [
            TextButton(
                onPressed: () async {
                  await wsUpdateProfile();
                  await wsUploadPortfolio();
                  isPortfolioUploaded = false;
                },
                child: Txt(
                  txt: "Save",
                  txtColor: MyTheme.gray5Color,
                  txtSize: MyTheme.txtSize,
                  txtAlign: TextAlign.start,
                  isBold: false,
                ))
          ],
        ),
        body: GestureDetector(
            onTap: () {
              FocusScope.of(context).requestFocus(new FocusNode());
            },
            child: drawLayout()),
      ),
    );
  }

  @override
  drawLayout() {
    return Container(
      color: Colors.white,
      child: ListView(
        shrinkWrap: true,
        primary: true,
        children: [
          drawGenInfo(),
          drawPvtInfo(),
          drawAdditionalInfo(),
          Padding(
            padding: const EdgeInsets.all(20),
            child: MMArrowBtn(
              txt: "Save",
              icon: null,
              height: getHP(context, MyTheme.btnHpa),
              width: MediaQuery.of(context).size.width,
              callback: () async {
                await wsUpdateProfile();
                await wsUploadPortfolio();
                isPortfolioUploaded = false;
              },
            ),
          ),
          drawPortFolio(this.runtimeType),
          drawAboutSkills(),
          SizedBox(height: 50),
        ],
      ),
    );
  }
}
