import 'package:aitl/config/server/APIProfileCfg.dart';
import 'package:aitl/config/theme/MyTheme.dart';
import 'package:aitl/data/app_data/UserData.dart';
import 'package:aitl/data/model/dashboard/more/profile/AboutAPIModel.dart';
import 'package:aitl/data/model/dashboard/more/profile/AboutModel.dart';
import 'package:aitl/data/network/ModelMgr.dart';
import 'package:aitl/data/network/NetworkMgr.dart';
import 'package:aitl/view/widgets/Txt.dart';
import 'package:aitl/view_model/api/api_view_model.dart';
import 'package:aitl/view_model/observer/APIStateProvider.dart';
import 'package:flutter/material.dart';
import 'add_skills_base.dart';

class AddSkillsPage extends StatefulWidget {
  final AboutModel aboutModel;
  final String title;
  final String hint;
  final String skills;
  AddSkillsPage({
    Key key,
    @required this.aboutModel,
    @required this.skills,
    @required this.title,
    @required this.hint,
  }) : super(key: key);
  @override
  State createState() => _AddSkillsPageState();
}

class _AddSkillsPageState extends BaseAddSkillsStatefull<AddSkillsPage>
    with APIStateListener {
  //  **************  app states start

  @override
  void onDetached() {
    try {
      log("app state = onDetached");
    } catch (e) {}
  }

  @override
  void onInactive() {
    try {
      log("app state = onInactive");
    } catch (e) {}
  }

  @override
  void onPaused() {
    try {
      log("app state = onPaused");
    } catch (e) {}
  }

  @override
  void onResumed() {
    try {
      log("app state = onResumed");
    } catch (e) {}
  }

  //  **************  app states end

  APIStateProvider _apiStateProvider;
  @override
  onAPIStateChanged(apiState, model) async {
    try {
      if (apiState.type == APIType.about_skills_post &&
          apiState.cls == this.runtimeType) {
        if (model != null && mounted) {
          if (model.success) {
            setState(() {});
          }
        }
      }
    } catch (e) {}
  }

  validate() {
    try {
      if (textField.text.trim().isEmpty) {
        showAlert(msg: 'Please enter some skill', isToast: true);
        return false;
      }
    } catch (e) {}
    return true;
  }

  wsAddSkill(bool isRemove) async {
    try {
      if (validate() || isRemove) {
        var transportation = widget.aboutModel.goAround;
        var languages = widget.aboutModel.languages;
        var education = widget.aboutModel.qualifications;
        var work = widget.aboutModel.experiences;
        var specialities = widget.aboutModel.whatIamlookingfor;
        var remark = widget.aboutModel.remarks;

        if (!isRemove)
          listSkills.add(textField.text.trim().replaceAll("|", ""));

        switch (widget.title) {
          case "Transportation":
            transportation = listSkills.join("|");
            textField.clear();
            break;
          case "Languages":
            languages = listSkills.join("|");
            textField.clear();
            break;
          case "Education":
            education = listSkills.join("|");
            textField.clear();
            break;
          case "Work":
            work = listSkills.join("|");
            textField.clear();
            break;
          case "Specialities":
            specialities = listSkills.join("|");
            textField.clear();
            break;
          default:
        }
        await APIViewModel().req<AboutAPIModel>(
          context: context,
          apiState: APIState(APIType.about_skills_post, this.runtimeType, null),
          url: APIProfileCFg.ABOUT_POST_URL,
          param: {
            "WhatIamlookingfor": specialities,
            "Languages": languages,
            "Qualifications": education,
            "Experiences": work,
            "GoAround": transportation,
            "Remarks": remark,
            "UserId": userData.userModel.id,
          },
          reqType: ReqType.Post,
        );
      }
    } catch (e) {}
  }

  @override
  void initState() {
    super.initState();
    appInit();
  }

  @override
  void dispose() {
    try {
      _apiStateProvider.unsubscribe(this);
      _apiStateProvider = null;
    } catch (e) {
      log(e.toString());
    }
    textField.dispose();
    listSkills = null;
    super.dispose();
  }

  appInit() async {
    try {
      _apiStateProvider = new APIStateProvider();
      _apiStateProvider.subscribe(this);
    } catch (e) {}
    try {
      listSkills = widget.skills.split("|");
      listSkills.remove("");
    } catch (e) {}
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
          elevation: MyTheme.appbarElevation,
          backgroundColor: MyTheme.bgColor,
          title: Txt(
              txt: widget.title,
              txtColor: MyTheme.appbarTxtColor,
              txtSize: MyTheme.appbarTitleFontSize,
              txtAlign: TextAlign.center,
              isBold: true),
          centerTitle: false,
        ),
        body: GestureDetector(
            onTap: () {
              FocusScope.of(context).requestFocus(new FocusNode());
            },
            child: drawLayout()),
      ),
    );
  }

  @override
  drawLayout() {
    return Container(
      child: ListView(
        shrinkWrap: true,
        primary: true,
        children: [
          drawAddSkillView(widget.title, widget.hint),
          drawSkillList(),
          SizedBox(height: 50),
        ],
      ),
    );
  }
}
