import 'package:aitl/config/theme/MyTheme.dart';
import 'package:aitl/mixin.dart';
import 'package:aitl/view/dashboard/more/noti/noti_settings_page.dart';
import 'package:aitl/view/dashboard/more/profile/edit_profile_page.dart';
import 'package:aitl/view/widgets/Txt.dart';
import 'package:aitl/view_model/helper/utils/UIHelper.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class SettingsPage extends StatelessWidget with Mixin, UIHelper {
  SettingsPage({Key key}) : super(key: key);

  final List<Map<String, dynamic>> listMore = [
    {"title": "Edit account", "route": () => EditProfilePage()},
    {"title": "Notification settings", "route": () => NotiSettingsPage()},
  ];

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: MyTheme.bgColor,
        appBar: AppBar(
          backgroundColor: MyTheme.bgColor,
          elevation: MyTheme.appbarElevation,
          title: Txt(
              txt: 'Settings',
              txtColor: Colors.black,
              txtSize: MyTheme.appbarTitleFontSize,
              txtAlign: TextAlign.center,
              isBold: false),
          centerTitle: false,
        ),
        body: drawLayout(),
      ),
    );
  }

  drawLayout() {
    return Padding(
      padding: const EdgeInsets.only(top: 10),
      child: Container(
        child: ListView.builder(
          itemCount: listMore.length,
          itemBuilder: (context, index) {
            Map<String, dynamic> mapMore = listMore[index];
            return GestureDetector(
              onTap: () async {
                if (mapMore['route'] != null) {
                  Get.to(mapMore['route']);
                }
              },
              child: Container(
                color: Colors.transparent,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    SizedBox(height: 20),
                    Padding(
                      padding: const EdgeInsets.only(left: 20, right: 20),
                      child: Row(
                        children: [
                          Expanded(
                            child: Txt(
                                txt: mapMore['title'].toString(),
                                txtColor: MyTheme.gray5Color,
                                txtSize: MyTheme.txtSize,
                                txtAlign: TextAlign.start,
                                isBold: false),
                          ),
                          Icon(
                            Icons.arrow_forward_ios,
                            color: MyTheme.gray3Color,
                            size: 20,
                          ),
                        ],
                      ),
                    ),
                    SizedBox(height: 30),
                    drawLine(),
                    SizedBox(height: 10),
                  ],
                ),
              ),
            );
          },
        ),
      ),
    );
  }
}
