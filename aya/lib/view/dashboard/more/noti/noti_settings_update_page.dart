import 'package:aitl/config/server/APINotiCfg.dart';
import 'package:aitl/config/theme/MyTheme.dart';
import 'package:aitl/data/app_data/UserData.dart';
import 'package:aitl/data/model/dashboard/more/noti/UserNotificationSettingAPIModel.dart';
import 'package:aitl/data/model/dashboard/more/noti/UserNotificationSettingModel.dart';
import 'package:aitl/data/network/ModelMgr.dart';
import 'package:aitl/data/network/NetworkMgr.dart';
import 'package:aitl/view/widgets/CustomSwitch.dart';
import 'package:aitl/view/widgets/Txt.dart';
import 'package:aitl/view_model/api/api_view_model.dart';
import 'package:aitl/view_model/observer/APIStateProvider.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'noti_settings_base.dart';

class NotiSettingsUpdatePage extends StatefulWidget {
  final index;
  final title;
  final heading;
  final bool isEmail;
  final bool isSms;
  final bool isPush;
  final UserNotificationSettingModel userNotificationSettingModel;
  NotiSettingsUpdatePage({
    Key key,
    @required this.index,
    @required this.title,
    @required this.heading,
    @required this.isEmail,
    @required this.isSms,
    @required this.isPush,
    @required this.userNotificationSettingModel,
  }) : super(key: key);
  @override
  State createState() => _NotiSettingsUpdatePageState();
}

class _NotiSettingsUpdatePageState
    extends NotiSettingsStatefull<NotiSettingsUpdatePage>
    with APIStateListener {
  bool isEmail = false;
  bool isSms = false;
  bool isPush = false;
  bool isUpdated = false;
  //  **************  app states start

  @override
  void onDetached() {
    try {
      log("app state = onDetached");
    } catch (e) {}
  }

  @override
  void onInactive() {
    try {
      log("app state = onInactive");
    } catch (e) {}
  }

  @override
  void onPaused() {
    try {
      log("app state = onPaused");
    } catch (e) {}
  }

  @override
  void onResumed() {
    try {
      log("app state = onResumed");
    } catch (e) {}
  }

  //  **************  app states end

  APIStateProvider _apiStateProvider;
  @override
  onAPIStateChanged(apiState, model) async {
    try {
      if (apiState.type == APIType.user_noti_settings_put &&
          apiState.cls == this.runtimeType) {
        if (model != null && mounted) {
          if (model.success) {
            isUpdated = true;
            setState(() {});
          }
        }
      }
    } catch (e) {}
  }

  wsOnUpdateNotiSettings() async {
    try {
      //final enumNotiSettings e = enumNotiSettings.values[widget.index];
      await APIViewModel().req<UserNotificationSettingAPIModel>(
        context: context,
        apiState:
            APIState(APIType.user_noti_settings_put, this.runtimeType, null),
        url: APINotiCfg.NOTI_SETTINGS_PUT_URL,
        param: {
          "IsHelpFullEmail":
              (widget.index == enumNotiSettings.HelpfulInfo.index)
                  ? isEmail
                  : widget.userNotificationSettingModel.isHelpFullEmail,
          "IsHelpFullNotification":
              (widget.index == enumNotiSettings.HelpfulInfo.index)
                  ? isPush
                  : widget.userNotificationSettingModel.isHelpFullNotification,
          "IsShohokariAlertEmail":
              (widget.index == enumNotiSettings.HeroTaskerAlerts.index)
                  ? isEmail
                  : widget.userNotificationSettingModel.isShohokariAlertEmail,
          "IsShohokariAlertNotification":
              (widget.index == enumNotiSettings.HeroTaskerAlerts.index)
                  ? isPush
                  : widget.userNotificationSettingModel
                      .isShohokariAlertNotification,
          "IsTaskRecomendationEmail": (widget.index ==
                  enumNotiSettings.TaskRecommendations.index)
              ? isEmail
              : widget.userNotificationSettingModel.isTaskRecomendationEmail,
          "IsTaskRecomendationNotification":
              (widget.index == enumNotiSettings.TaskRecommendations.index)
                  ? isPush
                  : widget.userNotificationSettingModel
                      .isTaskRecomendationNotification,
          "IsTaskReminderEmail":
              (widget.index == enumNotiSettings.TaskReminders.index)
                  ? isEmail
                  : widget.userNotificationSettingModel.isTaskReminderEmail,
          "IsTaskReminderNotification": (widget.index ==
                  enumNotiSettings.TaskReminders.index)
              ? isPush
              : widget.userNotificationSettingModel.isTaskReminderNotification,
          "IsTaskReminderSMS":
              (widget.index == enumNotiSettings.TaskReminders.index)
                  ? isSms
                  : widget.userNotificationSettingModel.isTaskReminderSMS,
          "IsTaskUpdateEmail":
              (widget.index == enumNotiSettings.TaskUpdates.index)
                  ? isEmail
                  : widget.userNotificationSettingModel.isTaskUpdateEmail,
          "IsTaskUpdateNotification": (widget.index ==
                  enumNotiSettings.TaskUpdates.index)
              ? isPush
              : widget.userNotificationSettingModel.isTaskUpdateNotification,
          "IsTaskUpdateSMS":
              (widget.index == enumNotiSettings.TaskUpdates.index)
                  ? isSms
                  : widget.userNotificationSettingModel.isTaskUpdateSMS,
          "IsTransactionalEmail":
              (widget.index == enumNotiSettings.Transactional.index)
                  ? isEmail
                  : widget.userNotificationSettingModel.isTransactionalEmail,
          "IsTransactionalNotification": (widget.index ==
                  enumNotiSettings.Transactional.index)
              ? isPush
              : widget.userNotificationSettingModel.isTransactionalNotification,
          "IsTransactionalSMS":
              (widget.index == enumNotiSettings.Transactional.index)
                  ? isSms
                  : widget.userNotificationSettingModel.isTransactionalSMS,
          "IsUpdateAndNewsLetterEmail": false,
          "IsUpdateAndNewsLetterNotification":
              (widget.index == enumNotiSettings.UpdateNewsLettters.index)
                  ? isPush
                  : widget.userNotificationSettingModel
                      .isUpdateAndNewsLetterNotification,
          "UserId": userData.userModel.id,
        },
        reqType: ReqType.Put,
      );
    } catch (e) {}
  }

  @override
  void initState() {
    super.initState();
    appInit();
  }

  @override
  void dispose() {
    try {
      _apiStateProvider.unsubscribe(this);
      _apiStateProvider = null;
    } catch (e) {
      log(e.toString());
    }
    super.dispose();
  }

  appInit() async {
    try {
      _apiStateProvider = new APIStateProvider();
      _apiStateProvider.subscribe(this);
    } catch (e) {}
    try {
      isEmail = (widget.isEmail != null) ? widget.isEmail : false;
      isSms = (widget.isSms != null) ? widget.isSms : false;
      isPush = (widget.isPush != null) ? widget.isPush : false;
    } catch (e) {}
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: MyTheme.bgColor,
        appBar: AppBar(
          elevation: MyTheme.appbarElevation,
          backgroundColor: MyTheme.bgColor,
          leading: IconButton(
              icon: Icon(Icons.arrow_back_ios),
              onPressed: () {
                Get.back(result: isUpdated);
              }),
          title: Txt(
              txt: widget.title,
              txtColor: MyTheme.appbarTxtColor,
              txtSize: MyTheme.appbarTitleFontSize,
              txtAlign: TextAlign.center,
              isBold: true),
          centerTitle: false,
        ),
        body: GestureDetector(
            onTap: () {
              FocusScope.of(context).requestFocus(new FocusNode());
            },
            child: drawLayout()),
      ),
    );
  }

  @override
  drawLayout() {
    return Container(
      child: SingleChildScrollView(
        primary: true,
        child: Column(
          children: [
            Padding(
              padding: const EdgeInsets.only(top: 20, left: 20, right: 20),
              child: Txt(
                txt: widget.heading,
                txtColor: MyTheme.gray4Color,
                txtSize: MyTheme.txtSize,
                txtAlign: TextAlign.start,
                isBold: false,
              ),
            ),
            SizedBox(height: 20),
            (widget.isEmail != null)
                ? ListTile(
                    title: Txt(
                      txt: "Email",
                      txtColor: MyTheme.gray4Color,
                      txtSize: MyTheme.txtSize,
                      txtAlign: TextAlign.start,
                      isBold: false,
                    ),
                    trailing: CustomSwitch(
                      value: isEmail,
                      onChanged: (bool val) {
                        isEmail = val;
                        wsOnUpdateNotiSettings();
                      },
                    ),
                  )
                : SizedBox(),
            (widget.isSms != null)
                ? ListTile(
                    title: Txt(
                      txt: "SMS",
                      txtColor: MyTheme.gray4Color,
                      txtSize: MyTheme.txtSize,
                      txtAlign: TextAlign.start,
                      isBold: false,
                    ),
                    trailing: CustomSwitch(
                      value: isSms,
                      onChanged: (bool val) {
                        isSms = val;
                        wsOnUpdateNotiSettings();
                      },
                    ),
                  )
                : SizedBox(),
            (widget.isPush != null)
                ? ListTile(
                    title: Txt(
                      txt: "Push notifications",
                      txtColor: MyTheme.gray4Color,
                      txtSize: MyTheme.txtSize,
                      txtAlign: TextAlign.start,
                      isBold: false,
                    ),
                    trailing: CustomSwitch(
                      value: isPush,
                      onChanged: (bool val) {
                        isPush = val;
                        wsOnUpdateNotiSettings();
                      },
                    ),
                  )
                : SizedBox()
          ],
        ),
      ),
    );
  }
}
