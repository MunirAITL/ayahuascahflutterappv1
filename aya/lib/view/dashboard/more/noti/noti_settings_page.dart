import 'package:aitl/config/server/APINotiCfg.dart';
import 'package:aitl/config/theme/MyTheme.dart';
import 'package:aitl/data/app_data/CommonData.dart';
import 'package:aitl/data/app_data/UserData.dart';
import 'package:aitl/data/model/dashboard/more/noti/NotiTestAPIModel.dart';
import 'package:aitl/data/model/dashboard/more/noti/UserNotificationSettingAPIModel.dart';
import 'package:aitl/data/model/dashboard/more/noti/UserNotificationSettingModel.dart';
import 'package:aitl/data/model/misc/CommonAPIModel.dart';
import 'package:aitl/data/network/ModelMgr.dart';
import 'package:aitl/data/network/NetworkMgr.dart';
import 'package:aitl/view/dashboard/more/noti/noti_settings_update_page.dart';
import 'package:aitl/view/widgets/Txt.dart';
import 'package:aitl/view_model/api/api_view_model.dart';
import 'package:aitl/view_model/observer/APIStateProvider.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'noti_settings_base.dart';

class NotiSettingsPage extends StatefulWidget {
  const NotiSettingsPage({Key key}) : super(key: key);
  @override
  State createState() => _NotiSettingsPageState();
}

class _NotiSettingsPageState extends NotiSettingsStatefull<NotiSettingsPage>
    with APIStateListener {
  UserNotificationSettingModel userNotificationSettingModel;

  //  **************  app states start

  @override
  void onDetached() {
    try {
      log("app state = onDetached");
    } catch (e) {}
  }

  @override
  void onInactive() {
    try {
      log("app state = onInactive");
    } catch (e) {}
  }

  @override
  void onPaused() {
    try {
      log("app state = onPaused");
    } catch (e) {}
  }

  @override
  void onResumed() {
    try {
      log("app state = onResumed");
    } catch (e) {}
  }

  //  **************  app states end

  APIStateProvider _apiStateProvider;
  @override
  onAPIStateChanged(apiState, model) async {
    try {
      if (apiState.type == APIType.user_noti_settings_get &&
          apiState.cls == this.runtimeType) {
        if (model != null && mounted) {
          if (model.success) {
            userNotificationSettingModel =
                (model as UserNotificationSettingAPIModel)
                    .responseData
                    .userNotificationSetting;
            setState(() {});
          }
        }
      }
    } catch (e) {}
  }

  wsGetNotiSettings() async {
    try {
      await APIViewModel().req<UserNotificationSettingAPIModel>(
        context: context,
        apiState:
            APIState(APIType.user_noti_settings_get, this.runtimeType, null),
        url: APINotiCfg.NOTI_SETTINGS_GET_URL,
        param: {"UserId": userData.userModel.id},
        reqType: ReqType.Get,
      );
    } catch (e) {}
  }

  wsTestIt() async {
    try {
      await APIViewModel().req<NotiTestAPIModel>(
          context: context,
          url: APINotiCfg.NOTI_SETTINGS_TEST_URL
              .replaceAll("#userId#", userData.userModel.id.toString()),
          reqType: ReqType.Get,
          callback: (model) {
            comData.isNotiTestPage = true;
            //print(model);
            /*try {
              final notification = model.responseData.notification;
              showAlert(
                  msg: notification.message + "\n\n" + notification.description,
                  isToast: true);
            } catch (e) {}*/
          });
    } catch (e) {}
  }

  @override
  void initState() {
    super.initState();
    appInit();
  }

  @override
  void dispose() {
    try {
      _apiStateProvider.unsubscribe(this);
      _apiStateProvider = null;
    } catch (e) {
      log(e.toString());
    }
    userNotificationSettingModel = null;
    comData.isNotiTestPage = false;
    super.dispose();
  }

  appInit() async {
    try {
      _apiStateProvider = new APIStateProvider();
      _apiStateProvider.subscribe(this);
    } catch (e) {}

    wsGetNotiSettings();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: MyTheme.bgColor,
        appBar: AppBar(
          elevation: MyTheme.appbarElevation,
          backgroundColor: MyTheme.bgColor,
          title: Txt(
              txt: 'Notification settings',
              txtColor: MyTheme.appbarTxtColor,
              txtSize: MyTheme.appbarTitleFontSize,
              txtAlign: TextAlign.center,
              isBold: true),
          centerTitle: false,
        ),
        body: GestureDetector(
            onTap: () {
              FocusScope.of(context).requestFocus(new FocusNode());
            },
            child: drawLayout()),
      ),
    );
  }

  @override
  drawLayout() {
    return Container(
      child: SingleChildScrollView(
        primary: true,
        child: Column(
          children: [
            drawTest(),
            drawList(),
          ],
        ),
      ),
    );
  }

  drawTest() {
    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Padding(
            padding: const EdgeInsets.only(left: 20, right: 20),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Txt(
                  txt: "Is it working?",
                  txtColor: MyTheme.gray4Color,
                  txtSize: MyTheme.txtSize,
                  txtAlign: TextAlign.center,
                  isBold: false,
                ),
                TextButton(
                    onPressed: () {
                      wsTestIt();
                    },
                    child: Txt(
                      txt: "Test it",
                      txtColor: MyTheme.blueColor.withOpacity(.7),
                      txtSize: MyTheme.txtSize,
                      txtAlign: TextAlign.start,
                      isBold: false,
                    )),
              ],
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(left: 20, right: 20),
            child: Txt(
              txt:
                  "Make sure you're actually getting those all important push notifications.",
              txtColor: MyTheme.gray4Color,
              txtSize: MyTheme.txtSize,
              txtAlign: TextAlign.start,
              isBold: false,
            ),
          ),
          SizedBox(height: 10),
          drawLine(h: 1),
          SizedBox(height: 10),
          Padding(
            padding: const EdgeInsets.only(left: 20, right: 20),
            child: Txt(
              txt:
                  "Your notifications can be updated at any time via the options below",
              txtColor: MyTheme.gray4Color,
              txtSize: MyTheme.txtSize,
              txtAlign: TextAlign.start,
              isBold: false,
            ),
          ),
          SizedBox(height: 10),
          drawLine(h: 1),
        ],
      ),
    );
  }

  drawList() {
    if (userNotificationSettingModel == null) return SizedBox();
    return ListView.builder(
      shrinkWrap: true,
      primary: false,
      itemCount: listItems.length,
      itemBuilder: (context, i) {
        var item = '';
        var title = '';
        var heading = '';
        bool isEmail;
        bool isSms;
        bool isPush;
        switch (enumNotiSettings.values[i]) {
          case enumNotiSettings.Transactional: //  Transactional
            title = "Transactional";
            heading =
                "You will always receive important notifications about any payments, cancellations and your account.";
            isEmail = userNotificationSettingModel.isTransactionalEmail;
            isSms = userNotificationSettingModel.isTransactionalSMS;
            isPush = userNotificationSettingModel.isTransactionalNotification;
            item = _makeNotiString(isEmail, isSms, isPush);
            break;
          case enumNotiSettings.TaskUpdates: //  Task updates
            title = "Task updates";
            heading =
                "Receive updates on any new comments, private messages, offers and reviews.";
            isEmail = userNotificationSettingModel.isTaskUpdateEmail;
            isSms = userNotificationSettingModel.isTaskUpdateSMS;
            isPush = userNotificationSettingModel.isTaskUpdateNotification;
            item = _makeNotiString(isEmail, isSms, isPush);
            break;
          case enumNotiSettings.TaskReminders: //  Task reminders
            title = "Task reminders";
            heading =
                "Friendly reminders if you've forgotten to accept an offer, release a payment or leave a review.";
            isEmail = userNotificationSettingModel.isTaskReminderEmail;
            isSms = userNotificationSettingModel.isTaskReminderSMS;
            isPush = userNotificationSettingModel.isTaskReminderNotification;
            item = _makeNotiString(isEmail, isSms, isPush);
            break;
          case enumNotiSettings.HeroTaskerAlerts: //  HeroTasker alerts
            title = "HeroTasker alerts";
            heading =
                "Once you've set up your HeroTasker Alerts, you'll be instantly notified when a task is posted that matches your requirements.";
            isEmail = userNotificationSettingModel.isShohokariAlertEmail;
            isPush = userNotificationSettingModel.isShohokariAlertNotification;
            item = _makeNotiString(isEmail, false, isPush);
            break;
          case enumNotiSettings.TaskRecommendations: //  Task recommendations
            title = "Task recommendations";
            heading =
                "Receive recommendations and be inspired by tasks close to you.";
            isEmail = userNotificationSettingModel.isTaskRecomendationEmail;
            isPush =
                userNotificationSettingModel.isTaskRecomendationNotification;
            item = _makeNotiString(isEmail, false, isPush);
            break;
          case enumNotiSettings.HelpfulInfo: //  Helpful information
            title = "Helpful information";
            heading = "";
            isEmail = userNotificationSettingModel.isHelpFullEmail;
            isPush = userNotificationSettingModel.isHelpFullNotification;
            item = _makeNotiString(isEmail, false, isPush);
            break;
          case enumNotiSettings.UpdateNewsLettters: //  Updates & newsletters
            title = "Updates & newsletters";
            heading = "";
            heading =
                "Be the first to hear about new features and exciting updates on HeroTasker.";
            isPush =
                userNotificationSettingModel.isUpdateAndNewsLetterNotification;
            item = _makeNotiString(false, false, isPush);
            break;
          default:
        }
        return GestureDetector(
          onTap: () {
            Get.to(() => NotiSettingsUpdatePage(
                    index: i,
                    title: title,
                    heading: heading,
                    isEmail: isEmail,
                    isSms: isSms,
                    isPush: isPush,
                    userNotificationSettingModel: userNotificationSettingModel))
                .then((value) {
              if (value) wsGetNotiSettings();
            });
          },
          child: Container(
            color: Colors.transparent,
            child: Column(
              children: [
                Padding(
                  padding: const EdgeInsets.only(
                      left: 20, right: 10, top: 20, bottom: 20),
                  child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Txt(
                          txt: listItems[i],
                          txtColor: MyTheme.gray5Color,
                          txtSize: MyTheme.txtSize,
                          txtAlign: TextAlign.start,
                          isBold: false,
                        ),
                        Expanded(
                          child: Txt(
                            txt: item,
                            txtColor: MyTheme.gray4Color,
                            txtSize: MyTheme.txtSize - .2,
                            txtAlign: TextAlign.end,
                            isBold: false,
                          ),
                        ),
                        SizedBox(width: 5),
                        Icon(
                          Icons.arrow_forward_ios,
                          color: MyTheme.gray3Color,
                          size: 20,
                        ),
                      ]),
                ),
                drawLine(),
              ],
            ),
          ),
        );
      },
    );
  }

  _makeNotiString(isEmail, isSms, isNoti) {
    String item = "";
    try {
      if (isEmail) item = "EMAIL, ";
      if (isSms) item += "SMS, ";
      if (isNoti) item += "PUSH, ";
      if (item.endsWith(", ")) {
        var pos = item.lastIndexOf(',');
        item = (pos != -1) ? item.substring(0, pos) : item;
      }
    } catch (e) {}
    return item;
  }
}
