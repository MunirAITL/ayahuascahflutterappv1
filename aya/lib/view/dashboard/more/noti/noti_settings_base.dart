import 'package:aitl/view/dashboard/dashboard_base.dart';
import 'package:flutter/material.dart';

enum enumNotiSettings {
  Transactional,
  TaskUpdates,
  TaskReminders,
  HeroTaskerAlerts,
  TaskRecommendations,
  HelpfulInfo,
  UpdateNewsLettters,
}

abstract class NotiSettingsStatefull<T extends StatefulWidget>
    extends BaseDashboard<T> {
  List<String> listItems = [
    "Transactional",
    "Task updates",
    "Task reminders",
    "HeroTasker alerts",
    "Task recommendations",
    "Helpful information",
    "Updates & newsletters",
  ];
}
