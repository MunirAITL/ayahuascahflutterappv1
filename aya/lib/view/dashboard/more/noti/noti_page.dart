import 'package:aitl/config/server/APINotiCfg.dart';
import 'package:aitl/config/theme/MyTheme.dart';
import 'package:aitl/data/app_data/UserData.dart';
import 'package:aitl/data/model/dashboard/more/noti/NotiAPIModel.dart';
import 'package:aitl/data/model/dashboard/more/noti/NotiModel.dart';
import 'package:aitl/data/network/ModelMgr.dart';
import 'package:aitl/data/network/NetworkMgr.dart';
import 'package:aitl/view/widgets/Txt.dart';
import 'package:aitl/view_model/api/api_view_model.dart';
import 'package:aitl/view_model/observer/APIStateProvider.dart';
import 'package:flutter/material.dart';
import '../../db_base.dart';

class NotiPage extends StatefulWidget {
  final int notificationId;
  final String title, body, webUrl;
  const NotiPage(
      {Key key, this.notificationId, this.title, this.body, this.webUrl})
      : super(key: key);
  @override
  State createState() => _NotiPageState();
}

class _NotiPageState extends BaseDashboardMoreStatefull<NotiPage>
    with APIStateListener {
  List<NotiModel> listNoti = [];

//  **************  app states start

  @override
  void onDetached() {
    try {
      log("app state = onDetached");
    } catch (e) {}
  }

  @override
  void onInactive() {
    try {
      log("app state = onInactive");
    } catch (e) {}
  }

  @override
  void onPaused() {
    try {
      log("app state = onPaused");
    } catch (e) {}
  }

  @override
  void onResumed() {
    try {
      log("app state = onResumed");
    } catch (e) {}
  }

  //  **************  app states end

  Future<void> refreshData() async {
    try {
      await APIViewModel().req<NotiAPIModel>(
          context: context,
          apiState: APIState(APIType.get_noti, this.runtimeType, null),
          url: APINotiCfg.NOTI_GET_URL,
          param: {"UserId": userData.userModel.id},
          reqType: ReqType.Get);
    } catch (e) {}
  }

  APIStateProvider _apiStateProvider;
  @override
  onAPIStateChanged(apiState, model) async {
    try {
      if (apiState.type == APIType.get_noti &&
          apiState.cls == this.runtimeType) {
        if (model != null && mounted) {
          if (model.success) {
            listNoti = (model as NotiAPIModel).responseData.notifications;
            setState(() {});
          }
        }
      }
    } catch (e) {}
  }

  @override
  void initState() {
    super.initState();
    appInit();
  }

  @override
  void dispose() {
    try {
      _apiStateProvider.unsubscribe(this);
      _apiStateProvider = null;
    } catch (e) {
      log(e.toString());
    }
    super.dispose();
  }

  appInit() async {
    try {
      _apiStateProvider = new APIStateProvider();
      _apiStateProvider.subscribe(this);
    } catch (e) {}
    refreshData();
    try {
      await APIViewModel().req<NotiAPIModel>(
          context: context,
          url: APINotiCfg.NOTI_PUT_URL,
          param: {"UserId": userData.userModel.id},
          reqType: ReqType.Put,
          callback: (model) {});
    } catch (e) {}
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: MyTheme.bgColor,
        appBar: AppBar(
          elevation: MyTheme.appbarElevation,
          backgroundColor: MyTheme.bgColor,
          title: Txt(
              txt: 'Notifications',
              txtColor: MyTheme.appbarTxtColor,
              txtSize: MyTheme.appbarTitleFontSize,
              txtAlign: TextAlign.center,
              isBold: true),
          centerTitle: false,
        ),
        body: GestureDetector(
            onTap: () {
              FocusScope.of(context).requestFocus(new FocusNode());
            },
            child: drawLayout()),
      ),
    );
  }

  drawLayout() {
    return Container(
      child: Theme(
        data: MyTheme.refreshIndicatorTheme,
        child: RefreshIndicator(
          onRefresh: refreshData,
          child: SingleChildScrollView(
            primary: true,
            child: Expanded(
              child: Container(), //drawNotiView(listNoti, true),
            ),
          ),
        ),
      ),
    );
  }
}
