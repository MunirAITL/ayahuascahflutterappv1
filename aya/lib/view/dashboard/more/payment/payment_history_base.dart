import 'package:aitl/config/cfg/AppConfig.dart';
import 'package:aitl/config/theme/MyTheme.dart';
import 'package:aitl/controller/classes/DateFun.dart';
import 'package:aitl/data/model/dashboard/more/payment/history/TaskPaymentsModel.dart';
import 'package:aitl/view/dashboard/dashboard_base.dart';
import 'package:aitl/view/widgets/DropDownWidget.dart';
import 'package:aitl/view/widgets/MyNetworkImage.dart';
import 'package:aitl/view/widgets/PriceBox.dart';
import 'package:aitl/view/widgets/SwitchView.dart';
import 'package:aitl/view/widgets/Txt.dart';
import 'package:aitl/view_model/helper/utils/DownloadFile.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:jiffy/jiffy.dart';

//  @PaymentsEarnedFragment
//  @PaymentsOutgoingFragment
abstract class PaymentHistoryStatefull<T extends StatefulWidget>
    extends BaseDashboard<T> {
  List<TaskPaymentsModel> taskPaymentListCurrentQuarter = [];
  List<TaskPaymentsModel> taskPaymentListLastQuarter = [];
  List<TaskPaymentsModel> taskPaymentListCurrentYear = [];
  List<TaskPaymentsModel> taskPaymentListLastYear = [];
  List<TaskPaymentsModel> taskAllTimes = [];
  List<TaskPaymentsModel> listPaymentsModel = [];
  List<TaskPaymentsModel> listPaymentsModel2 = [];

  final listDropdown = [
    DateFun.DT_CURRENT_QUARTER,
    DateFun.DT_LAST_QUARTER,
    DateFun.DT_CURRENT_FINANCIAL_YEAR,
    DateFun.DT_LAST_FINANCIAL_YEAR,
    DateFun.DT_ALL_TIME,
  ];
  String dropdownEarnedValue = DateFun.DT_ALL_TIME;
  String dropdownOutgoingValue = DateFun.DT_ALL_TIME;

  bool isEarned = true;
  double netEarning = 0;

  double earningAll = 0.0;
  double earningCurrentQuarter = 0.0;
  double earningLastQuarter = 0.0;
  double earningCurrentYear = 0.0;
  double earningLastYear = 0.0;

  refreshData();

  double calculation(String val) {
    earningAll = 0.0;
    earningCurrentQuarter = 0.0;
    earningLastQuarter = 0.0;
    earningCurrentYear = 0.0;
    earningLastYear = 0.0;
    taskPaymentListCurrentQuarter = [];
    taskPaymentListLastQuarter = [];
    taskPaymentListCurrentYear = [];
    taskPaymentListLastYear = [];
    taskAllTimes = [];

    for (var payModel in listPaymentsModel2) {
      if (payModel.transactionType == ((isEarned) ? "Receipt" : "Payment")) {
        if (val != DateFun.DT_ALL_TIME) {
          final creationDate = DateFun.getPaymentDate(payModel.creationDate);
          if (creationDate == DateFun.DT_CURRENT_QUARTER) {
            earningCurrentQuarter += payModel.paymentAmount;
            taskPaymentListCurrentQuarter.add(payModel);
            taskPaymentListCurrentYear.add(payModel);
          }
          if (creationDate == DateFun.DT_LAST_QUARTER) {
            earningLastQuarter += payModel.paymentAmount;
            taskPaymentListLastQuarter.add(payModel);
            taskPaymentListCurrentYear.add(payModel);
          }
          if (creationDate == DateFun.DT_CURRENT_FINANCIAL_YEAR ||
              creationDate == DateFun.DT_CURRENT_QUARTER ||
              creationDate == DateFun.DT_LAST_QUARTER) {
            earningCurrentYear += payModel.paymentAmount;
            taskPaymentListCurrentYear.add(payModel);
          }
          if (creationDate == DateFun.DT_LAST_FINANCIAL_YEAR) {
            earningLastYear += payModel.paymentAmount;
            taskPaymentListLastYear.add(payModel);
          }
        } else {
          taskAllTimes.add(payModel);
        }

        earningAll += payModel.paymentAmount;
      }
    }

    if (val == DateFun.DT_CURRENT_QUARTER) {
      return earningCurrentQuarter;
    } else if (val == DateFun.DT_LAST_QUARTER) {
      return earningLastQuarter;
    } else if (val == DateFun.DT_CURRENT_FINANCIAL_YEAR) {
      return earningCurrentYear;
    } else if (val == DateFun.DT_LAST_FINANCIAL_YEAR) {
      return earningLastYear;
    } else {
      return earningAll;
    }
  }

  drawSwitchView() {
    return Padding(
      padding: const EdgeInsets.all(10),
      child: Container(
        alignment: Alignment.center,
        width: double.infinity,
        child: Center(
          child: Card(
              elevation: 10,
              child: Padding(
                padding: const EdgeInsets.all(10),
                child: SwitchView(
                  onTxt: " Earned ",
                  offTxt: " Outgoing ",
                  value: isEarned,
                  bgColorOn: MyTheme.heroTheamColors,
                  bgColorOff: Colors.white,
                  txtColorOn: Colors.white,
                  txtColorOff: MyTheme.heroTheamColors,
                  onChanged: (value) {
                    isEarned = value;
                    netEarning = calculation((isEarned)
                        ? dropdownEarnedValue
                        : dropdownOutgoingValue);
                    setState(() {});
                    //callback((isSwitch) ? _companyName.text.trim() : '');
                  },
                ),
              )),
        ),
      ),
    );
  }

  drawShowingDropDown() {
    return Padding(
      padding: const EdgeInsets.only(top: 10, bottom: 10, left: 20, right: 20),
      child: Container(
        child: Row(
          children: [
            Row(
              children: [
                Icon(
                  Icons.calendar_today_outlined,
                  color: MyTheme.gray4Color,
                  size: 20,
                ),
                SizedBox(width: 10),
                Txt(
                    txt: 'Showing',
                    txtColor: MyTheme.gray5Color,
                    txtSize: MyTheme.txtSize,
                    txtAlign: TextAlign.start,
                    isBold: false),
              ],
            ),
            SizedBox(width: 10),
            Expanded(
              child: DropDownWidget(
                  list: listDropdown,
                  dropdownValue:
                      (isEarned) ? dropdownEarnedValue : dropdownOutgoingValue,
                  isExpanded: true,
                  txtColor: MyTheme.gray4Color,
                  callback: (val) {
                    (isEarned)
                        ? dropdownEarnedValue = val
                        : dropdownOutgoingValue = val;
                    netEarning = calculation(val);
                    setState(() {});
                  }),
            )
          ],
        ),
      ),
    );
  }

  drawDownload() {
    return Padding(
      padding: const EdgeInsets.only(top: 10),
      child: Container(
        color: MyTheme.gray2Color,
        width: getW(context),
        child: Column(
          children: [
            Padding(
              padding: const EdgeInsets.all(10),
              child: Txt(
                  txt: Jiffy(DateTime.now()).format("dd-MMM-yyyy"),
                  txtColor: MyTheme.gray4Color,
                  txtSize: MyTheme.txtSize,
                  txtAlign: TextAlign.center,
                  isBold: false),
            ),
            Padding(
              padding: const EdgeInsets.only(bottom: 2.5),
              child: IntrinsicHeight(
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Container(
                      color: Colors.white,
                      width: getWP(context, 49.29),
                      child: Padding(
                        padding: const EdgeInsets.only(top: 20, bottom: 20),
                        child: Column(
                          children: [
                            Txt(
                                txt: "Net earnings",
                                txtColor: MyTheme.gray4Color,
                                txtSize: MyTheme.txtSize - .5,
                                txtAlign: TextAlign.center,
                                isBold: false),
                            SizedBox(height: 5),
                            Txt(
                                txt: getCurSign() +
                                    netEarning.toStringAsFixed(
                                        (netEarning > 0) ? 2 : 0),
                                txtColor: MyTheme.gray5Color,
                                txtSize: MyTheme.txtSize + .5,
                                txtAlign: TextAlign.center,
                                isBold: false)
                          ],
                        ),
                      ),
                    ),
                    Container(
                      color: Colors.white,
                      width: getWP(context, 50),
                      child: Padding(
                        padding: const EdgeInsets.only(top: 20, bottom: 20),
                        child: Column(
                          children: [
                            Txt(
                                txt: 'CSV file',
                                txtColor: MyTheme.gray4Color,
                                txtSize: MyTheme.txtSize - .5,
                                txtAlign: TextAlign.center,
                                isBold: false),
                            SizedBox(height: 10),
                            GestureDetector(
                              onTap: () async {
                                if (listPaymentsModel.length > 0) {
                                  final file = await DownloadFile()
                                      .saveCSV(listPaymentsModel);
                                  showAlert(
                                      msg: "File download successfully.\n\n" +
                                          file.path
                                              .replaceAll("emulated/0/", ""),
                                      isToast: true,
                                      which: 1);
                                } else {
                                  showAlert(
                                      msg: 'payment history not found',
                                      isToast: true);
                                }
                              },
                              child: Txt(
                                  txt: 'Download',
                                  txtColor: MyTheme.gray4Color,
                                  txtSize: MyTheme.txtSize - .2,
                                  txtAlign: TextAlign.center,
                                  isBold: false),
                            )
                          ],
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            )
          ],
        ),
      ),
    );
  }

  //  *********************** Earned
  drawPaymentList() {
    if (isEarned) {
      //  earned
      if (dropdownEarnedValue == DateFun.DT_CURRENT_QUARTER) {
        listPaymentsModel = taskPaymentListCurrentQuarter;
      } else if (dropdownEarnedValue == DateFun.DT_LAST_QUARTER) {
        listPaymentsModel = taskPaymentListLastQuarter;
      } else if (dropdownEarnedValue == DateFun.DT_CURRENT_FINANCIAL_YEAR) {
        listPaymentsModel = taskPaymentListCurrentYear;
      } else if (dropdownEarnedValue == DateFun.DT_LAST_FINANCIAL_YEAR) {
        listPaymentsModel = taskPaymentListLastYear;
      } else {
        listPaymentsModel = taskAllTimes;
      }
    } else {
      //  outgoing
      if (dropdownOutgoingValue == DateFun.DT_CURRENT_QUARTER) {
        listPaymentsModel = taskPaymentListCurrentQuarter;
      } else if (dropdownOutgoingValue == DateFun.DT_LAST_QUARTER) {
        listPaymentsModel = taskPaymentListLastQuarter;
      } else if (dropdownOutgoingValue == DateFun.DT_CURRENT_FINANCIAL_YEAR) {
        listPaymentsModel = taskPaymentListCurrentYear;
      } else if (dropdownOutgoingValue == DateFun.DT_LAST_FINANCIAL_YEAR) {
        listPaymentsModel = taskPaymentListLastYear;
      } else {
        listPaymentsModel = taskAllTimes;
      }
    }

    if ((listPaymentsModel.length == 0)) {
      return (isEarned) ? drawEarnedNF() : drawOutgoingNF();
    } else {
      return Expanded(
        child: Padding(
          padding: const EdgeInsets.only(bottom: 20),
          child: Container(
            child: Theme(
              data: MyTheme.refreshIndicatorTheme,
              child: RefreshIndicator(
                onRefresh: refreshData,
                child: ListView.builder(
                  addAutomaticKeepAlives: true,
                  cacheExtent: AppConfig.page_limit.toDouble(),
                  scrollDirection: Axis.vertical,
                  shrinkWrap: true,
                  primary: true,
                  itemCount: listPaymentsModel.length,
                  itemBuilder: (BuildContext context, int index) {
                    return drawItem(listPaymentsModel[index]);
                  },
                ),
              ),
            ),
          ),
        ),
      );
    }
  }

  drawItem(TaskPaymentsModel taskPaymentsModel) {
    return Container(
      color: Colors.white,
      child: Padding(
        padding: const EdgeInsets.only(left: 20),
        child: ListTileTheme(
          contentPadding: EdgeInsets.all(0),
          child: ExpansionTile(
            onExpansionChanged: (v) {
              if (v) {}
            },
            title: Column(
              children: [
                SizedBox(height: 10),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Txt(
                        txt: DateFun.getTimeAgoTxt(
                            taskPaymentsModel.creationDate),
                        txtColor: MyTheme.gray4Color,
                        txtSize: MyTheme.txtSize - .2,
                        txtAlign: TextAlign.center,
                        isBold: false),
                    Txt(
                        txt: taskPaymentsModel.status,
                        txtColor: MyTheme.gray4Color,
                        txtSize: MyTheme.txtSize - .2,
                        txtAlign: TextAlign.center,
                        isBold: false),
                  ],
                ),
                SizedBox(height: 10),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Expanded(
                      flex: 4,
                      child: Txt(
                          txt: taskPaymentsModel.taskTitle,
                          txtColor: MyTheme.gray4Color,
                          txtSize: MyTheme.txtSize - .2,
                          txtAlign: TextAlign.start,
                          isBold: false),
                    ),
                    Flexible(
                      child: Txt(
                          txt: getCurSign() +
                              taskPaymentsModel.payableAmount
                                  .toStringAsFixed(1),
                          txtColor: MyTheme.gray4Color,
                          txtSize: MyTheme.txtSize - .2,
                          txtAlign: TextAlign.end,
                          isBold: false),
                    ),
                  ],
                ),
                SizedBox(height: 10),
                Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    CircleAvatar(
                      radius: 25,
                      backgroundColor: Colors.transparent,
                      backgroundImage: MyNetworkImage.loadProfileImage(
                          taskPaymentsModel.profileImageUrl),
                    ),
                    SizedBox(width: 10),
                    Flexible(
                      child: Txt(
                          txt: taskPaymentsModel.profileOwnerName,
                          txtColor: MyTheme.gray4Color,
                          txtSize: MyTheme.txtSize - .2,
                          txtAlign: TextAlign.center,
                          isBold: false),
                    ),
                  ],
                ),
                SizedBox(height: 10),
                drawLine(),
              ],
            ),
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.only(right: 20),
                child: Container(
                  color: MyTheme.gray4Color,
                  child: Padding(
                    padding: const EdgeInsets.all(10),
                    child: new Column(
                      children: [
                        _drawExpandableDiv(
                            "Funds released on:",
                            DateFun.getTimeAgoTxt(
                                taskPaymentsModel.creationDate)),
                        SizedBox(height: 5),
                        _drawExpandableDiv(
                            "Funds secured on:",
                            DateFun.getTimeAgoTxt(
                                taskPaymentsModel.updatedDate)),
                        SizedBox(height: 5),
                        _drawExpandableDiv(
                            "Payment method:",
                            DateFun.getTimeAgoTxt(
                                taskPaymentsModel.paymentMethod)),
                      ],
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  _drawExpandableDiv(title, val) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        Txt(
            txt: title,
            txtColor: Colors.white,
            txtSize: MyTheme.txtSize - .4,
            txtAlign: TextAlign.center,
            isBold: false),
        SizedBox(width: 10),
        Flexible(
          child: Txt(
              txt: val,
              txtColor: Colors.white,
              txtSize: MyTheme.txtSize - .4,
              txtAlign: TextAlign.center,
              isBold: false),
        ),
      ],
    );
  }

  drawEarnedNF() {
    return Padding(
      padding: const EdgeInsets.all(20),
      child: Container(
        width: double.infinity,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Txt(
                txt: getCurSign(),
                txtColor: MyTheme.gray5Color,
                txtSize: MyTheme.txtSize + 4,
                txtAlign: TextAlign.center,
                isBold: false),
            SizedBox(height: 20),
            Txt(
                txt:
                    "You don't have any earned to show here.\nChange your filter settings above.",
                txtColor: MyTheme.gray4Color,
                txtSize: MyTheme.txtSize - .2,
                txtAlign: TextAlign.center,
                isBold: false),
          ],
        ),
      ),
    );
  }

  drawOutgoingNF() {
    return Container(
      //color: Colors.black,
      width: double.infinity,
      child: Padding(
        padding: const EdgeInsets.all(20),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Image.asset(
                "assets/images/icons/empty_state_payments_outgoing.png"),
            SizedBox(height: 20),
            Txt(
                txt: "You haven't paid for any tasks yet.",
                txtColor: MyTheme.gray4Color,
                txtSize: MyTheme.txtSize - .2,
                txtAlign: TextAlign.center,
                isBold: false),
          ],
        ),
      ),
    );
  }
}
