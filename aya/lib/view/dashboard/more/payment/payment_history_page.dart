import 'package:aitl/config/server/APIPaymentCfg.dart';
import 'package:aitl/config/server/APIPostTaskCfg.dart';
import 'package:aitl/config/theme/MyTheme.dart';
import 'package:aitl/controller/classes/Common.dart';
import 'package:aitl/controller/classes/DateFun.dart';
import 'package:aitl/data/app_data/UserData.dart';
import 'package:aitl/data/model/dashboard/more/payment/history/TaskPaymentsHistoryAPIModel.dart';
import 'package:aitl/data/model/dashboard/more/payment/history/TaskPaymentsModel.dart';
import 'package:aitl/data/network/ModelMgr.dart';
import 'package:aitl/data/network/NetworkMgr.dart';
import 'package:aitl/view/widgets/Txt.dart';
import 'package:aitl/view/widgets/dropdown/DropListModel.dart';
import 'package:aitl/view_model/api/api_view_model.dart';
import 'package:aitl/view_model/observer/APIStateProvider.dart';
import 'package:flutter/material.dart';
import 'payment_history_base.dart';

class PaymentHistoryPage extends StatefulWidget {
  const PaymentHistoryPage({Key key}) : super(key: key);
  @override
  State createState() => _PaymentHistoryPageState();
}

class _PaymentHistoryPageState
    extends PaymentHistoryStatefull<PaymentHistoryPage> with APIStateListener {
  bool isLoading = false;

//  **************  app states start

  @override
  void onDetached() {
    try {
      log("app state = onDetached");
    } catch (e) {}
  }

  @override
  void onInactive() {
    try {
      log("app state = onInactive");
    } catch (e) {}
  }

  @override
  void onPaused() {
    try {
      log("app state = onPaused");
    } catch (e) {}
  }

  @override
  void onResumed() {
    try {
      log("app state = onResumed");
    } catch (e) {}
  }

  //  **************  app states end

  APIStateProvider _apiStateProvider;
  @override
  onAPIStateChanged(apiState, model) async {
    try {
      if (apiState.type == APIType.payment_history_get &&
          apiState.cls == this.runtimeType) {
        if (model != null && mounted) {
          if (model.success) {
            listPaymentsModel2 = (model as TaskPaymentsHistoryAPIModel)
                .responseData
                .taskPayments;
            netEarning = calculation(
                (isEarned) ? dropdownEarnedValue : dropdownOutgoingValue);
            setState(() {});
          }
        }
      }
    } catch (e) {}
  }

  Future<void> refreshData() async {
    onLazyLoadAPI();
  }

  onLazyLoadAPI() async {
    if (mounted) {
      setState(() {
        isLoading = true;
      });

      try {
        await APIViewModel().req<TaskPaymentsHistoryAPIModel>(
          context: context,
          apiState:
              APIState(APIType.payment_history_get, this.runtimeType, null),
          url: APIPaymentCfg.TASK_PAYMENT_GET_URL,
          reqType: ReqType.Get,
          param: {
            "UserId": userData.userModel.id,
          },
        );
      } catch (e) {}
    }

    if (mounted) {
      setState(() {
        isLoading = false;
      });
    }
  }

  @override
  void initState() {
    super.initState();
    appInit();
  }

  @override
  void dispose() {
    try {
      _apiStateProvider.unsubscribe(this);
      _apiStateProvider = null;
    } catch (e) {
      log(e.toString());
    }
    taskPaymentListCurrentQuarter = null;
    taskPaymentListLastQuarter = null;
    taskPaymentListCurrentYear = null;
    taskPaymentListLastYear = null;
    taskAllTimes = null;
    listPaymentsModel = null;
    listPaymentsModel2 = null;
    super.dispose();
  }

  appInit() async {
    try {
      _apiStateProvider = new APIStateProvider();
      _apiStateProvider.subscribe(this);
    } catch (e) {}
    //Common.getPaymentDate("2021-07-22T13:07:03.327")
    refreshData();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: MyTheme.bgColor,
        appBar: AppBar(
          elevation: MyTheme.appbarElevation,
          backgroundColor: MyTheme.bgColor,
          title: Txt(
              txt: 'Payment history',
              txtColor: MyTheme.appbarTxtColor,
              txtSize: MyTheme.appbarTitleFontSize,
              txtAlign: TextAlign.center,
              isBold: true),
          centerTitle: false,
        ),
        body: GestureDetector(
            onTap: () {
              FocusScope.of(context).requestFocus(new FocusNode());
            },
            child: drawLayout()),
      ),
    );
  }

  @override
  drawLayout() {
    return Container(
      //height: getH(context),
      child: Column(
        children: [
          drawSwitchView(),
          drawShowingDropDown(),
          drawDownload(),
          drawPaymentList(),
        ],
      ),
    );
  }
}
