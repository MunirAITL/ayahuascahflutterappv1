import 'package:aitl/config/app/status/TaskStatusCfg.dart';
import 'package:aitl/config/server/APIPaymentCfg.dart';
import 'package:aitl/config/theme/MyTheme.dart';
import 'package:aitl/data/app_data/PrefMgr.dart';
import 'package:aitl/data/app_data/UserData.dart';
import 'package:aitl/data/model/dashboard/more/payment/methods/PaymentMethodAPIModel.dart';
import 'package:aitl/data/model/dashboard/more/payment/methods/PaymentMethodsAPIModel.dart';
import 'package:aitl/data/model/dashboard/more/payment/methods/PaymentMethodsModel.dart';
import 'package:aitl/data/network/ModelMgr.dart';
import 'package:aitl/data/network/NetworkMgr.dart';
import 'package:aitl/view/widgets/BottomBtn.dart';
import 'package:aitl/view/widgets/CustomCheckbox.dart';
import 'package:aitl/view/widgets/InputBox.dart';
import 'package:aitl/view/widgets/MMArrowBtn.dart';
import 'package:aitl/view/widgets/Txt.dart';
import 'package:aitl/view_model/api/api_view_model.dart';
import 'package:aitl/view_model/observer/APIStateProvider.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'receive_payment_base.dart';

class AddBankAccountPage extends StatefulWidget {
  const AddBankAccountPage({Key key}) : super(key: key);
  @override
  State createState() => _AddBankAccountState();
}

class _AddBankAccountState
    extends BaseReceivePaymentStatefull<AddBankAccountPage>
    with APIStateListener {
  final accName = TextEditingController();
  final bnkName = TextEditingController();
  final accNo = TextEditingController();

  bool isDefaultPayMethod = true;

  List<PaymentMethodsModel> listPaymentMethodsModel;
  PaymentMethodsModel paymentMethodBank;

//  **************  app states start

  @override
  void onDetached() {
    try {
      log("app state = onDetached");
    } catch (e) {}
  }

  @override
  void onInactive() {
    try {
      log("app state = onInactive");
    } catch (e) {}
  }

  @override
  void onPaused() {
    try {
      log("app state = onPaused");
    } catch (e) {}
  }

  @override
  void onResumed() {
    try {
      log("app state = onResumed");
    } catch (e) {}
  }

  //  **************  app states end

  APIStateProvider _apiStateProvider;
  @override
  onAPIStateChanged(apiState, model) async {
    try {
      if (apiState.type == APIType.payment_methods_receive_post_bank_acc &&
          apiState.cls == this.runtimeType) {
        if (model != null && mounted) {
          if (model.success) {
            PrefMgr.shared.setBackAccount(true);
            showAlert(
                msg: 'Bank Account added successfully.',
                isToast: true,
                which: 1);
            Get.back();
          }
        }
      }
      if (apiState.type == APIType.payment_methods_receive_put_bank_acc &&
          apiState.cls == this.runtimeType) {
        if (model != null && mounted) {
          if (model.success) {
            PrefMgr.shared.setBackAccount(true);
            showAlert(
                msg: 'Bank Account added successfully.',
                isToast: true,
                which: 1);
            Get.back();
          }
        }
      }
      if (apiState.type == APIType.payment_methods_receive_get_bank_acc &&
          apiState.cls == this.runtimeType) {
        if (model != null && mounted) {
          if (model.success) {
            listPaymentMethodsModel =
                (model as PaymentMethodsAPIModel).responseData.paymentMethods;
            for (var model2 in listPaymentMethodsModel) {
              if (model2.paymentMethodType ==
                  TaskStatusCfg.TASK_PAYMENT_METHOD_BANK) {
                paymentMethodBank = model2;
                accName.text = paymentMethodBank.accountName.trim();
                bnkName.text = paymentMethodBank.bankName.trim();
                accNo.text = paymentMethodBank.accountNumber.trim();
              }
            }
            setState(() {});
          }
        }
      }
    } catch (e) {}
  }

  wsAddBankAccount() async {
    try {
      if (accName.text.isEmpty) {
        showAlert(msg: 'Please enter a valid account name', isToast: true);
      } else if (bnkName.text.isEmpty) {
        showAlert(msg: 'Please enter a valid bank name', isToast: true);
      } else if (accNo.text.isEmpty) {
        showAlert(msg: 'Please enter a valid account number', isToast: true);
      } else {
        if (paymentMethodBank != null) {
          APIViewModel().req<PaymentMethodAPIModel>(
            context: context,
            apiState: APIState(APIType.payment_methods_receive_put_bank_acc,
                this.runtimeType, null),
            url: APIPaymentCfg.PAYMENT_METHODS_PUT_URL,
            param: {
              "AccountName": accName.text.trim(),
              "AccountNumber": accNo.text.trim(),
              "BankName": bnkName.text.trim(),
              "CVC": "",
              "CardName": "",
              "CardNumber": "",
              "CreationDate": DateTime.now().toString(),
              "ExpiryDate": "",
              "ExpiryMonth": "",
              "Id": paymentMethodBank.id,
              "PaymentMethodType": TaskStatusCfg.TASK_PAYMENT_METHOD_BANK,
              "Status": TaskStatusCfg.TASK_STATUS_ACTIVE,
              "UpdatedDate": DateTime.now().toString(),
              "UserId": userData.userModel.id,
            },
            reqType: ReqType.Put,
          );
        } else {
          APIViewModel().req<PaymentMethodAPIModel>(
            context: context,
            apiState: APIState(APIType.payment_methods_receive_post_bank_acc,
                this.runtimeType, null),
            url: APIPaymentCfg.PAYMENT_METHODS_POST_URL,
            param: {
              "AccountName": accName.text.trim(),
              "AccountNumber": accNo.text.trim(),
              "BankName": bnkName.text.trim(),
              "CVC": "",
              "CardName": "",
              "CardNumber": "",
              "CreationDate": DateTime.now().toString(),
              "ExpiryDate": "",
              "ExpiryMonth": "",
              "PaymentMethodType": TaskStatusCfg.TASK_PAYMENT_METHOD_BANK,
              "Status": TaskStatusCfg.TASK_STATUS_ACTIVE,
              "UpdatedDate": DateTime.now().toString(),
              "UserId": userData.userModel.id,
            },
            reqType: ReqType.Post,
          );
        }
      }
    } catch (e) {}
  }

  @override
  void initState() {
    super.initState();
    appInit();
  }

  @override
  void dispose() {
    try {
      _apiStateProvider.unsubscribe(this);
      _apiStateProvider = null;
    } catch (e) {
      log(e.toString());
    }
    accName.dispose();
    bnkName.dispose();
    accNo.dispose();
    listPaymentMethodsModel = null;
    paymentMethodBank = null;
    super.dispose();
  }

  appInit() async {
    try {
      _apiStateProvider = new APIStateProvider();
      _apiStateProvider.subscribe(this);
    } catch (e) {}

    try {
      APIViewModel().req<PaymentMethodsAPIModel>(
        context: context,
        apiState: APIState(APIType.payment_methods_receive_get_bank_acc,
            this.runtimeType, null),
        url: APIPaymentCfg.PAYMENT_METHODS_GET_URL,
        param: {"UserId": userData.userModel.id},
        reqType: ReqType.Get,
      );
    } catch (e) {}
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: MyTheme.bgColor,
        appBar: AppBar(
          elevation: MyTheme.appbarElevation,
          backgroundColor: MyTheme.bgColor,
          title: Txt(
              txt: 'Add bank account',
              txtColor: MyTheme.appbarTxtColor,
              txtSize: MyTheme.appbarTitleFontSize,
              txtAlign: TextAlign.center,
              isBold: true),
          centerTitle: false,
        ),
        bottomNavigationBar: drawBottomBtn(
            context: context,
            text: "Add",
            callback: () async {
              wsAddBankAccount();
            }),
        body: GestureDetector(
            onTap: () {
              FocusScope.of(context).requestFocus(new FocusNode());
            },
            child: drawLayout()),
      ),
    );
  }

  @override
  drawLayout() {
    return Container(
      child: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.all(20),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              InputBox(
                ctrl: accName,
                lableTxt: "Account name",
                labelColor: MyTheme.gray4Color,
                align: TextAlign.start,
                kbType: TextInputType.text,
                len: 255,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Flexible(
                    child: InputBox(
                      ctrl: bnkName,
                      lableTxt: "Bank name",
                      labelColor: MyTheme.gray4Color,
                      align: TextAlign.start,
                      kbType: TextInputType.text,
                      len: 255,
                    ),
                  ),
                  SizedBox(width: 20),
                  Flexible(
                    child: InputBox(
                      ctrl: accNo,
                      lableTxt: "Account number",
                      labelColor: MyTheme.gray4Color,
                      align: TextAlign.start,
                      kbType: TextInputType.streetAddress,
                      len: 50,
                    ),
                  ),
                ],
              ),
              SizedBox(height: 5),
              CustomCheckbox(
                txt: 'Set as default payment method',
                callback: (v) {
                  isDefaultPayMethod = v;
                  setState(() {});
                },
                isSelected: isDefaultPayMethod,
                themeData: MyTheme.radioBlackThemeData,
              ),
              SizedBox(height: 50),
            ],
          ),
        ),
      ),
    );
  }
}
