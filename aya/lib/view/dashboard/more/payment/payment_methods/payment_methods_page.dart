import 'package:aitl/config/theme/MyTheme.dart';
import 'package:aitl/data/network/ModelMgr.dart';
import 'package:aitl/mixin.dart';
import 'package:aitl/view/dashboard/more/payment/payment_methods/make_payment/make_payment_page.dart';
import 'package:aitl/view/dashboard/more/payment/payment_methods/receive_payment/receive_payment_page.dart';
import 'package:aitl/view/widgets/Txt.dart';
import 'package:aitl/view_model/observer/APIStateProvider.dart';
import 'package:flutter/material.dart';
import 'make_payment/make_payment_base.dart';

class PaymentMethodsPage extends StatefulWidget {
  const PaymentMethodsPage({Key key}) : super(key: key);
  @override
  State createState() => _PaymentMethodsPageState();
}

class _PaymentMethodsPageState extends State<PaymentMethodsPage> with Mixin {
  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 2,
      child: SafeArea(
        child: Scaffold(
          backgroundColor: MyTheme.bgColor,
          appBar: AppBar(
            elevation: MyTheme.appbarElevation,
            backgroundColor: MyTheme.bgColor,
            title: Txt(
                txt: 'Payment settings',
                txtColor: MyTheme.appbarTxtColor,
                txtSize: MyTheme.appbarTitleFontSize,
                txtAlign: TextAlign.center,
                isBold: true),
            centerTitle: false,
            bottom: PreferredSize(
              preferredSize: Size.fromHeight(getHP(context, 7)),
              child: Container(
                color: MyTheme.redColor,
                height: getHP(context, MyTheme.btnHpa),
                child: TabBar(
                  //labelColor: Colors.deepOrange,
                  unselectedLabelColor: Colors.grey,
                  indicatorColor: Colors.black,
                  indicatorSize: TabBarIndicatorSize.tab,
                  indicatorWeight: 3,
                  tabs: [
                    Container(
                        //color: MyTheme.blueColor,
                        //height: getHP(context, MyTheme.btnHpa),
                        child: Txt(
                      txt: "MAKE PAYMENTS",
                      txtColor: Colors.white,
                      txtSize: MyTheme.txtSize - .2,
                      txtAlign: TextAlign.center,
                      isBold: false,
                    )),
                    Container(
                        child: Txt(
                      txt: "RECEIVE PAYMENTS",
                      txtColor: Colors.white,
                      txtSize: MyTheme.txtSize - .2,
                      txtAlign: TextAlign.center,
                      isBold: false,
                    )),
                  ],
                ),
              ),
            ),
          ),
          body: GestureDetector(
              onTap: () {
                FocusScope.of(context).requestFocus(new FocusNode());
              },
              child: TabBarView(
                children: [
                  MakePaymentPage(),
                  ReceivePaymentPage(),
                ],
              )),
        ),
      ),
    );
  }
}
