import 'package:aitl/config/server/APIMiscCfg.dart';
import 'package:aitl/config/server/APIPaymentCfg.dart';
import 'package:aitl/config/theme/MyTheme.dart';
import 'package:aitl/data/app_data/UserData.dart';
import 'package:aitl/data/model/dashboard/more/payment/methods/PromoCodeAPIModel.dart';
import 'package:aitl/data/model/misc/CommonAPIModel.dart';
import 'package:aitl/data/network/ModelMgr.dart';
import 'package:aitl/data/network/NetworkMgr.dart';
import 'package:aitl/view/widgets/InputBox.dart';
import 'package:aitl/view/widgets/MMArrowBtn.dart';
import 'package:aitl/view/widgets/Txt.dart';
import 'package:aitl/view_model/api/api_view_model.dart';
import 'package:aitl/view_model/observer/APIStateProvider.dart';
import 'package:flutter/material.dart';
import 'make_payment_base.dart';

class AddPromoCardPage extends StatefulWidget {
  const AddPromoCardPage({Key key}) : super(key: key);
  @override
  State createState() => _AddPromoCardState();
}

class _AddPromoCardState extends BaseMakePaymentStatefull<AddPromoCardPage>
    with APIStateListener {
  final code = TextEditingController();

  //  **************  app states start

  @override
  void onDetached() {
    try {
      log("app state = onDetached");
    } catch (e) {}
  }

  @override
  void onInactive() {
    try {
      log("app state = onInactive");
    } catch (e) {}
  }

  @override
  void onPaused() {
    try {
      log("app state = onPaused");
    } catch (e) {}
  }

  @override
  void onResumed() {
    try {
      log("app state = onResumed");
    } catch (e) {}
  }

  //  **************  app states end

  APIStateProvider _apiStateProvider;
  @override
  onAPIStateChanged(apiState, model) async {
    try {
      if (apiState.type == APIType.payment_methods_make_post_promo_card &&
          apiState.cls == this.runtimeType) {
        if (model != null && mounted) {
          if (model.success) {
            showAlert(
                msg: 'Promotion Code is successfully added',
                isToast: true,
                which: 1);
          } else {
            showAlert(
                msg:
                    'Invalid Promotion Code\nPlease enter a valid Promotion Code',
                isToast: true);
          }
        }
      }
      if (apiState.type == APIType.payment_methods_make_get_promo_card &&
          apiState.cls == this.runtimeType) {
        if (model != null && mounted) {
          if (model.success) {}
        }
      }
    } catch (e) {}
  }

  wsAddPromo() async {
    try {
      if (code.text.isNotEmpty) {
        APIViewModel().req<CommonAPIModel>(
          context: context,
          apiState: APIState(APIType.payment_methods_make_post_promo_card,
              this.runtimeType, null),
          url: APIPaymentCfg.PROMO_CODE_POST_URL,
          param: {
            "PromotionCode": code.text,
            "UserId": userData.userModel.id,
          },
          reqType: ReqType.Post,
        );
      } else {
        showAlert(msg: 'Promotion Code cannot be blank', isToast: true);
      }
    } catch (e) {}
  }

  @override
  void initState() {
    super.initState();
    appInit();
  }

  @override
  void dispose() {
    try {
      _apiStateProvider.unsubscribe(this);
      _apiStateProvider = null;
    } catch (e) {
      log(e.toString());
    }
    code.dispose();
    super.dispose();
  }

  appInit() async {
    try {
      _apiStateProvider = new APIStateProvider();
      _apiStateProvider.subscribe(this);
    } catch (e) {}

    try {
      APIViewModel().req<PromoCodeAPIModel>(
        context: context,
        apiState: APIState(APIType.payment_methods_make_get_promo_card,
            this.runtimeType, null),
        url: APIPaymentCfg.PROMO_CODE_GET_URL,
        param: {"UserId": userData.userModel.id},
        reqType: ReqType.Get,
      );
    } catch (e) {}
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: MyTheme.bgColor,
        appBar: AppBar(
          elevation: MyTheme.appbarElevation,
          backgroundColor: MyTheme.bgColor,
          title: Txt(
              txt: 'Add promo card',
              txtColor: MyTheme.appbarTxtColor,
              txtSize: MyTheme.appbarTitleFontSize,
              txtAlign: TextAlign.center,
              isBold: true),
          centerTitle: false,
        ),
        body: GestureDetector(
            onTap: () {
              FocusScope.of(context).requestFocus(new FocusNode());
            },
            child: drawLayout()),
      ),
    );
  }

  @override
  drawLayout() {
    return Container(
      color: Colors.white,
      child: Padding(
        padding: const EdgeInsets.all(20),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            SizedBox(height: 20),
            Txt(
                txt: "Code",
                txtColor: MyTheme.gray4Color,
                txtSize: MyTheme.txtSize,
                txtAlign: TextAlign.start,
                isBold: false),
            InputBox(
              ctrl: code,
              autofocus: true,
              lableTxt: "Enter a promo/gift code",
              labelColor: MyTheme.gray4Color,
              isShowHint: true,
              align: TextAlign.center,
              kbType: TextInputType.number,
              len: 10,
            ),
            SizedBox(height: 10),
            MMArrowBtn(
              txt: "Add",
              icon: null,
              height: getHP(context, MyTheme.btnHpa),
              width: getW(context),
              radius: 50,
              callback: () {
                wsAddPromo();
              },
            )
          ],
        ),
      ),
    );
  }
}
