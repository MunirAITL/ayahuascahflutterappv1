import 'package:aitl/config/theme/MyTheme.dart';
import 'package:aitl/data/app_data/UserData.dart';
import 'package:aitl/view/dashboard/dashboard_base.dart';
import 'package:aitl/view/dashboard/more/help/help_page.dart';
import 'package:aitl/view/dashboard/more/noti/noti_page.dart';
import 'package:aitl/view/dashboard/more/payment/payment_history_page.dart';
import 'package:aitl/view/dashboard/more/payment/payment_methods/payment_methods_page.dart';
import 'package:aitl/view/dashboard/more/profile/profile_page.dart';
import 'package:aitl/view/dashboard/more/reviews/reviews_page.dart';
import 'package:aitl/view/dashboard/more/settings/settings_page.dart';
import 'package:aitl/view/dashboard/more/task_settings/task_alert_page.dart';
import 'package:aitl/view/widgets/Txt.dart';
import 'package:flutter/material.dart';

abstract class BaseMoreStatefull<T extends StatefulWidget>
    extends BaseDashboard<T> {
  final List<Map<String, dynamic>> listMore = [
    {"title": "Dashboard", "route": () => Container()},
    {"title": "Profile", "route": () => ProfilePage()},
    {"title": "Payment History", "route": () => PaymentHistoryPage()},
    {"title": "Payment Methods", "route": () => PaymentMethodsPage()},
    {"title": "Reviews", "route": () => ReviewsPage()},
    {"title": "Notifications", "route": () => NotiPage()},
    {"title": "Alerts Settings", "route": () => TaskAlertPage()},
    {"title": "Settings", "route": () => SettingsPage()},
    {"title": "Help", "route": () => HelpPage()},
    {"title": "Log out", "route": null},
  ];

  drawNotiBadge() {
    return Container(
      decoration: BoxDecoration(
        color: MyTheme.brandColor,
        borderRadius: BorderRadius.circular(5),
      ),
      child: Padding(
        padding: const EdgeInsets.all(5),
        child: Txt(
            txt: userData.userModel.unreadNotificationCount.toString(),
            txtColor: Colors.white,
            txtSize: MyTheme.txtSize,
            txtAlign: TextAlign.center,
            isBold: false),
      ),
    );
  }
}
