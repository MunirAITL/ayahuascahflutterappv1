import 'package:aitl/config/server/APITaskAlertCfg.dart';
import 'package:aitl/config/theme/MyTheme.dart';
import 'package:aitl/controller/classes/Common.dart';
import 'package:aitl/data/app_data/UserData.dart';
import 'package:aitl/data/model/dashboard/more/task_alert/TaskAlertAPIModel.dart';
import 'package:aitl/data/model/dashboard/more/task_alert/TaskAlertsAPIModel.dart';
import 'package:aitl/data/model/dashboard/more/task_alert/TaskAlertsModel.dart';
import 'package:aitl/data/network/ModelMgr.dart';
import 'package:aitl/data/network/NetworkMgr.dart';
import 'package:aitl/view/widgets/BottomBtn.dart';
import 'package:aitl/view/widgets/DropDownWidget.dart';
import 'package:aitl/view/widgets/InputBox.dart';
import 'package:aitl/view/widgets/InputTitleBoxHT.dart';
import 'package:aitl/view/widgets/SwitchBar.dart';
import 'package:aitl/view/widgets/SwitchView.dart';
import 'package:aitl/view/widgets/ToggleSwitch.dart';
import 'package:aitl/view/widgets/Txt.dart';
import 'package:aitl/view/widgets/dialog/ConfirmDialog.dart';
import 'package:aitl/view/widgets/google/GPlacesView.dart';
import 'package:aitl/view_model/api/api_view_model.dart';
import 'package:aitl/view_model/observer/APIStateProvider.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:google_maps_webservice/geocoding.dart';
import 'task_alert_base.dart';

class TaskAlertAmendPage extends StatefulWidget {
  final TaskAlertKeywordsModel taskAlertKeywordsModel;
  const TaskAlertAmendPage({Key key, this.taskAlertKeywordsModel})
      : super(key: key);
  @override
  State createState() => _TaskAlertAmendPageState();
}

class _TaskAlertAmendPageState
    extends BaseTaskAlertKWStatefull<TaskAlertAmendPage> with APIStateListener {
  final kw = TextEditingController();

  final listDropdown = [
    "Within 5km",
    "Within 10km",
    "Within 15km",
    "Within 20km",
    "Within 25km",
    "Within 30km",
  ];

  final listDistance = [5, 10, 15, 20, 25, 30];

  var dropdownVal = "";
  bool isInPerson = true;
  int switchIndex = 0;

  String taskAddress = "Search";
  Location taskCord;

  //  **************  app states start

  @override
  void onDetached() {
    try {
      log("app state = onDetached");
    } catch (e) {}
  }

  @override
  void onInactive() {
    try {
      log("app state = onInactive");
    } catch (e) {}
  }

  @override
  void onPaused() {
    try {
      log("app state = onPaused");
    } catch (e) {}
  }

  @override
  void onResumed() {
    try {
      log("app state = onResumed");
    } catch (e) {}
  }

  //  **************  app states end

  APIStateProvider _apiStateProvider;
  @override
  onAPIStateChanged(apiState, model) async {
    try {
      if (apiState.type == APIType.task_alert_post &&
          apiState.cls == this.runtimeType) {
        if (model != null && mounted) {
          if (model.success) {
            setState(() {
              Get.back();
            });
          }
        }
      } else if (apiState.type == APIType.task_alert_put &&
          apiState.cls == this.runtimeType) {
        if (model != null && mounted) {
          if (model.success) {
            Get.back();
          }
        }
      } else if (apiState.type == APIType.task_alert_del &&
          apiState.cls == this.runtimeType) {
        if (model != null && mounted) {
          if (model.success) {
            setState(() {
              Get.back();
            });
          }
        }
      }
    } catch (e) {}
  }

  @override
  drawItem(item) {}

  validate() {
    if (kw.text.trim().length < 3) {
      showAlert(
          msg: 'keyword needs to be atleast 3 letters long', isToast: true);
      return false;
    }
    if (isInPerson && (taskAddress == "" || taskAddress == "Search")) {
      showAlert(msg: 'Please pick your location', isToast: true);
      return false;
    }
    return true;
  }

  wsTaskAlertAdd() async {
    try {
      var index = 0;
      double lat = 0;
      double lng = 0;
      try {
        index = Common.findIndexFromList(listDropdown, dropdownVal);
        lat = taskCord.lat;
        lng = taskCord.lng;
      } catch (e) {}
      await APIViewModel().req<TaskAlertAPIModel>(
          context: context,
          apiState: APIState(APIType.task_alert_post, this.runtimeType, null),
          url: APITaskAlertCfg.TASK_ALERT_POST_URL,
          param: {
            "Distance": listDistance[index],
            "IsOnline": !isInPerson,
            "Keyword": kw.text.trim(),
            "Latitude": (isInPerson) ? lat : 0,
            "Location": (isInPerson)
                ? taskAddress != 'Search'
                    ? taskAddress
                    : ''
                : '',
            "Longitude": (isInPerson) ? lng : 0,
            "UserId": userData.userModel.id,
          },
          reqType: ReqType.Post,
          callback: (model) {});
    } catch (e) {}
  }

  wsTaskAlertUpdate() async {
    try {
      var index = 0;
      double lat = 0;
      double lng = 0;
      try {
        index = Common.findIndexFromList(listDropdown, dropdownVal);
        lat = taskCord.lat;
        lng = taskCord.lng;
      } catch (e) {}
      await APIViewModel().req<TaskAlertAPIModel>(
          context: context,
          apiState: APIState(APIType.task_alert_put, this.runtimeType, null),
          url: APITaskAlertCfg.TASK_ALERT_PUT_URL,
          param: {
            "Distance": listDistance[index],
            "Id": widget.taskAlertKeywordsModel.id,
            "IsOnline": !isInPerson,
            "Keyword": kw.text.trim(),
            "Latitude": (isInPerson) ? lat : 0,
            "Location": (isInPerson)
                ? taskAddress != 'Search'
                    ? taskAddress
                    : ''
                : '',
            "Longitude": (isInPerson) ? lng : 0,
            "UserId": userData.userModel.id,
          },
          reqType: ReqType.Put,
          callback: (model) {});
    } catch (e) {}
  }

  onDelAlertClicked(int taskId) async {
    try {
      await APIViewModel().req<TaskAlertAPIModel>(
          context: context,
          apiState: APIState(APIType.task_alert_del, this.runtimeType, null),
          url: APITaskAlertCfg.TASK_ALERT_DEL_URL.replaceAll(
              "#alertId#", widget.taskAlertKeywordsModel.id.toString()),
          reqType: ReqType.Delete,
          callback: (model) {});
    } catch (e) {}
  }

  @override
  void initState() {
    super.initState();
    appInit();
  }

  @override
  void dispose() {
    try {
      _apiStateProvider.unsubscribe(this);
      _apiStateProvider = null;
    } catch (e) {
      log(e.toString());
    }
    kw.dispose();
    super.dispose();
  }

  appInit() async {
    try {
      _apiStateProvider = new APIStateProvider();
      _apiStateProvider.subscribe(this);
    } catch (e) {}

    try {
      final model = widget.taskAlertKeywordsModel;
      if (model != null) {
        //  edit
        var index = 0;
        try {
          index = Common.findIndexFromList(listDistance, model.distance);
        } catch (e) {}
        dropdownVal = listDropdown[index];
        kw.text = model.keyword;
        isInPerson = !model.isOnline;
        taskAddress = model.location;
        taskCord = Location(model.latitude, model.longitude);
      } else {
        //  add
        dropdownVal = listDropdown[listDropdown.length - 1];
      }
    } catch (e) {}
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        resizeToAvoidBottomInset: false,
        backgroundColor: MyTheme.bgColor,
        appBar: AppBar(
          elevation: MyTheme.appbarElevation,
          backgroundColor: MyTheme.bgColor,
          title: Txt(
              txt: (widget.taskAlertKeywordsModel == null)
                  ? "Add task alert"
                  : "Edit task alert",
              txtColor: MyTheme.appbarTxtColor,
              txtSize: MyTheme.appbarTitleFontSize,
              txtAlign: TextAlign.center,
              isBold: true),
          centerTitle: false,
          actions: [
            widget.taskAlertKeywordsModel != null
                ? IconButton(
                    icon: Icon(Icons.delete),
                    onPressed: () {
                      Get.dialog(
                        ConfirmDialog(
                            callback: () {
                              onDelAlertClicked(
                                  widget.taskAlertKeywordsModel.id);
                            },
                            title: "Delete alert",
                            msg:
                                "Are you sure, you want to delete this alert?"),
                      );
                    },
                  )
                : SizedBox(),
          ],
        ),
        bottomNavigationBar: drawBottomBtn(
            context: context,
            text: (widget.taskAlertKeywordsModel == null)
                ? "Add alert"
                : "Update alert",
            bgColor: Colors.white,
            callback: () async {
              if (widget.taskAlertKeywordsModel == null) {
                if (validate()) {
                  wsTaskAlertAdd();
                }
              } else {
                if (validate()) {
                  wsTaskAlertUpdate();
                }
              }
            }),
        body: GestureDetector(
            onTap: () {
              FocusScope.of(context).requestFocus(new FocusNode());
            },
            child: drawLayout()),
      ),
    );
  }

  @override
  drawLayout() {
    return Container(
      color: Colors.white,
      child: Padding(
        padding: const EdgeInsets.all(20),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            SizedBox(height: 20),
            Center(
              child: SwitchView(
                onTxt: " In person ",
                offTxt: " Remote ",
                value: isInPerson,
                bgColorOn: MyTheme.heroTheamColors,
                bgColorOff: Colors.white,
                txtColorOn: Colors.white,
                txtColorOff: MyTheme.heroTheamColors,
                onChanged: (value) {
                  isInPerson = value;
                  setState(() {});
                  //callback((isSwitch) ? _companyName.text.trim() : '');
                },
              ),
            ),
            SizedBox(height: 20),
            Txt(
                txt: "Keyword or phrase (Driver, Clean, Part Time, Web etc)",
                txtColor: MyTheme.gray4Color,
                txtSize: MyTheme.txtSize,
                txtAlign: TextAlign.start,
                isBold: false),
            InputBox(
              ctrl: kw,
              lableTxt: "Enter keyword or phrase",
              labelColor: MyTheme.gray4Color,
              isShowHint: true,
              kbType: TextInputType.text,
              len: 100,
            ),
            (isInPerson)
                ? Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      SizedBox(height: 20),
                      GPlacesView(
                        title: "Location",
                        address: taskAddress,
                        txtColor: MyTheme.gray4Color,
                        bgColor: Colors.white,
                        isTxtBold: false,
                        titlePadding: 0,
                        callback: (String _address, Location _loc) {
                          //callback(address);
                          taskAddress = _address;
                          taskCord = _loc;
                          setState(() {});
                        },
                      ),
                      SizedBox(height: 15),
                      Txt(
                          txt: "Distance",
                          txtColor: MyTheme.gray4Color,
                          txtSize: MyTheme.txtSize,
                          txtAlign: TextAlign.start,
                          isBold: false),
                      DropDownWidget(
                          list: listDropdown,
                          dropdownValue: dropdownVal,
                          isExpanded: true,
                          txtColor: MyTheme.gray4Color,
                          callback: (val) {
                            dropdownVal = val;
                            setState(() {});
                          }),
                    ],
                  )
                : SizedBox(),
          ],
        ),
      ),
    );
  }
}
