import 'package:aitl/config/server/APITaskAlertCfg.dart';
import 'package:aitl/config/theme/MyTheme.dart';
import 'package:aitl/data/app_data/UserData.dart';
import 'package:aitl/data/model/dashboard/more/task_alert/TaskAlertsAPIModel.dart';
import 'package:aitl/data/model/dashboard/more/task_alert/TaskAlertsModel.dart';
import 'package:aitl/data/network/ModelMgr.dart';
import 'package:aitl/data/network/NetworkMgr.dart';
import 'package:aitl/view/dashboard/more/task_settings/task_alert_amend_page.dart';
import 'package:aitl/view/dashboard/more/task_settings/task_alert_base.dart';
import 'package:aitl/view/widgets/Txt.dart';
import 'package:aitl/view_model/api/api_view_model.dart';
import 'package:aitl/view_model/observer/APIStateProvider.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class TaskAlertPage extends StatefulWidget {
  const TaskAlertPage({Key key}) : super(key: key);

  @override
  State createState() => _TaskAlertPageState();
}

class _TaskAlertPageState extends BaseTaskAlertKWStatefull<TaskAlertPage>
    with APIStateListener {
  List<TaskAlertKeywordsModel> listTaskAlertKWModel = [];

  //  **************  app states start

  @override
  void onDetached() {
    try {
      log("app state = onDetached");
    } catch (e) {}
  }

  @override
  void onInactive() {
    try {
      log("app state = onInactive");
    } catch (e) {}
  }

  @override
  void onPaused() {
    try {
      log("app state = onPaused");
    } catch (e) {}
  }

  @override
  void onResumed() {
    try {
      log("app state = onResumed");
    } catch (e) {}
  }

  //  **************  app states end

  APIStateProvider _apiStateProvider;
  @override
  onAPIStateChanged(apiState, model) async {
    try {
      if (apiState.type == APIType.task_alert_get &&
          apiState.cls == this.runtimeType) {
        if (model != null && mounted) {
          if (model.success) {
            listTaskAlertKWModel =
                (model as TaskAlertsAPIModel).responseData.taskAlertKeywords;
            setState(() {});
          }
        }
      }
    } catch (e) {}
  }

  wsGetTaskAlerts() async {
    try {
      await APIViewModel().req<TaskAlertsAPIModel>(
          context: context,
          apiState: APIState(APIType.task_alert_get, this.runtimeType, null),
          url: APITaskAlertCfg.TASK_ALERT_GET_URL,
          param: {"UserId": userData.userModel.id},
          reqType: ReqType.Get,
          callback: (model) {});
    } catch (e) {}
  }

  @override
  void initState() {
    super.initState();
    appInit();
  }

  @override
  void dispose() {
    try {
      _apiStateProvider.unsubscribe(this);
      _apiStateProvider = null;
    } catch (e) {
      log(e.toString());
    }
    super.dispose();
  }

  appInit() async {
    try {
      _apiStateProvider = new APIStateProvider();
      _apiStateProvider.subscribe(this);
    } catch (e) {}

    wsGetTaskAlerts();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: MyTheme.bgColor,
        appBar: AppBar(
          elevation: MyTheme.appbarElevation,
          backgroundColor: MyTheme.bgColor,
          title: Txt(
              txt: 'Task alerts',
              txtColor: MyTheme.appbarTxtColor,
              txtSize: MyTheme.appbarTitleFontSize,
              txtAlign: TextAlign.center,
              isBold: true),
          centerTitle: false,
        ),
        floatingActionButton: new FloatingActionButton(
            elevation: 0.0,
            child: new Icon(Icons.add, color: Colors.white, size: 40),
            backgroundColor: Colors.black,
            onPressed: () {
              Get.to(() => TaskAlertAmendPage())
                  .then((value) => wsGetTaskAlerts());
            }),
        body: GestureDetector(
            onTap: () {
              FocusScope.of(context).requestFocus(new FocusNode());
            },
            child: drawLayout()),
      ),
    );
  }

  @override
  drawLayout() {
    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          (listTaskAlertKWModel.length > 0)
              ? drawTaskAlertList(listTaskAlertKWModel)
              : drawNF(),
        ],
      ),
    );
  }

  drawItem(item) {
    final model = item as TaskAlertKeywordsModel;
    var distance = '';
    if (model.isOnline) {
      distance = "Remote";
    } else {
      distance =
          "Within " + model.distance.toString() + "km / " + model.location;
    }
    return GestureDetector(
      onTap: () {
        Get.to(() => TaskAlertAmendPage(taskAlertKeywordsModel: model))
            .then((value) => wsGetTaskAlerts());
      },
      child: Padding(
        padding: const EdgeInsets.only(top: 10),
        child: Container(
          color: Colors.transparent,
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              CircleAvatar(
                radius: 25,
                backgroundColor: Colors.transparent,
                backgroundImage: new AssetImage(
                    "assets/images/icons/ic_bell_invert_icon.png"),
              ),
              SizedBox(width: 10),
              Expanded(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Txt(
                        txt: model.keyword,
                        txtColor: MyTheme.gray5Color,
                        txtSize: MyTheme.txtSize,
                        txtAlign: TextAlign.start,
                        isBold: false),
                    SizedBox(height: 5),
                    Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Padding(
                          padding: const EdgeInsets.only(top: 5),
                          child: Image.asset(
                            "assets/images/icons/map_pin_icon.png",
                            width: 20,
                            height: 20,
                            color: Colors.grey,
                          ),
                        ),
                        SizedBox(width: 5),
                        Flexible(
                          child: Txt(
                              txt: distance,
                              txtColor: MyTheme.gray4Color,
                              txtSize: MyTheme.txtSize - .2,
                              txtAlign: TextAlign.start,
                              isBold: false),
                        )
                      ],
                    ),
                    SizedBox(height: 10),
                    drawLine(),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  drawNF() {
    return Center(
      child: SingleChildScrollView(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          //shrinkWrap: true,
          children: [
            Container(
              color: MyTheme.grayColor,
              child: Padding(
                padding: const EdgeInsets.all(10),
                child: Txt(
                  txt:
                      "If you add Keyword from below to the type of job you want to be notified, then you will receive notification immediately posting such type of work.",
                  txtColor: Colors.white70,
                  txtSize: MyTheme.txtSize,
                  txtAlign: TextAlign.center,
                  isBold: false,
                  //txtLineSpace: 1.5,
                ),
              ),
            ),
            SizedBox(height: 100),
            Container(
              width: getWP(context, 50),
              height: getWP(context, 30),
              decoration: BoxDecoration(
                image: DecorationImage(
                  image: AssetImage("assets/images/nf/nf_noti.png"),
                  fit: BoxFit.cover,
                ),
              ),
            ),
            //SizedBox(height: 40),
            Padding(
              padding: const EdgeInsets.only(top: 20, left: 20, right: 20),
              child: Container(
                child: Txt(
                  txt:
                      "Be notified when there's a task that you're interested in. Add keywords such as moving or clean.",
                  txtColor: MyTheme.mycasesNFBtnColor,
                  txtSize: MyTheme.txtSize,
                  txtAlign: TextAlign.center,
                  isBold: false,
                  //txtLineSpace: 1.5,
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
