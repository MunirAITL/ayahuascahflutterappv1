import 'package:aitl/config/server/APIYoutubeCfg.dart';
import 'package:aitl/config/server/ServerUrls.dart';
import 'package:aitl/config/theme/MyTheme.dart';
import 'package:aitl/mixin.dart';
import 'package:aitl/view/dashboard/dashboard_page.dart';
import 'package:aitl/view/dashboard/more/help/support_center_page.dart';
import 'package:aitl/view/dashboard/more/noti/noti_settings_page.dart';
import 'package:aitl/view/dashboard/more/profile/edit_profile_page.dart';
import 'package:aitl/view/splash/tutorials/app_tut2_page.dart';
import 'package:aitl/view/widgets/Txt.dart';
import 'package:aitl/view/widgets/webview/WebScreen.dart';
import 'package:aitl/view_model/helper/utils/UIHelper.dart';
import 'package:aitl/view_model/observer/StateProvider.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import '../more_page.dart';

class HelpPage extends StatefulWidget {
  HelpPage({Key key}) : super(key: key);
  @override
  State createState() => _HelpPageState();
}

const FIND_WORK_RELOAD_ON_APP_TUT2 = "Application tutorial 2";

class _HelpPageState extends State<HelpPage> with Mixin, UIHelper {
  List<Map<String, dynamic>> listMore;

  onTaskerVideoClicked() {
    openUrl(context, APIYoutubeCfg.HELP_YOUTUBE_URL);
  }

  onPosterVideoClicked() {
    openUrl(context, APIYoutubeCfg.HELP_YOUTUBE_URL);
  }

  @override
  void initState() {
    super.initState();
    appInit();
  }

  @override
  void dispose() {
    listMore = null;
    super.dispose();
  }

  appInit() {
    try {
      listMore = [
        {"title": "Support center", "route": () => SupportCenterPage()},
        {
          "title": "Terms & conditions",
          "route": () =>
              WebScreen(url: ServerUrls.TC_URL, title: "Terms & Conditions")
        },
        {
          "title": "Privacy",
          "route": () =>
              WebScreen(url: ServerUrls.PRIVACY_URL, title: "Privacy")
        },
        {"title": "Application tutorial 1", "route": null},
        {"title": FIND_WORK_RELOAD_ON_APP_TUT2, "route": () => AppTut2Page()},
        {
          "title": "Tasker tutorial (video)",
          "callback": () => onTaskerVideoClicked()
        },
        {
          "title": "Poster tutorial (video)",
          "callback": () => onPosterVideoClicked()
        },
      ];
    } catch (e) {}
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: MyTheme.bgColor,
        appBar: AppBar(
          backgroundColor: MyTheme.bgColor,
          elevation: MyTheme.appbarElevation,
          title: Txt(
              txt: 'Help',
              txtColor: Colors.black,
              txtSize: MyTheme.appbarTitleFontSize,
              txtAlign: TextAlign.center,
              isBold: false),
          centerTitle: false,
        ),
        body: drawLayout(),
      ),
    );
  }

  drawLayout() {
    return Padding(
      padding: const EdgeInsets.only(top: 10),
      child: Container(
        child: ListView.builder(
          itemCount: listMore.length,
          itemBuilder: (context, index) {
            Map<String, dynamic> mapMore = listMore[index];
            return GestureDetector(
              onTap: () async {
                if (mapMore['route'] != null) {
                  Get.to(mapMore['route']);
                } else if (mapMore['callback'] != null) {
                  Function.apply(mapMore['callback'], []);
                } else {
                  Get.back(result: {'moreEvent': eMoreEvent.app_tut1});
                }
              },
              child: Container(
                color: Colors.transparent,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    SizedBox(height: 20),
                    Padding(
                      padding: const EdgeInsets.only(left: 20, right: 20),
                      child: Row(
                        children: [
                          Expanded(
                            child: Txt(
                                txt: mapMore['title'].toString(),
                                txtColor: MyTheme.gray5Color,
                                txtSize: MyTheme.txtSize,
                                txtAlign: TextAlign.start,
                                isBold: false),
                          ),
                          Icon(
                            Icons.arrow_forward_ios,
                            color: MyTheme.gray3Color,
                            size: 20,
                          ),
                        ],
                      ),
                    ),
                    SizedBox(height: 30),
                    drawLine(),
                    SizedBox(height: 10),
                  ],
                ),
              ),
            );
          },
        ),
      ),
    );
  }
}
