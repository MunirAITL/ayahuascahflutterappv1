import 'package:aitl/config/cfg/AppDefine.dart';
import 'package:aitl/config/theme/MyTheme.dart';
import 'package:aitl/view/dashboard/more/resolutions/resolution_page.dart';
import 'package:aitl/view/widgets/Btn.dart';
import 'package:aitl/view/widgets/MMArrowBtn.dart';
import 'package:aitl/view/widgets/MMBtn.dart';
import 'package:aitl/view/widgets/Txt.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:url_launcher/url_launcher.dart';
import 'help_base.dart';

class SupportCenterPage extends StatefulWidget {
  const SupportCenterPage({Key key}) : super(key: key);

  @override
  State createState() => _SupportCenterPageState();
}

class _SupportCenterPageState extends BaseHelpStatefull<SupportCenterPage> {
  //  **************  app states start

  @override
  void onDetached() {
    try {
      log("app state = onDetached");
    } catch (e) {}
  }

  @override
  void onInactive() {
    try {
      log("app state = onInactive");
    } catch (e) {}
  }

  @override
  void onPaused() {
    try {
      log("app state = onPaused");
    } catch (e) {}
  }

  @override
  void onResumed() {
    try {
      log("app state = onResumed");
    } catch (e) {}
  }

  //  **************  app states end

  @override
  void initState() {
    super.initState();
    appInit();
  }

  @override
  void dispose() {
    super.dispose();
  }

  appInit() {
    try {} catch (e) {}
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: MyTheme.bgColor,
        appBar: AppBar(
          backgroundColor: MyTheme.bgColor,
          elevation: MyTheme.appbarElevation,
          title: Txt(
              txt: 'Support',
              txtColor: Colors.black,
              txtSize: MyTheme.appbarTitleFontSize,
              txtAlign: TextAlign.center,
              isBold: false),
          centerTitle: false,
        ),
        floatingActionButton: FloatingActionButton(
          backgroundColor: MyTheme.redColor,
          child: Icon(
            Icons.add,
            color: Colors.white, //The color which you want set.
          ),
          onPressed: () => {
            Get.to(
              () => ResolutionScreen(),
            )
          },
        ),
        body: drawLayout(),
      ),
    );
  }

  @override
  drawLayout() {
    return drawSupportButtons();
  }

  drawSupportButtons() {
    return Container(
      child: Column(
        children: [
          Padding(
            padding: const EdgeInsets.all(20),
            child: MMArrowBtn(
              txt: "Email: " + AppDefine.SUPPORT_EMAIL,
              icon: null,
              width: getW(context),
              height: getHP(context, MyTheme.btnHpa),
              radius: 20,
              callback: () {
                launch("mailto:" + AppDefine.SUPPORT_EMAIL);
              },
            ),
          ),
          Txt(
              txt: "or continue with",
              txtColor: Colors.black,
              txtSize: MyTheme.txtSize,
              txtAlign: TextAlign.center,
              isBold: false),
          Padding(
            padding: const EdgeInsets.all(20),
            child: MMArrowBtn(
              txt: "Call: " + AppDefine.SUPPORT_CALL,
              icon: null,
              width: getW(context),
              height: getHP(context, MyTheme.btnHpa),
              radius: 20,
              callback: () {
                launch("tel://" + AppDefine.SUPPORT_CALL);
              },
            ),
          ),
          Txt(
              txt: "or continue with",
              txtColor: Colors.black,
              txtSize: MyTheme.txtSize,
              txtAlign: TextAlign.center,
              isBold: false),
          Padding(
            padding: const EdgeInsets.all(20),
            child: MMArrowBtn(
              txt: "Send Message",
              icon: null,
              width: getW(context),
              height: getHP(context, MyTheme.btnHpa),
              radius: 20,
              callback: () async {
                Get.to(
                  () => ResolutionScreen(),
                );
              },
            ),
          ),
        ],
      ),
    );
  }
}
