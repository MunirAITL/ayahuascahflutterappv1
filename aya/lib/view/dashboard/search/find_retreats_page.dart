import 'package:aitl/config/theme/MyTheme.dart';
import 'package:aitl/data/network/ModelMgr.dart';
import 'package:aitl/data/network/NetworkMgr.dart';
import 'package:aitl/view/dashboard/search/find_retreats_base.dart';
import 'package:aitl/view/widgets/Txt.dart';
import 'package:aitl/view_model/observer/APIStateProvider.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import 'filters/filters_page.dart';

class FindRetreatsPage extends StatefulWidget {
  const FindRetreatsPage({Key key}) : super(key: key);

  @override
  _FindRetreatsPageState createState() => _FindRetreatsPageState();
}

class _FindRetreatsPageState extends BaseFindRetreatsStatefull<FindRetreatsPage>
    with APIStateListener {
  //  **************  app states start

  @override
  void onDetached() {
    try {
      log("app state = onDetached");
    } catch (e) {}
  }

  @override
  void onInactive() {
    try {
      log("app state = onInactive");
    } catch (e) {}
  }

  @override
  void onPaused() {
    try {
      log("app state = onPaused");
    } catch (e) {}
  }

  @override
  void onResumed() {
    try {
      log("app state = onResumed");
    } catch (e) {}
  }

  //  **************  app states end

  APIStateProvider _apiStateProvider;
  @override
  onAPIStateChanged(apiState, model) async {
    try {
      if (apiState.type == APIType.pending_review_rating_userid &&
          apiState.cls == this.runtimeType) {}
    } catch (e) {
      log(e.toString());
    }
  }

  refreshData() {
    try {} catch (e) {}
  }

  @override
  void initState() {
    super.initState();
    appInit();
  }

  //@mustCallSuper
  @override
  void dispose() {
    try {
      _apiStateProvider.unsubscribe(this);
      _apiStateProvider = null;
    } catch (e) {
      log(e.toString());
    }
    try {
      NetworkMgr().dispose();
    } catch (e) {}
    super.dispose();
  }

  appInit() async {
    try {
      _apiStateProvider = new APIStateProvider();
      _apiStateProvider.subscribe(this);
    } catch (e) {}
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: MyTheme.bgColor,
        appBar: AppBar(
          backgroundColor: MyTheme.bgColor,
          elevation: MyTheme.appbarElevation,
          automaticallyImplyLeading: false,
          title: Txt(
              txt: 'Find Retreats',
              txtColor: MyTheme.greenColor,
              txtSize: MyTheme.appbarTitleFontSize,
              txtAlign: TextAlign.center,
              isBold: true),
          centerTitle: false,
          actions: <Widget>[
            IconButton(
                icon: Image.asset("assets/images/icons/ico_filter.png"),
                onPressed: () {
                  Get.to(() => FilterPage()).then((value) async {
                    if (value != null) {
                      refreshData();
                    }
                  });
                }),
          ],
          bottom: drawAppbarNavBar((index) {}),
        ),
        body: drawLayout(),
      ),
    );
  }

  @override
  drawLayout() {
    return Padding(
      padding: const EdgeInsets.only(top: 20, left: 10, right: 10),
      child: Container(
        child: ListView.builder(
          primary: true,
          shrinkWrap: true,
          itemCount: listItems.length,
          itemBuilder: (BuildContext context, int index) {
            return drawItems(index);
          },
        ),
      ),
    );
  }
}
