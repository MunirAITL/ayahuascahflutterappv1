import 'package:aitl/config/theme/MyTheme.dart';
import 'package:aitl/view/dashboard/dashboard_base.dart';
import 'package:aitl/view/widgets/HorizontalBtns.dart';
import 'package:aitl/view/widgets/Txt.dart';
import 'package:aitl/view/widgets/google/GPlacesView.dart';
import 'package:aitl/view_model/helper/utils/FiltersHelper.dart';
import 'package:aitl/view_model/rx/FiltersController.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:google_maps_webservice/geolocation.dart';

abstract class BaseFilterStatefull<T extends StatefulWidget>
    extends BaseDashboard<T> {
  var filterController = Get.put(FiltersController());

  String taskAddress = "Search";
  Location taskCord;
  int indexSelected = 2;
  List<String> listTopBtn = [
    "In person",
    "Remotely",
    "All",
  ];

  drawHorizontalBtn(Function(int) callback) {
    return Padding(
      padding: const EdgeInsets.only(left: 20, right: 20, top: 30, bottom: 30),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Txt(
              txt: "To be done",
              txtColor: MyTheme.gray4Color,
              txtSize: MyTheme.txtSize,
              txtAlign: TextAlign.center,
              isBold: false),
          Container(
            color: Colors.transparent,
            child: HorizontalBtns(
              listTopBtn: listTopBtn,
              context: context,
              bgColor: Colors.white,
              bgColorHighlight: MyTheme.greenColor,
              txtColor: MyTheme.greenColor,
              txtColorHighlight: Colors.white,
              txtSize: MyTheme.txtSize,
              borderColor: Colors.black,
              radius: 10,
              index: indexSelected,
              fixedWidth:
                  (getW(context) / 3) - 25, //  0=in person, 1=remotely, 2= all
              callback: (v) {
                callback(v);
              },
            ),
          ),
        ],
      ),
    );
  }

  drawAll() {
    return Padding(
      padding: const EdgeInsets.only(left: 20, right: 20),
      child: Container(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            GPlacesView(
              title: "Location",
              address: (taskAddress != 'Search') ? taskAddress : '',
              bgColor: Colors.white,
              txtColor: MyTheme.gray4Color,
              isTxtBold: false,
              callback: (String _address, Location _loc) {
                //callback(address);
                taskAddress = _address;
                taskCord = _loc;
                setState(() {});
              },
            ),
            SizedBox(height: 30),
            FiltersHelper().drawDistanceSlider(filterController),
            SizedBox(height: 30),
            FiltersHelper().drawPriceSlider(filterController),
            SizedBox(height: 30),
            FiltersHelper().drawAvailableTaskOnly(filterController),
          ],
        ),
      ),
    );
  }

  drawRemotely() {
    return Padding(
      padding: const EdgeInsets.only(left: 20, right: 20),
      child: Container(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            SizedBox(height: 30),
            FiltersHelper().drawPriceSlider(filterController),
            SizedBox(height: 30),
            FiltersHelper().drawAvailableTaskOnly(filterController),
          ],
        ),
      ),
    );
  }

  drawInPerson() {
    return drawAll();
  }

  //  *********************** ui components

}
