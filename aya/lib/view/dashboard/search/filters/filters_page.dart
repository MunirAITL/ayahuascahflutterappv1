import 'package:aitl/config/theme/MyTheme.dart';
import 'package:aitl/data/model/dashboard/search/FiltersModel.dart';
import 'package:aitl/view/widgets/BottomBtn.dart';
import 'package:aitl/view/widgets/Txt.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:google_maps_webservice/directions.dart';
import 'filters_base.dart';

class FilterPage extends StatefulWidget {
  const FilterPage({Key key}) : super(key: key);
  @override
  State createState() => _FilterPageState();
}

class _FilterPageState extends BaseFilterStatefull<FilterPage> {
  FiltersModel filtersModel = FiltersModel();

//  **************  app states start

  @override
  void onDetached() {
    try {
      log("app state = onDetached");
    } catch (e) {}
  }

  @override
  void onInactive() {
    try {
      log("app state = onInactive");
    } catch (e) {}
  }

  @override
  void onPaused() {
    try {
      log("app state = onPaused");
    } catch (e) {}
  }

  @override
  void onResumed() {
    try {
      log("app state = onResumed");
    } catch (e) {}
  }

  //  **************  app states end

  @override
  void initState() {
    super.initState();
    appInit();
  }

  @override
  void dispose() {
    filterController.dispose();
    filterController = null;
    filtersModel = null;
    super.dispose();
  }

  appInit() async {
    try {
      try {
        filtersModel = await FiltersSharedPref().get();
        if (filtersModel != null) {
          taskAddress = filtersModel.location;
          taskCord = Location(filtersModel.lat, filtersModel.lng);
          indexSelected = filtersModel.tabNo;
          filterController.distance.value = filtersModel.distance ?? 6;
          filterController.isAvailableTasksOnly.value =
              filtersModel.isAvailableTasksOnly ?? false;
          filterController.minPrice.value = filtersModel.minPrice ?? 0;
          filterController.maxPrice.value = filtersModel.maxPrice ?? 9;
          filterController.setDistanceTxt(filterController.distance.value);
          filterController.setPriceTxt(
              filterController.minPrice.value, filterController.maxPrice.value);
          setState(() {});
        } else {
          //FiltersSharedPref().set(filtersModel);
          filterController.distance.value = 6;
          filterController.isAvailableTasksOnly.value = false;
          filterController.minPrice.value = 0;
          filterController.maxPrice.value = 9;
          filterController.setDistanceTxt(filterController.distance.value);
          filterController.setPriceTxt(
              filterController.minPrice.value, filterController.maxPrice.value);
          //setState(() {});
        }
      } catch (e) {}
    } catch (e) {}
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: Colors.white,
        resizeToAvoidBottomInset: true,
        appBar: AppBar(
          elevation: MyTheme.appbarElevation,
          backgroundColor: MyTheme.bgColor,
          leading: IconButton(
              icon: Icon(Icons.close),
              onPressed: () {
                Get.back();
              }),
          title: Txt(
              txt: 'Filters',
              txtColor: MyTheme.appbarTxtColor,
              txtSize: MyTheme.appbarTitleFontSize,
              txtAlign: TextAlign.center,
              isBold: true),
          centerTitle: false,
        ),
        bottomNavigationBar: drawBottomBtn(
            context: context,
            text: "Apply",
            callback: () async {
              filtersModel = FiltersModel.fromJson({
                'tabNo': indexSelected,
                'location': taskAddress != 'Search' ? taskAddress : '',
                'lat': taskCord != null ? taskCord.lat : 0.0,
                'lng': taskCord != null ? taskCord.lng : 0.0,
                'distance': filterController.distance.value,
                'isAvailableTasksOnly':
                    filterController.isAvailableTasksOnly.value,
                'minPrice': filterController.minPrice.value,
                'maxPrice': filterController.maxPrice.value,
              });
              FiltersSharedPref().set(filtersModel);
              Get.back(result: true);
            }),
        body: GestureDetector(
          onTap: () {
            FocusScope.of(context).requestFocus(new FocusNode());
          },
          child: drawLayout(),
        ),
      ),
    );
  }

  @override
  drawLayout() {
    return Container(
      child: SingleChildScrollView(
        child: Column(
          children: [
            Container(
              child: drawHorizontalBtn(
                (v) {
                  indexSelected = v;
                  setState(() {});
                },
              ),
            ),
            (indexSelected == 2)
                ? drawAll()
                : (indexSelected == 1)
                    ? drawRemotely()
                    : drawInPerson(),
          ],
        ),
      ),
    );
  }
}
