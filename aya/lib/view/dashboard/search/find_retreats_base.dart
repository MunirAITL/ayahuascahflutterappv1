import 'package:aitl/config/cfg/AppConfig.dart';
import 'package:aitl/config/server/ServerUrls.dart';
import 'package:aitl/config/theme/MyTheme.dart';
import 'package:aitl/view/dashboard/dashboard_base.dart';
import 'package:aitl/view/dashboard/search/filters/filters_page.dart';
import 'package:aitl/view/widgets/AppbarBotProgbar.dart';
import 'package:aitl/view/widgets/MyNetworkImage.dart';
import 'package:aitl/view/widgets/PriceBox.dart';
import 'package:aitl/view/widgets/Txt.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

abstract class BaseFindRetreatsStatefull<T extends StatefulWidget>
    extends BaseDashboard<T> {
  List<String> listTopBtn = [
    "Anywhere",
    getCurSign() + "100" + "-" + getCurSign() + "500",
    "16 June",
    "Nearby",
  ];

  final listItems = [
    {
      'image': "https://herotasker.com/api/content/media/default_avatar.png",
      'title': 'Ayahuasca Therapy',
      'date': 'Monday, 21 January, 2021',
      'addr': '28 Constituation Road, United Kingdom',
      'price': 200,
    },
    {
      'image': ServerUrls.MISSING_IMG,
      'title': 'Ayahuasca Therapy',
      'date': 'Monday, 21 January, 2021',
      'addr': '28 Constituation Road, United Kingdom',
      'price': 300,
    },
    {
      'image': ServerUrls.MISSING_IMG,
      'title': 'Ayahuasca Therapy',
      'date': 'Monday, 21 January, 2021',
      'addr': '28 Constituation Road, United Kingdom',
      'price': 400,
    },
    {
      'image': ServerUrls.MISSING_IMG,
      'title': 'Ayahuasca Therapy',
      'date': 'Monday, 21 January, 2021',
      'addr': '28 Constituation Road, United Kingdom',
      'price': 500,
    },
  ];

  bool isLoading = false;
  int btnFilterIndex = 0;

  drawAppbarNavBar(Function callback) {
    return PreferredSize(
      preferredSize: Size.fromHeight(AppConfig.findretreats_height),
      child: Container(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
              child: Padding(
                padding: const EdgeInsets.all(10),
                child: Container(
                  width: getW(context),
                  height: 40,
                  child: Center(
                    child: ListView.builder(
                      itemCount: listTopBtn.length,
                      scrollDirection: Axis.horizontal,
                      primary: false,
                      shrinkWrap: true,
                      itemBuilder: (context, index) {
                        return GestureDetector(
                          onTap: () {
                            btnFilterIndex = index;
                            setState(() {});
                          },
                          child: Card(
                            elevation: (index == btnFilterIndex) ? 2 : 5,
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(10),
                            ),
                            color: (index == btnFilterIndex)
                                ? MyTheme.greenColor
                                : Colors.white,
                            margin: EdgeInsets.only(left: 5, right: 5),
                            child: Padding(
                              padding: const EdgeInsets.only(
                                  top: 5, bottom: 5, left: 10, right: 10),
                              child: Center(
                                child: Txt(
                                    txt: listTopBtn[index],
                                    txtColor: (index == btnFilterIndex)
                                        ? Colors.white
                                        : Colors.grey,
                                    txtSize: MyTheme.txtSize - .2,
                                    txtAlign: TextAlign.center,
                                    isBold: false),
                              ),
                            ),
                          ),
                        );
                      },
                    ),
                  ),
                ),
              ),
            ),
            (isLoading)
                ? AppbarBotProgBar(
                    backgroundColor: MyTheme.greenColor,
                  )
                : Container(
                    height: 4,
                    //color: MyTheme.greenColor,
                  )
          ],
        ),
      ),
    );
  }

  drawItems(int index) {
    final map = listItems[index];
    return Card(
      elevation: 5,
      shadowColor: Colors.black38,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(15.0),
      ),
      child: Padding(
        padding: const EdgeInsets.all(10),
        child: Row(
          children: [
            Container(
              width: getWP(context, 15),
              height: getWP(context, 10),
              decoration: BoxDecoration(
                color: Colors.black,
                /*image: DecorationImage(
                  image: MyNetworkImage.loadProfileImage(map['image']),
                  fit: BoxFit.fill,
                ),*/
              ),
            ),
            SizedBox(width: 20),
            Expanded(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Txt(
                          txt: map['title'],
                          txtColor: Colors.black,
                          txtSize: MyTheme.txtSize,
                          txtAlign: TextAlign.center,
                          isBold: true),
                      Txt(
                          txt: getCurSign() + map['price'].toString(),
                          txtColor: Colors.red,
                          txtSize: MyTheme.txtSize,
                          txtAlign: TextAlign.start,
                          isBold: false),
                    ],
                  ),
                  SizedBox(height: 15),
                  Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Container(
                        width: 15,
                        height: 15,
                        child: Image.asset(
                          "assets/images/icons/ico_clock.png",
                          fit: BoxFit.fill,
                          color: Colors.grey,
                        ),
                      ),
                      SizedBox(width: 10),
                      Expanded(
                        child: Transform.translate(
                          offset: Offset(0, -5),
                          child: Txt(
                              txt: map['date'],
                              txtColor: Colors.grey,
                              txtSize: MyTheme.txtSize,
                              txtAlign: TextAlign.start,
                              isBold: false),
                        ),
                      ),
                    ],
                  ),
                  SizedBox(height: 10),
                  Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Container(
                        width: 15,
                        height: 15,
                        child: Image.asset(
                          "assets/images/icons/map_pin_icon.png",
                          fit: BoxFit.fill,
                          color: Colors.grey,
                        ),
                      ),
                      SizedBox(width: 10),
                      Expanded(
                        child: Transform.translate(
                          offset: Offset(0, -5),
                          child: Txt(
                              txt: map['addr'],
                              txtColor: Colors.grey,
                              txtSize: MyTheme.txtSize,
                              txtAlign: TextAlign.start,
                              isBold: false),
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
