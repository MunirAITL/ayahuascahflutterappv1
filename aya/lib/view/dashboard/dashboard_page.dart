import 'package:aitl/data/db/DBMgr.dart';
import 'package:aitl/data/network/CookieMgr.dart';
import 'package:aitl/data/network/ModelMgr.dart';
import 'package:aitl/data/network/NetworkMgr.dart';
import 'package:aitl/mixin.dart';
import 'package:aitl/view/auth/auth_screen.dart';
import 'package:aitl/view/dashboard/main/main_page.dart';
import 'package:aitl/view/dashboard/more/more_page.dart';
import 'package:aitl/view/dashboard/noti/NotiTab.dart';
import 'package:aitl/view/dashboard/search/find_retreats_page.dart';
import 'package:aitl/view/dashboard/timeline/TimeLineTab.dart';
import 'package:aitl/view/widgets/botnav/bottomNavigation.dart';
import 'package:aitl/view/widgets/botnav/tabItem.dart';
import 'package:aitl/view/widgets/dialog/HelpTutDialog.dart';
import 'package:aitl/view_model/observer/APIStateProvider.dart';
import 'package:aitl/view_model/observer/StateProvider.dart';
import 'package:aitl/view_model/rx/BotNavController.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

//  next on https://pub.dev/packages/persistent_bottom_nav_bar

class DashboardPage extends StatefulWidget {
  static const int TAB_POSTTASK = 0;
  static const int TAB_MYTASK = 1;
  static const int TAB_FINDWORKS = 2;
  static const int TAB_MESSAGES = 3;
  static const int TAB_MORE = 4;

  @override
  State createState() => DashboardPageState();
}

class DashboardPageState extends State<DashboardPage>
    with Mixin, APIStateListener, StateListener {
  final List<Widget> listTabbar = [];
  bool isDialogHelpOpenned = false;

  final botNavController = Get.put(BotNavController());

  //static int currentTab = 0;

  final List<TabItem> tabs = [
    TabItem(
      tabName: "Dashboard",
      icon: AssetImage("assets/images/tabbar/tabbar_db.png"),
      //page: NewCaseTab(),
      page: MainPage(),
    ),
    TabItem(
      tabName: "Search",
      icon: AssetImage("assets/images/tabbar/tabbar_search.png"),
      page: FindRetreatsPage(),
    ),
    TabItem(
      tabName: "Messages",
      icon: AssetImage("assets/images/tabbar/tabbar_msg.png"),
      page: TimeLineTab(),
    ),
    TabItem(
      tabName: "Notifications",
      icon: AssetImage("assets/images/tabbar/tabbar_noti.png"),
      page: NotiTab(),
    ),
    TabItem(
      tabName: "More",
      icon: AssetImage("assets/images/tabbar/tabbar_more.png"),
      page: MorePage(),
    )
  ];

  DashboardPageState() {
    tabs.asMap().forEach((index, details) {
      details.setIndex(index);
    });
  }

  void _selectTab(int index) {
    //if (index == botNavController.index.value) {
    // pop to first route
    // if the user taps on the active tab
    //tabs[index].key.currentState.popUntil((route) => route.isFirst);
    //setState(() {});
    //} else {
    // update the state
    // in order to repaint

    if (mounted) {
      setState(() => botNavController.index.value = index);
    }
    //}
  }

  StateProvider _stateProvider;
  @override
  onStateChanged(ObserverState state, dynamic data) {
    try {
      if (state == ObserverState.STATE_RELOAD_TAB) {
        botNavController.isShowHelpDialogExtraHand.value = false;
        _selectTab(data ?? 0);
      } else if (state == ObserverState.STATE_LOGOUT) {
        CookieMgr().delCookiee();
        DBMgr.shared.delTable("User");
        Get.offAll(
          () => AuthScreen(),
        ).then((value) {
          //callback(route);
        });
      } else if (state == ObserverState.STATE_OPEN_HELP_DIALOG) {
        if (!isDialogHelpOpenned) {
          isDialogHelpOpenned = true;
          botNavController.isShowHelpDialogExtraHand.value = false;
          _selectTab(DashboardPage.TAB_POSTTASK);
          Get.dialog(HelpTutDialog()).then((value) {
            setState(() {
              isDialogHelpOpenned = false;
              _selectTab(DashboardPage.TAB_FINDWORKS);
            });
          });
        }
      }
    } catch (e) {}
  }

  APIStateProvider _apiStateProvider;
  @override
  onAPIStateChanged(apiState, model) async {
    try {
      if (apiState.type == APIType.pending_review_rating_userid &&
          apiState.cls == this.runtimeType) {}
    } catch (e) {
      log(e.toString());
    }
  }

  @override
  void initState() {
    super.initState();
    appInit();
  }

  //@mustCallSuper
  @override
  void dispose() {
    try {
      _apiStateProvider.unsubscribe(this);
      _apiStateProvider = null;
    } catch (e) {
      log(e.toString());
    }
    try {
      _stateProvider.unsubscribe(this);
      _stateProvider = null;
    } catch (e) {
      log(e.toString());
    }
    try {
      botNavController.dispose();
    } catch (e) {}
    try {
      NetworkMgr().dispose();
    } catch (e) {}
    super.dispose();
  }

  appInit() async {
    try {
      _apiStateProvider = new APIStateProvider();
      _apiStateProvider.subscribe(this);
    } catch (e) {}
    try {
      _stateProvider = new StateProvider();
      _stateProvider.subscribe(this);
    } catch (e) {}
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        resizeToAvoidBottomInset: false,
        backgroundColor: Colors.white,
        body: WillPopScope(
          onWillPop: () async {
            final isFirstRouteInCurrentTab =
                !await tabs[botNavController.index.value]
                    .key
                    .currentState
                    .maybePop();
            if (isFirstRouteInCurrentTab) {
              // if not on the 'main' tab
              if (botNavController.index.value != 0) {
                // select 'main' tab
                _selectTab(0);
                // back button handled by app
                return false;
              }
            }
            // let system handle back button if we're on the first route
            return isFirstRouteInCurrentTab;
          },
          // this is the base scaffold
          // don't put appbar in here otherwise you might end up
          // with multiple appbars on one screen
          // eventually breaking the app
          child: Scaffold(
            // indexed stack shows only one child
            body: IndexedStack(
              index: botNavController.index.value,
              children: tabs.map((e) => e.page).toList(),
            ),
            // Bottom navigation
            bottomNavigationBar: BottomNavigation(
              context: context,
              onSelectTab: _selectTab,
              botNavController: botNavController,
              tabs: tabs,
              isHelpTut: isDialogHelpOpenned,
              totalMsg: 0,
              totalNoti: 0,
            ),
          ),
        ),
      ),
    );
  }
}
