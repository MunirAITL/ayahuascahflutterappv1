import 'package:aitl/config/cfg/AppConfig.dart';
import 'package:aitl/config/server/ServerUrls.dart';
import 'package:aitl/config/theme/MyTheme.dart';
import 'package:aitl/data/model/dashboard/more/noti/NotiModel.dart';
import 'package:aitl/data/model/dashboard/noti/DeleteNotification.dart';
import 'package:aitl/data/model/dashboard/noti/NotiAPIMgr.dart';
import 'package:aitl/data/network/NetworkMgr.dart';
import 'package:aitl/view/dashboard/dashboard_page.dart';
import 'package:aitl/view/widgets/AppbarBotProgbar.dart';
import 'package:aitl/view/widgets/BtnOutlineAndBackground.dart';
import 'package:aitl/view/widgets/MMBtn.dart';
import 'package:aitl/view/widgets/MyNetworkImage.dart';
import 'package:aitl/view/widgets/Txt.dart';
import 'package:aitl/view/widgets/dialog/DeleteConfirmationAlertDialog.dart';
import 'package:aitl/view_model/helper/noti/NotiHelper.dart';
import 'package:aitl/view_model/observer/StateProvider.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:aitl/Mixin.dart';

class NotiTab extends StatefulWidget {
  @override
  State createState() => _NotiTabState();
}

class _NotiTabState extends State<NotiTab> with Mixin, StateListener {
  StateProvider _stateProvider;

  List<NotiModel> listNotiModel = [];

  //  page stuff start here
  bool isPageDone = false;
  bool isLoading = false;
  int pageStart = 0;
  int pageCount = AppConfig.page_limit;

  @override
  onStateChanged(ObserverState state, code) async {
    isLoading = false;
    if (state == ObserverState.STATE_RELOAD_TAB) {
      _getRefreshData();
    }
  }

  onPageLoad() async {
    setState(() {
      isLoading = true;
    });
    try {
      NotiAPIMgr().wsOnPageLoad(
        context: context,
        pageStart: pageStart,
        pageCount: pageCount,
        callback: (model) {
          if (model != null && mounted) {
            try {
              if (model.success) {
                try {
                  final List<dynamic> notifications =
                      model.responseData.notifications;

                  if (notifications != null && mounted) {
                    //  checking to see whether page is finished to stop on reload data through API after end of scrolling for scalibility
                    if (notifications.length != pageCount) {
                      isPageDone = true;
                    }
                    try {
                      for (NotiModel noti in notifications) {
                        listNotiModel.add(noti);
                      }
                    } catch (e) {
                      log(e.toString());
                    }
                    log(listNotiModel.toString());
                    if (mounted) {
                      setState(() {
                        isLoading = false;
                      });
                    }
                  } else {
                    if (mounted) {
                      if (mounted) {
                        setState(() {
                          isLoading = false;
                        });
                      }
                    }
                  }
                } catch (e) {
                  log(e.toString());
                }
              } else {
                try {
                  //final err = model.errorMessages.login[0].toString();
                  if (mounted) {
                    showAlert(msg: "Notifications not found", isToast: true);
                  }
                } catch (e) {
                  log(e.toString());
                  if (mounted) {
                    setState(() {
                      isLoading = false;
                    });
                  }
                }
              }
            } catch (e) {
              log(e.toString());
              if (mounted) {
                setState(() {
                  isLoading = false;
                });
              }
            }
          }
        },
      );
    } catch (e) {
      log(e.toString());
      if (mounted) {
        setState(() {
          isLoading = false;
        });
      }
    }
  }

  Future<void> _getRefreshData() async {
    pageStart = 0;
    isPageDone = false;
    listNotiModel.clear();
    onPageLoad();
  }

  @override
  void initState() {
    super.initState();
    try {
      appInit();
    } catch (e) {}
  }

  @override
  void dispose() {
    listNotiModel = null;
    _stateProvider.unsubscribe(this);
    _stateProvider = null;
    try {
      NetworkMgr().dispose();
    } catch (e) {}
    super.dispose();
  }

  appInit() async {
    try {
      _stateProvider = new StateProvider();
      _stateProvider.subscribe(this);
    } catch (e) {}
    try {
      _getRefreshData();
    } catch (e) {}
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: MyTheme.themeData.accentColor,
        appBar: AppBar(
          centerTitle: false,
          elevation: 1,
          title: Txt(
              txt: "Notifications",
              txtColor: MyTheme.greenColor,
              txtSize: MyTheme.appbarTitleFontSize,
              txtAlign: TextAlign.start,
              isBold: true),
          actions: <Widget>[
            SizedBox(width: 30)
            /* IconButton(
              iconSize: 30,
              icon: Image.asset("assets/images/icons/help_circle_icon.png"),
              onPressed: () {
                _stateProvider.notify(ObserverState.STATE_CHANGED_tabbar5);
              },
            )*/
          ],
          iconTheme: IconThemeData(color: MyTheme.greenColor),
          backgroundColor: MyTheme.themeData.accentColor,
          bottom: PreferredSize(
            preferredSize:
                new Size(getW(context), getHP(context, (isLoading) ? 1 : 0)),
            child: (isLoading)
                ? AppbarBotProgBar(
                    backgroundColor: MyTheme.appbarProgColor,
                  )
                : SizedBox(),
          ),
        ),
        body: GestureDetector(
          behavior: HitTestBehavior.opaque,
          onPanDown: (detail) {
            FocusScope.of(context).requestFocus(new FocusNode());
          },
          onTap: () {
            FocusScope.of(context).requestFocus(new FocusNode());
          },
          child: drawLayout(),
        ),
      ),
    );
  }

  drawLayout() {
    try {
      return Container(
        child: (listNotiModel.length > 0)
            ? NotificationListener(
                onNotification: (scrollNotification) {
                  if (scrollNotification is ScrollStartNotification) {
                    //print('Widget has started scrolling');
                  } else if (scrollNotification is ScrollEndNotification) {
                    if (!isPageDone) {
                      pageStart++;
                      onPageLoad();
                    }
                  }
                  return true;
                },
                child: Theme(
                  data: MyTheme.refreshIndicatorTheme,
                  child: RefreshIndicator(
                    onRefresh: _getRefreshData,
                    child: Padding(
                      padding: const EdgeInsets.only(top: 20),
                      child: ListView.builder(
                        addAutomaticKeepAlives: true,
                        cacheExtent: AppConfig.page_limit.toDouble(),
                        scrollDirection: Axis.vertical,
                        shrinkWrap: true,
                        //primary: false,
                        itemCount: listNotiModel.length,
                        itemBuilder: (BuildContext context, int index) {
                          return drawNotiItem(index);
                        },
                      ),
                    ),
                  ),
                ),
              )
            : (!isLoading)
                ? Center(
                    child: Padding(
                      padding: const EdgeInsets.only(left: 50, right: 50),
                      child: ListView(
                        //mainAxisAlignment: MainAxisAlignment.start,
                        shrinkWrap: true,
                        children: [
                          Container(
                            width: getWP(context, 100),
                            height: getHP(context, 48),
                            child: Image.asset(
                              'assets/images/nf/case_nf.png',
                              fit: BoxFit.fill,
                            ),
                          ),
                          //SizedBox(height: 40),
                          Padding(
                            padding: const EdgeInsets.only(left: 10, right: 10),
                            child: Container(
                              width: getWP(context, 70),
                              child: Txt(
                                txt:
                                    "Looks like you haven't got any notification yet",
                                txtColor: MyTheme.mycasesNFBtnColor,
                                txtSize: MyTheme.txtSize + .2,
                                txtAlign: TextAlign.center,
                                isBold: false,
                                //txtLineSpace: 1.5,
                              ),
                            ),
                          ),
                          SizedBox(height: 20),
                          Padding(
                            padding: const EdgeInsets.only(left: 20, right: 20),
                            child: MMBtn(
                              txt: "Refresh",
                              bgColor: MyTheme.greenColor,
                              width: getWP(context, 50),
                              height: getHP(context, 6),
                              callback: () {
                                onPageLoad();
                              },
                            ),
                          ),
                        ],
                      ),
                    ),
                  )
                : SizedBox(),
      );
    } catch (e) {}
  }

  drawNotiItem(index) {
    try {
      NotiModel notiModel = listNotiModel[index];

      if (notiModel == null) return SizedBox();
      Map<String, dynamic> notiMap = NotiHelper().getNotiMap(model: notiModel);
      if (notiMap.length == 0) return SizedBox();

      String txt = notiMap['txt'].toString();
      String eventName = '';
      if ((txt.endsWith(' ' + notiModel.eventName))) {
        // txt = txt.replaceAll(notiModel.eventName.trim(), '');
        eventName = notiModel.eventName;
      }

      String initiatorDisplayName = '';
      if ((txt.startsWith(notiModel.initiatorDisplayName))) {
        // txt = txt.replaceAll(notiModel.initiatorDisplayName.trim(), '');
        initiatorDisplayName = notiModel.initiatorDisplayName;
      }

      return GestureDetector(
        onTap: () async {
          /*  if (mounted) {
            await NotiHelper().setRoute(
                context: context,
                notiModel: notiModel,
                notiMap: notiMap,
                callback: () {
                  _stateProvider.notify(ObserverState.STATE_CHANGED_tabbar3);
                });
          }*/
        },
        child: Container(
          //height: getHP(context, 25),
          //color: Colors.blue,
          child: Card(
            color: Colors.white,
            elevation: 0,
            child: Column(
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Padding(
                      padding: const EdgeInsets.only(left: 0, top: 10),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Container(
                            decoration: MyTheme.picEmboseCircleDeco,
                            child: CircleAvatar(
                              radius: 45,
                              backgroundColor: Colors.transparent,
                              backgroundImage: new CachedNetworkImageProvider(
                                MyNetworkImage.checkUrl((notiModel != null)
                                    ? notiModel.initiatorImageUrl
                                    : ServerUrls.MISSING_IMG),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                    Expanded(
                      child: Padding(
                        padding: const EdgeInsets.only(left: 20, right: 10),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            SizedBox(height: 20),
                            Txt(
                              txt: notiModel.initiatorDisplayName,
                              txtColor: Colors.black,
                              txtSize: 2,
                              isBold: true,
                              txtAlign: TextAlign.start,
                              maxLines: 1,
                            ),
                            Txt(
                              txt: txt,
                              txtColor: Colors.black,
                              txtSize: 1.5,
                              isBold: false,
                              txtAlign: TextAlign.start,
                              maxLines: 2,
                            ),
                            SizedBox(height: 20),
                            Txt(
                              txt: notiMap['publishDateTime'],
                              txtColor: MyTheme.mycasesNFBtnColor,
                              txtSize: MyTheme.txtSize - .6,
                              txtAlign: TextAlign.start,
                              isBold: false,
                            ),
                            SizedBox(height: 10),
                            Container(
                              width: getW(context),
                              height: 45,
                              child: Row(
                                crossAxisAlignment: CrossAxisAlignment.center,
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  Expanded(
                                    child: Container(
                                      child: BtnOutlineAndBackground(
                                          backgroundColor: Colors.white,
                                          txt: "View Case",
                                          txtColor: MyTheme.greenColor,
                                          borderColor: MyTheme.greenColor,
                                          callback: () {
                                            NotiHelper().setRoute(
                                                context: context,
                                                notiModel: notiModel,
                                                notiMap: notiMap,
                                                callback: () {
                                                  _stateProvider.notify(
                                                      ObserverState
                                                          .STATE_RELOAD_TAB,
                                                      DashboardPage
                                                          .TAB_MESSAGES);
                                                });
                                          }),
                                    ),
                                  ),
                                  SizedBox(width: 5),
                                  Expanded(
                                    child: Container(
                                      child: BtnOutlineAndBackground(
                                        txt: "Delete",
                                        txtColor: Colors.white,
                                        borderColor: MyTheme.greenColor,
                                        backgroundColor: MyTheme.greenColor,
                                        callback: () {
                                          DeleteConfirmationAlertDialog(
                                              deleteClick: () {
                                                Navigator.of(context,
                                                        rootNavigator: true)
                                                    .pop();

                                                NotiAPIMgr()
                                                    .wsDeleteNotification(
                                                        context: context,
                                                        id: notiModel.id
                                                            .toString(),
                                                        callback:
                                                            (DeleteNotificationModel
                                                                model) {
                                                          if (model != null &&
                                                              mounted) {
                                                            try {
                                                              if (model
                                                                  .success) {
                                                                log("Noti delete model = " +
                                                                    model
                                                                        .toJson()
                                                                        .toString());

                                                                _getRefreshData();
                                                              }
                                                            } catch (e) {
                                                              log("Noti delete err = " +
                                                                  e.toString());
                                                            }
                                                          } else {
                                                            log("Notification delete model getting null ");
                                                          }
                                                        });
                                              },
                                              cancelClick: () {
                                                Navigator.of(context,
                                                        rootNavigator: true)
                                                    .pop();
                                              },
                                              deleteTxt: "Delete",
                                              cancelTxt: "Cancel",
                                              context: context,
                                              alertBody:
                                                  "Press \'Delete\' to Remove this notification ?",
                                              alertTitle: "Delete Alert !");
                                        },
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            SizedBox(height: 10),
                          ],
                        ),
                      ),
                    ),
                    notiModel.isRead
                        ? Padding(
                            padding: const EdgeInsets.only(
                                left: 3.0, right: 9.0, top: 30.0, bottom: 3.0),
                            child: Container(
                              decoration: new BoxDecoration(
                                color: Colors.white,
                                borderRadius: new BorderRadius.circular(10.0),
                              ),
                              width: 15,
                              height: 15,
                            ),
                          )
                        : Padding(
                            padding: const EdgeInsets.only(
                                left: 3.0, right: 9.0, top: 3.0, bottom: 3.0),
                            child: Container(
                              decoration: new BoxDecoration(
                                color: Colors.blueAccent,
                                borderRadius: new BorderRadius.circular(10.0),
                              ),
                              width: 15,
                              height: 15,
                            ),
                          )
                  ],
                ),
                Container(
                  margin: EdgeInsets.only(left: 10, right: 10),
                  width: getW(context),
                  color: Colors.grey[300],
                  height: 1,
                )
              ],
            ),
          ),
        ),
      );
    } catch (e) {
      log(e.toString());
    }
    return SizedBox();
  }
}
