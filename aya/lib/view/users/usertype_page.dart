import 'package:aitl/config/theme/MyTheme.dart';
import 'package:aitl/view/auth/login_page.dart';
import 'package:aitl/view/widgets/MMBtn.dart';
import 'package:aitl/view/widgets/Txt.dart';
import 'package:aitl/view_model/rx/RadioSelController.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:aitl/mixin.dart';

class UserTypePage extends StatelessWidget with Mixin {
  final radioSelController = new RadioSelController();

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: MyTheme.bgColor,
        appBar: AppBar(
          backgroundColor: MyTheme.bgColor,
          elevation: MyTheme.appbarElevation,
          automaticallyImplyLeading: false,
          title: Txt(
              txt: 'User Role',
              txtColor: Colors.black,
              txtSize: MyTheme.appbarTitleFontSize,
              txtAlign: TextAlign.center,
              isBold: false),
          centerTitle: true,
        ),
        body: drawLayout(context),
        bottomNavigationBar: Padding(
          padding: const EdgeInsets.all(20),
          child: MMBtn(
              txt: "Get Started",
              height: getHP(context, MyTheme.btnHpa),
              width: getW(context),
              callback: () {
                Get.to(() => LoginPage());
              }),
        ),
      ),
    );
  }

  drawLayout(context) {
    return Center(
      child: Container(
        child: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              SizedBox(height: 40),
              Txt(
                txt: "What is your role?",
                txtColor: MyTheme.redColor,
                txtSize: MyTheme.txtSize + 1,
                txtAlign: TextAlign.center,
                isBold: true,
              ),
              SizedBox(height: 20),
              drawRadioBox(
                  context: context,
                  index: 0,
                  title: "Employer",
                  subTitle: "I want to give\nwork"),
              SizedBox(height: 20),
              drawRadioBox(
                  context: context,
                  index: 1,
                  title: "Worker",
                  subTitle: "I want to work\nand earn money"),
            ],
          ),
        ),
      ),
    );
  }

  drawRadioBox(
      {BuildContext context, int index, String title, String subTitle}) {
    return Container(
      width: getWP(context, 50),
      //height: getWP(context, 50),
      //color: Colors.black,
      child: Card(
        elevation: 5,
        child: Padding(
          padding: const EdgeInsets.all(10),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              SizedBox(height: 10),
              Txt(
                txt: title,
                txtColor: Colors.black,
                txtSize: MyTheme.txtSize + .3,
                txtAlign: TextAlign.center,
                isBold: true,
              ),
              SizedBox(height: 10),
              Txt(
                txt: subTitle,
                txtColor: Colors.black,
                txtSize: MyTheme.txtSize,
                txtAlign: TextAlign.center,
                isBold: false,
              ),
              SizedBox(height: 10),
              Obx(() => Theme(
                    data: MyTheme.radioThemeData,
                    child: Radio(
                        value: index,
                        groupValue: radioSelController.radioIndex.value,
                        onChanged: (value) {
                          radioSelController.setSelectedIndex(
                              selectValue: value);
                        }),
                  )),
              SizedBox(height: 10),
            ],
          ),
        ),
      ),
    );
  }
}
