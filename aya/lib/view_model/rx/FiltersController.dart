import 'package:aitl/config/cfg/AppDefine.dart';
import 'package:aitl/view/widgets/PriceBox.dart';
import 'package:get/get.dart';

class FiltersController extends GetxController {
  var distance = 6.obs;
  var minPrice = 0.obs;
  var maxPrice = 9.obs;
  var isAvailableTasksOnly = false.obs;

  //  tmp
  var distanceTxt = "".obs;
  var priceTxt = "".obs;

  setDistanceTxt(dis) {
    switch (dis) {
      case 0:
        distanceTxt.value = "5km";
        break;
      case 1:
        distanceTxt.value = "10km";
        break;
      case 2:
        distanceTxt.value = "15km";
        break;
      case 3:
        distanceTxt.value = "25km";
        break;
      case 4:
        distanceTxt.value = "50km";
        break;
      case 5:
        distanceTxt.value = "100km";
        break;
      case 6:
        distanceTxt.value = "100km+";
        break;
      default:
        break;
    }
  }

  getDistance(dis) {
    switch (dis) {
      case 0:
        return 5;
        break;
      case 1:
        return 10;
        break;
      case 2:
        return 15;
        break;
      case 3:
        return 25;
        break;
      case 4:
        return 50;
        break;
      case 5:
        return 100;
        break;
      case 6:
        return 500;
        break;
      default:
        return 500;
        break;
    }
  }

  setPriceTxt(minPric, maxPric) {
    switch (minPric) {
      case 0:
        priceTxt.value = getCurSign() + "50" + "-";
        break;
      case 1:
        priceTxt.value = getCurSign() + "100" + "-";
        break;
      case 2:
        priceTxt.value = getCurSign() + "300" + "-";
        break;
      case 3:
        priceTxt.value = getCurSign() + "500" + "-";
        break;
      case 4:
        priceTxt.value = getCurSign() + "1000" + "-";
        break;
      case 5:
        priceTxt.value = getCurSign() + "2000" + "-";
        break;
      case 6:
        priceTxt.value = getCurSign() + "3000" + "-";
        break;
      case 7:
        priceTxt.value = getCurSign() + "5000" + "-";
        break;
      case 8:
        priceTxt.value = getCurSign() + "10000" + "-";
        break;
      case 9:
        priceTxt.value = getCurSign() + "100000" + "-";
        break;
      default:
    }
    switch (maxPric) {
      case 0:
        priceTxt.value += getCurSign() + "50";
        break;
      case 1:
        priceTxt.value += getCurSign() + "100";
        break;
      case 2:
        priceTxt.value += getCurSign() + "300";
        break;
      case 3:
        priceTxt.value += getCurSign() + "500";
        break;
      case 4:
        priceTxt.value += getCurSign() + "1000";
        break;
      case 5:
        priceTxt.value += getCurSign() + "2000";
        break;
      case 6:
        priceTxt.value += getCurSign() + "3000";
        break;
      case 7:
        priceTxt.value += getCurSign() + "5000";
        break;
      case 8:
        priceTxt.value += getCurSign() + "10000";
        break;
      case 9:
        priceTxt.value += getCurSign() + "100000+";
        break;
      default:
    }
  }

  getMinPrice(minPric) {
    switch (minPric) {
      case 0:
        return 50;
        break;
      case 1:
        return 100;
        break;
      case 2:
        return 300;
        break;
      case 3:
        return 500;
        break;
      case 4:
        return 1000;
        break;
      case 5:
        return 2000;
        break;
      case 6:
        return 3000;
        break;
      case 7:
        return 5000;
        break;
      case 8:
        return 10000;
        break;
      case 9:
        return 100000;
        break;
      default:
        return 50;
    }
  }

  getMaxPrice(maxPric) {
    switch (maxPric) {
      case 0:
        return 50;
        break;
      case 1:
        return 100;
        break;
      case 2:
        return 300;
        break;
      case 3:
        return 500;
        break;
      case 4:
        return 1000;
        break;
      case 5:
        return 2000;
        break;
      case 6:
        return 3000;
        break;
      case 7:
        return 5000;
        break;
      case 8:
        return 10000;
        break;
      case 9:
        return 100000;
        break;
      default:
        return 100000;
    }
  }
}
