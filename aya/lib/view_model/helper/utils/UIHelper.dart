import 'package:aitl/config/cfg/AppConfig.dart';
import 'package:aitl/config/theme/MyTheme.dart';
import 'package:aitl/controller/classes/Common.dart';
import 'package:aitl/view/dashboard/dashboard_page.dart';
import 'package:aitl/view/dashboard/more/resolutions/res_page.dart';
import 'package:aitl/view/widgets/Txt.dart';
import 'package:aitl/view_model/observer/StateProvider.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:polygon_clipper/polygon_border.dart';
import 'package:responsive_flutter/responsive_flutter.dart';

class UIHelper {
  drawLine({Color colr = Colors.grey, double h = .5}) {
    return Container(color: colr, height: h);
  }

  scrollUp(ScrollController scrollController, double h) {
    if (scrollController.hasClients) {
      scrollController.animateTo(
        0.0,
        curve: Curves.easeOut,
        duration: const Duration(milliseconds: 300),
      );
    }
  }

  scrollDown(ScrollController scrollController, double h) {
    if (scrollController.hasClients) {
      scrollController.animateTo(
        scrollController.position.maxScrollExtent + h + 50,
        duration: Duration(seconds: 1),
        curve: Curves.fastOutSlowIn,
      );
    }
  }

  drawTopbar(bool isShowArrow, String title) {
    return Padding(
      padding: const EdgeInsets.only(left: 0, right: 40, top: 20),
      child: Container(
          child: Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
            Container(
              child: (isShowArrow)
                  ? IconButton(
                      icon: Image.asset(
                        "assets/images/icons/ico_arrow_back.png",
                      ),
                      onPressed: () {
                        Get.back();
                      },
                    )
                  : SizedBox(),
            ),
            Expanded(
              child: Txt(
                  txt: title,
                  txtColor: Colors.black,
                  txtSize: MyTheme.txtSize + 1,
                  txtAlign: TextAlign.center,
                  isBold: true),
            ),
          ])),
    );
  }

  void onReportClicked(
      context, Offset offset, int userId, String statusStr) async {
    double left = offset.dx;
    double top = offset.dy;
    await showMenu(
      color: Colors.white,
      context: context,
      position: RelativeRect.fromLTRB(left, top + 50, 0, 0),
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.all(Radius.circular(10))),
      items: [
        PopupMenuItem<String>(
          child: GestureDetector(
            onTap: () {
              Get.to(
                () => ResPage(
                  from: statusStr,
                  userId: userId,
                ),
              );
            },
            child: Container(
              child: const Text(
                'Report',
                style: TextStyle(color: Colors.black),
              ),
            ),
          ),
        ), //PopupMenuItem<String>(child: const Text('Lion'), value: 'Lion'),
      ],
      elevation: 5,
    );
  }

  drawCircle({BuildContext context, Color color, double size = 3}) {
    double width = MediaQuery.of(context).size.width;
    //double height = MediaQuery.of(context).size.height;
    //var padding = MediaQuery.of(context).padding;
    //double newheight = height - padding.top - padding.bottom;
    return Container(
      width: width * size / 100,
      height: width * size / 100,
      decoration: BoxDecoration(shape: BoxShape.circle, color: color),
    );
  }

  getStarsRow(int rate, Color colr) {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.center,
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        for (int i = 0; i < 5; i++)
          Icon(
            (i < rate) ? Icons.star : Icons.star_outline,
            color: colr,
            size: 20,
          ),
      ],
    );
  }

  getStarRatingView({
    int rate,
    int reviews,
    String reviewTxt1 = '',
    String reviewTxt2 = ' reviews',
    Color starColor,
    Color txtColor = Colors.black,
    MainAxisAlignment align = MainAxisAlignment.center,
    bool isRow = true,
  }) {
    if (starColor == null) starColor = MyTheme.brandColor;
    return Container(
      child: (isRow)
          ? Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: align,
              children: [
                getStarsRow(rate, starColor),
                SizedBox(width: 10),
                (reviews != null)
                    ? Txt(
                        txt: reviewTxt1 + reviews.toString() + reviewTxt2,
                        txtColor: txtColor,
                        txtSize: MyTheme.txtSize - .3,
                        txtAlign: TextAlign.center,
                        isBold: false)
                    : SizedBox(),
              ],
            )
          : Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: align,
              children: [
                getStarsRow(rate, starColor),
                SizedBox(height: 10),
                (reviews != null)
                    ? Txt(
                        txt: reviewTxt1 + reviews.toString() + reviewTxt2,
                        txtColor: txtColor,
                        txtSize: MyTheme.txtSize - .3,
                        txtAlign: TextAlign.center,
                        isBold: false)
                    : SizedBox(),
              ],
            ),
    );
  }

  getCompletionText(
      {int pa,
      MainAxisAlignment align = MainAxisAlignment.center,
      Color txtColor = Colors.black,
      Function callbackInfo}) {
    return Center(
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: align,
        children: [
          Flexible(
            child: Txt(
                txt: (pa > 0 ? (pa.toString() + "%") : "No") +
                    " Completion Rate",
                txtColor: txtColor,
                txtSize: MyTheme.txtSize - .3,
                txtAlign: TextAlign.start,
                isBold: false),
          ),
          (callbackInfo != null)
              ? IconButton(
                  iconSize: 20,
                  icon: Icon(
                    Icons.info,
                    color: MyTheme.gray5Color.withOpacity(.6),
                  ),
                  onPressed: () {
                    callbackInfo();
                  })
              : SizedBox(),
        ],
      ),
    );
  }

  expandableTxt(String txt, Color txtColor, Color arrowColor) {
    final txt1 = txt.substring(0, txt.indexOf(".")).trim();
    final txt2 = txt.substring(txt.indexOf(".") + 1).trim();

    return Container(
      child: ListTileTheme(
        contentPadding: EdgeInsets.all(0),
        iconColor: arrowColor,
        child: Theme(
          data: ThemeData.light()
              .copyWith(accentColor: Colors.black, primaryColor: Colors.red),
          child: ExpansionTile(
            title: Txt(
                txt: txt1 + '...',
                txtColor: txtColor,
                txtSize: MyTheme.txtSize - .3,
                txtAlign: TextAlign.start,
                maxLines: 3,
                isBold: false),
            children: <Widget>[
              Container(
                child: Padding(
                  padding: const EdgeInsets.only(top: 10, bottom: 10),
                  child: Txt(
                      txt: txt2,
                      txtColor: txtColor,
                      txtSize: MyTheme.txtSize - .3,
                      txtAlign: TextAlign.start,
                      isBold: false),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
