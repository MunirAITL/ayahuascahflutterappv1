import 'package:aitl/config/server/APIMiscCfg.dart';
import 'package:aitl/data/app_data/UserData.dart';
import 'package:aitl/data/model/misc/device_info/FcmDeviceInfoAPIModel.dart';
import 'package:aitl/data/model/misc/device_info/UserDeviceAPIModel.dart';
import 'package:aitl/data/network/NetworkMgr.dart';
import 'package:aitl/view_model/api/api_view_model.dart';
import 'package:aitl/view_model/helper/misc/FcmDeviceInfoHelper.dart';
import 'package:flutter/cupertino.dart';

class APIHelper {
  static var map;

  APIHelper() {
    getDeviceInfo();
  }

  getDeviceInfo() async {
    map = await FcmDeviceInfoHelper().getDeviceInfo();
  }

  wsFCMDeviceInfo(
      BuildContext context, Function(FcmDeviceInfoAPIModel) callback) async {
    try {
      final param = await FcmDeviceInfoHelper().getParam();
      APIViewModel().req<FcmDeviceInfoAPIModel>(
          context: context,
          url: APIMiscCfg.FCM_DEVICE_INFO_URL,
          param: param,
          isLoading: false,
          reqType: ReqType.Post,
          callback: (model) {
            callback(model);
          });
    } catch (e) {}
  }

  wsUserDevice(
      {BuildContext context,
      @required String eventType,
      Map<String, dynamic> param2,
      Function(UserDeviceAPIModel) callback}) async {
    try {
      int userId = 0;
      try {
        userId = userData.userModel.id;
      } catch (e) {}
      var param = {
        "DeviceId": map['deviceId'],
        "DeviceName": map['deviceName'],
        "EventDate": DateTime.now().toString(),
        "EventType": eventType ?? '',
        "ModleName": map['modelName'],
        "OSVersion": map['deviceVersion'].toString(),
        "Remarks": "",
        "UserId": userId ?? 0,
      };
      if (param2 != null) {
        param.addAll(param2);
      }
      print(param.toString());
      APIViewModel().req<UserDeviceAPIModel>(
          context: context,
          reqType: ReqType.Post,
          url: APIMiscCfg.USER_DEVICES_POST_URL,
          isLoading: false,
          param: param,
          callback: (model) {
            callback(model);
          });
    } catch (e) {}
  }
}
