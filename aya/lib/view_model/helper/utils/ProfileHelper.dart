import 'package:aitl/config/theme/MyTheme.dart';
import 'package:aitl/controller/classes/Common.dart';
import 'package:aitl/controller/classes/DateFun.dart';
import 'package:aitl/data/app_data/UserData.dart';
import 'package:aitl/data/model/dashboard/more/profile/UserRatingsModel.dart';
import 'package:aitl/view/widgets/MyNetworkImage.dart';
import 'package:aitl/view/widgets/Txt.dart';
import 'package:aitl/view_model/helper/utils/UIHelper.dart';
import 'package:flutter/material.dart';

class ProfileHelper {
  drawProfileUserRatingView(List<UserRatingsModel> listUserRating) {
    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          for (UserRatingsModel ratingModel in listUserRating)
            Padding(
              padding: const EdgeInsets.only(left: 20, right: 20, top: 20),
              child: Column(
                children: [
                  Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Padding(
                        padding: const EdgeInsets.only(left: 10),
                        child: CircleAvatar(
                          radius: 30,
                          backgroundColor: Colors.transparent,
                          backgroundImage: MyNetworkImage.loadProfileImage(
                              ratingModel.profileImageUrl),
                        ),
                      ),
                      SizedBox(width: 20),
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          UIHelper().getStarRatingView(
                              rate: ratingModel.rating.toInt(),
                              reviews: null,
                              starColor: Colors.blueAccent),
                          SizedBox(height: 5),
                          Txt(
                              txt: DateFun.getTimeAgoTxt(
                                  ratingModel.creationDate),
                              txtColor: MyTheme.gray4Color,
                              txtSize: MyTheme.txtSize,
                              txtAlign: TextAlign.center,
                              isBold: false),
                        ],
                      ),
                    ],
                  ),
                  SizedBox(height: 10),
                  Txt(
                      txt: ratingModel.taskTitle,
                      txtColor: MyTheme.blueColor,
                      txtSize: MyTheme.txtSize,
                      txtAlign: TextAlign.center,
                      isBold: false),
                  SizedBox(height: 10),
                  Txt(
                      txt: ratingModel.comments,
                      txtColor: MyTheme.gray5Color,
                      txtSize: MyTheme.txtSize,
                      txtAlign: TextAlign.center,
                      isBold: false)
                ],
              ),
            ),
        ],
      ),
    );
  }

  drawTaskDetailsUserRatingView(List<UserRatingsModel> listUserRating) {
    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          for (UserRatingsModel ratingModel in listUserRating)
            Padding(
              padding: const EdgeInsets.only(left: 20, right: 20, top: 20),
              child: Column(
                children: [
                  Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Padding(
                        padding: const EdgeInsets.only(left: 10),
                        child: CircleAvatar(
                          radius: 30,
                          backgroundColor: Colors.transparent,
                          backgroundImage: MyNetworkImage.loadProfileImage(
                              ratingModel.profileImageUrl),
                        ),
                      ),
                      SizedBox(width: 20),
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          UIHelper().getStarRatingView(
                              rate: ratingModel.rating.toInt(),
                              reviews: null,
                              starColor: Colors.blueAccent),
                          SizedBox(height: 5),
                          Txt(
                              txt: DateFun.getTimeAgoTxt(
                                  ratingModel.creationDate),
                              txtColor: MyTheme.gray4Color,
                              txtSize: MyTheme.txtSize,
                              txtAlign: TextAlign.center,
                              isBold: false),
                        ],
                      ),
                    ],
                  ),
                  SizedBox(height: 10),
                  Txt(
                      txt: (ratingModel.initiatorName +
                          " left a review for " +
                          ratingModel.employeeName),
                      txtColor: MyTheme.gray4Color,
                      txtSize: MyTheme.txtSize,
                      txtAlign: TextAlign.center,
                      isBold: false),
                  SizedBox(height: 10),
                  Txt(
                      txt: ratingModel.comments,
                      txtColor: MyTheme.gray5Color,
                      txtSize: MyTheme.txtSize,
                      txtAlign: TextAlign.center,
                      isBold: false)
                ],
              ),
            ),
        ],
      ),
    );
  }
}
