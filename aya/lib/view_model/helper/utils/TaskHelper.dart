import 'package:flutter/cupertino.dart';
import 'package:flutter_svg/flutter_svg.dart';

class TaskHelper {
  getBadgeTypeSvg(String userBadgeType) {
    switch (userBadgeType) {
      case "Email":
        return SvgPicture.asset(
          'assets/images/svg/badge_types/ic_type_email.svg',
        );
      case "CompanyDocuments":
        return SvgPicture.asset(
          'assets/images/svg/badge_types/ic_type_pdf.svg',
        );
      case "Mobile":
        return SvgPicture.asset(
          'assets/images/svg/badge_types/ic_type_mobile.svg',
        );
      case "payment_method":
        return SvgPicture.asset(
          'assets/images/svg/badge_types/ic_type_card.svg',
        );
      case "facebook":
        return SvgPicture.asset(
          'assets/images/svg/badge_types/ic_type_fb.svg',
        );
      case "linkedin":
        return SvgPicture.asset(
          'assets/images/svg/badge_types/ic_type_linkedin.svg',
        );
      case "twitter":
        return SvgPicture.asset(
          'assets/images/svg/badge_types/ic_type_twitter.svg',
        );
      default:
        return SizedBox(width: 30);
    }
  }

  getBadgeTypeTxt(String userBadgeType) {
    switch (userBadgeType) {
      case "Email":
        return "Email";
      case "CompanyDocuments":
        return "Company Documents";
      case "Mobile":
        return "Mobile";
      case "payment_method":
        return "Credit Card";
      case "facebook":
        return "Facebook";
      case "linkedin":
        return "LinkedIn";
      case "twitter":
        return "Twitter";
      default:
        return "";
    }
  }
}
