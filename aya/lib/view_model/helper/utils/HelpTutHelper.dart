class HelpTutHelper {
  List<String> listTips = [
    "To find assistant for necessary work tap on the Post A Task Icon",
    "To see all your work together tap on the My tasks icon",
    "To work as a Tasker tap on the Browse tasks icon",
    "To view your messages tap on the Messages icon",
    "Tap on More icon for your profile, payment notifications, and other settings.",
    "Tap the Task alerts icon to get your favorite work alert as a Tasker"
  ];

  /*List<String> listTab = [
    "Create Case",
    "My Cases",
    "Messages",
    "Notifications",
    "More"
  ];*/
}
