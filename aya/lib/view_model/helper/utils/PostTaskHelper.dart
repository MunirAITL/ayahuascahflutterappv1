class PostTaskHelper {
  static const List<Map<String, dynamic>> listTask = [
    {
      'isCat': false,
      'name': 'Cleaning',
      'title': 'Cleaning',
      'ph': 'eg. Clean my 2 bedroom apartment',
      'playstore_appId': '',
      'appstore_appId': '',
      'icon': 'assets/images/tasks/task_cleaning.svg',
    },
    {
      'isCat': false,
      'name': 'Anythings',
      'title': 'Anythings',
      'ph': 'eg. Suggest a name for my new company etc.',
      'playstore_appId': '',
      'appstore_appId': '',
      'icon': 'assets/images/tasks/task_others.svg',
    },
    {
      'isCat': true,
      'name': 'All Categories',
      'title': 'All Categories',
      'ph': '',
      'playstore_appId': '',
      'appstore_appId': '',
      'icon': 'assets/images/tasks/task_allcat.svg',
    },
    {
      'isCat': false,
      'name': 'Handy Man',
      'title': 'Handy Man',
      'ph': 'eg. Need a handy man for fixing my TV etc.',
      'playstore_appId': '',
      'appstore_appId': '',
      'icon': 'assets/images/tasks/task_handyman.svg',
    },
    {
      'isCat': false,
      'name': 'Maid',
      'title': 'Maid',
      'ph': 'eg. Need a maid for home',
      'playstore_appId': '',
      'appstore_appId': '',
      'icon': 'assets/images/tasks/task_maid.svg',
    },
    {
      'isCat': false,
      'name': 'Electrician',
      'title': 'Electrician',
      'ph':
          'eg. Need electrician in my home or office for electrical repair or setup.',
      'playstore_appId': '',
      'appstore_appId': '',
      'icon': 'assets/images/tasks/task_electrician.svg',
    },
    {
      'isCat': false,
      'name': 'Part Time Job',
      'title': 'Part Time Job',
      'ph': 'eg. Need few sales representatives',
      'playstore_appId': '',
      'appstore_appId': '',
      'icon': 'assets/images/tasks/task_part_time_job.svg',
    },
    {
      'isCat': false,
      'name': 'Delivery',
      'title': 'Delivery',
      'ph': 'eg. Need a delivery man to send a parcel',
      'playstore_appId': '',
      'appstore_appId': '',
      'icon': 'assets/images/tasks/task_delivery.svg',
    },
    {
      'isCat': false,
      'name': 'Computer/IT',
      'title': 'Computer/IT',
      'ph': 'eg. Need to fix my computer/ Need do write an article',
      'playstore_appId': '',
      'appstore_appId': '',
      'icon': 'assets/images/tasks/task_it.svg',
    },
    {
      'isCat': false,
      'name': 'Pest Control',
      'title': 'Pest Control',
      'ph': 'eg. Need pest controller etc.',
      'playstore_appId': '',
      'appstore_appId': '',
      'icon': 'assets/images/tasks/task_pest.svg',
    },
    {
      'isCat': false,
      'name': 'Shifting',
      'title': 'Shifting/Moving',
      'ph': 'eg. Need a truck for shifting etc.',
      'playstore_appId': '',
      'appstore_appId': '',
      'icon': 'assets/images/tasks/task_moving.svg',
    },
    {
      'isCat': false,
      'name': 'Beauty Parlour',
      'title': 'Beauty Parlour',
      'ph': 'eg. I need a facial',
      'playstore_appId': '',
      'appstore_appId': '',
      'icon': 'assets/images/tasks/task_beautician.svg',
    },
    {
      'isCat': false,
      'name': 'Tutor & Trainer',
      'title': 'Tutor & Trainer',
      'ph': 'eg. Need a tutor for teaching English',
      'playstore_appId': '',
      'appstore_appId': '',
      'icon': 'assets/images/tasks/task_tutor.svg',
    },
    {
      'isCat': false,
      'name': 'Marketing',
      'title': 'Marketing',
      'ph': 'eg. Need support staff for marketing etc.',
      'playstore_appId': '',
      'appstore_appId': '',
      'icon': 'assets/images/tasks/task_marketting.svg',
    },
    {
      'isCat': false,
      'name': 'Tailor',
      'title': 'Tailor',
      'ph': 'eg. Need tailor for making a few shirts etc.',
      'playstore_appId': '',
      'appstore_appId': '',
      'icon': 'assets/images/tasks/task_tailor.svg',
    },
    {
      'isCat': false,
      'name': 'Hire Driver',
      'title': 'Hire Driver',
      'ph': 'eg. Need a Sedan for 6 hours etc.',
      'playstore_appId': '',
      'appstore_appId': '',
      'icon': 'assets/images/tasks/task_driver.svg',
    },
    {
      'isCat': false,
      'name': 'Rent Car',
      'title': 'Rent Car',
      'ph': '',
      'playstore_appId': 'com.gari',
      'appstore_appId': '',
      'icon': 'assets/images/tasks/task_rentcar.svg',
    },
    {
      'isCat': false,
      'name': '',
      'title': 'Rent Truck',
      'ph': '',
      'playstore_appId': 'com.gari',
      'appstore_appId': '',
      'icon': 'assets/images/tasks/task_renttruck.svg',
    },
    {
      'isCat': false,
      'name': 'Security Gaurd',
      'title': 'Security Gaurd',
      'ph': 'eg. Need secuirty guard for office etc.',
      'playstore_appId': '',
      'appstore_appId': '',
      'icon': 'assets/images/tasks/task_guard.svg',
    },
    {
      'isCat': false,
      'name': 'Admin',
      'title': 'Admin',
      'ph': 'eg. Need an admin assistant',
      'playstore_appId': '',
      'appstore_appId': '',
      'icon': 'assets/images/tasks/task_admin.svg',
    },
    {
      'isCat': false,
      'name': 'Shopping',
      'title': 'Shopping',
      'ph':
          'eg. Need someone for shopping my medicine, household or office needs etc.',
      'playstore_appId': '',
      'appstore_appId': '',
      'icon': 'assets/images/tasks/task_shopping.svg',
    },
    {
      'isCat': false,
      'name': 'Events',
      'title': 'Events',
      'ph': 'eg. Need event management service etc.',
      'playstore_appId': '',
      'appstore_appId': '',
      'icon': 'assets/images/tasks/task_events.svg',
    },
    {
      'isCat': false,
      'name': 'Laundry',
      'title': 'Laundry',
      'ph': 'eg. Need laundry service etc.',
      'playstore_appId': '',
      'appstore_appId': '',
      'icon': 'assets/images/tasks/task_laundry.svg',
    },
  ];
}
