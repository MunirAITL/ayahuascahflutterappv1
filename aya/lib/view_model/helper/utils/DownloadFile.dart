import 'dart:io';
import 'package:aitl/data/model/dashboard/more/payment/history/TaskPaymentsModel.dart';
import 'dart:io' as Io;
import 'package:path_provider/path_provider.dart';

class DownloadFile {
  Future<File> saveCSV(List<TaskPaymentsModel> listTaskPaymentModel) async {
    try {
      final dir = await getExternalStorageDirectory();
      final folder = await new Io.Directory('${dir.path}/shohokari')
          .create(recursive: true);
      final file = new Io.File(
          '${folder.path}/Earned_${DateTime.now().toUtc().toIso8601String()}.csv');

      //  create csv file
      var data =
          'Invoice Id,Task Id,Task Title,Payment Method,Payable Amount,Payment Amount,Net Total Amount,Date,\n';
      for (var model in listTaskPaymentModel) {
        data += (model.id.toString() + ',');
        data += (model.taskId.toString() + ',');
        data += (model.taskTitle.toString() + ',');
        data += (model.paymentMethod.toString() + ',');
        data += (model.payableAmount.toString() + ',');
        data += (model.netTotalAmount.toString() + ',');
        data += (model.creationDate.toString() + ',\n');
      }
      await file.writeAsString(data, mode: FileMode.write, flush: true);
      return file;
    } catch (e) {
      print(e);
      return null;
    }
  }
}
