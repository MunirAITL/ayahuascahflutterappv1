import 'package:aitl/data/model/dashboard/timeline/MessageTypeAdvisorAPIModel.dart';
import 'package:aitl/data/network/NetworkMgr.dart';
import 'package:aitl/view_model/helper/timeline/MessageTypeURlHelper.dart';
import 'package:flutter/cupertino.dart';
import 'package:aitl/Mixin.dart';

class MessageTypeAPIMgr with Mixin {
  static final MessageTypeAPIMgr _shared = MessageTypeAPIMgr._internal();

  factory MessageTypeAPIMgr() {
    return _shared;
  }

  MessageTypeAPIMgr._internal();

  wsOnMessageTypeLoad({
    BuildContext context,
    int adviserOrIntroducerId,
    Function(MessageTypeAdvisorAPIModel) callback,
  }) async {
    try {
      final url = MessageTypeURIHelper()
          .getUrl(adviserOrIntroducerId: adviserOrIntroducerId);
      log("Message Type url = " + url);
      await NetworkMgr()
          .req<MessageTypeAdvisorAPIModel, Null>(
        context: context,
        reqType: ReqType.Get,
        url: url,
      )
          .then((model) async {
        callback(model);
      });
    } catch (e) {
      log(e.toString());
    }
  }
}
