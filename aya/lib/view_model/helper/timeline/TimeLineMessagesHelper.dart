import 'package:aitl/config/server/APITimelineCfg.dart';
import 'package:aitl/data/app_data/UserData.dart';

class TimeLineMessagesHelper {
  getUrl({
    pageStart,
    pageCount,
    taskId,
    isPrivate,
    customerId,
    receiverId,
    timeLineId,
  }) {
    var url = APITimelineCfg.TIMELINE_URL;
    url = url.replaceAll("#isPrivate#", isPrivate.toString());
    url = url.replaceAll("#receiverId#", receiverId.toString());
    url = url.replaceAll("#senderId#", userData.userModel.id.toString());
    url = url.replaceAll("#taskId#", taskId.toString());
    url = url.replaceAll("#Count#", pageCount.toString());
    url = url.replaceAll("#count#", pageCount.toString());
    url = url.replaceAll("#customerId#", customerId.toString());
    url = url.replaceAll("#page#", pageStart.toString());
    url = url.replaceAll("#timeLineId#", timeLineId.toString());
    return url;
  }
}
