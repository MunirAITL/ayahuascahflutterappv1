import 'package:aitl/config/server/APITimelineCfg.dart';
import 'package:aitl/data/app_data/UserData.dart';

class MessageTypeURIHelper {
  getUrl({adviserOrIntroducerId}) {
    var url = APITimelineCfg.TIMELINE_MESSAGE_TYPE_URL;
    url = url.replaceAll(
        "#UserCompanyId#", userData.userModel.userCompanyID.toString());
    url = url.replaceAll("#CustomerId#", userData.userModel.id.toString());
    url = url.replaceAll(
        "#AdviserOrIntroducerId#", adviserOrIntroducerId.toString());
    return url;
  }
}
