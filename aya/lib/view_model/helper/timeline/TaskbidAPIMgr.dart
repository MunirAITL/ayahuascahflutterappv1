import 'package:aitl/data/model/dashboard/timeline/TaskBiddingAPIModel.dart';
import 'package:aitl/data/network/NetworkMgr.dart';
import 'package:aitl/view_model/helper/timeline/TaskBiddingHelper.dart';
import 'package:flutter/cupertino.dart';
import 'package:aitl/Mixin.dart';

class TaskbidAPIMgr with Mixin {
  static final TaskbidAPIMgr _shared = TaskbidAPIMgr._internal();

  factory TaskbidAPIMgr() {
    return _shared;
  }

  TaskbidAPIMgr._internal();

  wsOnPageLoad({
    BuildContext context,
    int taskId,
    int pageStart,
    int pageCount,
    Function(TaskBiddingAPIModel) callback,
  }) async {
    try {
      var url = TaskBiddingHelper().getUrl(taskId: taskId.toString());
      log(url);
      await NetworkMgr()
          .req<TaskBiddingAPIModel, Null>(
        context: context,
        reqType: ReqType.Get,
        url: url,
        isLoading: (pageStart == 0) ? true : false,
      )
          .then((model) async {
        callback(model);
      });
    } catch (e) {
      log(e.toString());
    }
  }
}
