import 'package:aitl/config/server/APITimelineCfg.dart';

class TaskBiddingHelper {
  getUrl({pageStart, pageCount, taskId}) {
    var url = APITimelineCfg.TASKBIDDING_URL;
    url = url.replaceAll("#taskId#", taskId.toString());
    return url;
  }
}
