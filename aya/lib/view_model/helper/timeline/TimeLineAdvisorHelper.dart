import 'package:aitl/config/server/APITimelineCfg.dart';
import 'package:aitl/data/app_data/UserData.dart';

class TimeLineAdvisorHelper {
  getUrl({
    pageStart,
    pageCount,
  }) {
    var url = APITimelineCfg.TIMELINE_ADVISOR_URL;
    url = url.replaceAll("#userId#", userData.userModel.id.toString());
    url = url.replaceAll(
        "#communityId#", userData.userModel.communityID.toString());
    url = url.replaceAll(
        "#companyId#", userData.userModel.userCompanyInfo.id.toString());
    url = url.replaceAll("#count#", pageCount.toString());
    url = url.replaceAll("#page#", pageStart.toString());
    return url;
  }
}
