import 'package:aitl/data/model/dashboard/timeline/TimeLineAdvisorAPIModel.dart';
import 'package:aitl/data/network/NetworkMgr.dart';
import 'package:aitl/view_model/helper/timeline/TimeLineAdvisorHelper.dart';
import 'package:flutter/cupertino.dart';
import 'package:aitl/Mixin.dart';

class TimelineAPIMgr with Mixin {
  static final TimelineAPIMgr _shared = TimelineAPIMgr._internal();

  factory TimelineAPIMgr() {
    return _shared;
  }

  TimelineAPIMgr._internal();

  wsOnPageLoad({
    BuildContext context,
    int pageStart,
    int pageCount,
    int status,
    Function(TimeLineAdvisorAPIModel) callback,
  }) async {
    try {
      final url = TimeLineAdvisorHelper().getUrl(
        pageStart: pageStart,
        pageCount: pageCount,
      );
      log("Message list url = " + url);
      await NetworkMgr()
          .req<TimeLineAdvisorAPIModel, Null>(
        context: context,
        reqType: ReqType.Get,
        url: url,
        isLoading: (pageStart == 0) ? true : false,
      )
          .then((model) async {
        callback(model);
      });
    } catch (e) {
      log(e.toString());
    }
  }
}
