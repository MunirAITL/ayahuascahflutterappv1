import 'package:aitl/config/server/Server.dart';
import 'package:geocoder/geocoder.dart';
import 'dart:math' show cos, sqrt, asin;
import 'package:google_maps_flutter/google_maps_flutter.dart';

class MapFun {
  double calculateDistanceMeter(lat1, lon1, lat2, lon2) {
    /*return await Geolocator().distanceBetween(
      lat1,
      lon1,
      lat2,
      lon2,
    );*/
    var p = 0.017453292519943295;
    var c = cos;
    var a = 0.5 -
        c((lat2 - lat1) * p) / 2 +
        c(lat1 * p) * c(lat2 * p) * (1 - c((lon2 - lon1) * p)) / 2;
    double dis = 12742 * asin(sqrt(a));
    //print(dis.toString());
    return dis * 1000; //  conver KM 2 Meter
  }

  Future<Coordinates> getCordByAddr(addr) async {
    try {
      //if (!Server.isTest) {
      var addresses = await Geocoder.local.findAddressesFromQuery(addr);
      var first = addresses.first;
      return first.coordinates;
      //}
    } catch (e) {}
    return null;
  }

  Future<String> getAddrByCord(String cordStr) async {
    try {
      final cordArr = cordStr.split(',');
      final cord = LatLng(double.parse(cordArr[0]), double.parse(cordArr[1]));
      final address = await Geocoder.local.findAddressesFromCoordinates(
          new Coordinates(cord.latitude, cord.longitude));
      return address.first.addressLine;
    } catch (e) {
      return '';
    }
  }
}
