import 'dart:io';
import 'package:device_info/device_info.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:html/parser.dart';
import 'package:intl/intl.dart';

class Common {
  static Future<String> getUDID(BuildContext context) async {
    DeviceInfoPlugin deviceInfo = DeviceInfoPlugin();
    if (Theme.of(context).platform == TargetPlatform.iOS) {
      IosDeviceInfo iosDeviceInfo = await deviceInfo.iosInfo;
      return iosDeviceInfo.identifierForVendor; // unique ID on iOS
    } else {
      AndroidDeviceInfo androidDeviceInfo = await deviceInfo.androidInfo;
      return androidDeviceInfo.androidId; // unique ID on Android
    }
  }

  static String getDeviceType() {
    return Platform.isAndroid ? 'Android' : 'iOS';
  }

  static String unEscapeString(String str) {
    //return json.decode(json.decode(str));
    return str;
  }

  static num tryParse(input) {
    String source = input.trim();
    return int.tryParse(source) ?? double.tryParse(source);
  }

  static String parseHtmlString(String htmlString) {
    return Bidi.stripHtmlIfNeeded(htmlString);
  }

  static findIndexFromList(List<dynamic> list, val) {
    for (int i = 0; i < list.length; i++) {
      if (list[i] == val) return i;
    }
    return 0;
  }

  static splitName(String name) {
    try {
      final fName = name.substring(0, name.indexOf(" "));
      final lName = name.substring(name.indexOf(" ") + 1);
      return {'fname': fName, 'lname': lName};
    } catch (e) {}
    return {'fname': name, 'lname': ''};
  }
}
