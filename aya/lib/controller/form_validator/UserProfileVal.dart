import 'package:flutter/material.dart';
import 'package:aitl/mixin.dart';

class UserProfileVal with Mixin {
  static const int PHONE_LIMIT = 8;

  isEmpty(TextEditingController tf, arg, Color bgColor) {
    if (tf.text.isEmpty) {
      showAlert(msg: arg, isToast: true);
      return true;
    }
    return false;
  }

  isFullName(String name, Color bgColor) {
    try {
      final isOk = (name.trim().contains(" ")) ? true : false;
      if (!isOk) {
        showAlert(msg: "Invalid full name", isToast: true);
      }
      return isOk;
    } catch (e) {
      return false;
    }
  }

  isFNameOK(TextEditingController tf, Color bgColor) {
    if (tf.text.length == 0) {
      showAlert(msg: "Invalid first name", isToast: true);
      return false;
    }
    return true;
  }

  isLNameOK(TextEditingController tf, Color bgColor) {
    if (tf.text.length == 0) {
      showAlert(msg: "Invalid last name", isToast: true);
      return false;
    }
    return true;
  }

  isEmailOK(TextEditingController tf, String msg, Color bgColor) {
    if (!RegExp(
            r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+")
        .hasMatch(tf.text.trim())) {
      showAlert(msg: msg, isToast: true);
      return false;
    } else if (tf.text.trim().contains("@help.mortgage-magic.co.uk")) {
      showAlert(msg: "Email address does not exists", isToast: true);
      return false;
    }
    return true;
  }

  isPhoneOK(TextEditingController tf, Color bgColor) {
    if (tf.text.length < PHONE_LIMIT) {
      showAlert(msg: "Invalid Mobile Number", isToast: true);
      return false;
    }
    return true;
  }

  isPwdOK(TextEditingController tf, Color bgColor) {
    if (tf.text.length < 3) {
      showAlert(
          msg: "Password should be greater by 4 characters", isToast: true);
      return false;
    }
    return true;
  }

  isComNameOK(TextEditingController tf, Color bgColor) {
    if (tf.text.length < 6) {
      showAlert(msg: "Invalid Company Name", isToast: true);
      return false;
    }
    return true;
  }

  isDOBOK(str, Color bgColor) {
    if (str == '') {
      showAlert(msg: "Invalid Date of Birth", isToast: true);
      return false;
    }
    return true;
  }
}
