import 'dart:async';
import 'dart:math';
import 'package:aitl/config/theme/MyTheme.dart';
import 'package:aitl/view/dashboard/dashboard_page.dart';
import 'package:aitl/view/widgets/AlrtDialog.dart';
import 'package:aitl/view_model/observer/StateProvider.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:package_info/package_info.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:responsive_flutter/responsive_flutter.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:jiffy/jiffy.dart';
import 'package:rflutter_alert/rflutter_alert.dart';
import 'package:maps_launcher/maps_launcher.dart';

mixin Mixin {
  getW(context) {
    return ResponsiveFlutter.of(context).wp(100);
  }

  getH(context) {
    return ResponsiveFlutter.of(context).hp(100);
  }

  getWP(context, p) {
    return ResponsiveFlutter.of(context).wp(p);
  }

  getHP(context, p) {
    return ResponsiveFlutter.of(context).hp(p);
  }

  getTxtSize({BuildContext context, double txtSize = 0}) {
    if (txtSize == 0) txtSize = 2;
    return ResponsiveFlutter.of(context).fontSize(txtSize);
  }

  obsUpdateTabs(int pageNo) async {
    switch (pageNo) {
      case 0:
        StateProvider()
            .notify(ObserverState.STATE_RELOAD_TAB, DashboardPage.TAB_MYTASK);
        break;
      case 4:
        StateProvider()
            .notify(ObserverState.STATE_RELOAD_TAB, DashboardPage.TAB_MYTASK);
        StateProvider().notify(
            ObserverState.STATE_RELOAD_TAB, DashboardPage.TAB_FINDWORKS);
        break;
      default:
        break;
    }
  }

  /*navTo({
    BuildContext context,
    Widget Function() page,
    isRep = false,
    isFullscreenDialog = false,
  }) {
    if (!isRep) {
      return Navigator.of(context).push(MaterialPageRoute(
          builder: (BuildContext context) {
            return page();
          },
          fullscreenDialog: isFullscreenDialog));
    } else {
      return Navigator.of(context).pushReplacement(MaterialPageRoute(
          builder: (BuildContext context) {
            return page();
          },
          fullscreenDialog: isFullscreenDialog));
    }
  }*/

  openUrl(context, url) async {
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      Alert(context: context, title: "Alert", desc: 'Could not launch $url')
          .show();
    }
  }

  openMap(context, double latitude, double longitude, String title) async {
    MapsLauncher.launchCoordinates(latitude, longitude, title);
  }

  Iterable<E> mapIndexed<E, T>(
      Iterable<T> items, E Function(int index, T item) f) sync* {
    var index = 0;
    for (final item in items) {
      yield f(index, item);
      index = index + 1;
    }
  }

  void startLoading() {
    EasyLoading.show(status: "Loading...");
  }

  void stopLoading() {
    EasyLoading.dismiss();
  }

  void showAlert({msg, isToast = false, which = 2}) {
    //EasyLoading.instance..loadingStyle = EasyLoadingStyle.dark;
    //  0 = error,
    //  1= success,
    //  2= info
    //  3= default toast

    if (isToast) {
      switch (which) {
        case 0:
          EasyLoading.showError(msg);
          break;
        case 1:
          EasyLoading.showSuccess(msg);
          break;
        case 2:
          EasyLoading.showInfo(msg);
          break;
        default:
          EasyLoading.showInfo(msg);
      }
    } else {
      Get.dialog(AlrtDialog(
        which: which,
        msg: msg,
        txtColor: Colors.black,
        bgColor: MyTheme.gray1Color,
      ));
    }

    /*if (isToast) {
      EasyLoading.showInfo(msg);
    } else {
      Get.dialog(AlrtDialog(
        which: which,
        msg: msg,
        txtColor: Colors.black,
        bgColor: MyTheme.gray1Color,
      ));
    }*/
  }

  void showSnake(String title, String msg) {
    Get.snackbar(title, msg);
  }

  /*showAlert({msg, which = 2}) async {
    //  0 = error = AlertType.error
    //  1= success,
    //  2= info
    /*var t = AlertType.info;
    switch (which) {
      case 0:
        t = AlertType.error;
        break;
      case 1:
        t = AlertType.success;
        break;
      case 2:
        t = AlertType.info;
        break;
      default:
        t = AlertType.info;
        break;
    }
    Alert(
      context: context,
      type: t,
      title: "",
      desc: msg,
      buttons: [
        DialogButton(
          child: Text(
            "Ok",
            style: TextStyle(color: Colors.white, fontSize: 20),
          ),
          onPressed: () => Navigator.pop(context),
          width: 120,
        )
      ],
    ).show();*/

    Get.defaultDialog(
        onConfirm: () {
          Get.back();
        },
        middleText: msg);
  }*/

  /*showAlertErr({context, title, msg, isServerErr = false}) async {
    if (msg != null) {
      //var geocode = await getGeoCode(true);
      sendMail(context, runtimeType.toString() + '-' + title, msg);
    }
  }*/

  log(str) {
    if (const String.fromEnvironment('DEBUG') != null) {
      print(str);
      //final pattern = new RegExp('.{1,800}'); // 800 is the size of each chunk
      //pattern.allMatches(str).forEach((match) => print(match.group(0)));
    }
  }
}

extension HexColor on Color {
  /// String is in the format "aabbcc" or "ffaabbcc" with an optional leading "#".
  static Color fromHex(String hexString) {
    final buffer = StringBuffer();
    if (hexString.length == 6 || hexString.length == 7) buffer.write('ff');
    buffer.write(hexString.replaceFirst('#', ''));
    return Color(int.parse(buffer.toString(), radix: 16));
  }

  /// Prefixes a hash sign if [leadingHashSign] is set to `true` (default is `true`).
  String toHex({bool leadingHashSign = true}) => '${leadingHashSign ? '#' : ''}'
      '${alpha.toRadixString(16).padLeft(2, '0')}'
      '${red.toRadixString(16).padLeft(2, '0')}'
      '${green.toRadixString(16).padLeft(2, '0')}'
      '${blue.toRadixString(16).padLeft(2, '0')}';
}

extension CapExtension on String {
  String get inCaps =>
      this.length > 0 ? '${this[0].toUpperCase()}${this.substring(1)}' : '';
  String get allInCaps => this.toUpperCase();
  String get capitalizeFirstofEach => this.toLowerCase().split(' ').map((word) {
        String leftText =
            (word.length > 1) ? word.substring(1, word.length) : '';
        return word[0].toUpperCase() + leftText;
      }).join(' ');
}
