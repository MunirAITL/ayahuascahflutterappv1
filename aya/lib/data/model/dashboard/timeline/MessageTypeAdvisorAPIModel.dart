import 'dart:developer';

import 'MessageTypeAdviserModel.dart';

class MessageTypeAdvisorAPIModel {
  bool success;
  ErrorMessages errorMessages;
  Messages messages;
  ResponseData responseData;

  MessageTypeAdvisorAPIModel(
      {this.success, this.errorMessages, this.messages, this.responseData});

  MessageTypeAdvisorAPIModel.fromJson(Map<String, dynamic> json) {
    success = json['Success'];
    errorMessages = json['ErrorMessages'] != null
        ? new ErrorMessages.fromJson(json['ErrorMessages'])
        : null;
    messages = json['Messages'] != null
        ? new Messages.fromJson(json['Messages'])
        : null;
    responseData = json['ResponseData'] != null
        ? new ResponseData.fromJson(json['ResponseData'])
        : [];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['Success'] = this.success;
    if (this.errorMessages != null) {
      data['ErrorMessages'] = this.errorMessages.toJson();
    }
    if (this.messages != null) {
      data['Messages'] = this.messages.toJson();
    }
    if (this.responseData != null) {
      data['ResponseData'] = this.responseData.toJson();
    }
    return data;
  }
}

class ErrorMessages {
  ErrorMessages();
  ErrorMessages.fromJson(Map<String, dynamic> json);
  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    return data;
  }
}

class Messages {
  Messages();
  Messages.fromJson(Map<String, dynamic> json);
  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    return data;
  }
}

class ResponseData {
  List<MessageTypeAdviserModel> messageTypeAdviserModelList;
  ResponseData({this.messageTypeAdviserModelList});
  ResponseData.fromJson(Map<String, dynamic> json) {
    if (json['CaseListForPrivateMessageData'] != null) {
      messageTypeAdviserModelList = [];
      json['CaseListForPrivateMessageData'].forEach((v) {
        try {
          messageTypeAdviserModelList.add(new MessageTypeAdviserModel.fromJson(v));
        } catch (e) {
          log(e.toString());
        }
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.messageTypeAdviserModelList != null) {
      data['CaseListForPrivateMessageData'] = this.messageTypeAdviserModelList.map((v) => v.toJson()).toList();
    }
    return data;
  }
}
