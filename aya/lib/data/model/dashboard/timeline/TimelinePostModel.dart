class TimeLinePostModel {
  bool isFromMe;
  int id;
  int ownerId;
  String ownerEntityType;
  String ownerImageUrl;
  String ownerProfileUrl;
  String ownerName;
  String postTypeName;
  bool isSponsored;
  String message;
  List<dynamic> additionalAttributeValue;
  String dateCreatedUtc;
  String dateUpdatedUtc;
  String dateCreated;
  String dateUpdated;
  int totalLikes;
  int totalComments;
  bool canDelete;
  String publishDateUtc;
  String publishDate;
  int likeStatus;
  bool isOwner;
  String checkin;
  double fromLat;
  double fromLng;
  List<dynamic> userCommentPublicModelList;
  int receiverId;
  int senderId;
  int taskId;

  TimeLinePostModel(
      {this.isFromMe,
      this.id,
      this.ownerId,
      this.ownerEntityType,
      this.ownerImageUrl,
      this.ownerProfileUrl,
      this.ownerName,
      this.postTypeName,
      this.isSponsored,
      this.message,
      this.additionalAttributeValue,
      this.dateCreatedUtc,
      this.dateUpdatedUtc,
      this.dateCreated,
      this.dateUpdated,
      this.totalLikes,
      this.totalComments,
      this.canDelete,
      this.publishDateUtc,
      this.publishDate,
      this.likeStatus,
      this.isOwner,
      this.checkin,
      this.fromLat,
      this.fromLng,
      this.userCommentPublicModelList,
      this.receiverId,
      this.senderId,
      this.taskId});

  TimeLinePostModel.fromJson(Map<String, dynamic> json) {
    id = json['Id'] ?? 0;
    ownerId = json['OwnerId'] ?? 0;
    ownerEntityType = json['OwnerEntityType'] ?? '';
    ownerImageUrl = json['OwnerImageUrl'] ?? '';
    ownerProfileUrl = json['OwnerProfileUrl'] ?? '';
    ownerName = json['OwnerName'] ?? '';
    postTypeName = json['PostTypeName'] ?? '';
    isSponsored = json['IsSponsored'] ?? false;
    message = json['Message'] ?? '';
    additionalAttributeValue = json['AdditionalAttributeValue'] ?? [];
    dateCreatedUtc = json['DateCreatedUtc'] ?? '';
    dateUpdatedUtc = json['DateUpdatedUtc'] ?? '';
    dateCreated = json['DateCreated'] ?? '';
    dateUpdated = json['DateUpdated'] ?? '';
    totalLikes = json['TotalLikes'] ?? 0;
    totalComments = json['TotalComments'] ?? 0;
    canDelete = json['CanDelete'] ?? false;
    publishDateUtc = json['PublishDateUtc'] ?? '';
    publishDate = json['PublishDate'] ?? '';
    likeStatus = json['LikeStatus'] ?? 0;
    isOwner = json['IsOwner'] ?? false;
    checkin = json['Checkin'] ?? '';
    fromLat = json['FromLat'] ?? 0.0;
    fromLng = json['FromLng'] ?? 0.0;
    userCommentPublicModelList = json['UserCommentPublicModelList'] ?? [];
    receiverId = json['ReceiverId'] ?? 0;
    senderId = json['SenderId'] ?? 0;
    taskId = json['TaskId'] ?? 0;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['Id'] = this.id;
    data['OwnerId'] = this.ownerId;
    data['OwnerEntityType'] = this.ownerEntityType;
    data['OwnerImageUrl'] = this.ownerImageUrl;
    data['OwnerProfileUrl'] = this.ownerProfileUrl;
    data['OwnerName'] = this.ownerName;
    data['PostTypeName'] = this.postTypeName;
    data['IsSponsored'] = this.isSponsored;
    data['Message'] = this.message;
    data['AdditionalAttributeValue'] = this.additionalAttributeValue;
    data['DateCreatedUtc'] = this.dateCreatedUtc;
    data['DateUpdatedUtc'] = this.dateUpdatedUtc;
    data['DateCreated'] = this.dateCreated;
    data['DateUpdated'] = this.dateUpdated;
    data['TotalLikes'] = this.totalLikes;
    data['TotalComments'] = this.totalComments;
    data['CanDelete'] = this.canDelete;
    data['PublishDateUtc'] = this.publishDateUtc;
    data['PublishDate'] = this.publishDate;
    data['LikeStatus'] = this.likeStatus;
    data['IsOwner'] = this.isOwner;
    data['Checkin'] = this.checkin;
    data['FromLat'] = this.fromLat;
    data['FromLng'] = this.fromLng;
    data['UserCommentPublicModelList'] = this.userCommentPublicModelList;
    data['ReceiverId'] = this.receiverId;
    data['SenderId'] = this.senderId;
    data['TaskId'] = this.taskId;
    return data;
  }
}

class _AdditionalAttributeValueModel {
  _AdditionalAttributeValueModel();

  factory _AdditionalAttributeValueModel.fromJson(Map<String, dynamic> j) {
    return _AdditionalAttributeValueModel();
  }

  Map<String, dynamic> toMap() => {};
}
