import 'package:aitl/data/model/dashboard/common/LocationsModel.dart';

class TaskInfoSearchAPIModel {
  bool success;
  //_ErrorMessages errorMessages;
  //_Messages messages;
  dynamic errorMessages;
  dynamic messages;
  _ResponseData responseData;

  TaskInfoSearchAPIModel(
      {this.success, this.errorMessages, this.messages, this.responseData});

  factory TaskInfoSearchAPIModel.fromJson(Map<String, dynamic> j) {
    return TaskInfoSearchAPIModel(
      success: j['Success'] as bool,
      errorMessages: j['ErrorMessages'] ?? {},
      messages: j['Messages'] ?? {},
      responseData: (j['ResponseData'] != null)
          ? _ResponseData.fromJson(j['ResponseData'])
          : null,
    );
  }

  Map<String, dynamic> toMap() => {
        'Success': success,
        'ErrorMessages': errorMessages,
        'Messages': messages,
        'ResponseData': responseData,
      };
}

class _ResponseData {
  List<dynamic> locations;
  _ResponseData({this.locations});
  factory _ResponseData.fromJson(Map<String, dynamic> j) {
    var list_Locations = [];
    try {
      list_Locations = (j['Locations'] != null)
          ? j['Locations'].map((i) => LocationsModel.fromJson(i)).toList()
          : [];
    } catch (e) {
      print(e.toString());
    }
    return _ResponseData(locations: list_Locations ?? []);
  }
  Map<String, dynamic> toMap() => {
        'Locations': locations,
      };
}
