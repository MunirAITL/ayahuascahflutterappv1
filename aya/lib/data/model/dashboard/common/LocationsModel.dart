class LocationsModel {
  dynamic id;
  dynamic userId;
  dynamic status;
  String creationDate;
  String updatedDate;
  dynamic versionNumber;
  String title;
  String description;
  bool isInPersonOrOnline;
  dynamic dutDateType;
  String deliveryDate;
  String deliveryTime;
  dynamic workerNumber;
  String skill;
  bool isFixedPrice;
  dynamic hourlyRate;
  dynamic fixedBudgetAmount;
  dynamic netTotalAmount;
  dynamic paidAmount;
  dynamic dueAmount;
  String jobCategory;
  dynamic employeeId;
  dynamic totalBidsNumber;
  bool isArchive;
  String preferedLocation;
  dynamic latitude;
  dynamic longitude;
  String ownerName;
  String thumbnailPath;
  String referenceType;
  String referenceId;
  String remarks;
  String taskReferenceNumber;
  String imageServerUrl;
  String ownerImageUrl;
  String requirements;
  dynamic totalHours;
  dynamic totalAcceptedNumber;
  dynamic totalCompletedNumber;
  dynamic companyId;
  String companyName;
  dynamic notificationUnreadTaskCount;
  dynamic notificationTaskCount;
  String addressOfPropertyToBeMortgaged;
  String overAllCaseGrade;
  dynamic caseObservationAssessmentId;
  dynamic entityId;
  String entityName;
  Null namePrefix;
  Null lastName;
  String taskTitleUrl;
  String areYouChargingAFee;
  dynamic chargeFeeAmount;
  String chargingFeeWhenPayable;
  String chargingFeeRefundable;
  String areYouChargingAnotherFee;
  dynamic chargeAnotherFeeAmount;
  String chargingAnotherFeeWhenPayable;
  String chargingAnotherFeeRefundable;
  Null referenceSource;
  String adviserName;
  String introducerName;

  LocationsModel(
      {this.id,
      this.userId,
      this.status,
      this.creationDate,
      this.updatedDate,
      this.versionNumber,
      this.title,
      this.description,
      this.isInPersonOrOnline,
      this.dutDateType,
      this.deliveryDate,
      this.deliveryTime,
      this.workerNumber,
      this.skill,
      this.isFixedPrice,
      this.hourlyRate,
      this.fixedBudgetAmount,
      this.netTotalAmount,
      this.paidAmount,
      this.dueAmount,
      this.jobCategory,
      this.employeeId,
      this.totalBidsNumber,
      this.isArchive,
      this.preferedLocation,
      this.latitude,
      this.longitude,
      this.ownerName,
      this.thumbnailPath,
      this.referenceType,
      this.referenceId,
      this.remarks,
      this.taskReferenceNumber,
      this.imageServerUrl,
      this.ownerImageUrl,
      this.requirements,
      this.totalHours,
      this.totalAcceptedNumber,
      this.totalCompletedNumber,
      this.companyId,
      this.companyName,
      this.notificationUnreadTaskCount,
      this.notificationTaskCount,
      this.addressOfPropertyToBeMortgaged,
      this.overAllCaseGrade,
      this.caseObservationAssessmentId,
      this.entityId,
      this.entityName,
      this.namePrefix,
      this.lastName,
      this.taskTitleUrl,
      this.areYouChargingAFee,
      this.chargeFeeAmount,
      this.chargingFeeWhenPayable,
      this.chargingFeeRefundable,
      this.areYouChargingAnotherFee,
      this.chargeAnotherFeeAmount,
      this.chargingAnotherFeeWhenPayable,
      this.chargingAnotherFeeRefundable,
      this.referenceSource,
      this.adviserName,
      this.introducerName});

  LocationsModel.fromJson(Map<String, dynamic> json) {
    id = json['Id'];
    userId = json['UserId'];
    status = json['Status'];
    creationDate = json['CreationDate'];
    updatedDate = json['UpdatedDate'];
    versionNumber = json['VersionNumber'];
    title = json['Title'];
    description = json['Description'];
    isInPersonOrOnline = json['IsInPersonOrOnline'];
    dutDateType = json['DutDateType'];
    deliveryDate = json['DeliveryDate'];
    deliveryTime = json['DeliveryTime'];
    workerNumber = json['WorkerNumber'];
    skill = json['Skill'];
    isFixedPrice = json['IsFixedPrice'];
    hourlyRate = json['HourlyRate'];
    fixedBudgetAmount = json['FixedBudgetAmount'];
    netTotalAmount = json['NetTotalAmount'];
    paidAmount = json['PaidAmount'];
    dueAmount = json['DueAmount'];
    jobCategory = json['JobCategory'];
    employeeId = json['EmployeeId'];
    totalBidsNumber = json['TotalBidsNumber'];
    isArchive = json['IsArchive'];
    preferedLocation = json['PreferedLocation'];
    latitude = json['Latitude'];
    longitude = json['Longitude'];
    ownerName = json['OwnerName'];
    thumbnailPath = json['ThumbnailPath'];
    referenceType = json['ReferenceType'];
    referenceId = json['ReferenceId'];
    remarks = json['Remarks'];
    taskReferenceNumber = json['TaskReferenceNumber'];
    imageServerUrl = json['ImageServerUrl'];
    ownerImageUrl = json['OwnerImageUrl'];
    requirements = json['Requirements'];
    totalHours = json['TotalHours'];
    totalAcceptedNumber = json['TotalAcceptedNumber'];
    totalCompletedNumber = json['TotalCompletedNumber'];
    companyId = json['CompanyId'];
    companyName = json['CompanyName'];
    notificationUnreadTaskCount = json['NotificationUnreadTaskCount'];
    notificationTaskCount = json['NotificationTaskCount'];
    addressOfPropertyToBeMortgaged = json['AddressOfPropertyToBeMortgaged'];
    overAllCaseGrade = json['OverAllCaseGrade'];
    caseObservationAssessmentId = json['CaseObservationAssessmentId'];
    entityId = json['EntityId'];
    entityName = json['EntityName'];
    namePrefix = json['NamePrefix'];
    lastName = json['LastName'];
    taskTitleUrl = json['TaskTitleUrl'];
    areYouChargingAFee = json['AreYouChargingAFee'];
    chargeFeeAmount = json['ChargeFeeAmount'];
    chargingFeeWhenPayable = json['ChargingFeeWhenPayable'];
    chargingFeeRefundable = json['ChargingFeeRefundable'];
    areYouChargingAnotherFee = json['AreYouChargingAnotherFee'];
    chargeAnotherFeeAmount = json['ChargeAnotherFeeAmount'];
    chargingAnotherFeeWhenPayable = json['ChargingAnotherFeeWhenPayable'];
    chargingAnotherFeeRefundable = json['ChargingAnotherFeeRefundable'];
    referenceSource = json['ReferenceSource'];
    adviserName = json['AdviserName'];
    introducerName = json['IntroducerName'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['Id'] = this.id;
    data['UserId'] = this.userId;
    data['Status'] = this.status;
    data['CreationDate'] = this.creationDate;
    data['UpdatedDate'] = this.updatedDate;
    data['VersionNumber'] = this.versionNumber;
    data['Title'] = this.title;
    data['Description'] = this.description;
    data['IsInPersonOrOnline'] = this.isInPersonOrOnline;
    data['DutDateType'] = this.dutDateType;
    data['DeliveryDate'] = this.deliveryDate;
    data['DeliveryTime'] = this.deliveryTime;
    data['WorkerNumber'] = this.workerNumber;
    data['Skill'] = this.skill;
    data['IsFixedPrice'] = this.isFixedPrice;
    data['HourlyRate'] = this.hourlyRate;
    data['FixedBudgetAmount'] = this.fixedBudgetAmount;
    data['NetTotalAmount'] = this.netTotalAmount;
    data['PaidAmount'] = this.paidAmount;
    data['DueAmount'] = this.dueAmount;
    data['JobCategory'] = this.jobCategory;
    data['EmployeeId'] = this.employeeId;
    data['TotalBidsNumber'] = this.totalBidsNumber;
    data['IsArchive'] = this.isArchive;
    data['PreferedLocation'] = this.preferedLocation;
    data['Latitude'] = this.latitude;
    data['Longitude'] = this.longitude;
    data['OwnerName'] = this.ownerName;
    data['ThumbnailPath'] = this.thumbnailPath;
    data['ReferenceType'] = this.referenceType;
    data['ReferenceId'] = this.referenceId;
    data['Remarks'] = this.remarks;
    data['TaskReferenceNumber'] = this.taskReferenceNumber;
    data['ImageServerUrl'] = this.imageServerUrl;
    data['OwnerImageUrl'] = this.ownerImageUrl;
    data['Requirements'] = this.requirements;
    data['TotalHours'] = this.totalHours;
    data['TotalAcceptedNumber'] = this.totalAcceptedNumber;
    data['TotalCompletedNumber'] = this.totalCompletedNumber;
    data['CompanyId'] = this.companyId;
    data['CompanyName'] = this.companyName;
    data['NotificationUnreadTaskCount'] = this.notificationUnreadTaskCount;
    data['NotificationTaskCount'] = this.notificationTaskCount;
    data['AddressOfPropertyToBeMortgaged'] =
        this.addressOfPropertyToBeMortgaged;
    data['OverAllCaseGrade'] = this.overAllCaseGrade;
    data['CaseObservationAssessmentId'] = this.caseObservationAssessmentId;
    data['EntityId'] = this.entityId;
    data['EntityName'] = this.entityName;
    data['NamePrefix'] = this.namePrefix;
    data['LastName'] = this.lastName;
    data['TaskTitleUrl'] = this.taskTitleUrl;
    data['AreYouChargingAFee'] = this.areYouChargingAFee;
    data['ChargeFeeAmount'] = this.chargeFeeAmount;
    data['ChargingFeeWhenPayable'] = this.chargingFeeWhenPayable;
    data['ChargingFeeRefundable'] = this.chargingFeeRefundable;
    data['AreYouChargingAnotherFee'] = this.areYouChargingAnotherFee;
    data['ChargeAnotherFeeAmount'] = this.chargeAnotherFeeAmount;
    data['ChargingAnotherFeeWhenPayable'] = this.chargingAnotherFeeWhenPayable;
    data['ChargingAnotherFeeRefundable'] = this.chargingAnotherFeeRefundable;
    data['ReferenceSource'] = this.referenceSource;
    data['AdviserName'] = this.adviserName;
    data['IntroducerName'] = this.introducerName;
    return data;
  }

/*  Map<String, dynamic> toMap() => {
    'UserId': userId,
    'Status': status,
    'CreationDate': creationDate,
    'UpdatedDate': updatedDate,
    'VersionNumber': versionNumber,
    'Title': title,
    'Description': description,
    'IsInPersonOrOnline': isInPersonOrOnline,
    'DutDateType': dutDateType,
    'DeliveryDate': deliveryDate,
    'DeliveryTime': deliveryTime,
    'WorkerNumber': workerNumber,
    'Skill': skill,
    'IsFixedPrice': isFixedPrice,
    'HourlyRate': hourlyRate,
    'FixedBudgetAmount': fixedBudgetAmount,
    'NetTotalAmount': netTotalAmount,
    'PaidAmount': paidAmount,
    'DueAmount': dueAmount,
    'JobCategory': jobCategory,
    'EmployeeId': employeeId,
    'TotalBidsNumber': totalBidsNumber,
    'IsArchive': isArchive,
    'PreferedLocation': preferedLocation,
    'Latitude': latitude,
    'Longitude': longitude,
    'OwnerName': ownerName,
    'ThumbnailPath': thumbnailPath,
    'Remarks': remarks,
    'TaskReferenceNumber': taskReferenceNumber,
    'ImageServerUrl': imageServerUrl,
    'OwnerImageUrl': ownerImageUrl,
    'Requirements': requirements,
    'TotalHours': totalHours,
    'TaskTitleUrl': taskTitleUrl,
    'OwnerProfileUrl': ownerImageUrl,
    'TotalAcceptedNumber': totalAcceptedNumber,
    'TotalCompletedNumber': totalCompletedNumber,
    'CompanyId': companyId,
    'CompanyName': companyName,
    'EntityId': entityId,
    'EntityName': entityName,
    // 'IntroducerFeeShareAmount': introducerFeeShareAmount,
    // 'IntroducerPaymentedAmount': introducerPaymentedAmount,
    // 'AdviserAmount': adviserAmount,
    // 'AdviserPaymentedAmount': adviserPaymentedAmount,
    'NotificationUnreadTaskCount': notificationUnreadTaskCount,
    'NotificationTaskCount': notificationTaskCount,
    'AddressOfPropertyToBeMortgaged': addressOfPropertyToBeMortgaged,
    'OverAllCaseGrade': overAllCaseGrade,
    'CaseObservationAssessmentId': caseObservationAssessmentId,
    'NamePrefix': namePrefix,
    'LastName': lastName,
    'Id': id,
  };*/

}
