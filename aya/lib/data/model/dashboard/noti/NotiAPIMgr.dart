import 'package:aitl/config/server/APINotiCfg.dart';
import 'package:aitl/data/model/dashboard/more/noti/NotiAPIModel.dart';
import 'package:aitl/data/model/dashboard/noti/DeleteNotification.dart';
import 'package:aitl/data/network/NetworkMgr.dart';
import 'package:aitl/view_model/helper/noti/NotiHelper.dart';
import 'package:flutter/cupertino.dart';
import 'package:aitl/Mixin.dart';

class NotiAPIMgr with Mixin {
  static final NotiAPIMgr _shared = NotiAPIMgr._internal();

  factory NotiAPIMgr() {
    return _shared;
  }

  NotiAPIMgr._internal();

  wsOnPageLoad({
    BuildContext context,
    int pageStart,
    int pageCount,
    Function(NotiAPIModel) callback,
  }) async {
    try {
      final url =
          NotiHelper().getUrl(pageStart: pageStart, pageCount: pageCount);
      log(url);
      await NetworkMgr()
          .req<NotiAPIModel, Null>(
        context: context,
        reqType: ReqType.Get,
        url: url,
        isLoading: (pageStart == 0) ? true : false,
      )
          .then((model) async {
        callback(model);
      });
    } catch (e) {
      log(e.toString());
    }
  }

  wsDeleteNotification(
      {BuildContext context,
      String id,
      Function(DeleteNotificationModel model) callback}) async {
    try {
      await NetworkMgr().req<DeleteNotificationModel, Null>(
        context: context,
        url: APINotiCfg.NOTI_DELETE_URL.replaceAll("#notiId#", id.toString()),
        reqType: ReqType.Delete,
        param: {},
      ).then((model) async {
        callback(model);
      });
    } catch (e) {
      log(e.toString());
    }
  }
}
