import 'package:aitl/data/model/dashboard/user/PublicUserModel.dart';

class WelcomeNotificationModel {
  int userId;
  User user;
  bool isRead;
  String publishDateTime;
  String readDateTime;
  int entityId;
  String entityName;
  String notificationEventId;
  String notificationEvent;
  int initiatorId;
  String initiatorName;
  String message;
  String webUrl;
  String description;
  int id;

  WelcomeNotificationModel(
      {this.userId,
      this.user,
      this.isRead,
      this.publishDateTime,
      this.readDateTime,
      this.entityId,
      this.entityName,
      this.notificationEventId,
      this.notificationEvent,
      this.initiatorId,
      this.initiatorName,
      this.message,
      this.webUrl,
      this.description,
      this.id});

  WelcomeNotificationModel.fromJson(Map<String, dynamic> json) {
    userId = json['UserId'] ?? 0;
    user = json['User'] != null ? new User.fromJson(json['User']) : null;
    isRead = json['IsRead'] ?? false;
    publishDateTime = json['PublishDateTime'] ?? '';
    readDateTime = json['ReadDateTime'] ?? '';
    entityId = json['EntityId'] ?? 0;
    entityName = json['EntityName'] ?? '';
    notificationEventId = json['NotificationEventId'] ?? '';
    notificationEvent = json['NotificationEvent'] ?? '';
    initiatorId = json['InitiatorId'] ?? 0;
    initiatorName = json['InitiatorName'] ?? '';
    message = json['Message'] ?? '';
    webUrl = json['WebUrl'] ?? '';
    description = json['Description'] ?? '';
    id = json['Id'] ?? 0;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['UserId'] = this.userId;
    if (this.user != null) {
      data['User'] = this.user.toJson();
    }
    data['IsRead'] = this.isRead;
    data['PublishDateTime'] = this.publishDateTime;
    data['ReadDateTime'] = this.readDateTime;
    data['EntityId'] = this.entityId;
    data['EntityName'] = this.entityName;
    data['NotificationEventId'] = this.notificationEventId;
    data['NotificationEvent'] = this.notificationEvent;
    data['InitiatorId'] = this.initiatorId;
    data['InitiatorName'] = this.initiatorName;
    data['Message'] = this.message;
    data['WebUrl'] = this.webUrl;
    data['Description'] = this.description;
    data['Id'] = this.id;
    return data;
  }
}
