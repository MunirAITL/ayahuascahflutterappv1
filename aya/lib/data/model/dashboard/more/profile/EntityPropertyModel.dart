class EntityPropertyModel {
  int entityId;
  String entityName;
  String propertyName;
  String value;

  EntityPropertyModel(
      {this.entityId, this.entityName, this.propertyName, this.value});

  EntityPropertyModel.fromJson(Map<String, dynamic> json) {
    entityId = json['EntityId'] ?? 0;
    entityName = json['EntityName'] ?? '';
    propertyName = json['PropertyName'] ?? '';
    value = json['Value'] ?? '';
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['EntityId'] = this.entityId;
    data['EntityName'] = this.entityName;
    data['PropertyName'] = this.propertyName;
    data['Value'] = this.value;
    return data;
  }
}
