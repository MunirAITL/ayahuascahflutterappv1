class UserNotificationSettingModel {
  int id;
  bool isHelpFullEmail;
  bool isHelpFullNotification;
  bool isShohokariAlertEmail;
  bool isShohokariAlertNotification;
  bool isTaskRecomendationEmail;
  bool isTaskRecomendationNotification;
  bool isTaskReminderEmail;
  bool isTaskReminderNotification;
  bool isTaskReminderSMS;
  bool isTaskUpdateEmail;
  bool isTaskUpdateNotification;
  bool isTaskUpdateSMS;
  bool isTransactionalEmail;
  bool isTransactionalNotification;
  bool isTransactionalSMS;
  bool isUpdateAndNewsLetterNotification;
  int userId;

  UserNotificationSettingModel(
      {this.id,
      this.isHelpFullEmail,
      this.isHelpFullNotification,
      this.isShohokariAlertEmail,
      this.isShohokariAlertNotification,
      this.isTaskRecomendationEmail,
      this.isTaskRecomendationNotification,
      this.isTaskReminderEmail,
      this.isTaskReminderNotification,
      this.isTaskReminderSMS,
      this.isTaskUpdateEmail,
      this.isTaskUpdateNotification,
      this.isTaskUpdateSMS,
      this.isTransactionalEmail,
      this.isTransactionalNotification,
      this.isTransactionalSMS,
      this.isUpdateAndNewsLetterNotification,
      this.userId});

  UserNotificationSettingModel.fromJson(Map<String, dynamic> json) {
    id = json['Id'] ?? 0;
    isHelpFullEmail = json['IsHelpFullEmail'] ?? false;
    isHelpFullNotification = json['IsHelpFullNotification'] ?? false;
    isShohokariAlertEmail = json['IsShohokariAlertEmail'] ?? false;
    isShohokariAlertNotification =
        json['IsShohokariAlertNotification'] ?? false;
    isTaskRecomendationEmail = json['IsTaskRecomendationEmail'] ?? false;
    isTaskRecomendationNotification =
        json['IsTaskRecomendationNotification'] ?? false;
    isTaskReminderEmail = json['IsTaskReminderEmail'] ?? false;
    isTaskReminderNotification = json['IsTaskReminderNotification'] ?? false;
    isTaskReminderSMS = json['IsTaskReminderSMS'] ?? false;
    isTaskUpdateEmail = json['IsTaskUpdateEmail'] ?? false;
    isTaskUpdateNotification = json['IsTaskUpdateNotification'] ?? false;
    isTaskUpdateSMS = json['IsTaskUpdateSMS'] ?? false;
    isTransactionalEmail = json['IsTransactionalEmail'] ?? false;
    isTransactionalNotification = json['IsTransactionalNotification'] ?? false;
    isTransactionalSMS = json['IsTransactionalSMS'] ?? false;
    isUpdateAndNewsLetterNotification =
        json['IsUpdateAndNewsLetterNotification'] ?? false;
    userId = json['UserId'] ?? 0;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['Id'] = this.id;
    data['IsHelpFullEmail'] = this.isHelpFullEmail;
    data['IsHelpFullNotification'] = this.isHelpFullNotification;
    data['IsShohokariAlertEmail'] = this.isShohokariAlertEmail;
    data['IsShohokariAlertNotification'] = this.isShohokariAlertNotification;
    data['IsTaskRecomendationEmail'] = this.isTaskRecomendationEmail;
    data['IsTaskRecomendationNotification'] =
        this.isTaskRecomendationNotification;
    data['IsTaskReminderEmail'] = this.isTaskReminderEmail;
    data['IsTaskReminderNotification'] = this.isTaskReminderNotification;
    data['IsTaskReminderSMS'] = this.isTaskReminderSMS;
    data['IsTaskUpdateEmail'] = this.isTaskUpdateEmail;
    data['IsTaskUpdateNotification'] = this.isTaskUpdateNotification;
    data['IsTaskUpdateSMS'] = this.isTaskUpdateSMS;
    data['IsTransactionalEmail'] = this.isTransactionalEmail;
    data['IsTransactionalNotification'] = this.isTransactionalNotification;
    data['IsTransactionalSMS'] = this.isTransactionalSMS;
    data['IsUpdateAndNewsLetterNotification'] =
        this.isUpdateAndNewsLetterNotification;
    data['UserId'] = this.userId;
    return data;
  }
}
