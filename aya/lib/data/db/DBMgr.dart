import 'dart:convert';
import 'dart:io';
import 'package:aitl/data/model/auth/UserModel.dart';
import 'package:aitl/mixin.dart';
import 'package:path/path.dart';
import 'package:path_provider/path_provider.dart';
import 'package:sqflite/sqflite.dart';

class DBMgr with Mixin {
  DBMgr._();
  static final DBMgr shared = DBMgr._();
  static Database _database;

  Future<Database> get database async {
    if (_database != null) return _database;
    // if _database is null we instantiate it
    _database = await initDB();
    return _database;
  }

  initDB() async {
    try {
      Directory documentsDirectory = await getApplicationDocumentsDirectory();
      String path = join(documentsDirectory.path, "herotasker.db");
      return await openDatabase(path, version: 1, onOpen: (db) {},
          onCreate: (Database db, int version) async {
        await db.execute("CREATE TABLE User ("
            "id INTEGER PRIMARY KEY, otpId Text, otpMobileNumber Text, json TEXT"
            ")");
      });
    } catch (e) {
      log(e.toString());
    }
  }

  //  ********************  USER  start here***********
  //  USER: set/add
  Future<bool> setUserProfile(
      {UserModel user, String otpID = '', String otpMobileNumber = ''}) async {
    try {
      final j = json.encode(user.toJson());
      final db = await database;
      await db.rawDelete("Delete FROM User");
      await db.rawInsert("INSERT INTO User (json, otpId, otpMobileNumber)"
              " VALUES ('" +
          j +
          "', '" +
          otpID +
          "', '" +
          otpMobileNumber +
          "')");
      return true;
    } catch (e) {
      log(e.toString());
      return false;
    }
  }

  //  USER: get
  Future<UserModel> getUserProfile() async {
    try {
      final db = await database;
      List<Map> list = await db.rawQuery("SELECT json from User");
      //list.forEach((row) => print(row));  //for show all
      final map = list[0];
      return UserModel.fromJson(json.decode(map['json']));
    } catch (e) {
      log(e.toString());
      return null;
    }
  }

  //  ********************  USER  end here***********

  //  ********************  CRUD  ********************

  //  table: count row
  Future<int> getTotalRow(table) async {
    try {
      final db = await database;
      return Sqflite.firstIntValue(
          await db.rawQuery('SELECT COUNT(*) FROM ' + table));
    } catch (e) {
      log(e.toString());
      return 0;
    }
  }

  //  table: delete all rows
  delTable(table) async {
    try {
      final db = await database;
      await db.rawDelete("Delete FROM " + table);
    } catch (e) {
      log(e.toString());
    }
  }
}
