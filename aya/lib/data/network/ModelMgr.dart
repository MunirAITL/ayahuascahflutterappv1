//  https://stackoverflow.com/questions/56271651/how-to-pass-a-generic-type-as-a-parameter-to-a-future-in-flutter
import 'package:aitl/data/model/auth/ForgotAPIModel.dart';
import 'package:aitl/data/model/auth/LoginAPIModel.dart';
import 'package:aitl/data/model/auth/RegAPIModel.dart';
import 'package:aitl/data/model/auth/loginWith/LoginWithModel.dart';
import 'package:aitl/data/model/auth/otp/sms1/MobileUserOtpPostAPIModel.dart';
import 'package:aitl/data/model/auth/otp/sms1/SendOtpNotiAPIModel.dart';
import 'package:aitl/data/model/auth/otp/sms2/LoginRegOtpFBAPIModel.dart';
import 'package:aitl/data/model/auth/otp/sms2/MobileUserOtpPutAPIModel.dart';
import 'package:aitl/data/model/auth/profile/RegProfileAPIModel.dart';
import 'package:aitl/data/model/dashboard/common/TaskInfoSearchAPIModel.dart';
import 'package:aitl/data/model/dashboard/more/help/WelcomeSmsAPIModel.dart';
import 'package:aitl/data/model/dashboard/more/noti/NotiAPIModel.dart';
import 'package:aitl/data/model/dashboard/more/noti/NotiTestAPIModel.dart';
import 'package:aitl/data/model/dashboard/more/noti/UserNotificationSettingAPIModel.dart';
import 'package:aitl/data/model/dashboard/more/payment/history/TaskPaymentsHistoryAPIModel.dart';
import 'package:aitl/data/model/dashboard/more/payment/methods/BillingAddrAPIModel.dart';
import 'package:aitl/data/model/dashboard/more/payment/methods/PaymentMethodAPIModel.dart';
import 'package:aitl/data/model/dashboard/more/payment/methods/PaymentMethodsAPIModel.dart';
import 'package:aitl/data/model/dashboard/more/payment/methods/PromoCodeAPIModel.dart';
import 'package:aitl/data/model/dashboard/more/profile/AboutAPIModel.dart';
import 'package:aitl/data/model/dashboard/more/profile/EntityPropertyAPIModel.dart';
import 'package:aitl/data/model/dashboard/more/profile/PublicProfileAPIModel.dart';
import 'package:aitl/data/model/dashboard/more/profile/UserBadgeAPIModel.dart';
import 'package:aitl/data/model/dashboard/more/profile/UserPortFolioAPIModel.dart';
import 'package:aitl/data/model/dashboard/more/profile/UserRatingSummaryAPIModel.dart';
import 'package:aitl/data/model/dashboard/more/profile/UserRatingAPIModel.dart';
import 'package:aitl/data/model/dashboard/more/res/ResAPIModel.dart';
import 'package:aitl/data/model/dashboard/more/res/ResolutionAPIModel.dart';
import 'package:aitl/data/model/dashboard/more/task_alert/TaskAlertAPIModel.dart';
import 'package:aitl/data/model/dashboard/more/task_alert/TaskAlertsAPIModel.dart';
import 'package:aitl/data/model/dashboard/timeline/MessageTypeAdvisorAPIModel.dart';
import 'package:aitl/data/model/dashboard/timeline/TaskBiddingAPIModel.dart';
import 'package:aitl/data/model/dashboard/timeline/TimeLineAPIModel.dart';
import 'package:aitl/data/model/dashboard/timeline/TimeLineAdvisorAPIModel.dart';
import 'package:aitl/data/model/dashboard/timeline/TimeLinePostAPIModel.dart';
import 'package:aitl/data/model/misc/CommonAPIModel.dart';
import 'package:aitl/data/model/misc/device_info/FcmDeviceInfoAPIModel.dart';
import 'package:aitl/data/model/misc/device_info/UserDeviceAPIModel.dart';
import 'package:aitl/data/model/misc/media_upload/MediaUploadFilesAPIModel.dart';
import 'package:aitl/data/model/misc/rating/PendingReviewRatingByUserIdAPIModel.dart';

enum APIType {
  login,
  loginWithG,
  loginWithFB,
  forgot,
  reg1,
  reg2,
  reg_fb_mobile,
  otp_post,
  otp_put,
  pending_review_rating_userid,
  task_rating_summary,
  user_rating_summary,
  user_rating,
  user_badge,
  about_skills,
  about_skills_post,
  edit_profile,
  public_profile,
  entity_profile_cover_image,
  entity_profile_image,
  media_profile_cover_image,
  media_profile_image,
  portfolio,
  portfolio_upload,
  taskinfo_search,
  media_upload_file,
  post_task1,
  post_task3,
  put_task1,
  put_task3,
  del_task,
  email_noti,
  get_noti,
  put_noti,
  user_noti_settings_get,
  user_noti_settings_put,
  parent_all_cat,
  child_all_cat,
  get_pic,
  save_pic,
  del_pic,
  get_timeline_by_app,
  post_timeline,
  post_comment,
  get_comments,
  task_biddings,
  task_bidding_put,
  task_bidding_del,
  res,
  resolution,
  put_payment_confirmation,
  post_review,
  payment_history_get,
  payment_methods_make_post_promo_card,
  payment_methods_make_get_promo_card,
  payment_methods_receive_post_billing_addr,
  payment_methods_receive_put_billing_addr,
  payment_methods_receive_get_billing_addr,
  payment_methods_receive_post_bank_acc,
  payment_methods_receive_put_bank_acc,
  payment_methods_receive_get_bank_acc,
  stripe_payment_intent_post,
  task_alert_get,
  task_alert_post,
  task_alert_put,
  task_alert_del,
  welcome_sms,
}

class APIState {
  APIType type;
  dynamic cls;
  dynamic data;
  APIState(type, cls, data) {
    this.type = type;
    this.cls = cls;
    this.data = data;
  }
}

class ModelMgr {
  /// If T is a List, K is the subtype of the list.
  ///
  /*List<T> fromJsonList<T>(List<dynamic> jsonList) {
    return jsonList
        ?.map<T>((dynamic json) => fromJson<T, void>(json))
        ?.toList();
  }*/

  Future<T> fromJson<T, K>(dynamic json) async {
    //return fromJsonList<K>(json) as T;

    if (identical(T, FcmDeviceInfoAPIModel)) {
      return FcmDeviceInfoAPIModel.fromJson(json) as T;
    } else if (identical(T, UserDeviceAPIModel)) {
      return UserDeviceAPIModel.fromJson(json) as T;
    } else if (identical(T, MediaUploadFilesAPIModel)) {
      return MediaUploadFilesAPIModel.fromJson(json) as T;
    } else if (identical(T, LoginAPIModel)) {
      return LoginAPIModel.fromJson(json) as T;
    } else if (identical(T, MobileUserOtpPostAPIModel)) {
      return MobileUserOtpPostAPIModel.fromJson(json) as T;
    } else if (identical(T, SendOtpNotiAPIModel)) {
      return SendOtpNotiAPIModel.fromJson(json) as T;
    } else if (identical(T, NotiAPIModel)) {
      return NotiAPIModel.fromJson(json) as T;
    } else if (identical(T, MobileUserOtpPutAPIModel)) {
      return MobileUserOtpPutAPIModel.fromJson(json) as T;
    } else if (identical(T, LoginRegOtpFBAPIModel)) {
      return LoginRegOtpFBAPIModel.fromJson(json) as T;
    } else if (identical(T, ForgotAPIModel)) {
      return ForgotAPIModel.fromJson(json) as T;
    } else if (identical(T, RegAPIModel)) {
      return RegAPIModel.fromJson(json) as T;
    } else if (identical(T, RegProfileAPIModel)) {
      return RegProfileAPIModel.fromJson(json) as T;
    } else if (identical(T, WelcomeSmsAPIModel)) {
      return WelcomeSmsAPIModel.fromJson(json) as T;
    } else if (identical(T, PendingReviewRatingByUserIdAPIModel)) {
      return PendingReviewRatingByUserIdAPIModel.fromJson(json) as T;
    } else if (identical(T, UserRatingSummaryAPIModel)) {
      return UserRatingSummaryAPIModel.fromJson(json) as T;
    } else if (identical(T, UserRatingAPIModel)) {
      return UserRatingAPIModel.fromJson(json) as T;
    } else if (identical(T, UserBadgeAPIModel)) {
      return UserBadgeAPIModel.fromJson(json) as T;
    } else if (identical(T, AboutAPIModel)) {
      return AboutAPIModel.fromJson(json) as T;
    } else if (identical(T, PublicProfileAPIModel)) {
      return PublicProfileAPIModel.fromJson(json) as T;
    } else if (identical(T, UserPortFolioAPIModel)) {
      return UserPortFolioAPIModel.fromJson(json) as T;
    } else if (identical(T, EntityPropertyAPIModel)) {
      return EntityPropertyAPIModel.fromJson(json) as T;
    } else if (identical(T, TaskInfoSearchAPIModel)) {
      return TaskInfoSearchAPIModel.fromJson(json) as T;
    } else if (identical(T, CommonAPIModel)) {
      return CommonAPIModel.fromJson(json) as T;
    } else if (identical(T, TaskBiddingAPIModel)) {
      return TaskBiddingAPIModel.fromJson(json) as T;
    } else if (identical(T, ResAPIModel)) {
      return ResAPIModel.fromJson(json) as T;
    } else if (identical(T, ResolutionAPIModel)) {
      return ResolutionAPIModel.fromJson(json) as T;
    } else if (identical(T, TaskPaymentsHistoryAPIModel)) {
      return TaskPaymentsHistoryAPIModel.fromJson(json) as T;
    } else if (identical(T, PromoCodeAPIModel)) {
      return PromoCodeAPIModel.fromJson(json) as T;
    } else if (identical(T, BillingAddrAPIModel)) {
      return BillingAddrAPIModel.fromJson(json) as T;
    } else if (identical(T, PaymentMethodAPIModel)) {
      return PaymentMethodAPIModel.fromJson(json) as T;
    } else if (identical(T, PaymentMethodsAPIModel)) {
      return PaymentMethodsAPIModel.fromJson(json) as T;
    } else if (identical(T, TaskAlertAPIModel)) {
      return TaskAlertAPIModel.fromJson(json) as T;
    } else if (identical(T, TaskAlertsAPIModel)) {
      return TaskAlertsAPIModel.fromJson(json) as T;
    } else if (identical(T, UserNotificationSettingAPIModel)) {
      return UserNotificationSettingAPIModel.fromJson(json) as T;
    } else if (identical(T, NotiTestAPIModel)) {
      return NotiTestAPIModel.fromJson(json) as T;
    } else if (identical(T, TimeLineAdvisorAPIModel)) {
      return TimeLineAdvisorAPIModel.fromJson(json) as T;
    } else if (identical(T, TimeLineAPIModel)) {
      return TimeLineAPIModel.fromJson(json) as T;
    } else if (identical(T, TimeLinePostAPIModel)) {
      return TimeLinePostAPIModel.fromJson(json) as T;
    } else if (identical(T, TaskBiddingAPIModel)) {
      return TaskBiddingAPIModel.fromJson(json) as T;
    } else if (identical(T, MessageTypeAdvisorAPIModel)) {
      return MessageTypeAdvisorAPIModel.fromJson(json) as T;
    }

    /*if (json is Iterable) {
      return _fromJsonList<K>(json) as T;
    } else if (identical(T, LoginModel)) {
      return LoginModel.fromJson(json) as T;
    } else if (T == bool ||
        T == String ||
        T == int ||
        T == double ||
        T == Map) {
      // primitives
      return json;
    } else {
      throw Exception("Unknown class");
    }*/
  }

  /*List<K> _fromJsonList<K>(List<dynamic> jsonList) {
    return jsonList
        ?.map<K>((dynamic json) => fromJson<K, void>(json))
        ?.toList();
  }*/
}
