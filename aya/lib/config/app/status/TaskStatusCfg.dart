class TaskStatusCfg {
  // Task Status
  static const int TASK_STATUS_DRAFT = 903;
  static const int TASK_STATUS_ACTIVE = 101;
  static const int TASK_STATUS_ACCEPTED = 102;
  static const int TASK_STATUS_PAYMENTED = 103;
  static const int TASK_STATUS_CANCELLED = 104;
  static const int TASK_STATUS_DELETED = 105;
  static const int TASK_STATUS_ALL = 901;
  static const int TASK_STATUS_PENDINGTASK = 902;
  static const int STATUS_ALL_PRIVATEMESSAGE = 904;

  static const TASK_PAYMENT_METHOD_CASH = "Cash";
  static const TASK_PAYMENT_METHOD_CARD = "Bank";

  static const TASK_PAYMENT_METHOD_BANK = "Bank";
  static const TASK_PAYMENT_METHOD_CREDIT_CARD = "Card";

  static const STATUS_PAYMENT_METHOD_CASH = "CASH";
  static const STATUS_PAYMENT_METHOD_FUNDED = "FUNDED";
  static const STATUS_PAYMENT_METHOD_FUNDED_REQUESTPAYMENT =
      "FUNDED_REQUESTPAYMENT";
  static const STATUS_PAYMENT_METHOD_FUNDED_RELEASEPAYMENT =
      "FUNDED_RELEASEPAYMENT";
  static const STATUS_TASK_BIDDING_REQUEST_PAYMENT = "REQUESTPAYMENT";
  static const STATUS_TASK_BIDDING_RECEIVED_PAYMENT = "RECEIVEDPAYMENT";

  static const PREF_KEY_USER_SEND_PAYMENT_METHOD_BIKASH =
      "PREF_KEY_USER_SEND_PAYMENT_METHOD_BIKASH";
  static const PREF_KEY_USER_SEND_PAYMENT_METHOD_ROCKET =
      "PREF_KEY_USER_SEND_PAYMENT_METHOD_ROCKET";
  static const PREF_KEY_USER_SEND_PAYMENT_METHOD_BANK =
      "PREF_KEY_USER_SEND_PAYMENT_METHOD_BANK";

  String getSatus(int status) {
    switch (status) {
      case 1:
        return "Receipt";
      case 2:
        return "Payment";
      case 3:
        return "POST";
      case 4:
        return "PURCHASE ORDER";
      case 101:
        return "ACTIVE";
      case 102:
        return "ASSIGNED";
      case 103:
        return "PAID";
      case 104:
        return "CANCELLED";
      case 105:
        return "DELETED";
      case 901:
        return "ALLTASK";
      case 902:
        return "PENDINGTASK";
      case 903:
        return "DRAFT";
      case 904:
        return "PRIVATEMESSAGE";
      case 201:
        return "REQUESTPAYMENT";
      case 202:
        return "RECEIVEDPAYMENT";
      case 203:
        return "FUNDED";
      case 204:
        return "CASH";
      case 205:
        return "FUNDED_REQUESTPAYMENT";
      case 206:
        return "FUNDED_RELEASEPAYMENT";
      case 905:
        return "VERIFIED";
      case 906:
        return "UNVERIFIED";
      default:
        return "";
    }
  }

  int getSatusCode(String status) {
    try {
      if (int.parse(status) > 0) {
        return int.parse(status);
      }
    } catch (e) {}

    switch (status) {
      case "Receipt":
        return 1;
      case "Payment":
        return 2;
      case "POST":
        return 3;
      case "PURCHASE ORDER":
        return 4;
      case "ACTIVE":
        return 101;
      case "ASSIGNED":
        return 102;
      case "PAID":
        return 103;
      case "CANCELLED":
        return 104;
      case "DELETED":
        return 105;
      case "ALLTASK":
        return 901;
      case "PENDINGTASK":
        return 902;
      case "DRAFT":
        return 903;
      case "PRIVATEMESSAGE":
        return 904;
      case "REQUESTPAYMENT":
        return 201;
      case "RECEIVEDPAYMENT":
        return 202;
      case "FUNDED":
        return 203;
      case "CASH":
        return 204;
      case "FUNDED_REQUESTPAYMENT":
        return 205;
      case "FUNDED_RELEASEPAYMENT":
        return 205;
      case "VERIFIED":
        return 905;
      case "UNVERIFIED":
        return 906;
      default:
        return 0;
    }
  }
}
