import 'Server.dart';

class APITimelineCfg {
  static const int hideFieldsIfLessByYear = 3;

  //  Timeline
  //static const String TIMELINE_URL = BASE_URL +
  // "/api/timeline/get?IsPrivate=#isPrivate#&ReceiverId=#receiverId#&SenderId=#senderId#&TaskId=#taskId#&Count=#count#&CustomerId=#customerId#&Page=#page#&timeLineId=#timeLineId#";
  static const String TIMELINE_ADVISOR_URL = Server.BASE_URL +
      "/api/users/get/userbycomunityidandcompanyuseridforprivatemessage?UserId=#userId#&CommunityId=#communityId#&UserCompanyId=#companyId#";
  static const String TIMELINE_MESSAGE_TYPE_URL = Server.BASE_URL +
      "/api/casereport/get/caselistforprivatemessagedata?UserCompanyId=#UserCompanyId#&CustomerId=#CustomerId#&AdviserOrIntroducerId=#AdviserOrIntroducerId#";
  static const String TASKBIDDING_URL =
      Server.BASE_URL + "/api/taskbidding/get?taskId=#taskId#";
  static const String TIMELINE_URL = Server.BASE_URL +
      "/api/timeline/gettimelinebyapp?Count=#count#&count=#count#&IsPrivate=#isPrivate#&ReceiverId=#receiverId#&SenderId=#senderId#&TaskId=#taskId#&customerId=#customerId#&page=#page#&timeLineId=#timeLineId#";
  static const String TIMELINE_POST_URL =
      Server.BASE_URL + "/api/timeline/post";
}
