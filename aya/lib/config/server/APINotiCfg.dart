import 'package:aitl/config/server/Server.dart';

class APINotiCfg {
  static const NOTI_GET_URL = Server.BASE_URL + "/api/notifications/get";
  static const NOTI_PUT_URL = Server.BASE_URL + "/api/notifications/put";
  static const NOTI_SETTINGS_GET_URL =
      Server.BASE_URL + "/api/usernotificationsetting/get";
  static const NOTI_SETTINGS_PUT_URL =
      Server.BASE_URL + "/api/usernotificationsetting/put";
  static const NOTI_SETTINGS_TEST_URL = Server.BASE_URL +
      "/api/notifications/sendtestpushnotificationtouser/#userId#";

  //  Notification
  static const String NOTI_URL =
      Server.BASE_URL + "/api/notifications/get?userId=#userId#";
  static const String NOTI_DELETE_URL =
      Server.BASE_URL + "/api/notifications/delete/#notiId#";

  static const String CASEDETAILS_WEBVIEW_URL =
      Server.BASE_URL + "/apps/about-me/#title#-#taskId#";
  static const String BASE_URL_NOTI_WEB = Server.BASE_URL + "/apps/about-me";
}
