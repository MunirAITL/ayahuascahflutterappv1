import 'package:aitl/config/server/Server.dart';

class APIMyTasksCfg {
  static const String GET_TASK_URL = Server.BASE_URL + "/api/task/get/#taskId#";
  static const String GET_TASKBIDDING_URL =
      Server.BASE_URL + "/api/taskbidding/get/#taskBiddingId#";
  static const String PUT_TASKBIDDING_URL =
      Server.BASE_URL + "/api/taskbidding/put";
  static const String DEL_TASKBIDDING_URL =
      Server.BASE_URL + "/api/taskbidding/delete/#taskBiddingId#";
  static const String GET_USERRATING_BYTASKID_URL =
      Server.BASE_URL + "/api/userrating/getuserratingbytaskId/#taskId#";
}
