import 'package:aitl/config/server/Server.dart';

class APIProfileCFg {
  //  more->dashboard->rating summary
  static const String GET_USER_RATING_SUMMARY_URL =
      Server.BASE_URL + "/api/userrating/getuserratingsummarydata";

  //  user rating
  static const String USER_RATING_GET_URL =
      Server.BASE_URL + "/api/userrating/get";

  static const String USER_RATING_POST_URL =
      Server.BASE_URL + "/api/userrating/post";

  //
  static const String USER_PORTFOLIO_URL =
      Server.BASE_URL + "/api/userportfulio/get";

  static const String USER_PORTFOLIO_UPLOAD_URL =
      Server.BASE_URL + "/api/userportfulio/post/mobileapp";

  //
  static const String ABOUT_GET_URL = Server.BASE_URL + "/api/about/get";
  static const String ABOUT_POST_URL =
      Server.BASE_URL + "/api/about/post/mobileapp";

  //
  static const String USER_BADGE_URL = Server.BASE_URL + "/api/userbadge/get";

  //
  static const String ENTITY_PROPERTY_POST_URL =
      Server.BASE_URL + "/api/entityproperty/post";

  //
  static const String PUBLIC_USER_GET_URL =
      Server.BASE_URL + "/api/users/get/#userId#/basic";
}
