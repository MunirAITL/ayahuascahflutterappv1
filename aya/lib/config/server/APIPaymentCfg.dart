import 'package:aitl/config/server/Server.dart';

class APIPaymentCfg {
  static const String TASK_PAYMENT_GET_URL =
      Server.BASE_URL + "/api/taskpayment/get";

  static const String PROMO_CODE_POST_URL =
      Server.BASE_URL + "/api/userpromotion/post/acceptuserpromotion";

  static const String PROMO_CODE_GET_URL =
      Server.BASE_URL + "/api/userpromotion/get";

  //
  static const String BILLING_ADDR_GET_URL =
      Server.BASE_URL + "/api/billingaddress/get";

  static const String BILLING_ADDR_PUT_URL =
      Server.BASE_URL + "/api/billingaddress/put";

  static const String BILLING_ADDR_POST_URL =
      Server.BASE_URL + "/api/billingaddress/post";

  //
  static const String PAYMENT_METHODS_GET_URL =
      Server.BASE_URL + "/api/paymentmethod/get";

  static const String PAYMENT_METHODS_PUT_URL =
      Server.BASE_URL + "/api/paymentmethod/put";

  static const String PAYMENT_METHODS_POST_URL =
      Server.BASE_URL + "/api/paymentmethod/post";

  static const String STRIPE_PAYMENT_INTENT_POST_URL =
      Server.BASE_URL + "/api/taskpayment/createorupdatepaymentintent/#taskId#";
}
