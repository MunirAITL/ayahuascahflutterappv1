class AppConfig {
  //static const
  static const int page_limit_dashboard = 3;
  static const int page_limit = 50;
  static const int totalUploadLimit = 5;

  static const double picSize = 400;
  static const double chatScrollHeight = .2;
  static const int textExpandableSize = 70;
  static const AlertDismisSec = 5;

  //  appbar height per page fixed pixel
  static const double post_add_height = 60;
  static const double mytasks_height = 100;
  static const double findretreats_height = 60;
  static const double findworks_map_height = 50;
  static const double private_msg_height = 80;
  static const double myprofile_icon_height = 10;
}
