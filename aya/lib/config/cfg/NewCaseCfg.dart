import 'package:aitl/config/server/Server.dart';

class NewCaseCfg {
  static const int ALL = 901;
  static const int IN_PROGRESS = 902;
  static const int SUBMITTED = 903;
  static const int FMA_SUBMITTED = 904;
  static const int COMPLETED = 905;
}
